<div class="table-responsive">
<table id="example1" class="table table- table-bordered table-striped">
    <thead>
    <tr>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','id','<th scope="col" data-column="id">'. __('id') ).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','type','<th scope="col"  data-column="type">'. __('type') ).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','user_id','<th scope="col"  data-column="user.username">'. __('brand')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','reference','<th scope="col" data-column="reference">'. __('reference')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_name','<th scope="col" data-column="receiver_name">'. __('receiver_name')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_phone','<th scope="col" data-column="receiver_phone">'. __('receiver_phone')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_address','<th scope="col" data-column="receiver_address">'. __('receiver_address')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','city','<th scope="col" data-column="city">'. __('city') ).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','cod','<th scope="col" data-column="cod">'. __('cod')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','fees','<th scope="col" data-column="fees">'. __('fees')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','notes','<th scope="col" data-column="notes">'. __('Notes')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','package_description','<th scope="col" data-column="package_description">'. __('package description')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','statues','<th scope="col" data-className="status-element" data-column="statues">'. __('statues')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','driver_id','<th scope="col" data-column="drivers.0.name" data-searchable="true">'. __('Driver')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','patch_number','<th scope="col" data-column="patch_number" data-searchable="true">'. __('patch number')).'</th>' ?>
        <?php echo render_meta_columns('Orders'); ?>
        <?php $this->Hooks->do_action('orders_list_view_table_after_meta'); ?>

        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','created','<th scope="col" data-column="created" class="created">'. __('created')).'</th>' ?>
        <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','modified','<th scope="col" data-column="modified" class="modified">'. __('modified')).'</th>' ?>
        <th scope="col" class="actions" data-column="actions" data-searchable="false"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orders as $order) : ?>
        <tr>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','id','<td>'. DID($order->id) ).'</td>'?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','type','<td>'. HSTT($order->type) ).'</td>'?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','user_id','<td>'. h($order->user->username) ).'</td>'?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','reference','<td>'. h($order->reference)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_name','<td>'. h($order->receiver_name)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_phone','<td>'. h($order->receiver_phone)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','receiver_address','<td>'. h($order->receiver_address)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','city','<td>'. h($order->city)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','cod','<td>'. $this->Number->format($order->cod)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','fees','<td>'. $this->Number->format($order->fees)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','notes','<td>'. h($order->notes)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','package_description','<td>'. h($order->package_description)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','statues','<td class="status-element">'. HSTT($order->statues)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','created','<td>'.h($order->created)).'</td>' ?>
            <?= checkbox_show_if_option_equels('columns_view_in_the_dashboard','modified','<td>'.h($order->modified)).'</td>' ?>




            <td class="actions">
                <div class="dropdown" style="display: inline-block">
                    <button id="dLabel" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= __('Update');?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dLabel">
                        <?php
                        foreach ($status as $update){
                            echo  $this->Html->link(__($update), ['action' => 'update', $order->id,$update], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary ajaxinLink']);
                        }
                        ?>
                    </div>

                </div>
                <div class="dropdown" style="display: inline-block" >
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?= $this->Html->link(__('Print'), ['action' => 'policy', $order->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary' ,'target' => '_blank']) ?>
                        <?= $this->Html->link(__('View'), ['action' => 'view', $order->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary','target' => '_blank']) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $order->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn btn-secondary']) ?>

                        <?php
                        $delete_link =$this->Form->postLink(__('Delete'), ['action' => 'delete', $order->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $order->id),
                                'title' => __('Delete'), 'class' => 'dropdown-item btn btn-danger']);

                        echo show_if_option_equels('allow_delete_for_orders',1,$delete_link);
                        ?>
                        <?php $this->Hooks->do_action('orders_action_buttons_list',$order->id) ?>



                    </div>
                </div>

            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
</div>
