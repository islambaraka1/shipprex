
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $this->fetch('title') ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=ROOT_URL?>dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>dist/css/custom.css?version=<?= date('Ymd');?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?=ROOT_URL?>plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.5.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">


    <?php
    echo $this->Html->script('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js', ['block' => 'scriptBottom']);
    $this->Html->scriptStart(['block' => 'scriptcode']); ?>
        $(document).ready(function() {
            // fix label input styling
            $('.options_label label').removeAttr('class');
            $('.onOffToggel').change(function () {
                $hidden = $(this).parent('div').parent().find('.above_the_on_off');
                $value = $($hidden).attr('value') == 1 ? 0 : 1;
                $($hidden).attr('value', $value);
            })
            $('.color_selector').click(function () {
                //remove selection of others
                $($(this)).parents('div.setting_row').find('.color_selector').removeClass('selectedColor');
                // add selection tag to this
                $($(this)).addClass('selectedColor');
                //store in hidden input
                $($(this)).parents('div.setting_row').find('input').val($(this).data('value'));
            })

        });
        <?php
        $this->Html->scriptEnd();
        ?>


</head>

