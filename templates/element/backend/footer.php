<!-- /.content-wrapper -->
<footer class="main-footer">
   <?=PROJECT_COPY?>
    <div class="float-right d-none d-sm-inline-block">
        <b><?= __("Version"); ?></b> <?=PROJECT_VERSION?>
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Note"); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- Preloader HTML -->
<!--<div id="preloader">-->
<!--    <div class="preloader-spinner"></div>-->
<!--</div>-->


<!-- Preloader CSS -->
<style>
    #preloader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ffffff;
        z-index: 9999;
        opacity: 0.8;
    }
    .preloader-spinner {
        width: 50px;
        height: 50px;
        border: 8px solid #000000;
        border-top-color: transparent;
        border-radius: 50%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        animation: preloader-spin 1s linear infinite;
    }
    @keyframes preloader-spin {
        0% {
            transform: translate(-50%, -50%) rotate(0deg);
        }
        100% {
            transform: translate(-50%, -50%) rotate(360deg);
        }
    }
</style>

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=ROOT_URL?>dist/js/jquery.nestable.js"></script>

<script src="https://itsjavi.com/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script><!-- jQuery UI 1.11.4 -->
<script src="<?=ROOT_URL?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
    var BASE_URL = "<?=ROOT_URL?>";
</script>

<!-- Preloader JavaScript -->
<script>
    $(document).ready(function() {
        // Show preloader when an Ajax request is started
        $(document).ajaxStart(function() {
            $('#preloader').show();
        });
        // Hide preloader when an Ajax request is completed (success or fail)
        $(document).ajaxComplete(function() {
            $('#preloader').hide();
        });
        $(window).on('load', function() {
            $('#preloader').hide();
        });
    });


</script>

<!-- Bootstrap 4 -->
<script src="<?=ROOT_URL?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=ROOT_URL?>plugins/select2/js/select2.full.js"></script>
a
<!-- ChartJS -->
<script src="<?=ROOT_URL?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=ROOT_URL?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?=ROOT_URL?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=ROOT_URL?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=ROOT_URL?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=ROOT_URL?>plugins/moment/moment.min.js"></script>
<script src="<?=ROOT_URL?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=ROOT_URL?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!--Bootstrab switch-->
<script src="<?=ROOT_URL?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<!-- Summernote -->
<script src="<?=ROOT_URL?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=ROOT_URL?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=ROOT_URL?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=ROOT_URL?>dist/js/pages/dashboard.js"></script>
<script src="<?=ROOT_URL?>/uploader-filetype/js/plugins/buffer.min.js" type="text/javascript"></script>
<script src="<?=ROOT_URL?>/uploader-filetype/js/plugins/filetype.min.js" type="text/javascript"></script>

<!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="<?=ROOT_URL?>/uploader-filetype/js/plugins/piexif.min.js" type="text/javascript"></script>

<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
    This must be loaded before fileinput.min.js -->
<script src="<?=ROOT_URL?>/uploader-filetype/js/plugins/sortable.min.js" type="text/javascript"></script>

<!-- bootstrap.bundle.min.js below is needed if you wish to zoom and preview file content in a detail modal
    dialog. bootstrap 5.x or 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

<!-- the main fileinput plugin script JS file -->
<script src="<?=ROOT_URL?>/uploader-filetype/js/fileinput.min.js"></script>

<!-- following theme script is needed to use the Font Awesome 5.x theme (`fa5`). Uncomment if needed. -->
 <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.5.0/themes/fa5/theme.min.js"></script

<!-- optionally if you need translation for your language then include the locale file as mentioned below (replace LANG.js with your language locale) -->
<script src="<?=ROOT_URL?>/uploader-filetype/js/locales/LANG.js"></script>


<script type="module">
    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.7/firebase-app.js";
    import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.7/firebase-analytics.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyCspnZ7SbP3bcBTtnvea7nEjZdi2aM6Vp4",
        authDomain: "shipprex-415ed.firebaseapp.com",
        projectId: "shipprex-415ed",
        storageBucket: "shipprex-415ed.appspot.com",
        messagingSenderId: "614019351842",
        appId: "1:614019351842:web:7d0809b0133bf40966517b",
        measurementId: "G-L5EP4Z21BF"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const messaging=firebase.messaging();
    //Custom function made to run firebase service
    getStartToken();
    //This code recieve message from server /your app and print message to console if same tab is opened as of project in browser
    messaging.onMessage(function(payload){
        console.log("on Message",payload);
    });

    function getStartToken(){
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                console.log(currentToken);
                sendTokenToServer(currentToken);
            }
            else {
// Show permission request.
                console.log("check the per");
                RequestPermission();
                setTokenSentToServer(false);
            }
        }).catch((err) => {
            setTokenSentToServer(false);
        });
    }
    function RequestPermission(){
        messaging.requestPermission()
            .then(function(permission){
                if (permission === 'granted') {
                    console.log("have Permission");
//calls method again and to sent token to server
                    getStartToken();
                }
                else{
                    console.log("Permission Denied");
                }})
            .catch(function(err){
                    console.log(err);
                })
            }
        function sendTokenToServer(token){
            if (!isTokensendTokenToServer()) {
                $.ajax({
                    url: URL,
                    type: 'POST',
                    data: {
//whatever you wanna send
                        push_token:token,
                    },
                    success: function (response) {
                        setTokenSentToServer(true);
                    },
                    error: function (err) {
                        setTokenSentToServer(false);
                    },
                });
            }}
        function isTokensendTokenToServer() {
            return window.localStorage.getItem('sendTokenToServer') === '1';
        }
        function setTokenSentToServer(sent) {
            window.localStorage.setItem('sendTokenToServer', sent ? '1' : '0');
        }

</script>

<!-- AdminLTE for demo purposes -->
<script src="<?=ROOT_URL?>dist/js/demo.js?version=<?= date('Ymd');?>"></script>
<link rel="manifest" href="manifest.json">

<script src="https://www.gstatic.com/firebasejs/8.2.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.4/firebase-messaging.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBsYDGIjVHAV7euIa2hj610xYto7bfD7Nc",

        authDomain: "shipprex-all.firebaseapp.com",

        projectId: "shipprex-all",

        storageBucket: "shipprex-all.appspot.com",

        messagingSenderId: "716915756262",

        appId: "1:716915756262:web:6abb7b5539247a79b37f27",

        measurementId: "G-D2X4HJLGV0"

    };
    firebase.initializeApp(config);

    // Retrieve Firebase Messaging object.
    const messaging = firebase.messaging();

    messaging.requestPermission().then(function() {
        //console.log('Notification permission granted.');

        if(isTokenSentToServer()){

            console.log("Token Already sent");
        }else{
            getRegisterToken();
        }

        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // ...
    }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
    });



    function getRegisterToken(){
        // Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
        messaging.getToken().then(function(currentToken) {
            if (currentToken) {
                saveToken(currentToken);
                console.log(currentToken);
                sendTokenToServer(currentToken);
                // updateUIForPushEnabled(currentToken);
            } else {
                // Show permission request.
                console.log('No Instance ID token available. Request permission to generate one.');
                // Show permission UI.
                // updateUIForPushPermissionRequired();
                setTokenSentToServer(false);
            }
        }).catch(function(err) {
            console.log('An error occurred while retrieving token. ', err);
            //showToken('Error retrieving Instance ID token. ', err);
            setTokenSentToServer(false);
        });
    }

    function setTokenSentToServer(sent) {
        window.localStorage.setItem('sentToServer', sent ? '1' : '0');
    }

    function sendTokenToServer(currentToken) {
        // if (!isTokenSentToServer()) {
        //     console.log('Sending token to server...');
        //     // TODO(developer): Send the current token to your server.
        //     setTokenSentToServer(true);
        // } else {
        //     console.log('Token already sent to server so won\'t send it again ' +
        //         'unless it changes');
        // }
    }
    function isTokenSentToServer() {
        return window.localStorage.getItem('sentToServer') === '1';
    }

    function saveToken(currentToken){
        $driver = "";
        if (window.location.href.indexOf("drivers/online") > -1) {
             url = window.location.href;

            lastSegment = url.split("/").pop();

            console.log(lastSegment); // "playlist"
            $driver = lastSegment;
        }
        jQuery.ajax({
            data: {"token":currentToken},
            type: "post",
            url: "<?=ROOT_URL?>notifictions/fcms/add/"+$driver,
            success: function(result){
                console.log(result);
            }

        });
    }

    messaging.onMessage(function(payload) {
        console.log('Message received. ', payload);
        //refresh the notification area
        //playAudio("<?//=ROOT_URL?>//pristine-609.mp3");
        // alert(555555)
        updateNotifictionList();
        var  title =payload.notification.title;

        var options ={
            body: payload.notification.body,
            icon: payload.data.icon,
            image: payload.data.image,
            data:{
                time: new Date(Date.now()).toString(),
                click_action: payload.data.click_action
            }
        };
        var myNotification = new Notification(title, options);
    });

    function playAudio(url) {
        new Audio(url).play();
    }
    function updateNotifictionList(){
        jQuery.ajax({

            url: "<?=ROOT_URL?>notifictions/notifications/get_recent",
            success: function(result){
                console.log(result);
                $('#noteBadge').text(result.length);
                $last = result.length -1;
                $last = result[$last];
                $('#notificationsList').append('<a href="#" class="modal-opener" data-url="<?=ROOT_URL?>notifictions/notifications/view/'+ $last.id +'" class="dropdown-item">' +
                    '        <i class="fas fa-envelope mr-2"></i> '+ $last.title +
                    '        <span class="float-right text-muted text-sm">'+ $last.created +'</span>' +
                    '    </a>' +
                    '    <div class="dropdown-divider"></div>');
            }

        });
    }

</script>
<?php
echo $this->fetch('scriptBottom');
echo $this->fetch('scriptcode');
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="<?=ROOT_URL?>js/event_handler.js"></script>
<script src="<?=ROOT_URL?>js/ui_events.js"></script>
<div id="flash-message-container"></div>

</body>
</html>
