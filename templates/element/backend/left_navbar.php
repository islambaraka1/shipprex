<!-- Main Sidebar Container -->
<aside class="main-sidebar  bg-<?=get_option_value('theme_basic_color_left_bar')?>   elevation-4">
    <!-- Brand Logo -->
    <a href="<?=ROOT_URL?>" class="brand-link bg-<?=get_option_value('theme_basic_color_brand_bg')?>">
        <img src="<?=ROOT_URL?>dist/img/AdminLTELogo.png" alt="Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">

        <span class="brand-text font-weight-light"><?=SETTING_KEY['global']['site_settings_']['site_name'];?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php $this->Hooks->do_action('before_main_menu'); ?>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <?php
                    echo $this->Treelist->generateUls($Array_for_menu);

                if($_SESSION['Auth']['User']['group_id'] != 3){
                    $this->Hooks->do_action('after_main_menu');

                ?>


                <?php
                } else {
                    $this->Hooks->do_action('after_main_menu_seller');
                }

                ?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->

<script>

    <?php
    $this->Html->scriptStart(['block' => 'scriptcode']);
    ?>
    // $(document).ready(function(){
    //     $('#branches_select').on('change',function(e){
    //         console.log("jo")
    //
    // });
    // });
    function getSelectedBranch(sel)
    {
        url = "<?=ROOT_URL?>/multi-branches/branch/choose"
        $.ajax({
            url: url,
            method: "POST",
            data:{
                branch_id:sel.value,
            }
        }).done(function($data) {
            //refresh
            location.reload();
        });
        // alert(sel.value);
    }
    <?php
    $this->Html->scriptEnd();
    ?>

</script>
