<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

<?php
echo $this->Html->script(ROOT_URL.'plugins/datatables/jquery.dataTables.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-bs4/js/dataTables.bootstrap4.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/dataTables.responsive.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/responsive.bootstrap4.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables/celledit.js', ['block' => 'scriptBottom']);
?>

<table id='empTable' class="table table-bordered table-striped">
<thead>
<tr>
    <th>Id</th>
    <th>Locale</th>
    <th>singular</th>
    <th>value_0</th>
</tr>
</thead>

</table>


<script>
    <?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>

    $(document).ready(function(){
        let $table = $('#empTable').DataTable({
            'processing': true,
            'serverSide': true,
            'serverMethod': 'post',
            'ajax': {
                'url': '<?=ROOT_URL?>translation/translations/get_data',
            },
            'columns': [
                {data: 'id'},
                {data: 'locale'},
                {data: 'singular'},
                {data: 'value_0'},
            ]
        });


        function  myCallbackFunction(updatedCell, updatedRow, oldValue) {

            console.log("The new value for the cell is: " + updatedCell.data());
            console.log("The values for each cell in that row are: " + updatedRow.data());
            console.log(updatedRow.data());
            //jquery ajax request for update translation
            $.ajax({
                url: '<?=ROOT_URL?>translation/translations/update',
                type: 'POST',
                data: {
                    id: updatedRow.data().id,
                    value_0: updatedCell.data()
                },
                success: function(data) {

                    //update the row cell with the new value
                    updatedCell.data(data.value_0); //update the cell with the new value

                    console.log(data);
                }
            });
        }

        $table.MakeCellsEditable({
            "onUpdate": myCallbackFunction,
            "inputCss":'my-input-class',
            "columns": [3],
            "allowNulls": {
                "columns": [1],
                "errorClass": 'error'
            },
            "confirmationButton": {
                "confirmCss": 'btn btn-success',
                "cancelCss": 'btn btn-danger'
            },

        });

    });

    <?php $this->Html->scriptEnd(['block' => 'scriptcode']); ?>
</script>
