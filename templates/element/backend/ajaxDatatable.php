<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

<?php
echo $this->Html->script(ROOT_URL.'plugins/datatables/jquery.dataTables.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-bs4/js/dataTables.bootstrap4.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/dataTables.responsive.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/responsive.bootstrap4.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js', ['block' => 'scriptBottom']);


echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/pdfmake.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/vfs_fonts.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js', ['block' => 'scriptBottom']);

?>

<table border="0" cellspacing="5" cellpadding="5" style="display: none">
    <tbody>
    <tr>
        <td><?= __("Minimum Date:"); ?></td>
        <td><input name="min" id="min" type="text"></td>
    </tr>
    <tr>
        <td><?= __("Maximum Date:"); ?></td>
        <td><input name="max" id="max" type="text"></td>
    </tr>
    </tbody>
</table>

<style>

    @keyframes flickerAnimation {
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-o-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-moz-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-webkit-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    .animate-flicker {
        -webkit-animation: flickerAnimation .9s forwards;
        -moz-animation: flickerAnimation .9s forwards;
        -o-animation: flickerAnimation .9s forwards;
        animation: flickerAnimation .9s forwards;
    }


    .dt-buttons{

    }
    .dt-buttons .btn-group{}
    .dt-buttons .example1_filter{
        width: 200px;
    }
    .dt-buttons .example1_length{

    }
    .dt-button a{
        display: block;
        width: 100%;
        padding: 0.25rem 1rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }
    .buttons-columnVisibility  a:hover {
        background: #F5F5F5;
    }
    .buttons-columnVisibility.active  a{
        color: #007bff;
        text-decoration: none;

    }
    .buttons-columnVisibility  a{
        color: #007bff;
        text-decoration: line-through;
        padding: 5px;
        display: block;
        text-transform: capitalize;
        border-bottom: 1px solid #ddd;
    }


    .dt-buttons {
        margin-bottom: 10px;
    }
    .dt-buttons.btn-group{
        float: none;
        margin-right: 2%;
        clear: both;
        width: 100%;
    }
    .dataTables_filter {
        float: left;
        margin-top: 4px;
        margin-right: 2%;
        text-align: left;
    }
    .dataTables_info {
        float: right;
    }
    .dataTables_length{
        float: right;
        margin-top: 4px;
        margin-left: 2%;
    }
    .emp{
        background: #fff;
        position: fixed;
        z-index: 1;
        padding: 50px;
        right: -30%;
        bottom: 0;
        transition: all 0.7s ease-in-out;
    }
    .show-emp{
        right: 0px;
        bottom: 0;
    }


    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#0C9;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float{
        margin-top:22px;
    }

    .currentChange{
        transition: all 0.7s ease-in-out;
        background: #da2839!important;

    }
    @media only screen and (max-width: 768px) {
        /* For mobile phones: */

        .emp , .float{
            display:none!important;
        }
        .dt-bootstrap4{
            width: 100%;
            overflow: scroll;
        }

    }
</style>
<a href="#" class="float">
    <i class="fa fa-filter my-float"></i>
</a>


<script>
    function selectedCallback($rows,action){
        $total = $rows.length;
        console.log($rows);
        var $ids = '';
        for ($x = 0; $x < $total; $x++){
            $currentRow = $rows[$x];
            console.log($currentRow);
            $id = $currentRow.id;
            $ids += $id +',';

        }
        console.log($ids);
        window.open("<?=ROOT_URL?>"+action+"?ids="+$ids , "_blank");

        // console.log();
    }
</script>
<script>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
function extractContent(s) {
var span = document.createElement('span');
span.innerHTML = s;
return span.textContent || span.innerText;
};



    $(document).ready(function() {


        var $targetAction = '';
        // var $modifiedIs
        $('#example1 tbody').on('shiprex.print', function () {
            var rows = table.rows( { selected: true } ).data();
            selectedCallback(rows,$targetAction);
            console.log(rows);
        });
        $("#reservationtime").on('show.daterangepicker', function () {
            $('.drp-buttons').find('.cancelBtn ').remove();
            $('.drp-buttons').find('.ModifiedBtn').remove();
            $("<button>", {
                text: "Search Modified",
                click: function () {
                    var start = $('#reservationtime').data('daterangepicker').startDate.format('YYYY-MM-DD');
                    var end = $('#reservationtime').data('daterangepicker').endDate.format('YYYY-MM-DD');
                    $('#min').val(start );
                    $('#max').val(end);
                    $searchObject = {start:extractContent($('#min').val()) , end:extractContent($('#max').val())};
                    $searchIbjectJson = JSON.stringify($searchObject);
                    // table.column(created_index).search($searchIbjectJson).draw();
                    table.column(created_index).search("");
                    table.column(modified_index).search($searchIbjectJson).draw();
                }
            }).appendTo('.drp-buttons').addClass("ModifiedBtn btn btn-sm btn-primary");
            $('.applyBtn').click(function () {
                var start = $('#reservationtime').data('daterangepicker').startDate.format('YYYY-MM-DD');
                var end = $('#reservationtime').data('daterangepicker').endDate.format('YYYY-MM-DD');
                $('#min').val(start );
                $('#max').val(end);
                $searchObject = {start:extractContent($('#min').val()) , end:extractContent($('#max').val())};
                $searchIbjectJson = JSON.stringify($searchObject);
                // table.column(created_index).search($searchIbjectJson).draw();
                table.column(modified_index).search("");
                table.column(created_index).search($searchIbjectJson).draw();
            });
            // $('.drp-buttons').append()
        });


        //check created index
        var created_index = $('th.created').index();
        var modified_index = $('th.modified').index();
        $('.float').click(function(){
            console.log('clicked');
            $('.emp').addClass('show-emp');
            $('.closeit').click(function(){
                $('.emp').removeClass('show-emp');
            });


        })


        $('#reservationtime').daterangepicker({
        showButtonPanel: true,
        beforeShow: function (input) {
            alert("test");
            // dpClearButton(input);
            SearchModified(input);
        },
        timePicker: false,
        timePickerIncrement: 30,

        locale: {
        format: 'YYYY/MM/DD hh:mm A'
        }
        }, function(start, end, label) {
            // alert('search');
            if (created_index == -1 ) {
                alert('created column not found please add this column to search by date range');
                return false;
            }
        });
        document.title='<?=PROJECT_NAME?>';
        // DataTable initialisation
        // create columns array from the table html for the datatable
        var columns = [];
        $('#example1 thead tr th').each(function(){
            var column = {};
            column.data = $(this).attr('data-column');
            column.name = $(this).text();
            column.searchable = $(this).attr('data-searchable');
            column.orderable = $(this).attr('data-orderable');
            column.visible = $(this).attr('data-visible');
            column.className = $(this).attr('data-className');
            column.render = $(this).attr('data-render');
            columns.push(column);
        });
        var table  = $('#example1').DataTable(
            {
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'searchDelay': 500,
                'ajax': {
                    'url': '<?=ROOT_URL.$url?>',
                },
                'columns': columns,
                "dom": '<"testme"<"emp">fl>Btip',
                "paging": true,
                "autoWidth": true,
                "fixedHeader": false,
                "select": true,
<?php if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'ar'){ ?>
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/ar.json'
                },
<?php } ?>
                "order": [[ 0, "desc" ]],
                "buttons": [
                    'selectAll',
                    'selectNone',
                    ,
                    {
                        extend: 'collection',
                        text: '<?=__('Bulk Action')?>',
                        buttons: [
                            {  text: '<?=__('Print all selected')?>',
                                action: function (e) {
                                    // console.log(this);
                                    $targetAction = "orders/newprintall";
                                    $('#example1 tbody').trigger('shiprex.print');
                                    // var rows = table.rows( { selected: true } ).data();
                                    // selectedCallback(rows,'orders/newprintall');
                                }
                            },
                            <?php if($_SESSION['Auth']['User']['group_id']  == 1 || $_SESSION['Auth']['User']['group_id']  == 4){ ?>
                            {  text: '<?=__('Assign selected to driver')?>',
                                action: function () {
                                    var rows = table.rows( { selected: true } ).data();
                                    selectedCallback(rows,'drivers/assign');
                                }
                            },
                            <?php $this->Hooks->do_action('orders_bulk_action_dropdown'); ?>
                            {  text: '<?=__('Bulk change status')?>',
                                action: function () {
                                    var rows = table.rows( { selected: true } ).data();
                                    selectedCallback(rows,'orders/bulk');
                                }
                            },
                            {  text: '<?=__('Bulk Delete')?>',
                                action: function () {
                                    var rows = table.rows( { selected: true } ).data();
                                    selectedCallback(rows,'orders/bulk-delete');
                                }
                            }
                            <?php } ?>
                        ],
                        pop: true
                    },
                    'csvHtml5',
                    'excelHtml5',
                    'pdfHtml5',
                    'print',
                ],
                initComplete: function () {
                    console.log('inited');
                    $ind =0;
                    $lastRecord = this.api().columns()[0].length - 1 ;
                    this.api().columns().every( function () {
                        if($ind < $lastRecord-1){
                            var column = this;
                            $label = $('#example1 thead tr:eq(0) th:eq('+$ind+')').text();
                            $field_name = $('#example1 thead tr:eq(0) th:eq('+$ind+')').attr('data-column');
                            $field_searchable = $('#example1 thead tr:eq(0) th:eq('+$ind+')').attr('data-searchable');
                            if ($field_searchable == 'false') {
                                $ind++;
                                return;
                            }
                            var select = $('<select style="width: 300px" name="'+$field_name+'" data-placeholder="'+$label+'" class="select2bs4 form-control "><option value="">'+$label+'</option></select>')
                                .appendTo( $('.emp ') )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );
                            column.data().unique().sort().each( function ( d, j ) {
                                $newD  =  extractContent(d)


                                select.append( '<option value="'+$newD+'">'+$newD+'</option>' )
                            } );
                            $ind++;
                        }
                    } );
                    setTimeout(function(){
                        $('.select2bs4').select2({
                            theme: 'bootstrap4',
                            allowClear: true,
                            placeholder: function () {
                               return  $(this).data('placeholder');
                            },
                            ajax: {
                                url: '<?=ROOT_URL?>orders/columns_value_search',
                                delay: 750,
                                data: function (params) {
                                    var query = {
                                            search: params.term?params.term:"",
                                            field: this[0].name,
                                            type: 'public'
                                        }
                                        console.log(this[0].name);
                                    return query;
                                },
                                dataType: 'json'
                                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                            }
                        })
                    },50);
                    $('<button class="btn btn-danger closeit"><?= __("Close Filter"); ?></button>').appendTo( $('.emp ') );

                },
                on_off:function(){

                }


            }
        );


        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                console.log("called")
                var min = $('#min').datepicker("getDate");
                var max = $('#max').datepicker("getDate");
                var startDate = new Date(data[created_index]);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
            }
        );
        $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
            if (settings.jqXHR.status == 500){
                alert("<?= __("Server Error"); ?>");
                // window.location.reload();
            }
        };


    });


<?php $this->Html->scriptEnd(); ?>
</script>
