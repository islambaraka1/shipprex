<div class="row">
<?php
//        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'yellow',
    'count' => count($totalOrders),
    'title' => __('Active Orders'),
    'icon' => 'map',
    'sub_msg' => __('More Info'),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'cyan',
    'count' => $TotalReminingBalance,
    'title' => __('Your Available  Balance '),
    'icon' => 'money-check-alt',
    'sub_msg' => __('More Info'),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'orange',
    'count' => $totalCollectedCods,
    'title' => __('Collected COD'),
        'icon' => 'wallet',
    'sub_msg' => __('More Info'),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'blue',
    'count' => $totalFeesCosts,
    'title' => __('Invoices'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => __('More Info'),
]);

function check($number){
    if($number == 0)
        return 1;
    else if($number == 1)
        return 0;
    else if($number<0)
        return check(-$number);
    else
        return check($number-2);
}
$transes = $transactions;
?>
</div>





<?php
$key = 1 ;
foreach ($days as $day =>$transactions) {
$day = date('y-m-d H:i:s',$day);

?>
<div class="bs-example">
    <div class="accordion" id="accordionExample">

            <div class="card">
                <div class="card-header" id="headingOne<?=$key?>">
                    <h2 class="mb-0 d-flex justify-content-between">
                        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne<?=$key?>">View <?=$day?></button>
<!--                        <a  href="--><?//=ROOT_URL?><!--/transactions/invoiceprint/?day=--><?//=urlencode($day)?><!--"  target ='_blank' class="btn btn-primary align-self-md-end" >Print Order --><?//=$day?><!--</a>-->
                    </h2>
                </div>
                <div id="collapseOne<?=$key?>" class="collapse" aria-labelledby="headingOne<?=$key?>" data-parent="#accordionExample">
                    <div class="card-body">


                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th scope="col"><?= __('Id') ?></th>
                                    <th scope="col"><?= __('Receiver Name') ?></th>
                                    <th scope="col"><?= __('Receiver Phone') ?></th>
                                    <th scope="col"><?= __('City') ?></th>
                                    <th scope="col"><?= __('Reference') ?></th>
                                    <th scope="col"><?= __('COD') ?></th>
                                    <th scope="col"><?= __('Fees') ?></th>
                                    <th scope="col"><?= __('Created') ?></th>

                                </tr>
                                <?php
                                $count = 1;
                                $totalCod = 0;
                                $totalFees = 0;
                                foreach ($transactions as $keyz => $trans):
                                    $orders = $trans->order;
                                if(isset($orders->id) && !check($count)){
                                    ?>
                                    <tr>
                                        <td><?= DID($orders->id) ?></td>

                                        <td><?= h($orders->receiver_name) ?></td>
                                        <td><?= h($orders->receiver_phone) ?></td>
                                        <td><?= h($orders->city) ?></td>
                                        <td><?= h($trans->reference) ?></td>
                                        <td><?= h($trans->amount) ?></td>
                                        <td><?= h($transactions[$keyz+1]->amount) ?></td>
                                        <td><?= h($orders->created) ?></td>

                                    </tr>
                                <?php
                                    $totalCod += $trans->amount;
                                    $totalFees += $transactions[$keyz+1]->amount;
                                }
                                $count++;
                                endforeach; ?>
                            </table>
                            <?php
                            echo "<h2> Total Cod  : $totalCod</h2>";
                            echo "<h2> Total Fees : $totalFees</h2>";
                            $total = $totalCod + $totalFees;
                            echo "<h2> Total Payout :  $total</h2>";

                            ?>
                        </div>



                    </div>
                </div>
            </div>

    </div>
</div>

    <?php
    $key++;
} ?>




<div class="form-group" style="float: left" >
    <label>Date and time range:</label>

    <div class="input-group" >
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control float-right" id="reservationtime">
    </div>
    <!-- /.input group -->
</div>
    <table id="example1" class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('details') ?></th>
            <th scope="col"><?= $this->Paginator->sort('receiver') ?></th>
            <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
            <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('pickup_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($transes as $transaction) : ?>
            <tr>
                <td><?= h($transaction->type) ?></td>
                <td><?= h($transaction->details) ?></td>
                <td><?= $transaction->has('order') ? $this->Html->link($transaction->order->receiver_name , ['controller' => 'Orders', 'action' => 'view', $transaction->order->id]) : '' ?></td>
                <td><?= $transaction->has('order') ? $this->Html->link($transaction->order->receiver_phone , ['controller' => 'Orders', 'action' => 'view', $transaction->order->id]) : '' ?></td>
                <td><?= $transaction->has('order') ? $this->Html->link($transaction->order->id, ['controller' => 'Orders', 'action' => 'view', $transaction->order->id]) : '' ?></td>
                <td><?= $transaction->has('pickup') ? $this->Html->link($transaction->pickup->id, ['controller' => 'Pickups', 'action' => 'view', $transaction->pickup->id]) : '' ?></td>
                <td><?= $this->Number->format($transaction->amount) ?></td>
                <td><?= h($transaction->created) ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?=$this->element('backend/datatablejs')?>
