<h1> Clear balance for user : #<?=$user->id;?> - <?=$user->full_name;?></h1>
<div class="row">
<?php
//        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));

echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'cyan',
    'count' => $TotalReminingBalance,
    'title' => 'What you Will Pay in Cash  ',
    'icon' => 'money-check-alt',
    'sub_msg' => 'More Info',
]);

echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'blue',
    'count' => abs((int)$totalFeesCosts),
    'title' => 'What you will Cutoff',
    'icon' => 'file-invoice-dollar',
    'sub_msg' => 'More Info',
]);

echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'orange',
    'count' => $totalCollectedCods,
    'title' => 'Total for Collected COD',
    'icon' => 'wallet',
    'sub_msg' => 'More Info',
]);
?>

</div>
<div class="">
    <h3><?=__('Everything is good')?></h3>
    <br>
    <a href="<?=ROOT_URL?>transactions/dbalance/<?=$user->id?>" class="btn btn-primary btn-lg text-light">Proceed <i class="fa fa-check-circle"></i></a>

</div>
