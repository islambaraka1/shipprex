<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'count' => $orders_active,
        'title' => __('Active Orders'),
        'icon' => 'map',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'count' => $orders_inactive,
        'title' => __('InActive Orders'),
        'icon' => 'map',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'count' => $toBePaid,
        'title' => __('Payouts  '),
        'icon' => 'money-check-alt',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'orange',
        'count' => $cod,
        'title' => __('Collected COD'),
        'icon' => 'wallet',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'green',
        'count' => $cutoffPaid,
        'title' => __('Profit'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'lightblue',
        'count' => $toBePaidWeek,
        'title' => __('Last week payouts'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'orange',
        'count' => $codPaidWeek,
        'title' => __('Last week COD'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'green',
        'count' => $cutoffPaidWeek,
        'title' => __('Last week profits'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);


    ?>
</div>
<div class="row">
    <section class="col-lg-7 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart1',[
            'canv_id' => 'chartscanv',
            'count' => '120',
            'title' => __('last 7 days of profit'),
            'icon' => 'file-invoice-dollar',
            'sub_msg' => __('More Info'),
            'chart_data' => $cahrtData,
            'chart_data' => $cahrtData,
            'scale' => SUSTEM_CURRENCY
        ]);

        ?>
    </section>
</div>
