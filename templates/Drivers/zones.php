<div class="row">
    <?= $this->Form->create() ?>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <td>Zone Name</td>
                <td>Zone Value</td>
            </tr>
            <tbody>
            <?php foreach ($zones as $zone) : ?>
                <tr>
                    <td><?=$zone->name?></td>
                    <td><?=$this->Form->control('zone.'.$zone->id.'.price' , ['label'=>false , 'value'=>$zone->default_price ]); ?> </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php

