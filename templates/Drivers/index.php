<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Driver[]|\Cake\Collection\CollectionInterface $drivers
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Driver'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>


<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'count' => $reports_data_last_days_diliveries,
        'title' => __('Last Days Deliveries '),
        'icon' => 'truck',
        'sub_msg' => __('15 Days'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'count' => $reports_data_count_of_diliveries,
        'title' => __('Total Deliveries'),
        'icon' => 'truck',
        'sub_msg' =>__( 'Orders assigned to drivers  ') ,
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'green',
        'count' => $reports_data_all_on_routes,
        'title' => __('On Routes '),
        'icon' => 'truck',
        'sub_msg' => __('Total in routes orders  '),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'count' => $reports_data_active_drivers,
        'title' => __('Active Drivers '),
        'icon' => 'truck',
        'sub_msg' => __('Active drivers in the last 30 days  : '),
    ]);

    ?>
</div>

<?= $this->Html->link(__('New Driver'), ['action' => 'add'], ['class' => 'btn btn-primary']) ?>

<table class="table table-striped" id="example1">
    <thead>
    <tr>
        <th scope="col"><?= __('id') ?></th>
        <th scope="col"><?= __('name') ?></th>
        <th scope="col"><?= __('otp') ?></th>
        <th scope="col"><?= __('license') ?></th>
        <th scope="col"><?= __('vehicle_num') ?></th>
        <th scope="col"><?= __('phone') ?></th>
        <?php
            $this->Hooks->do_action('drivers_index_head_col');
        ?>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($drivers as $driver) : ?>
        <tr>
            <td><?= $this->Number->format($driver->id) ?></td>
            <td><?= h($driver->name) ?></td>
            <td><?= h($driver->otp) ?></td>
            <td><?= h($driver->license) ?></td>
            <td><?= h($driver->vehicle_num) ?></td>
            <td><?= h($driver->phone) ?></td>
            <?php
            $this->Hooks->do_action('drivers_index_body_col',$driver);
            ?>
            <td class="actions">
                <?php
                $driver_online_sec = $driver->id;
                    if (get_option_value('driver_online_sec')=="hash"){
                        $driver_online_sec = encrypt_decrypt('encrypt',$driver->id);
                    }
                ?>
                <?= $this->Html->link(__('Online'), ['action' => 'online', $driver_online_sec], ['title' => __('View'), 'class' => 'btn btn-success']) ?>
                <?= $this->Html->link(__('View'), ['action' => 'view', $driver->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?php
                    if(get_option_value('driver_multi_zones') == 1 ){
                        echo $this->Html->link(__('Zones'), ['action' => 'zones', $driver->id], ['title' => __('Zones'), 'class' => 'btn btn-secondary']);
                    }
                ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $driver->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $driver->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driver->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?=$this->element('backend/datatablejs')?>
