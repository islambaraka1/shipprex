<div class="container">
    <div class="card m-auto w-50">
        <div class="card-body">
            <h4 class="card-title"><?=__('Login to your account')?> </h4>
            <p class="card-text"><?=__('Please enter your password to view the account page')?> </p>
            <?php
            //otp login form
            echo $this->Form->create([], ['url' => ['action' => 'otp', $this->getRequest()->getParam('pass')[0]]]);
            echo $this->Form->control('otp', ['type' => 'text', 'label' => 'Enter OTP', 'class' => 'form-control', 'placeholder' => 'Enter OTP']);
            echo $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']);
            echo $this->Form->end();
            ?>
        </div>
    </div>


</div>

