<table width="100%">
    <tr>
        <td><h2><?= __("Driver"); ?> : <b><?=$driver->name?></b></h2>
        </td>
        <td align="right"><h2><?= __("Date"); ?> : <b><?=$_GET['day']?></b></h2>
        </td>
    </tr>
</table>

<table width="100%">
    <thead>
    <?php echo show_if_option_equels('order_serial_in_print',1,"<td>#</td>"); ?>
    <td>#</td>
    <td>Ref</td>
    <td><?= __("Receiver Name"); ?></td>
    <td><?= __("Address"); ?></td>
    <td><?= __("Phone"); ?></td>
    <td><?= __("Brand"); ?></td>
    <td><?= __("Product"); ?></td>
    <td><?= __("COD"); ?></td>
    </thead>

    <tbody>
    <?php
    $count=1;
    foreach ($ordersList as $order){
        ?>
        <tr>
            <?php echo show_if_option_equels('order_serial_in_print',1,"<td>$count</td>"); ?>
            <td><?=DID($order->id)?></td>
            <td><?=$order->reference?></td>
            <td><?=$order->receiver_name?></td>
            <td><?=$order->receiver_address?></td>
            <td><?=$order->receiver_phone?></td>
            <td><?=$order->user->username?></td>
            <td><?=$order->package_description?></td>
            <td><?=$order->cod?></td>
        </tr>
        <?php
        $count++;
    }
    ?>
    </tbody>
</table>
