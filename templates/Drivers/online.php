<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Driver $driver
 */
?>
<div class="drivers  view large-9 medium-8 columns content">
    <h3><?= h($driver->name) ?></h3>


    <div class="row">

<?php
//        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));
//Total Orders
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'danger',
    'count' => $driver_state['total_active_orders_count'],
    'title' => __('Active Orders'),
    'icon' => 'map',
    'sub_msg' => __('On route orders'),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'blue',
    'count' => $driver_state['total_delivered_orders_sum']>0?$driver_state['total_delivered_orders_sum_old']:0 ,
    'title' => __('Total Delivered Orders'),
    'icon' => 'car-side',
    'sub_msg' => __('Total orders amount '),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'cyan',
    'count' => $driver_state['total_delivered_orders_sum']>0?(int)$driver_state['total_delivered_orders_sum'] :0,
    'title' => __('Money to be collected '),
    'icon' => 'money-check-alt',
    'sub_msg' => __('COD of the active '),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'yellow',
    'count' => (int)$driver_state['total_delivered_orders_count'] ,
    'title' => __('Total Commissions '),
    'icon' => 'money-check-alt',
    'sub_msg' => __('SUM of the total commissions'),
]);

?>


    </div>


    <div class="row">
        <section class="col-lg-6 connectedSortable">
            <?php
            echo $this->element('Utils.backend/widgets/chart_dimminsions',[
                'canv_id' => 'chartscanv',
                'height' => '400',
                'title' => 'Orders Since account creation  ',
                'icon' => 'file-invoice-dollar',
                'chart_type' => 'bar',
                'cahrtData' => $driver_timeline,
                'cahrtData' => $driver_timeline,
                'scale' =>SUSTEM_CURRENCY
            ]);

            ?>
        </section>
        <?php $this->Hooks->do_action('driver_online_widget',$driver->id) ?>
        <?php

        // declare the drivers option for online view
        $allow_comment = get_option_value('drivers_show_comments_in_online');
        $allowed_updates = (array) json_decode(get_option_value('drivers_update_status_allowed'));
        ?>
    </div>

    <div class="row">

    </div>

    <div class="related">

<!--        accordion element -->
<div class="row search-main">
    <div class="col-12">
        <div class="search-container">
                <input type="text" id="search_input" placeholder="Search.." name="search">
                <button type="submit" id="search_trigger" ><i class="fa fa-search"></i></button>
        </div>
    </div>
</div>
        <div class="bs-example">
            <div class="accordion" id="accordionExample">
                <?php
                $key = 1 ;
                foreach ($ordersList as $day => $gorder) {
//                    $day = arabic_to_english_date($day);

                     ?>
                <div class="card">
                    <div class="card-header" id="headingOne<?=$key?>">
                        <h2 class="mb-0 d-flex justify-content-between">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne<?=$key?>">View <?=$day?></button>
                            <a  href="#"  target ='_blank' class="btn btn-primary align-self-md-end export-it" >Export Day <?=$day?></a>
                            <a  href="<?=ROOT_URL?>/drivers/print/<?=$driver->id?>?day=<?=urlencode($day)?>"  target ='_blank' class="btn btn-primary align-self-md-end" >Print Order <?=$day?></a>
                            <a  href="<?=ROOT_URL?>/drivers/print_all/<?=$driver->id?>?day=<?=$day?>"  target ='_blank' class="btn btn-primary align-self-md-end" >Print All <?=$day?></a>


                        </h2>
                    </div>
                    <div id="collapseOne<?=$key?>" class="collapse" aria-labelledby="headingOne<?=$key?>" data-parent="#accordionExample">
                        <div class="card-body">

                            <div class="table-responsive" style="min-height: 400px">
                                <table class="table table-striped Serialized">
                                    <tr>
                                        <th scope="col"><?= __('Id') ?></th>
                                        <th scope="col"><?= __('Reference') ?></th>
                                        <th scope="col"><?= __('Brand') ?></th>
                                        <th scope="col"><?= __('Receiver Name') ?></th>
                                        <th scope="col"><?= __('Receiver Phone') ?></th>
                                        <th scope="col"><?= __('Receiver Address') ?></th>
                                        <th scope="col"><?= __('City') ?></th>
                                        <th scope="col"><?= __('Description') ?></th>
                                        <th scope="col"><?= __('Cod') ?></th>
                                        <th scope="col"><?= __('Statues') ?></th>

                                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                                    </tr>
                                    <?php foreach ($gorder as $orders):
                                        ?>
                                        <tr>
                                            <td><?= DID($orders->id) ?></td>
                                            <td><?= $orders->reference ?></td>
                                            <td><?= h($orders->user->username) ?></td>

                                            <td><?= h($orders->receiver_name) ?></td>
                                            <td><?= $this->PhoneNumbers->phoneIt($orders->receiver_phone) ?></td>
                                            <td><?= h($orders->receiver_address) ?></td>
                                            <td><?= h($orders->city) ?></td>
                                            <td><?= h($orders->package_description) ?></td>
                                            <td><?= h($orders->cod) ?></td>
                                            <td class="status-element"><?= $this->Hooks->apply_filters('status_button_driver',HSTT($orders->statues),$orders) ?></td>
                                            <td class="actions">
                                                <div class="dropdown" style="display: inline-block">
                                                    <button id="dLabel" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?=__('Update')?>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dLabel">
                                                        <?php
                                                        foreach (types_for_state as $update){
                                                            if( in_array($update,$allowed_updates)){
                                                                echo  $this->Html->link(__($update),$this->Hooks->apply_filters('update_order_link', ['action' => '/update','controller'=>'orders', $orders->id,$update])
                                                                    , ['title' => __('View'), 'class' => $this->Hooks->apply_filters('update_order_link_ajax_implementation','dropdown-item btn btn-secondary btn-sm ajaxinLink')]);
                                                            }
                                                        }
                                                        if($allow_comment == 1){
                                                            echo  $this->Html->link(__('Comment'), ['action' => '/addcomment','controller'=>'orders', $orders->id],
                                                                ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary btn-sm add_comment']);
                                                        }
                                                        $this->Hooks->do_action('driver_online_order_actions',$orders->id);
                                                        ?>
                                                    </div>

                                                </div>
                                                <?php $this->Hooks->do_action('driver_online_order_actions_container',$orders); ?>




                                                </div>                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>



                        </div>
                    </div>
                </div>
                <?php
                $key++;
                } ?>

            </div>
        </div>



    </div>

</div>

<script>
    <?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>

    $(document).ready(function () {
        $('.add_comment').click(function () {
            url = window.location.href;

            lastSegment = url.split("/").pop();

            $url = $(this).attr('href');
            var resp = window.prompt("<?=__('Write Your Comment')?>");
            alert("<?=__('Comment has been added')?>");
            // ajax request
            $.ajax($url+"/"+lastSegment+"/?message="+resp);
            return false;
        })

        //search
        $('#search_trigger').click(function () {
            $value = $('#search_input').val();
            clearSearches();
            searchFunc($value);
        })
        function searchFunc($value) {
            $('td:contains("'+$value+'")').css('border','1px solid red');
            $('td:contains("'+$value+'")').parent('tr').css('border','2px solid red');
            $('td:contains("'+$value+'")').parents( "div.card" )
                .css( "border", "2px red solid" );
            setTimeout(function () {
                $('td:contains("'+$value+'")').parents( "div.card" ).find('.card-header h2 button').click();
            },600)
        }
        function clearSearches() {
            $('td').removeAttr('style');
            $('tr').removeAttr('style');
            $('div.card').removeAttr('style');
            // $('td:contains("'+$value+'")').parent('tr').css('border','2px solid red');

            $( "div.card" ).find('.card-header h2 button[aria-expanded="true"]').click();
        }

    })

    <?php $this->Html->scriptEnd(); ?>

</script>
