<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Driver $driver
 */
?>
<div class="drivers  view large-9 medium-8 columns content">
    <h3><?= h($driver->name) ?></h3>
    <div class="row">
        <?php $this->Hooks->do_action('drivers_control_view_buttons'); ?>

    </div>
    <div class="row">

<?php
//        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));
//Total Orders
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'danger',
    'count' => $driver_state['total_active_orders_count'],
    'title' => __('Active Orders'),
    'icon' => 'map',
    'sub_msg' => __('On route orders'),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'blue',
    'count' => $driver_state['total_orders_count'],
    'title' => __('Total Orders'),
    'icon' => 'car-side',
    'sub_msg' => __('Orders from the account creation '),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'cyan',
    'count' => $driver_state['total_active_orders_sum'],
    'title' => __('Money to be collected '),
    'icon' => 'money-check-alt',
    'sub_msg' => __('COD of the active '),
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'yellow',
    'count' => $driver_state['total_driver_orders_sum'],
    'title' => __('Total Trade '),
    'icon' => 'money-check-alt',
    'sub_msg' => __('SUM of the COD of all orders  '),
]);

?>


    </div>


    <div class="row">
        <section class="col-lg-6 connectedSortable">
            <?php
            echo $this->element('Utils.backend/widgets/chart_dimminsions',[
                'canv_id' => 'chartscanv',
                'height' => '400',
                'title' => 'Orders Since account creation  ',
                'icon' => 'file-invoice-dollar',
                'chart_type' => 'bar',
                'cahrtData' => $driver_timeline,
                'cahrtData' => $driver_timeline,
                'scale' =>SUSTEM_CURRENCY
            ]);

            ?>
        </section>
        <?php
        echo $this->element('Utils.backend/widgets/data_table',[
            'color' => 'green',
            'size' => '6',
            'table_data' => $driver_statues,
            'title' => __('Order Statues '),
            'icon' => 'file-invoice-dollar',
            'sub_msg' => __('Order Status List ') ,

        ]);

        ?>
    </div>

    <div class="related">

<!--        accordion element -->

        <div class="bs-example">
            <div class="accordion" id="accordionExample">
                <?php
                $key = 1 ;
                foreach ($ordersList as $day => $gorder) {
                     ?>
                <div class="card">
                    <div class="card-header" id="headingOne<?=$key?>">
                        <h2 class="mb-0 d-flex justify-content-between">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne<?=$key?>">View <?=$day?></button>
                            <?php $this->Hooks->do_action('driver_view_daily_buttons',strtotime($day) ) ?>
                            <a  href="#"  target ='_blank' class="btn btn-primary align-self-md-end export-it" >Export Day <?=$day?></a>
                            <a  href="<?=ROOT_URL?>/drivers/print/<?=$driver->id?>?day=<?=$day?>"  target ='_blank' class="btn btn-primary align-self-md-end" >Print Order <?=$day?></a>
                            <a  href="<?=ROOT_URL?>/drivers/print_all/<?=$driver->id?>?day=<?=$day?>"  target ='_blank' class="btn btn-primary align-self-md-end" >Print All <?=$day?></a>
                        </h2>
                    </div>
                    <div id="collapseOne<?=$key?>" class="collapse" aria-labelledby="headingOne<?=$key?>" data-parent="#accordionExample">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-striped Serialized">
                                    <tr>
                                        <th scope="col"><?= __('Id') ?></th>
                                        <th scope="col"><?= __('Brand') ?></th>
                                        <th scope="col"><?= __('Receiver Name') ?></th>
                                        <th scope="col"><?= __('Receiver Phone') ?></th>
                                        <th scope="col"><?= __('Receiver Address') ?></th>
                                        <th scope="col"><?= __('City') ?></th>
                                        <th scope="col"><?= __('Description') ?></th>
                                        <th scope="col"><?= __('Cod') ?></th>
                                        <th scope="col"><?= __('statues') ?></th>
                                        <th scope="col"><?= __('Fees') ?></th>

                                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                                    </tr>
                                    <?php foreach ($gorder as $orders):
                                        ?>
                                        <tr>
                                            <td><?= DID($orders->id) ?></td>
                                            <td><?= h($orders->user->username) ?></td>

                                            <td><?= h($orders->receiver_name) ?></td>
                                            <td><?= h($orders->receiver_phone) ?></td>
                                            <td><?= h($orders->receiver_address) ?></td>
                                            <td><?= h($orders->city) ?></td>
                                            <td><?= h($orders->package_description) ?></td>
                                            <td><?= h($orders->cod) ?></td>
                                            <td class="status-element"><?= HSTT($orders->statues) ?></td>
                                            <td><?= h($orders->fees) ?></td>
                                            <td class="actions">
                                                <div class="dropdown" style="display: inline-block">
                                                    <button id="dLabel" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?=__('Update')?>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dLabel">
                                                        <?php
                                                        foreach (types_for_state as $update){
                                                            if($update =="Delivered" || $update =="Returned to warehouse" || $update =="Collected" || $update =="Canceled by client" || $update =="Terminated" ){
                                                                echo  $this->Html->link(__($update), ['action' => '/update','controller'=>'orders', $orders->id,$update], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary btn-sm ajaxinLink']);
                                                            }
                                                        }
                                                        $this->Hooks->do_action('driver_view_action_status_update',$orders->id );
                                                        ?>
                                                    </div>

                                                </div>
                                                <div class="dropdown" style="display: inline-block" >
                                                    <?= $this->Html->link(__('unassigned'), ['action' => 'unassigned','controller'=>'drivers', $orders->id,$driver->id], ['title' => __('delete the link'), 'class' => 'btn  btn-danger ajaxinLinkAuto']) ?>

                                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <?= $this->Html->link(__('View'), ['action' => 'view','controller'=>'orders', $orders->id], ['title' => __('View'), 'class' => 'dropdown-item btn  btn-secondary']) ?>
                                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit','controller'=>'orders', $orders->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn  btn-secondary']) ?>

                                                    </div>
                                                </div>                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>



                        </div>
                    </div>
                </div>
                <?php
                $key++;
                } ?>

            </div>
        </div>



    </div>
    <div class="related">
        <h4><?= __('Related Driver Pickup') ?></h4>
        <?php if (!empty($driver->driver_pickup)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Driver Id') ?></th>
                    <th scope="col"><?= __('Order Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($driver->driver_pickup as $driverPickup): ?>
                <tr>
                    <td><?= h($driverPickup->id) ?></td>
                    <td><?= h($driverPickup->driver_id) ?></td>
                    <td><?= h($driverPickup->order_id) ?></td>
                    <td><?= h($driverPickup->created) ?></td>
                    <td><?= h($driverPickup->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'DriverPickup', 'action' => 'view', $driverPickup->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'DriverPickup', 'action' => 'edit', $driverPickup->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'DriverPickup', 'action' => 'delete', $driverPickup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverPickup->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>

