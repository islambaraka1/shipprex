<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>


<div class="form-group" style="float: left" >
    <label>Date and time range:</label>

    <div class="input-group" >r
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control float-right" id="reservationtime">
    </div>
    <!-- /.input group -->
</div>
<?php set_modelValues('columns_view_in_the_dashboard'); ?>

<?php echo $this->element('orders_table',['orders'=>$orders,'status'=>$status]);?>

<?php
if($this->Hooks->verify ('system_show_data_table')){
    echo $this->element('backend/ajaxDatatable',['url'=>'orders/get_data/active']);
} ?>
