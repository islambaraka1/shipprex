<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Pickup[]|\Cake\Collection\CollectionInterface $pickups
 * @var \App\Model\Entity\Action[]|\Cake\Collection\CollectionInterface $actions
 * @var \App\Model\Entity\DriverPickup[]|\Cake\Collection\CollectionInterface $driverPickup
 * @var \App\Model\Entity\Transaction[]|\Cake\Collection\CollectionInterface $transactions
 * @var \App\Model\Entity\Driver[]|\Cake\Collection\CollectionInterface $drivers
 */
$templateHalf =[
    'inputContainer' => '<div class="form-group col-sm-6 {{type}}{{required}}">{{content}}</div>',
];
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Orders'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Pickups'), ['controller' => 'Pickups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Pickup'), ['controller' => 'Pickups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Actions'), ['controller' => 'Actions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Action'), ['controller' => 'Actions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Drivers'), ['controller' => 'Drivers', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Driver'), ['controller' => 'Drivers', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="orders form content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Add Order') ?></legend>
        <?php
        if($_SESSION['Auth']['User']['group_id'] == 1 || $_SESSION['Auth']['User']['group_id'] == 4 ) {
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'select','value' => $_SESSION['Auth']['User']['id']]);
        }else{
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'hidden','value' => $_SESSION['Auth']['User']['id']]);

        }

        echo $this->Form->control('type',['options' => translateList($orderType)]);

        echo "<div class='row'>";
            echo $this->Form->control('receiver_name',['templates'=>$templateHalf]);
            echo $this->Form->control('receiver_phone',['templates'=> $templateHalf ]);
            echo '</div>';

            echo $this->Form->control('receiver_address',['type'=>'text']);

            echo "<div class='row'>";
            echo $this->Form->control('reference',['label'=>__('Business Reference ') ,'placeholder' => '#9865654','templates'=>$templateHalf]);
            $userZones = [];
            if( isset($usersZone[0]->id)){
                foreach($usersZone as $key => $zoneUsersRecord){
                    $userZones[$zoneUsersRecord->zone_id] = $zoneUsersRecord->price;
                }
            }
            $ZonesList = $zones->toArray();
            $newZones = [];
            foreach ($ZonesList as $key => $zone){
                $setPrice = isset($userZones[$zone->id] )? $userZones[$zone->id] : $zone->default_price;
                $newZones[$zone->name] = " (". $setPrice ." ".SUSTEM_CURRENCY.") " . $zone->name ;
            }

            echo $this->Form->control('city',['templates'=>$templateHalf , 'options'=>$newZones,'class'=>'select2bs4'] );
            echo "</div>";
            $this->Hooks->do_action('order_add_form_before_description');
            echo $this->Form->control('package_description');
            echo $this->Form->control('cod',['label' => 'COD (including delivery  fees)' ]);
            $this->Hooks->do_action('orders_add_form_before_end');

                ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<!-- Select2 -->
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
$(function () {
//Initialize Select2 Elements
$('.select2').select2()

//Initialize Select2 Elements
$('.select2bs4').select2({
theme: 'bootstrap4'
})
});

<?php $this->Html->scriptEnd(); ?>
