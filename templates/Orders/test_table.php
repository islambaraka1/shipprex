<?php

/** @var \App\View\AppView $this */
/** @var \App\View\Helper\TablesRenderHelper $this->TablesRender */
/** @var Array $orders */

/*
 * combine types [ 'vertical', 'horizontal','card','element','table', 'icons' ,'tooltips' ]
 */
$this->TablesRender->setFieldMap([
    'id' => [
        "functionName" => "DID"
    ],
    'user_id' => [
        "functionName" => [
            "link" => [
                "url" => [
                    "controller" => "Users",
                    "action" => "view",
                    "plugin" => "UsersManager",
                    "parameter" => "user.id"
                ],
                "text" => "user_id"
            ]
        ],
        "icon" => "user",
        "fieldName" => "user.username",
        "append" => [
            "type" =>[
                'functionName' => 'HSTT'
            ], "reference" => [
                'icon' => 'barcode'
            ]
        ],
        'combine_style' => 'newline'
    ],
    'receiver_name' => [
        "rename" => "Receiver Data",
        "combine" => [
            "receiver_name"=>['icon'=>'user'], "receiver_phone"=>['icon'=>'phone'],"city"=>['icon'=>'map-marker'], "receiver_address"=>['icon'=>'home'] , "receiver_email"
    ],
        "combine_type" => "vertical"
        ],
    'receiver_email' => [
        "hidden" => true,
    ],
    'pickup_id' => [
        "hidden" => true,
    ],
    'statues' => [
        "functionName" => "HSTT"
    ],
    'type' => [
        "hidden" => true,
    ],
    'receiver_phone' => ["hidden" => true,],
    'reference' => ["hidden" => true,],
    'receiver_address' => ["hidden" => true,],
    'work_address' => ["hidden" => true,],
    'driver_id' => ["hidden" => true,],
    'invoice_id' => ["hidden" => true,],
    'pickup' => ["hidden" => true,],
    'user' => ["hidden" => true,],
    'fees' => ["functionName" => "getAvatar",],
]);
echo $this->TablesRender->renderTableFromArray($orders->toArray());




