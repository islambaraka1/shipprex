<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 * @var $order_full_counts
 *
 */

?>

<div class="row">
    <?php
        //top row notifications bar
        $this->Hooks->do_action('before_orders_list_top_row');
    ?>
</div>
<div class="row">
    <div class="col-md-6">
    <div class="row">

    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'size' => 6,

        'count' => $due_orders_count,
        'title' => __('Due Orders '),
        'icon' => 'truck',
        'sub_msg' => __('Orders not collected in the past 3 days  '),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'size' => 6,
        'count' => $last_day_orders,
        'title' => __('New Orders'),
        'icon' => 'truck',
        'sub_msg' => __('Last 24 Hours Created Orders') ,
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'size' => 6,
        'count' => $order_full_counts['total_orders_amount_sum'],
        'title' => __('Total UpComing COD '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('COD Average : ').$order_full_counts['total_orders_amount_avg'],
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'maroon',
        'size' => 6,
        'count' => $order_full_counts['total_orders_fees_sum'],
        'title' => __('Total UpComing Fees '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Fees Average : ').$order_full_counts['total_orders_fees_avg'],
    ]);

//
    ?>
    </div>
    </div>
    <?php
    echo $this->element('Utils.backend/widgets/data_table',[
        'color' => 'green',
        'size' => '6',
        'table_data' => $statues_table,
        'title' => __('Order Statues '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => 'Order Status List ' ,
        'max_height' => '335',

    ]);


    ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->Html->link(__('List inactive orders'), ['action' => 'unactive'], ['class' => 'btn btn-primary']) ?>
        <?php $this->Hooks->do_action('orders_list_top_buttons') ?>
    </div>
</div>






<div class="form-group" style="float: left" >
    <label>Date and time range:</label>

    <div class="input-group" >
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control float-right" id="reservationtime">
    </div>
    <!-- /.input group -->
</div>
<?php set_modelValues('columns_view_in_the_dashboard'); ?>

<?php echo $this->element('orders_table',['orders'=>$orders,'status'=>$status]);?>

<?php
if($this->Hooks->verify ('system_show_data_table')){
    echo $this->element('backend/ajaxDatatable',['url'=>'orders/get_data/active']);
} ?>

