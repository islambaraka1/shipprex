<div class="d-flex justify-content-between m-2">
    <h2>Please select file and upload it </h2>

    <a href="<?=get_option_value('excel_template_file')?>" class="btn btn-primary">Download Template</a>
</div>
<form action="<?=ROOT_URL?>/orders/uploadfile" method="post" enctype="multipart/form-data">
    <div class="custom-file">
        <input type="file" class="custom-file-input"  name="file" id="customFile">
        <?php
        if($_SESSION['Auth']['User']['group_id'] == 1 || $_SESSION['Auth']['User']['group_id'] == 4 ) {
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'select','value' => $_SESSION['Auth']['User']['id']]);
        }else{
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'hidden','value' => $_SESSION['Auth']['User']['id']]);

        }
        ?>
        <label class="custom-file-label" for="customFile">Choose file</label>
    </div>
    <input type="submit" value="Upload" id="but_upload" class="btn btn-default">
</form>

<div class="flex-d flex-row hider" style="display: none;">
    <h2>Please check all if everything looks good </h2>
    <p>if not try upload a new file </p>

    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <td><?=__('Receiver name')?></td>
                <td><?=__('Receiver phone')?></td>
                <td><?=__('Receiver Address')?></td>
                <td><?=__('Receiver City')?></td>
                <td><?=__('COD - cost including the shipping fees')?></td>
                <td><?=__('Description')?></td>
                <td><?=__('Reference')?></td>
            </tr>
            <tbody class="table-body">

            </tbody>
        </table>
    </div>
    <div class="holder">
        <a href="#" id="save" class="btn btn-primary"><?=__('Everything looks good')?> <i class="fa fa-heart"></i></a>
    </div>

</div>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
<!--<script>-->

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
var fileName = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});



$(document).ready(function(){
var $fileName;
function changeThetable(data){
$('.hider').show('slow');
$('.table-body').empty();
for (var property in data) {
if (data.hasOwnProperty(property)) {
$row = data[property];
console.log();
$('.table-body').append("<tr>" +
    "            <td>"+$row.Name+"</td>" +
    "            <td>"+$row.Phone+"</td>" +
    "            <td>"+$row.Address+"</td>" +
    "            <td>"+$row.City+"</td>" +
    "            <td>"+$row.Cost+"</td>" +
    "            <td>"+$row.Description+"</td>" +
    "            <td>"+$row.Ref+"</td>" +
    "        </tr>");
}
}
$userId = $('#user-id').val();
console.log($userId);
$('#save').attr('href', "<?=ROOT_URL?>/excel/create-orders/"+$fileName+"/"+$userId);
}

$("#but_upload").click(function(){

var fd = new FormData();
var files = $('#customFile')[0].files[0];
fd.append('file',files);

$.ajax({
url: "<?=ROOT_URL?>/orders/uploadfile",
type: 'post',
data: fd,
contentType: false,
processData: false,
success: function(response){
if(response != 0){
jsonres = JSON.parse(response);
console.log(jsonres.data);
$fileName = jsonres.file;

changeThetable(jsonres.data);

}else{
alert('file not uploaded');
}
},
});
return false;
});
});

<?php $this->Html->scriptEnd(); ?>
<script>
</script>
