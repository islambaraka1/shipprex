<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */

?>


<div class="row">
    <div class="col-md-12">
        <?= $this->Html->link(__('List active orders'), ['action' => 'list'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>


<div class="form-group" style="float: left" >
    <label>Date and time range:</label>

    <div class="input-group" >
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control float-right" id="reservationtime">
    </div>
    <!-- /.input group -->
</div>
<?php set_modelValues('columns_view_in_the_dashboard'); ?>

<?php echo $this->element('orders_table',['orders'=>$orders,'status'=>$status]);?>


<?=$this->element('backend/ajaxDatatable',['url'=>'orders/get_data/inactive'])?>
