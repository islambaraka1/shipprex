<!-- Select2 -->
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="drivers form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Bulk Actions') ?></legend>


        <?php
        echo $this->Form->control('orders._ids', ['options' => $orderList ,'class'=>'select2bs4','label'=> __('Please use barcode to search all orders ids')]);
        echo $this->Form->control('status', ['options' => translateList($status),'id'=>'status' ,'label'=> __('Update All To')]);
        echo $this->Form->control('tags', ['options' => [],'label'=> __('Update All To')]);
        ?>
    </fieldset>


    <table id="example1" class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><?= __('id') ?></th>
            <th scope="col"><?= __('created') ?></th>
            <th scope="col"><?= __('cod') ?></th>
            <th scope="col"><?= __('fees') ?></th>
            <th scope="col"><?= __('reciver_name') ?></th>
            <th scope="col"><?= __('reciver_phone') ?></th>
            <th scope="col"><?= __('addresses') ?></th>
        </tr>
        </thead>
        <tbody id="target_body">

        </tbody>
    </table>

    <?= $this->Form->button(__('Submit')) ?>

    <?= $this->Form->end() ?>
</div>
<?php
//echo $this->Html->script(ROOT_URL.'plugins/select2/js/select2.full.js">', ['block' => 'scriptBottom']);
?>
<script>
    <?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>

$(document).ready(function() {
    //on status change
    $('#status').on('change', function() {
        var status = $(this).val();
        //ajax request the status
        $.ajax({
            url: "<?=ROOT_URL?>order-extera/tags/getTagList/"+status,
            type: "GET",
            success: function(data) {
                console.log(data);
                //loop through the data and append to the select
                var tags = data;
                console.log(tags);
                var options = '';
                $.each(tags, function(key, value) {
                    options += '<option value="' + key + '">' + value + '</option>';
                });
                $('#tags').html(options);
            }
        });
    });

    //Initialize Select2 Elements
$('.select2').select2()

//Initialize Select2 Elements
$('.select2bs4').select2({
theme: 'bootstrap4'
    ,
    ajax: {
        url: '<?=ROOT_URL?>orders/bulk_items',
        delay: 750,
            data: function (params) {
            var query = {
                search: params.term?params.term:"",
                field: "id",
                type: 'public'
            }
            return query;
        },
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
})
$('.select2bs4').on('select2:select', function (e) {
// Do something
var data = e.params.data;
console.log(data);
    $.get( "<?=ROOT_URL?>orders/orderrow/"+data.id, function( row ) {
    $trData = JSON.parse(row);
    $('#target_body').append('<tr> <td>'+$trData.id+'</td> <td>'+$trData.created+'</td> <td>'+$trData.cod+'</td> <td>'+$trData.fees+'</td> <td>'+$trData.receiver_name+'</td> <td>'+$trData.receiver_phone+'</td> <td>'+$trData.receiver_address+'</td> </tr>');
    });
});

$('.select2bs4').on('select2:unselect', function (e) {
// Do something
var data = e.params.data;
    $("td:contains("+data.id+")").parent().remove();
});

// auto insert
    setTimeout(function(){
        autoInsert('.select2bs4');
    },1000);
});
<?php $this->Html->scriptEnd(); ?>
</script>
