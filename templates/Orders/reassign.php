<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Driver $driver
 * @var \App\Model\Entity\DriverPickup[]|\Cake\Collection\CollectionInterface $driverPickup
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Drivers'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Driver Pickup'), ['controller' => 'DriverPickup', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>
<!-- Select2 -->
<?php
//pr($order);
?>
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="drivers form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Assign Driver') ?></legend>
        <?php
        $half =[
            'inputContainer' => '<div class="form-group col-sm-6 {{type}}{{required}}">{{content}}</div>',
        ];?>
        <div class="row">
            <?php
            echo $this->Form->control('id',['label'=>'Select Driver', 'options'=>$driver ,'templates' => $half ]);
            echo $this->Form->control('orders._joinData.target_day',[ 'label'=>'Target day','type' =>'date', 'value'=>'today' , 'templates' => $half]);
            ?>
        </div>
        <?php
//        echo $this->Form->control('orders._ids', ['options' => $orderList ,'class'=>'select2bs4']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php
//echo $this->Html->script(ROOT_URL.'plugins/select2/js/select2.full.js">', ['block' => 'scriptBottom']);
?>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>

$(function () {
//Initialize Select2 Elements
$('.select2').select2()

//Initialize Select2 Elements
$('.select2bs4').select2({
theme: 'bootstrap4'
})
});
<?php $this->Html->scriptEnd(); ?>
