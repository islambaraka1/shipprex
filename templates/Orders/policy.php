
<style>
    p, td{
        font-size: <?=get_option_value('print_font_size_number_in_pixels') > 0 ?get_option_value('print_font_size_number_in_pixels') : 14 ?>px;
    }
</style>
<div id="customer">

    <table id="meta">
        <tr>
            <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left" width="62%">
                <strong><?=__('Bill to')?></strong> </br>
                <table id="items">
                    <b>
                        <?php
                        $print = $data->user->username;
                        echo show_if_option_equels('print_show_sender_name',1,$print);
                        ?>

                        </b></br> </br>

                    <?php
                        $print = $data->user->city;
                        echo show_if_option_equels('print_show_sender_city',1,$print);
                    ?>
                    </br>
                    <?php
                        $print = $data->user->phone;
                        echo show_if_option_equels('print_show_sender_phone',1,$print);
                    ?>
                    </br>
                    <?php
                    $print = "Notes: ".$data->notes;
                    echo show_if_option_equels('print_show_ship_notes',1,$print);
                    ?>
                    <div class="row">
                        <?php $this->Hooks->do_action('orders_print_under_bills_info',$data); ?>
                    </div>
                </table>
            </td>
            <td class="meta-head"><p style="color:black;"><?=__('Pay Mode')?></p></td>
            <td>
                <?=__('Cash')?>				</td>
        </tr>
        <tr>
            <td class="meta-head"><p style="color:black;"><?=__('Courier Company')?></p></td>
            <td> <?=SETTING_KEY['global']['site_settings_']['site_name'];?> </td>
        </tr>
        <tr>
            <td class="meta-head"><p style="color:black;"><?=__('Shipping date')?></p></td>
            <td><?=$data->created?></td>
        </tr>
        <tr>
            <td class="meta-head"><p style="color:black;"><?=__('Invoice No..')?></p></td>
            <td><b>INV<?=$barcode?></b></td>
        </tr>
    </table>
</div>
<table id="items">
    <tr>

        <td>
            <?php
            $print = __('Business REF:')."$data->reference";
            echo show_if_option_equels('print_show_sender_ref',1,$print);
            ?>
            </td>
        <td>
            <?php
            $print =__('Destination: ')."<b> $data->city </b>";
            echo show_if_option_equels('print_show_sender_city',1,$print);
            ?>
            </td>
    </tr>
    <tr>
        <td>
            <?=__('Shipper')?> :  <?php
            $print = $data->user->username;
            $print = show_if_option_equels('print_show_sender_name',1,$print);
            echo $print == '' ? 'Confidential':$print;
            ?>
            <br>
            <?php
            $print ="$data->type";
            echo show_if_option_equels('print_show_ship_type',1,$print);
            ?>
            <br>

            <?php
            $print = $data->user->phone;
            echo show_if_option_equels('print_show_sender_phone',1,$print);
            ?>
            </br>

            <br />


        </td>
        <td>
            <b><?=__('Reciver')?> :</b>
            <br />
            <?php
            $print ="$data->receiver_name";
            echo show_if_option_equels('print_show_receiver_name',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_email;
            echo show_if_option_equels('print_show_receiver_email',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_address;
            echo show_if_option_equels('print_show_receiver_address',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_phone;
            echo show_if_option_equels('print_show_receiver_phone',1,$print);
            ?>
            <br />
            <?php
            $print = $data->package_description;
            echo show_if_option_equels('print_show_ship_description',1,$print);
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" colspan="" class="total-line balance"><p style="color:black;"><?=__('Grand total')?> </p></td>
        <td align="right" class="total-value balance"><div class="due"> <p style="color:black;"><b><?=SUSTEM_CURRENCY?> <?=$data->cod?> </b></p></div></td>
    </tr>
</table>

<!--    end related transactions -->
