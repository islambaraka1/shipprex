<?php

function update_button($view, $status , $order){
    $update_content = "";
    //fixing bug with users permission of seller
    if($_SESSION['Auth']['User']['group_id'] == 3){
        $status = ['Canceled by client'];
    }
    foreach ($status as $update){
        $update_content .=  $view->Html->link(__($update), ['action' => 'update', $order->id,$update], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary ajaxinLink']);
    }
    return '  <div class="dropdown dropleft" style="display: inline-block">
                    <button id="dLabel" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-edit"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dLabel">
                        '.
        $update_content
                        .'
                    </div>

                </div>';
}

function action_buttons($view, $status , $order){
    $bottom = "";
    $delete_link =$view->Form->postLink(__('Delete'), ['action' => 'delete', $order->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $order->id),
            'title' => __('Delete'), 'class' => 'dropdown-item btn btn-danger']);

    $bottom .= show_if_option_equels('allow_delete_for_orders',1,$delete_link);
    ob_start(); // Start output buffering

    $view->Hooks->do_action('orders_action_buttons_list',$order->id);
    $view->Hooks->do_action('settings_menu_patching',$order->id);
    $action_res = ob_get_contents(); // Store buffer in variable

    ob_end_clean(); // End buffering and clean up

     return '<div class="dropdown dropleft" style="display: inline-block" >
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        '. $view->Html->link(__('Print'), ['action' => 'policy', $order->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary' ,'target' => '_blank']) .'
                        '. $view->Html->link(__('View'), ['action' => 'view', $order->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary','target' => '_blank']) .'
                        '. $view->Html->link(__('Edit'), ['action' => 'edit', $order->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn btn-secondary']) .'


                    '.$bottom.'
                    '.$action_res.'
                    </div>
                </div>';
}

function extras($view, $status , $order){
    ob_start(); // Start output buffering

    $view->Hooks->do_action('orders_action_after_buttons_list',$order);
    $action_res = ob_get_contents(); // Store buffer in variable

    ob_end_clean(); // End buffering and clean up
    return $action_res;
}
foreach ($orders['aaData'] as $key => $order){
    $orders['aaData'][$key]['actions'] = $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $order['id']], ['escape' => false, 'class' => 'btn btn-sm btn-outline-primary']);
    $orders['aaData'][$key]['actions'] .= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $order['id']], ['escape' => false, 'class' => 'btn btn-sm btn-outline-primary']);
    if($_SESSION['Auth']['User']['group_id'] == 1 || $_SESSION['Auth']['User']['group_id'] == 4){

    $orders['aaData'][$key]['actions'] .= update_button($this, $status, $order);
    $orders['aaData'][$key]['actions'] .= action_buttons($this, $status, $order);
    $orders['aaData'][$key]['actions'] .= extras($this, $status, $order);
    }
    $orders['aaData'][$key]['id'] = DID($orders['aaData'][$key]['id']);
    $orders['aaData'][$key]['statues'] = HSTT($orders['aaData'][$key]['statues']);
    //if the tags is there then show itz
    if(isset($orders['aaData'][$key]['tags']) && !empty($orders['aaData'][$key]['tags'])){
        $orders['aaData'][$key]['statues'] .= $this->Misc->displayTags($orders['aaData'][$key]['tags']);
        $orders['aaData'][$key]['notes'] .= $this->Misc->displayTags($orders['aaData'][$key]['tags']);
    }
    //convert created value to the format 2021-10-16 14:08:23
    $formatted = $orders['aaData'][$key]['created']->format('Y-m-d H:i:s');
    $orders['aaData'][$key]['created'] =  $formatted;
    $orders['aaData'][$key]['modified'] =  $orders['aaData'][$key]['modified']->format('Y-m-d H:i:s');
    // meta rendering for the table
    foreach ($orders['aaData'][$key]['meta'] as $meta_key => $meta_value){
        $orders['aaData'][$key]['meta'] [$meta_value->name] = render_icon($meta_value->value);
    }
}
echo json_encode($orders);
?>
