
<style>
    p, td{
        font-size: <?=get_option_value('print_font_size_number_in_pixels') > 0 ?get_option_value('print_font_size_number_in_pixels') : 14 ?>px;
    }
</style>
<div id="customer">

    <table id="meta">
        <tr>
            <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left" width="90%">
                <strong>Bill to</strong>
                <table id="items">

                        <?php
                        $data = $order;
                        $barcode = DID($order->id);
                        $print = $data->user->username;
                        echo show_if_option_equels('print_show_sender_name',1,$print);
                        ?>



                    <?php
                    $print = $data->user->city;
                    echo show_if_option_equels('print_show_sender_city',1,$print);
                    ?>
                    </br>
                    <?php
                    $print = $data->user->phone;
                    echo show_if_option_equels('print_show_sender_phone',1,$print);
                    ?>
                    </br>
                    <?php
                    $print = "Notes: ".$data->notes;
                    echo show_if_option_equels('print_show_ship_notes',1,$print);
                    ?>

                </table>
            </td>

        </tr>

    </table>
</div>
<table id="items">
    <tr>

        <td>
            <?php
            $print = "Business REF:$data->reference";
            echo show_if_option_equels('print_show_sender_ref',1,$print);
            ?>
        </td>
        <td>
            <?php
            $print ="Destination: <b> $data->city </b>";
            echo show_if_option_equels('print_show_sender_city',1,$print);
            ?>
        </td>
    </tr>
    <tr>
        <td>
            Shipper :  <?php
            $print = $data->user->username;
            $print = show_if_option_equels('print_show_sender_name',1,$print);
            echo $print == '' ? 'Confidential':$print;
            ?>
            <br>
            <?php
            $print ="$data->type";
            echo show_if_option_equels('print_show_ship_type',1,$print);
            ?>
            <br>

                <?php
                $print = $data->user->phone;
                echo show_if_option_equels('print_show_sender_phone',1,$print);
                ?>
            </br>

            <br />


        </td>
        <td>
            <b>Reciver :</b>
            <br />
            <?php
            $print ="$data->receiver_name";
            echo show_if_option_equels('print_show_receiver_name',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_email;
            echo show_if_option_equels('print_show_receiver_email',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_address;
            echo show_if_option_equels('print_show_receiver_address',1,$print);
            ?>
            <br />
            <?php
            $print = $data->receiver_phone;
            echo show_if_option_equels('print_show_receiver_phone',1,$print);
            ?>
            <br />
            <?php
            $print = $data->package_description;
            echo show_if_option_equels('print_show_ship_description',1,$print);
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" colspan="" class="total-line balance"><p style="color:black;">Grand total </p></td>
        <td align="right" class="total-value balance"><div class="due"> <p style="color:black;"><b><?=SUSTEM_CURRENCY?> <?=$data->cod?> </b></p></div></td>
    </tr>
</table>

<!--    end related transactions -->
