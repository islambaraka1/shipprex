<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>

<div class="row">
<style>
    .CoolScroll::-webkit-scrollbar {
        width: 8px;
        background-color: #F5F5F5;
    }


    .CoolScroll::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(104, 140, 240, 0.3);
    }

    .CoolScroll::-webkit-scrollbar-thumb {
        background-color: #1D00AF;
        outline: 1px solid slategrey;
    }
    .orders-display .row{
        margin: 10px auto;
    }
    .orders-display i.fa{
        color: #da2839;
    }
</style>
    <div class="col-md-3 CoolScroll" style="overflow-x: scroll; max-height: 650px;">
        <!-- Timelime example  -->
        <div class="row">
            <div class="col-md-12">
                <!-- The time line -->
                <div class="timeline">
                    <!-- timeline time label -->
                    <?php foreach ($order->actions as $action){
                        ?>

                    <div class="time-label">
                        <span class="bg-gradient-indigo"><?=$action->created?></span>
                    </div>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <div>
                        <i class="fas fa-step-forward bg-blue"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fas fa-clock"></i> <?=$action->created->timeAgoInWords()?></span>
                            <h3 class="timeline-header"><?=__($action->name)?> <?=__(" by: " ). $action->user->username;?> </h3>

                            <div class="timeline-body">
                                <?=$action->description?>
                            </div>

                        </div>
                    </div>
                    <!-- END timeline item -->


                    <?php } ?>















                    <div>
                        <i class="fas fa-clock bg-gray"></i>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <!-- /.timeline -->



<div class=" col-md-9">
    <div class="orders-display">
        <div class="row">
            <div class="col-md-6"> <h2><?=__('Tracking No.')?></h2>
            </div>
            <div class="col-md-6"> <h2><?=DID($order->id)?> </h2></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4 text-center"><i class="fa fa-map-pin largeicon fa-4x"></i></div>
                    <div class="col-md-8">
                        <h3><?=__('Pickup Location')?></h3>
                        <span> <?=$order->city?> </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4 text-center"><i class="fa fa-money-check-alt largeicon fa-4x"></i></div>
                    <div class="col-md-8">
                        <h3><?=__('COD')?></h3>
                        <span><?=$order->cod?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4 text-center"><i class="fa fa-adjust largeicon fa-4x"></i></div>
                    <div class="col-md-8">
                        <h3><?=__('Status')?></h3>
                        <span><?=HSTT($order->statues)?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"><h2><?=__('Receiver Details')?></h2>
                <div class="table-responsive">
                    <table class="table table-striped">

                        <tr>
                            <th scope="row"><?= __('Receiver Name') ?></th>
                            <td><?= h($order->receiver_name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Receiver Email') ?></th>
                            <td><?= h($order->receiver_email) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Receiver Phone') ?></th>
                            <td><?= h($order->receiver_phone) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('City') ?></th>
                            <td><?= h($order->city) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Statues') ?></th>
                            <td><?= h($order->statues) ?></td>
                        </tr>

                        <tr>
                            <th scope="row"><?= __('Reference') ?></th>
                            <td><?= h($order->reference) ?></td>
                        </tr>

                    </table>
                </div>
                <?php $this->Hooks->do_action('view_order_after_receiver_details', $order); ?>
            </div>
            <div class="col-md-6"><h2><?=__('Shipment Details')?></h2>
                <div class="table-responsive">
                    <table class="table table-striped">


                        <tr>
                            <th scope="row"><?= __('Cod') ?></th>
                            <td><?= $this->Number->format($order->cod) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Fees') ?></th>
                            <td><?= $this->Number->format($order->fees) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Driver Id') ?></th>
                            <td><?= $this->Number->format($order->driver_id) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Created') ?></th>
                            <td><?= h($order->created) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Modified') ?></th>
                            <td><?= h($order->modified) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Work Address') ?></th>
                            <td><?= $order->work_address ? __('Yes') : __('No'); ?></td>
                        </tr>

                    </table>
                </div>
                <?php $this->Hooks->do_action('view_order_after_shipment_details', $order); ?>

            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <h2><?=__('Other Info')?></h2>
                <div class="row">
                    <h4><?= __('Receiver Address') ?> : </h4>
                    <h4> <?= $order->receiver_address?></h4>
                </div>
                <div class="row">
                    <h4><?= __('Package Description') ?> : </h4>
                    <h4> <?= $order->package_description; ?> </h4>
                </div>
                <div class="row">
                    <h4><?= __('Notes') ?> : </h4>
                    <h4> <?= $order->notes; ?> </h4>
                </div>
                <?php $this->Hooks->do_action('view_order_after_other_info', $order); ?>

            </div>
            <div class="col-md-6">
                <h2><?=__('Driver Info')?></h2>
                <div class="related">
                    <?php if (!empty($order->drivers)): ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th scope="col"><?= __('Name') ?></th>
                                    <th scope="col"><?= __('Phone') ?></th>
                                </tr>
                                <?php foreach ($order->drivers as $drivers): ?>
                                    <tr>
                                        <td><?= h($drivers->name) ?></td>
                                        <td><?= h($drivers->phone) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    <?php endif; ?>
                </div>
                <?php $this->Hooks->do_action('view_Order_Driver_Info', $order); ?>

            </div>
        </div>
    </div>


    <hr / >
    <div class="row">
        <?php $this->Hooks->do_action('orders_view_under_details', $order); ?>
    </div>

    <hr />
    <div class="related">
        <?php if (!empty($order->transactions)): ?>
            <h4><?= __('Related Transactions') ?></h4>

            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th scope="col"><?= __('Type') ?></th>
                        <th scope="col"><?= __('Order Id') ?></th>
                        <th scope="col"><?= __('Pickup Id') ?></th>
                        <th scope="col"><?= __('Amount') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Created By') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($order->transactions as $transactions): ?>
                        <tr>
                            <td><?= h($transactions->type) ?></td>
                            <td><?= h($transactions->order_id) ?></td>
                            <td><?= h($transactions->pickup_id) ?></td>
                            <td><?= h($transactions->amount) ?></td>
                            <td><?= h($transactions->created) ?></td>
                            <td><?= h($transactions->created_by) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Transactions', 'action' => 'view', $transactions->id], ['class' => 'btn btn-secondary']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>



</div>
