<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pickup[]|\Cake\Collection\CollectionInterface $pickups
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Pickup'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Locations'), ['controller' => 'Locations', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Location'), ['controller' => 'Locations', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Actions'), ['controller' => 'Actions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Action'), ['controller' => 'Actions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table id="example1" class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('location_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('pickup_date') ?></th>
        <th scope="col"><?= $this->Paginator->sort('pickup_time') ?></th>
        <th scope="col"><?= $this->Paginator->sort('contact_name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('contact_phone') ?></th>
        <th scope="col"><?= $this->Paginator->sort('contact_email') ?></th>
        <th scope="col"><?= $this->Paginator->sort('statues') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($pickups as $pickup) : ?>
        <tr>
            <td><?= $this->Number->format($pickup->id) ?></td>
            <td><?= $pickup->has('location') ? $this->Html->link($pickup->location->name, ['controller' => 'Locations', 'action' => 'view', $pickup->location->id]) : '' ?></td>
            <td><?= h($pickup->pickup_date) ?></td>
            <td><?= h($pickup->pickup_time) ?></td>
            <td><?= h($pickup->contact_name) ?></td>
            <td><?= h($pickup->contact_phone) ?></td>
            <td><?= h($pickup->contact_email) ?></td>
            <td><?= h($pickup->statues) ?></td>
            <td><?= h($pickup->created) ?></td>
            <td><?= h($pickup->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $pickup->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pickup->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pickup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pickup->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?=$this->element('backend/datatablejs')?>
