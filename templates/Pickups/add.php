<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pickup $pickup
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Location[]|\Cake\Collection\CollectionInterface $locations
 * @var \App\Model\Entity\Action[]|\Cake\Collection\CollectionInterface $actions
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 * @var \App\Model\Entity\Transaction[]|\Cake\Collection\CollectionInterface $transactions
 */
$templateHalf =[
    'inputContainer' => '<div class="form-group col-sm-6 {{type}}{{required}}">{{content}}</div>',
];

?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Pickups'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Locations'), ['controller' => 'Locations', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Location'), ['controller' => 'Locations', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Actions'), ['controller' => 'Actions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Action'), ['controller' => 'Actions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="pickups form content">
    <?= $this->Form->create($pickup) ?>
    <fieldset>
        <legend><?= __('Add Pickup') ?></legend>
        <?php
        if($_SESSION['Auth']['User']['group_id'] == 1) {
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'select','value' => $_SESSION['Auth']['User']['id']]);
        }else{
            echo $this->Form->control('user_id', ['options' => $users,'type'=>'hidden','value' => $_SESSION['Auth']['User']['id']]);

        }
            echo "<div class='row'>";
            echo $this->Form->control('location_id', ['options' => $locations,'empty'=>'Select','templates'=>$templateHalf]);
            echo $this->Form->control('pickup_date');
        echo $this->Form->control('pickup_time',['options'=> [
            '10:00AM to 01:00PM'=>'10:00AM to 01:00PM',
            '01:00PM to 04:00PM'=>'01:00PM to 04:00PM',
        ] ]);
            echo "</div>";
            echo "<div class='row'>";
            echo $this->Form->control('contact_name' ,['templates' =>$templateHalf]);
            echo $this->Form->control('contact_phone',['templates' =>$templateHalf]);
            echo "</div>";
            echo $this->Form->control('contact_email');
            echo $this->Form->control('description');
//            echo $this->Form->control('statues');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
    $(function () {
        $('#location-id').change(function(){
            $id= $(this).val();
            console.log($id);
            GetAjaxDataAndFillItToForm("<?=ROOT_URL?>/pickups/locations/"+$id , {
                contact_name:"contact-name",
                contact_phone:"contact-phone",
                contact_email:"contact-email",
            });
        })
    });
<?php $this->Html->scriptEnd(); ?>
