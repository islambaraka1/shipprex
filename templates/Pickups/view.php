<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pickup $pickup
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Pickup'), ['action' => 'edit', $pickup->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Pickup'), ['action' => 'delete', $pickup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pickup->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Pickups'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Pickup'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Locations'), ['controller' => 'Locations', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Location'), ['controller' => 'Locations', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Actions'), ['controller' => 'Actions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Action'), ['controller' => 'Actions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="pickups view large-9 medium-8 columns content">
    <h3><?= h($pickup->id) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $pickup->has('user') ? $this->Html->link($pickup->user->id, ['controller' => 'Users', 'action' => 'view', $pickup->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Location') ?></th>
                <td><?= $pickup->has('location') ? $this->Html->link($pickup->location->name, ['controller' => 'Locations', 'action' => 'view', $pickup->location->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Pickup Time') ?></th>
                <td><?= h($pickup->pickup_time) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Name') ?></th>
                <td><?= h($pickup->contact_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Phone') ?></th>
                <td><?= h($pickup->contact_phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Email') ?></th>
                <td><?= h($pickup->contact_email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Statues') ?></th>
                <td><?= h($pickup->statues) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($pickup->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Pickup Date') ?></th>
                <td><?= h($pickup->pickup_date) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($pickup->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($pickup->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($pickup->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Actions') ?></h4>
        <?php if (!empty($pickup->actions)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Pickup Id') ?></th>
                    <th scope="col"><?= __('Order Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($pickup->actions as $actions): ?>
                <tr>
                    <td><?= h($actions->id) ?></td>
                    <td><?= h($actions->name) ?></td>
                    <td><?= h($actions->description) ?></td>
                    <td><?= h($actions->pickup_id) ?></td>
                    <td><?= h($actions->order_id) ?></td>
                    <td><?= h($actions->created) ?></td>
                    <td><?= h($actions->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Actions', 'action' => 'view', $actions->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Actions', 'action' => 'edit', $actions->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Actions', 'action' => 'delete', $actions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actions->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($pickup->orders)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Pickup Id') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
                    <th scope="col"><?= __('Receiver Name') ?></th>
                    <th scope="col"><?= __('Receiver Email') ?></th>
                    <th scope="col"><?= __('Receiver Phone') ?></th>
                    <th scope="col"><?= __('Receiver Address') ?></th>
                    <th scope="col"><?= __('City') ?></th>
                    <th scope="col"><?= __('Work Address') ?></th>
                    <th scope="col"><?= __('Package Description') ?></th>
                    <th scope="col"><?= __('Cod') ?></th>
                    <th scope="col"><?= __('Notes') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Fees') ?></th>
                    <th scope="col"><?= __('Driver Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($pickup->orders as $orders): ?>
                <tr>
                    <td><?= h($orders->id) ?></td>
                    <td><?= h($orders->user_id) ?></td>
                    <td><?= h($orders->pickup_id) ?></td>
                    <td><?= h($orders->type) ?></td>
                    <td><?= h($orders->reference) ?></td>
                    <td><?= h($orders->receiver_name) ?></td>
                    <td><?= h($orders->receiver_email) ?></td>
                    <td><?= h($orders->receiver_phone) ?></td>
                    <td><?= h($orders->receiver_address) ?></td>
                    <td><?= h($orders->city) ?></td>
                    <td><?= h($orders->work_address) ?></td>
                    <td><?= h($orders->package_description) ?></td>
                    <td><?= h($orders->cod) ?></td>
                    <td><?= h($orders->notes) ?></td>
                    <td><?= h($orders->statues) ?></td>
                    <td><?= h($orders->fees) ?></td>
                    <td><?= h($orders->driver_id) ?></td>
                    <td><?= h($orders->created) ?></td>
                    <td><?= h($orders->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Transactions') ?></h4>
        <?php if (!empty($pickup->transactions)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Order Id') ?></th>
                    <th scope="col"><?= __('Pickup Id') ?></th>
                    <th scope="col"><?= __('Amount') ?></th>
                    <th scope="col"><?= __('Details') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col"><?= __('Created By') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($pickup->transactions as $transactions): ?>
                <tr>
                    <td><?= h($transactions->id) ?></td>
                    <td><?= h($transactions->type) ?></td>
                    <td><?= h($transactions->user_id) ?></td>
                    <td><?= h($transactions->order_id) ?></td>
                    <td><?= h($transactions->pickup_id) ?></td>
                    <td><?= h($transactions->amount) ?></td>
                    <td><?= h($transactions->details) ?></td>
                    <td><?= h($transactions->created) ?></td>
                    <td><?= h($transactions->modified) ?></td>
                    <td><?= h($transactions->created_by) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Transactions', 'action' => 'view', $transactions->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Transactions', 'action' => 'edit', $transactions->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Transactions', 'action' => 'delete', $transactions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transactions->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
