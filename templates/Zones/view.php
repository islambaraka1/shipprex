<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Zone $zone
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Zone'), ['action' => 'edit', $zone->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Zone'), ['action' => 'delete', $zone->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zone->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Zones'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Zone'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Parent Zones'), ['controller' => 'Zones', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Parent Zone'), ['controller' => 'Zones', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Child Zones'), ['controller' => 'Zones', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Child Zone'), ['controller' => 'Zones', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="zones view large-9 medium-8 columns content">
    <h3><?= h($zone->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($zone->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Parent Zone') ?></th>
                <td><?= $zone->has('parent_zone') ? $this->Html->link($zone->parent_zone->name, ['controller' => 'Zones', 'action' => 'view', $zone->parent_zone->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($zone->id) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Default Price') ?></th>
                <td><?= $this->Number->format($zone->default_price) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($zone->users)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Username') ?></th>
                    <th scope="col"><?= __('Email') ?></th>
                    <th scope="col"><?= __('Full Name') ?></th>

                </tr>
                <?php foreach ($zone->users as $users): ?>
                <tr>
                    <td><?= h($users->id) ?></td>
                    <td><?= h($users->username) ?></td>
                    <td><?= h($users->email) ?></td>
                    <td><?= h($users->full_name) ?></td>

                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Zones') ?></h4>
        <?php if (!empty($zone->child_zones)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Lft') ?></th>
                    <th scope="col"><?= __('Rght') ?></th>
                    <th scope="col"><?= __('Default Price') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($zone->child_zones as $childZones): ?>
                <tr>
                    <td><?= h($childZones->id) ?></td>
                    <td><?= h($childZones->name) ?></td>
                    <td><?= h($childZones->parent_id) ?></td>
                    <td><?= h($childZones->lft) ?></td>
                    <td><?= h($childZones->rght) ?></td>
                    <td><?= h($childZones->default_price) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Zones', 'action' => 'view', $childZones->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Zones', 'action' => 'edit', $childZones->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Zones', 'action' => 'delete', $childZones->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childZones->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
