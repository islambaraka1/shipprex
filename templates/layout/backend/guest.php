<?php $Hooks = \App\Hooks\Hooks::getInstance(); ?>

<?=$this->element('backend/header')?>
<body class="hold-transition  layout-fixed">
<div class="wrapper">






        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <h2 class="FalshMsg"><?= $this->Flash->render() ?></h2>

                <?= $this->fetch('content'); ?>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <?=$this->element('backend/footer')?>
