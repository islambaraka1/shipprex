<?php //pr($invoice); ?>
<table width="100%">
    <tr>
        <td><h2><?= __("Brand"); ?>  : <b><?=$invoice->user->username?></b></h2>
        </td>
        <td align="right"><h2><?= __("Date"); ?> : <b><?=$invoice['cleared_at']?></b></h2>
        </td>
    </tr>
</table>

<table width="100%">
    <thead>
    <?php echo show_if_option_equels('order_serial_in_print',1,"<td>#</td>"); ?>
    <td><?= __("#"); ?></td>
    <td><?= __("ref"); ?></td>
    <td><?= __("Product"); ?></td>
    <td><?= __("Receiver Name"); ?></td>
    <td><?= __("Address"); ?></td>
    <td><?= __("Phone"); ?></td>
    <td><?= __("Note"); ?></td>
    <td><?= __("COD"); ?></td>
    <td><?= __("Fees"); ?></td>
    </thead>

    <tbody>
    <?php
    $count = 1;
    foreach ($invoice->orders as $order){
        ?>
        <tr>
            <?php echo show_if_option_equels('order_serial_in_print',1,"<td>$count</td>"); ?>
            <td><?=DID($order->id)?>
                <?php $this->Hooks->do_action('invoices_print_under_id',$order); ?>
            </td>
            <td><?=$order->reference?></td>
            <td><?=$order->package_description?></td>
            <td><?=$order->receiver_name?></td>
            <td><?=$order->receiver_address?></td>
            <td><?=$order->receiver_phone?></td>
            <td><?=$order->notes. $this->Misc->displayTags($order['tags']) ?></td>
            <td><?=$order->cod?></td>
            <td><?=$order->fees?></td>
        </tr>
        <?php
        $count++;
    }
    ?>
    </tbody>
</table>
<table id="items">
    <tr>

        <td><?= __("Invoice Id"); ?> :<?=$invoice->name?></td>
        <td><?= __("Date"); ?>: <b> <?=$invoice->cleared_at?> </b></td>
    </tr>
    <tr>
        <td>
            <?= __("Brand"); ?> : <?=$invoice->user->username?><br>


        </td>
        <td>
            <?=$invoice->user->username?><br />
            <?=$invoice->user->email?><br />
            <?=$invoice->user->phone?><br />
            <?=$invoice->user->addresses?><br />

        </td>
    </tr>
    <tr>
        <td align="right" colspan="" class="total-line balance"><p style="color:black;"><?= __("Total COD"); ?> </p></td>
        <td align="right" class="total-value balance"><div class="due"> <p style="color:black;"><b><?= SUSTEM_CURRENCY ?> <?=$invoice->total_amount?> </b></p></div></td>
    </tr>

    <tr>
        <td align="right" colspan="" class="total-line balance"><p style="color:black;"><?= __("Total Fees"); ?> </p></td>
        <td align="right" class="total-value balance"><div class="due"> <p style="color:black;"><b><?= SUSTEM_CURRENCY ?> <?=$invoice->total_fees?> </b></p></div></td>
    </tr>
    <tr>
        <td align="right" colspan="" class="total-line balance"><p style="color:black;"><?= __("Total Payout"); ?> </p></td>
        <td align="right" class="total-value balance"><div class="due"> <p style="color:black;"><b><?= SUSTEM_CURRENCY ?> <?=$invoice->total_payout?> </b></p></div></td>
    </tr>

</table>
