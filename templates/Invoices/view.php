<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invoice $invoice
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Invoice'), ['action' => 'edit', $invoice->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Invoice'), ['action' => 'delete', $invoice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoice->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Invoices'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Invoice'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="invoices view large-9 medium-8 columns content">
    <h3><?= h($invoice->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($invoice->name) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Total Payout') ?></th>
                <td><?= $this->Number->format($invoice->total_payout) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Total Fees') ?></th>
                <td><?= $this->Number->format($invoice->total_fees) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Total Amount') ?></th>
                <td><?= $this->Number->format($invoice->total_amount) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Number Of Orders') ?></th>
                <td><?= $this->Number->format($invoice->number_of_orders) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($invoice->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($invoice->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Cleared At') ?></th>
                <td><?= h($invoice->cleared_at) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>

        <?php if (!empty($invoice->orders)): ?>
            <a  href="<?=ROOT_URL?>invoices/print/<?=$invoice->id?>"  target ='_blank' class="btn btn-primary align-self-md-end" ><?= __("Print Orders"); ?></a>
            <a  href="#"  onclick="exportTableToExcel('')"  class="btn btn-primary downloadExcelbtn align-self-md-end" ><?= __("export excel"); ?></a>

            <div class="table-responsive">
            <table id="invoiceList" class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
                    <th scope="col"><?= __('Receiver Name') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Receiver Phone') ?></th>
                    <th scope="col"><?= __('Receiver Address') ?></th>
                    <th scope="col"><?= __('City') ?></th>
                    <th scope="col"><?= __('Cod') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Notes') ?></th>
                    <th scope="col"><?= __('Fees') ?></th>
                    <?php echo render_meta_columns('Orders'); ?>

                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Last Edit') ?></th>
                </tr>
                <?php foreach ($invoice->orders as $orders): ?>
                <tr>
                    <td><?= DID($orders->id) ?></td>
                    <td><?= h($orders->type) ?></td>
                    <td><?= h($orders->reference) ?></td>
                    <td><?= h($orders->receiver_name) ?></td>
                    <td><?= HSTT($orders->statues) ?></td>
                    <td><?= h($orders->receiver_phone) ?></td>
                    <td><?= h($orders->receiver_address) ?></td>
                    <td><?= h($orders->city) ?></td>
                    <td><?= h($orders->cod) ?></td>
                    <td><?= h($orders->statues) ?></td>
                    <td><?= h($orders->notes) . $this->Misc->displayTags($orders['tags']) ?></td>
                    <td><?= h($orders->fees) ?></td>
                    <?php echo render_meta_fields('Orders',$orders); ?>

                    <td><?= h($orders->created) ?></td>
                    <td><?= h($orders->modified) ?></td>

                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
