<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invoice[]|\Cake\Collection\CollectionInterface $invoices
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Invoice'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'count' => $reports_data['total_invoices_count'],
        'title' => __('Invoices count '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Collected Orders : ' . $reports_data['total_invoices_orders_sum']),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'count' => $reports_data['total_invoices_amount_sum'],
        'title' => __('Total Trade'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Invoice Average Is :  ' . $reports_data['total_invoices_amount_avg']) ,
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'green',
        'count' => $reports_data['total_invoices_payout_sum'],
        'title' => __('Total Payouts '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Single Payout Average : ' . $reports_data['total_invoices_payout_avg']) ,
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'count' => $reports_data['total_invoices_fees_sum'],
        'title' => __('Total Fees '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Fees Average : ' . $reports_data['total_invoices_fees_avg']) ,
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'dark',
        'count' => $orders_active['total_orders_count'],
        'title' => __('Active Orders Count'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Total Active Orders Count  '),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'white',
        'count' => $orders_active['total_orders_amount_sum'],
        'title' => __('Active COD '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Order COD  Average : ') . $orders_active['total_orders_amount_avg'],
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'red',
        'count' => $next_invoice_total['cod'],
        'title' => __('Next Invoice COD '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Fees: ') . $next_invoice_total['fees'],
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'count' => $next_invoice_total['orders'],
        'title' => __('Next Invoice Orders '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Payoff: ') .  ($next_invoice_total['cod'] - $next_invoice_total['fees']),
    ]);

    ?>
</div>
<div class="row">
    <section class="col-lg-6 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanv',
            'height' => '400',
            'title' =>__('Invoices Since account creation  '),
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'bar',
            'cahrtData' => $timeline_data,
            'cahrtData' => $timeline_data,
            'scale' => SUSTEM_CURRENCY
        ]);

        ?>
    </section>
    <section class="col-lg-6 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanvtoo',
            'height' => '400',
            'title' => __('Orders Timeline   '),
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'bar',
            'cahrtData' => $orders_timeline,
            'cahrtData' => $orders_timeline,
            'scale' => SUSTEM_CURRENCY
        ]);

        ?>
    </section>
</div>
<table id="example1" class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= __('id') ?></th>
        <th scope="col"><?= __('code') ?></th>
        <th scope="col"><?= __('Open Date') ?></th>
        <th scope="col"><?= __('cleared_at') ?></th>
        <th scope="col"><?= __('total_payout') ?></th>
        <th scope="col"><?= __('total_fees') ?></th>
        <th scope="col"><?= __('total_amount') ?></th>
        <th scope="col"><?= __('number_of_orders') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php $count=1; foreach ($invoices as $invoice) : ?>
        <tr>
            <td><?= h($count) ?></td>
            <td><?= h($invoice->name) ?></td>
            <td><?= h($invoice->created) ?></td>
            <td><?= h($invoice->cleared_at) ?></td>
            <td><?= $this->Number->format($invoice->total_payout) ?></td>
            <td><?= $this->Number->format($invoice->total_fees) ?></td>
            <td><?= $this->Number->format($invoice->total_amount) ?></td>
            <td><?= $this->Number->format($invoice->number_of_orders) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $invoice->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
            </td>
        </tr>
        <?php $count++; endforeach; ?>
    </tbody>
</table>

<?=$this->element('backend/datatablejs')?>
