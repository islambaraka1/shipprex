importScripts('https://www.gstatic.com/firebasejs/8.2.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.4/firebase-messaging.js');

var config = {
  apiKey: "AIzaSyBsYDGIjVHAV7euIa2hj610xYto7bfD7Nc",

  authDomain: "shipprex-all.firebaseapp.com",

  projectId: "shipprex-all",

  storageBucket: "shipprex-all.appspot.com",

  messagingSenderId: "716915756262",

  appId: "1:716915756262:web:6abb7b5539247a79b37f27",

  measurementId: "G-D2X4HJLGV0"

  };
  firebase.initializeApp(config);
  
  // Retrieve Firebase Messaging object.
const messaging = firebase.messaging();


messaging.setBackgroundMessageHandler(function(payload) {
    
 var  title =payload.data.title;
  
 var options ={
        body: payload.data.body,
        icon: payload.data.icon,
        image: payload.data.image,
     data:{
            time: new Date(Date.now()).toString(),
            click_action: payload.data.click_action
        }
      
  };
 return self.registration.showNotification(title, options);

  
});


self.addEventListener('notificationclick', function(event) {

   var action_click=event.notification.data.click_action;
  event.notification.close();

  event.waitUntil(
    clients.openWindow(action_click)
  );
});
