/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
let modalId = $('#image-gallery');
//ajax codes for the server side code loading and actions
// load data into select box
function loadAjaxJsonIntoSelect($elementSelector,$url){
    // load the data into the select box
    $.ajax({
        url: $url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $elementSelector.empty();
            $.each(data, function (key, value) {
                $elementSelector.append('<option value="' + value + '">' + value + '</option>');
            });
        }
    })
}

$(document)
    .ready(function () {

        var tables = $(".Serialized");

        tables.each(function() {
            var table = $(this);
            var rows = table.find('tr');
            var columnIndex = 0;

            rows.each(function(index, element) {
                if(index == 0) { // this is the header row
                    $(element).prepend('<th>#</th>');
                } else {
                    $(element).prepend('<td>' + index + '</td>');
                }
            });
        });
        //auto insert the phone root field when the settings-intl-local-country is changed
        $('#settings-intl-local-country').on('change', function () {
            // alert('changed');
            $countryCode = $(this).val();
            //request the full country data from  https://restcountries.com/v3.1/name/{name}?fullText=true
            $.ajax({
                url: 'https://restcountries.com/v3.1/name/' + $countryCode + '?fullText=true',
            }).done(function (data) {
                console.log(data);
                //get the first currency key from data[0].currencies and set it to the settings-intl-local-currency field
                $currency = Object.keys(data[0].currencies)[0];
                console.log($currency);
                $('#settings-intl-local-country-code').val(data[0].cca2).attr('readonly', true);
                $('#settings-intl-local-phone-number-root').val(data[0].idd.root+data[0].idd.suffixes[0]).attr('readonly', true);
                $('#settings-intl-local-currency').val($currency).attr('readonly', true);
            });
        });

        $('.downloadExcelbtn').on('click', function () {
            exportTableToExcel('invoiceList' );
        });
        //create a function to export table by id to excel
        function exportTableToExcel(tableID, filename = ''){
            let downloadLink;
            let dataType = 'application/vnd.ms-excel';
            let tableSelect = document.getElementById(tableID);
            let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

            // Specify file name
            filename = filename?filename+'.xls':'excel_data.xls';

            // Create download link element
            downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if(navigator.msSaveOrOpenBlob){
                let blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                // Setting the file name
                downloadLink.download = filename;

                //triggering the function
                downloadLink.click();
            }
        }

        //add ajax request after removing the image from the gallery
        $('.remove-image').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            let $url = $this.attr('href');
            $.ajax({
                url: $url,
                type: 'GET',
                success: function (response) {
                    alert("image is removed");
                    $this.closest('.thumb').remove();
                }
            });
        });
        loadGallery(true, 'a.thumbnail');

        //This function disables buttons when needed
        function disableButtons(counter_max, counter_current) {
            $('#show-previous-image, #show-next-image')
                .show();
            if (counter_max === counter_current) {
                $('#show-next-image')
                    .hide();
            } else if (counter_current === 1) {
                $('#show-previous-image')
                    .hide();
            }
        }

        /**
         *
         * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
         * @param setClickAttr  Sets the attribute for the click handler.
         */

        function loadGallery(setIDs, setClickAttr) {
            let current_image,
                selector,
                counter = 0;

            $('#show-next-image, #show-previous-image')
                .click(function () {
                    if ($(this)
                        .attr('id') === 'show-previous-image') {
                        current_image--;
                    } else {
                        current_image++;
                    }

                    selector = $('[data-image-id="' + current_image + '"]');
                    updateGallery(selector);
                });

            function updateGallery(selector) {
                let $sel = selector;
                current_image = $sel.data('image-id');
                $('#image-gallery-title')
                    .text($sel.data('title'));
                $('#image-gallery-image')
                    .attr('src', $sel.data('image'));
                disableButtons(counter, $sel.data('image-id'));
            }

            if (setIDs == true) {
                $('[data-image-id]')
                    .each(function () {
                        counter++;
                        $(this)
                            .attr('data-image-id', counter);
                    });
            }
            $(setClickAttr)
                .on('click', function () {
                    updateGallery($(this));
                });
        }
    });

// build key actions
$(document)
    .keydown(function (e) {
        switch (e.which) {
            case 37: // left
                if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                    $('#show-previous-image')
                        .click();
                }
                break;

            case 39: // right
                if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                    $('#show-next-image')
                        .click();
                }
                break;

            default:
                return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });


// auto insert method for bulk action
function autoInsert($targetElement){
    // get the ids from the location ids =
    queryString = window.location.search;
    result = queryString.replace("?ids=", "");
    myArray = result.split(",");
    $value = [];
    console.log(myArray);
    myArray.forEach(function (row) {
        row = parseInt(row);
        console.log(row);
        $elementVal = $($targetElement).find('option:contains("'+row+'")').val();
        if($elementVal != undefined){
            // write it down
            $value.push($elementVal);
            $($targetElement).trigger({
                type: 'select2:select',
                params: {
                    data: {id:$elementVal}
                }
            });
        }
    })
    console.log($value);
    $($targetElement).val($value); // Select the option with a value of '1'

    $($targetElement).trigger('change');


}

(function ($) {
  'use strict'


    // baraka code
    $('.btn-group-fab').on('click', '.btn', function() {
        $('.btn-group-fab').toggleClass('active');
    });
    $('has-tooltip').tooltip();

  var $sidebar   = $('.control-sidebar')
  var $container = $('<div />', {
    class: 'p-3 control-sidebar-content'
  });


  //image uploader
    $.fn.fileinputBsVersion = "3.3.7"; // if not set, this will be auto-derived

// initialize plugin with defaults
//     $("#image-uploader-fancy").fileinput();

    function image_uploader() {
        console.log('image uploader');
        var $el1 = $("#input-705");

        $el1.fileinput('destroy').fileinput({
            allowedFileExtensions: ['jpg', 'png', 'gif'],
            uploadUrl: BASE_URL+"comments/comments/upload/" + $($el1).data('model') + "/" + $($el1).data('folder'),
            uploadAsync: true,
            deleteUrl: "/site/file-delete",
            showUpload: false, // hide upload button
            overwriteInitial: false, // append files to initial preview
            minFileCount: 1,
            maxFileCount: 5,
            resizeImage: true,
            maxImageWidth: 200,
            maxImageHeight: 200,
            resizePreference: 'width',
            browseOnZoneClick: true,
            initialPreviewAsData: true,
        }).on("filebatchselected", function (event, files) {
            $el1.fileinput("upload");
        });
    }

// with plugin options
    image_uploader();


    // search function
  //   $('#searchTriggerInput').css('border',"5px solid red")
    $('#searchTriggerInput').keyup(function () {
        //start the search ajax
        var $keyword = $(this).val();
        if($keyword.length > 3){
            $.ajax({
                url: BASE_URL+"/search/search/all?keyword="+$keyword,
            }).done(function($data) {
                console.log(($data));
                // fill the data into the search form
                //fill orders
                $('#search-orders-list li').remove();
                $data.result.Orders.forEach(function (row) {
                    $('#search-orders-list').append('<li><a href="'+BASE_URL+'/orders/view/'+row.id+'">'+row.id+' -- '+ row.receiver_name +' </a></li>');
                })
                $('#search-invoices-list li').remove();
                $data.result.invoices.forEach(function (row) {
                    $('#search-invoices-list').append('<li><a href="'+BASE_URL+'/invoices/view/'+row.id+'">'+row.id+' -- '+ row.name +' </a></li>');
                })
                $('#search-users-list li').remove();
                $data.result.users.forEach(function (row) {
                    $('#search-users-list').append('<li><a href="'+BASE_URL+'/users-manager/users/view/'+row.id+'">'+row.id+' -- '+ row.full_name +' </a></li>');
                })
                $('#search-zone-list li').remove();
                $data.result.zones.forEach(function (row) {
                    $('#search-zone-list').append('<li><a href="'+BASE_URL+'/zones/view/'+row.id+'">'+row.id+' -- '+ row.name +' </a></li>');
                })



                $('.search-result').show();

            });
        }


    })


    $('.search-result > .close').click(function () {
        $('.search-result').hide();
    })
    // menu active issues
    var $current_location = window.location;
    $('.nav-sidebar li a').each(function(index,current){
        // console.log(current);
        if($(current).attr('href') == $current_location){
            $(current).addClass('active');
            $(current).parent().parent().parent().addClass('menu-open');
        }
    });
    // ajax datatable action
    function ajaxLinkWithInATable($link,$fieldToChangeByResult){
        // condition to check if link contains the word collected
        if($link.indexOf('Collected') > -1){
            //confirm the action
            // confirm("Are you sure you want to collect this order?");
            if(confirm('Are you sure you want to change the status of this order to collected?')){
                console.log('confirmed');
            } else {
                $('.currentChange').removeClass('currentChange');
                return false;
            }
        }
        $.ajax({
            url: $link,
        }).done(function($data) {
            console.log($data);
            var $returnText = $($data).text();
            console.log($returnText);
            $('.currentChange').find($fieldToChangeByResult).html($data).addClass('animate-flicker');
            //remove the row if it's a collected statues
            if($returnText == " Collected "){
                alert('This Order is now Marked as Collected it wont receive any other updates ');
                $('.currentChange').remove();
            }
            if( $data == "unassigned"){

                // alert('test');
                // alert('This Order is now Marked as Collected it wont receive any other updates ');
                $('.currentChange').remove();
            }
            $('.currentChange').removeClass('currentChange');

        });
    }
    /*
    * Announcments managmanet code
     */

    // ajax request to read announcment json file
    $.ajax({
        url: "https://shiprexnow.com/announcements.json",
        cache:true
    }).done(function($data) {
        handelAnnouncements($data);
    });
    // handelAnnouncements
    function handelAnnouncements($data){
        // console.log($data);
        add_announcment_to_sidebar($data);
        $data.forEach(function (row) {
            console.log(row);
            if(row.show_alert == true){
                $('#announcement-alert span.alert-description').html(row.description);
                $('#announcement-alert a.alert-button').attr('href',row.url);
                $('#announcement-alert').show();
            }
            // $('#announcements-list').append('<li><a href="'+row.url+'">'+row.name+'</a></li>');
        })
    }




    // export to csv JS code

    function download_table_as_csv(table_id, separator = ',') {
        // Select rows from table_id
        var rows = document.querySelectorAll('table#' + table_id + ' tr');
        // Construct csv
        var csv = [];
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll('td, th');
            for (var j = 0; j < cols.length; j++) {
                // Clean innertext to remove multiple spaces and jumpline (break csv)
                var data = cols[j].innerText.replace(/(\r\n|\n|\r)/gm, '').replace(/(\s\s)/gm, ' ')
                // Escape double-quote with double-double-quote (see https://stackoverflow.com/questions/17808511/properly-escape-a-double-quote-in-csv)
                data = data.replace(/"/g, '""');
                // Push escaped string
                row.push('"' + data + '"');
            }
            csv.push(row.join(separator));
        }
        var csv_string = csv.join('\n');
        console.log(csv_string);
        // Download it
        var filename = 'export_' + table_id + '_' + new Date().toLocaleDateString() + '.csv';
        var link = document.createElement('a');
        var csvString = 'ı,ü,ü,ğ,ş,#Hashtag,ä,ö';
        var universalBOM = "\uFEFF";
        link.style.display = 'none';
        link.setAttribute('target', '_blank');
        link.setAttribute('href', 'data:text/csv;charset=utf-8-sig,' + encodeURIComponent(universalBOM+csv_string));
        link.setAttribute('download', filename);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    $('.export-it').click(function (e){
        $(this).parents('div.card').find('table').attr('id','toBePrinted');
        download_table_as_csv('toBePrinted');
        setTimeout(function (){
            $('table#toBePrinted').removeAttr('id');
        },1000);
        e.preventDefault();
    })



// function to create an bootstrap ajax loaded modal with form actions and data
    function createModalLoadedWithAjaxRequest($url,$afterFormLoaded,$afterFormSubmit) {
        $.ajax({
            url: $url,
            type: 'GET',
            dataType: 'html',
        }).done(function ($data) {
            $('#myModal .modal-body').html($data);
            $('#myModal').modal('show');
            setTimeout($afterFormLoaded(),200);
            $('#myModal .modal-body form').submit(function (e) {
                e.preventDefault();
                var $form = $(this);
                var $formData = $form.serialize();
                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    data: $formData,
                    dataType: 'html',
                }).done(function ($data) {
                    $('#myModal .modal-body').html($data);
                    $afterFormSubmit();
                }).fail(function ($data) {
                    console.log($data);
                }).always(function ($data) {
                    console.log($data);
                });
            });
            $('#myModal').on('hide.bs.modal', function (e) {
                $('#myModal .modal-body').html('');
            });

        });
    }




    $('.ajax_form_open_modal').one('click' ,function (e) {
        e.preventDefault();
        var $url = $(this).attr('href');
        createModalLoadedWithAjaxRequest($url,  function (){
            $('#myModal .modal-body select').hide();
            $('#myModal .modal-body select.forceShow').show();
            setTimeout(function (){
                $("#input-705").fileinput('destroy');
                image_uploader();
            },1500);

        },function () {
            //update the row after form submit action
        });
    });





    // trigger
    $( document ).ajaxComplete(function( event, xhr, settings ) {
        $('a.ajaxinLink').off().on('click',function(){
            console.log($(this).parent().parent().parent().parent().addClass('currentChange'));
            ajaxLinkWithInATable($(this).attr('href'),".status-element");
            return false;
        });

        $('a.ajaxinLinkAuto').click(function(){
            console.log($(this).parents('tr').addClass('currentChange'));
            ajaxLinkWithInATable($(this).attr('href'),".status-element");
            return false;
        });

        $('.ajax_form_open_modal').off().on('click',function (e) {
            e.preventDefault();
            var $url = $(this).attr('href');
            createModalLoadedWithAjaxRequest($url,  function (){
                $('#myModal .modal-body select').hide();
                $('#myModal .modal-body select.forceShow').show();
                setTimeout(function (){
                    $("#input-705").fileinput('destroy');
                    image_uploader();
                },1500);

            },function () {
                //update the row after form submit action
            });
        });

    });



    $('a.ajaxinLink').click(function(){
        console.log($(this).parent().parent().parent().parent().addClass('currentChange'));
        ajaxLinkWithInATable($(this).attr('href'),".status-element");
        return false;
    });

    $('a.ajaxinLinkAuto').click(function(){
        console.log($(this).parents('tr').addClass('currentChange'));
        ajaxLinkWithInATable($(this).attr('href'),".status-element");
        return false;
    });

  $sidebar.append($container)

    function generate_announcment_html_out_of_announcment_json($data){
        var $html = '';
        $data.forEach(function (row) {
            $html += '<li class="announcement-card"><a href="'+row.url+'">'+row.name+'</a>';
            $html += '<p>'+row.description+'</p>';
            $html += '<a class="btn btn-main" href="'+row.url+'">View</a>';
            $html += '</li>';
        })
        return $html;
    }
    function add_announcment_to_sidebar($announcement){
        console.log($announcement);
        //count of the announcments
        var $count = $announcement.length;
        announce_badge_count($count);
        $announcement = generate_announcment_html_out_of_announcment_json($announcement);
        console.log($announcement);
        $container.append($announcement);
    }

    // function to get the new announcments only
    //function to change the announceBadge count to the count of the announcements
    function announce_badge_count($count){
      $('#announceBadge').text($count);
    }


})(jQuery)
