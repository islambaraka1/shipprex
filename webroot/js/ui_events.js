function showFlashMessage(message, type = 'success', duration = 3000) {
    const container = document.getElementById('flash-message-container');
    const flashMessage = document.createElement('div');
    flashMessage.classList.add('flash-message', `alert-${type}`);
    flashMessage.innerHTML = message;
    container.appendChild(flashMessage);
    setTimeout(() => {
        flashMessage.classList.add('show');
        setTimeout(() => {
            flashMessage.classList.remove('show');
            setTimeout(() => {
                container.removeChild(flashMessage);
            }, 500);
        }, duration);
    }, 100);
}


// partial delivery update in online view table
function handel_update_record(record) {
    //find the row in the table where order_id = record.id by jQuery
    console.log(record);
    $finder = parseInt(record.id) + 8000000;
    $tdElement = $('td').filter(function() {
        return $(this).text() == $finder;
    });
    $($tdElement).parents('tr').find('td:nth-child(8)').text(record.data.cod);
}
function handel_update_record_status(record) {
    //find the row in the table where order_id = record.id by jQuery
    console.log(record);
    $finder = parseInt(record.id) + 8000000;
    $tdElement = $('td').filter(function() {
        return $(this).text() == $finder;
    });
    $($tdElement).parents('tr').find('td:nth-child(11)').html(record.data.statues);
}
