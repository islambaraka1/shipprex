const evtSource = new EventSource(BASE_URL+"server_event.php");
// flash message event handler
evtSource.addEventListener("flash", (event) => {
    $data = JSON.parse(event.data);
    console.log($data);
    if ($data.type == 'warning'){
        toastr.warning($data.msg);
    }
    else if ($data.type == 'error'){
        toastr.error($data.msg);
    }
    else {
        toastr.success($data.msg);
    }
});

evtSource.addEventListener("update_record", (event) => {
    //this method runs after the drivers are marking their orders as partial delivery
    handel_update_record(JSON.parse(event.data));
});

evtSource.addEventListener("handel_update_record_status", (event) => {
    //this method runs after the drivers are marking their orders as partial delivery
    handel_update_record_status(JSON.parse(event.data));
});
evtSource.addEventListener("session_message", (event) => {
    console.log(event.data);
});
