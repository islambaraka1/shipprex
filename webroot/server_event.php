<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
session_start();

function sendMsg($id,$event, $msg) {
    echo "id: $id" . PHP_EOL;
    echo "event: $event" . PHP_EOL;
    echo "data:".$msg . PHP_EOL;
    echo PHP_EOL;
    ob_flush();
    flush();
}

$serverTime = time();

//flash render data
if(isset($_SESSION['FrontFlash'])){
    if(isset($_SESSION['flash_type']))
        sendMsg($serverTime,'flash',json_encode(['msg' => $_SESSION['FrontFlash'] , 'type' => $_SESSION['flash_type'] ] ));
    else
        sendMsg($serverTime,'flash',json_encode(['msg' => $_SESSION['FrontFlash']] ));
    unset($_SESSION['FrontFlash']);
    unset($_SESSION['flash_type']);
}

if (!empty($_SESSION['front_callback'])) {
    foreach ($_SESSION['front_callback'] as $event => $data) {
        sendMsg($serverTime,$event, json_encode($data));
    }
    unset($_SESSION['front_callback']);
}



?>
