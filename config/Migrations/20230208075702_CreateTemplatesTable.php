<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateTemplatesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {


        if (!$this->table('templates')->exists()) {
            $table = $this->table('templates');
            $table->addColumn('id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]);
            $table->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
            $table->addColumn('slug', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
            $table->addColumn('type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
            $table->addColumn('content', 'text', [
                'default' => null,
                'null' => false,
            ]);
            $table->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ]);
            $table->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ]);
            $table->create();
            $this->table('templates')
                ->insert([
                    'id' => 1,
                    'name' => 'الاوردر قيد التوصيل',
                    'slug' => 'alawrdr-qyd-altwsyl',
                    'type' => 'WHATSAPP',
                    'content' => 'السادة : {receiver_name} \r\nرجاء العلم ان الاوردر الخاص بكم قيد التوصيل وهو الان مع المندوب {driver} \r\nويمكنكم التواصل مع المندوب على الرقم : {driver.phone} \r\nشركة : {company}',
                    'created' => '2022-12-24 00:32:28',
                    'modified' => '2022-12-24 00:32:28'
                ])
                ->saveData();
        }
    }
}
