<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateVersion190fixes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        //cretae zones_drivers table
        /*
         * created datetime
            driver_id int
            id int
            modified datetime
            price int
            zone_id int
         */
        $tableExists = $this->hasTable('zones_drivers');
        if (!$tableExists) {
            $table = $this->table('zones_drivers');
            $table->addColumn('driver_id', 'integer')
                ->addColumn('zone_id', 'integer')
                ->addColumn('price', 'integer')
                ->addColumn('created', 'datetime')
                ->addColumn('modified', 'datetime')
                ->create();
        }
        //add zones alterative_names column as text
        $tableExists = $this->hasTable('zones');
        if ($tableExists) {
            $table = $this->table('zones');
            $table->addColumn('alternative_names', 'text', [
                'after' => 'name',
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
                ->update();
        }

        // add country column to orders table and patch_number column both as string and nullable
        $tableExists = $this->hasTable('orders');
        if ($tableExists) {
            $table = $this->table('orders');
            //check if country column exists
            $columnExists = $table->hasColumn('country');
            if (!$columnExists) {
                $table->addColumn('country', 'string', [
                    'after' => 'notes',
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ])
                    ->update();
            }

        }


    }
}
