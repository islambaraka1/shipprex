<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Version185 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        if(!$this->table('tags')->exists()) {
            $table = $this->table('tags');
            $table->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
            $table->addColumn('slug', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
            $table->addColumn('parent_status', 'string', [
                'default' => null,
                'null' => true,
            ]);
            $table->addColumn('automation_handler', 'string', [
                'default' => null,
                'null' => true,
            ]);

            $table->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ]);
            $table->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ]);
            $table->create();
            if (!$this->table('tags_orders')->exists()) {
                // schema for cross table tags_orders
                $table = $this->table('tags_orders');
                $table->addColumn('tag_id', 'integer', [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]);
                $table->addColumn('order_id', 'integer', [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]);
                $table->addColumn('created', 'datetime', [
                    'default' => null,
                    'null' => false,
                ]);
                $table->addColumn('modified', 'datetime', [
                    'default' => null,
                    'null' => false,
                ]);
                $table->create();
            }
        }
    }
}
