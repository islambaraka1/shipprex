<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddSortingToMenuitems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('menuitems');
        if (!$table->hasColumn('sorting')) {
            $table->addColumn('sorting', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])->addIndex(['sorting'])->update();
        }
    }

    public function rollback()
    {
        $table = $this->table('menuitems');
        $table->removeColumn('sorting')->update();
    }

}
