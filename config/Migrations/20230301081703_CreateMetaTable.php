<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateMetaTable extends AbstractMigration
{
    public function up()
    {
        if($this->table('meta')->exists()) {
            return;
        }
        $this->table('meta')
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('value', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('model_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created_at', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified_at', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addIndex(['model', 'name', 'model_key'], [
                'name' => 'META_INDEX',
                'unique' => false,
            ])
            ->create();
        //check if zones has alterntive names
        $table = $this->table('zones');
        if($table->hasColumn('alternative_names')) {
            return;
        }
        $table->addColumn('alternative_names', 'text', [
            'after' => 'name',
            'default' => null,
            'limit' => null,
            'null' => true,
        ])
            ->update();
    }

    public function down()
    {
        $this->table('meta')->drop()->save();
    }
}
