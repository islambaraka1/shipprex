<?php

use App\Event\ActionsListener;
use Cake\ORM\TableRegistry;
use App\Event\DriversListener;
use App\Event\InvoicesListener;
use Cake\Event\EventManager;


$aCallback = function ($event, $entity, $options) {
    $subject = $event->getSubject();
    $alias = $subject->getRegistryAlias();
    //if it's not a debugKit event then do the following
    if (
        strpos($alias, "DebugKit") === false &&
        strpos($alias, "Logger") === false
    ) {
        try {
            saveToLogger($event, $entity , $options);
        } catch (\Exception $e) {
            //do nothing
        }
    }
};

//function to save on logger table
function saveToLogger(Cake\Event\Event $event, $entity , $options) {
    $logger = TableRegistry::getTableLocator()->get('Logger');
    $request = \Cake\Routing\Router::getRequest();
    $log = $logger->newEmptyEntity();
    $log->model = $event->getSubject()->getRegistryAlias(); ;
    $log->current_url = $request->getUri();
    $user_id = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
    $log->user_id = $user_id;
    $log->action = $request->getParam('action');
    $log->controller = $request->getParam('controller');
    $log->ip = $_SERVER['REMOTE_ADDR'];
    $log->request_header = json_encode($_SERVER);
    $log->post_paramaters = json_encode($_POST);
    $log->entity_before_action = json_encode($entity);
    $log->created = date('Y-m-d H:i:s');
    $log->modified = date('Y-m-d H:i:s');
    try {
        $logger->save($log);
    } catch (\Exception $e) {

        //do nothing
    }
    unset($log);
    unset($logger);
    unset($request);
}


EventManager::instance()->on(
    'Model.afterSaveCommit',
    $aCallback
);

EventManager::instance()->on(
    'Model.beforeDelete',
    $aCallback
);

TableRegistry::getTableLocator()->get('DriversOrders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $driverListner = new DriversListener();
        $driverListner->afterSave($event,$entity);
    });

TableRegistry::getTableLocator()->get('Orders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $InvoicesListener = new InvoicesListener();
        $InvoicesListener->OrdersSaved($event,$entity);
    });

TableRegistry::getTableLocator()->get('UsersManager.Users')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $InvoicesListener = new InvoicesListener();
        $InvoicesListener->UserSaved($event,$entity);
    });

$actionLisner = new ActionsListener();
EventManager::instance()->on($actionLisner);

