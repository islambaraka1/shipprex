<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Features seed.
 */
class FeaturesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'name' => 'test',
                'plugin' => 'test',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.0.2',
                'created' => '2022-01-07 12:05:38',
                'modified' => '2022-01-07 12:19:06',
            ],
            [
                'id' => 2,
                'name' => 'Settings',
                'plugin' => 'Settings',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.0.2',
                'created' => '2022-01-07 12:17:02',
                'modified' => '2022-01-07 12:17:02',
            ],
            [
                'id' => 3,
                'name' => 'Search',
                'plugin' => 'Search',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.0.1',
                'created' => '2022-01-07 12:18:52',
                'modified' => '2022-01-07 12:18:52',
            ],
            [
                'id' => 4,
                'name' => 'Orders',
                'plugin' => 'Orders',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-12 21:21:19',
                'modified' => '2022-01-12 21:21:19',
            ],
            [
                'id' => 5,
                'name' => 'Accountant',
                'plugin' => 'Accountant',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-12 21:21:19',
                'modified' => '2022-01-12 21:21:19',
            ],
            [
                'id' => 6,
                'name' => 'Themes',
                'plugin' => 'Themes',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 7,
                'name' => 'Notifications',
                'plugin' => 'Notifications',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 8,
                'name' => 'Stocks',
                'plugin' => 'Stocks',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 9,
                'name' => 'Delegation',
                'plugin' => 'Delegation',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.5.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 10,
                'name' => 'Zones',
                'plugin' => 'Zones',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 11,
                'name' => 'Driver',
                'plugin' => 'Driver',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 12,
                'name' => 'Reports',
                'plugin' => 'Reports',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 13,
                'name' => 'Delegations',
                'plugin' => 'Delegations',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 14,
                'name' => 'Comments',
                'plugin' => 'Comments',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 15,
                'name' => 'Extras',
                'plugin' => 'Extras',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 16,
                'name' => 'MultiBranches',
                'plugin' => 'MultiBranches',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
            [
                'id' => 17,
                'name' => 'Companies',
                'plugin' => 'Companies',
                'active' => 1,
                'depend_on' => '',
                'version' => '1.6.0',
                'created' => '2022-01-23 22:50:40',
                'modified' => '2022-01-23 22:50:40',
            ],
        ];

        $table = $this->table('features');
        //empty the table first before seeding
        $table->truncate();
        $table->insert($data)->save();
    }
}
