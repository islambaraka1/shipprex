<?php
use Cake\Core\Configure;

$optionsTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Settings.Options');
//$field['field']['value']=$optionsTable->get_option_value($field['slug']);
$option_array = $optionsTable->find('all')->toArray();
Configure::write('cached_options', store_options_to_array($option_array));
// function to convert options array of object to array map of name => value
function store_options_to_array($options) {
    $options_array = [];
    foreach ($options as $option) {
        $options_array[$option->name] = $option->value;
    }
    return $options_array;
}


function get_value_from_array_where_key($key ){
    $options = Configure::read('cached_options');
//    var_dump($options);
//    die();
//    //find by array search
//    //foreach was performing 2.5 second in rendering time
//    foreach($options as $item){
//        if($item->name == $value){
//            return $item;
//        }
//    }
    return isset($options[$key]) ? $options[$key] : null;
}

/**
 * @param $settingPath
 * @param $settingArray
 * @return void "true if the setting added / false if not"
 * @author Islam Baraka
 * @version 1.0.1
 */
function set_new_setting($settingPath,$settingArray){
    $target = Configure::read($settingPath);
    $cachedSettingPath = $settingPath;
    if($target === null)
        return false;
    $target = array_merge($target,$settingArray);
    Configure::write($cachedSettingPath,$target);
    return true;
}

/**
 * this is a shortcut for getting any setting object/array
 * this is used for return the full object and not the value of the setting
 * @author Islam Baraka
 * @uses \Cake\Core\Configure
 * @version 1.0.1
 * @param $settingPath
 * @return array|bool[]|mixed
 */
function get_setting_array($settingPath){
    return Configure::read($settingPath);
}

function get_all_settings(){
    return Configure::read('Settings');
}


function easy_new_setting($path,$slug,$name,$type="text",$default = '' , $options = null,$help = '',$fixOptopn= true,$class = '',$id = ''){
    $data = [
        $slug  =>[
            'name'      => __($name),
            'slug'      => $slug,
            'default'   => $default,
            'fixOptions'=> $fixOptopn,
            'field'     =>[
                'type'      =>$type,
                'field_help'=>__($help),
                'options' => $options,
                'on_click'  =>null,
                'class'     =>$class,
                'id'        =>$id,
                'parent_tab'=>null
            ]
        ]
    ];
    set_new_setting($path,$data);
}

function load_setting_record_by_setting_object($field,$setting){
    $optionsTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Settings.Options');
    $field['field']['value']=$optionsTable->get_option_value($field['slug']);
    return $field;
}

function get_columns_names($model){
    $model  = \Cake\ORM\TableRegistry::getTableLocator()->get($model);
    $table = $model->getSchema();
    return $table->columns();
    ;
}

function get_option_value($option_name,$feature_id = null,$plugin = null ){
    $field = get_value_from_array_where_key($option_name);
    $option_value = $field;
    $option_value = $option_value != null ? $option_value : get_default_value($option_name);
    return $option_value;
    ;
}

function get_default_value($slug){
    $settings = get_all_settings();
    foreach ($settings as $feature => $setting){
        if(!empty($setting[$slug])){
            return  $setting[$slug]['default'];
        }
    }
    return false;
}
function excite_if_option_is_equal($option,$value,$callback,$arg=null){
    if(get_option_value($option) == $value){
        echo"islam";
        return $callback($arg);
    }
    return false;

}
function show_if_option_equels($option,$value,$string){
    if(get_option_value($option) == $value){
        return $string;
    }
    return '';
}
$targetModelValues = [];
function set_modelValues($option){
    global $targetModelValues;
    $targetModelValues = json_decode( get_option_value($option));
}
function checkbox_show_if_option_equels($option,$value,$string){
    global $targetModelValues;
    $fieldValue = $targetModelValues;
    foreach ($fieldValue as $singelValue){
        if($singelValue == $value)
            return $string;
    }
    return '';
}
