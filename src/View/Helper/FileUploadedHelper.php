<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * FileUploaded helper
 */
class FileUploadedHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    //function to retrieve the files saved by FileUpload Behavior
    public function getFile($entity, $field = "photo")
    {
        $dir = isset($entity->dir)? $entity->dir : '';
        $files = $dir.$entity->get($field);
        //remove webroot from the path and replace it by ROOT_URL
        $files = str_replace("webroot", ROOT_URL, $files);
        return $files;
    }

    public function getEditFileUpload($entity, $field = "photo")
    {
        //logic for edit upload input
    }

}
