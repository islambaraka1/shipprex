<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * TablesRander helper
 */
class TablesRenderHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    var $useDataTable;
    var $useDataTableSearch;
    var $useDataTableDateRange;
    var $useDataTableFilters;
    var $CakePagination;
    var $tableClasses;
    var $tableId;
    var $tableJs;
    var $tableCss;
    var $appendHtml;
    var $prependHtml;
    var $responsiveTable;
    var $displayValidation;
    var $displayValidationKey;
    var $fieldMap;
    var $currentValue;
    var $currentValueKey;
    var $targetKey;

    var $tableData;
    var $tableHeaders;
    // $this->getView()->get()
    // $this->getView()->element()

    public $helpers = ['Html'];


    /**
     * @return mixed
     */
    public function getUseDataTable()
    {
        return $this->useDataTable;
    }

    /**
     * @param mixed $useDataTable
     */
    public function setUseDataTable($useDataTable): void
    {
        $this->useDataTable = $useDataTable;
    }

    /**
     * @return mixed
     */
    public function getUseDataTableSearch()
    {
        return $this->useDataTableSearch;
    }

    /**
     * @param mixed $useDataTableSearch
     */
    public function setUseDataTableSearch($useDataTableSearch): void
    {
        $this->useDataTableSearch = $useDataTableSearch;
    }

    /**
     * @return mixed
     */
    public function getUseDataTableDateRange()
    {
        return $this->useDataTableDateRange;
    }

    /**
     * @param mixed $useDataTableDateRange
     */
    public function setUseDataTableDateRange($useDataTableDateRange): void
    {
        $this->useDataTableDateRange = $useDataTableDateRange;
    }

    /**
     * @return mixed
     */
    public function getUseDataTableFilters()
    {
        return $this->useDataTableFilters;
    }

    /**
     * @param mixed $useDataTableFilters
     */
    public function setUseDataTableFilters($useDataTableFilters): void
    {
        $this->useDataTableFilters = $useDataTableFilters;
    }

    /**
     * @return mixed
     */
    public function getCakePagination()
    {
        return $this->CakePagination;
    }

    /**
     * @param mixed $CakePagination
     */
    public function setCakePagination($CakePagination): void
    {
        $this->CakePagination = $CakePagination;
    }

    /**
     * @return mixed
     */
    public function getTableClasses()
    {
        return $this->tableClasses;
    }

    /**
     * @param mixed $tableClasses
     */
    public function setTableClasses($tableClasses): void
    {
        $this->tableClasses = $tableClasses;
    }

    /**
     * @return mixed
     */
    public function getTableId()
    {
        return $this->tableId;
    }

    /**
     * @param mixed $tableId
     */
    public function setTableId($tableId): void
    {
        $this->tableId = $tableId;
    }

    /**
     * @return mixed
     */
    public function getTableJs()
    {
        return $this->tableJs;
    }

    /**
     * @param mixed $tableJs
     */
    public function setTableJs($tableJs): void
    {
        $this->tableJs = $tableJs;
    }

    /**
     * @return mixed
     */
    public function getTableCss()
    {
        return $this->tableCss;
    }

    /**
     * @param mixed $tableCss
     */
    public function setTableCss($tableCss): void
    {
        $this->tableCss = $tableCss;
    }

    /**
     * @return mixed
     */
    public function getAppendHtml()
    {
        return $this->appendHtml;
    }

    /**
     * @param mixed $appendHtml
     */
    public function setAppendHtml($appendHtml): void
    {
        $this->appendHtml = $appendHtml;
    }

    /**
     * @return mixed
     */
    public function getPrependHtml()
    {
        return $this->prependHtml;
    }

    /**
     * @param mixed $prependHtml
     */
    public function setPrependHtml($prependHtml): void
    {
        $this->prependHtml = $prependHtml;
    }

    /**
     * @return mixed
     */
    public function getResponsiveTable()
    {
        return $this->responsiveTable;
    }

    /**
     * @param mixed $responsiveTable
     */
    public function setResponsiveTable($responsiveTable): void
    {
        $this->responsiveTable = $responsiveTable;
    }

    /**
     * @return mixed
     */
    public function getDisplayValidation()
    {
        return $this->displayValidation;
    }

    /**
     * @param mixed $displayValidation
     */
    public function setDisplayValidation($displayValidation): void
    {
        $this->displayValidation = $displayValidation;
    }

    /**
     * @return mixed
     */
    public function getDisplayValidationKey()
    {
        return $this->displayValidationKey;
    }

    /**
     * @param mixed $displayValidationKey
     */
    public function setDisplayValidationKey($displayValidationKey): void
    {
        $this->displayValidationKey = $displayValidationKey;
    }

    /**
     * @return mixed
     */
    public function getFieldMap()
    {
        return $this->fieldMap;
    }

    /**
     * @param mixed $fieldMap
     */
    public function setFieldMap($fieldMap): void
    {
        $this->fieldMap = $fieldMap;
    }

    function initialize(array $config): void
    {
        parent::initialize($config); // TODO: Change the autogenerated stub
        foreach ($config as $key => $value){
            $this->$key = $value;
        }
    }

    // function to convert underscored to camelCase with first letter capitalized
    function camelCase($string) {
        $string = str_replace(' ', ' ', ucwords(str_replace('_', '  ', $string)));
        return $string;
    }


    // function to extract headers table of array
    function extract_headers_table_from_array($array){
        $array = $array->toArray();
        $headers = [];
        foreach ($array as $key => $value) {
                $headers[] = $key;
        }
        $this->tableHeaders = $headers;
        return $headers;
    }





    //function to render the full table from array
    function renderTableFromArray($array){
        $headers = $this->extract_headers_table_from_array($array[0]);
        $html = "";
        $html .= "<table class='table table-striped table-bordered table-hover table-sm ".$this->tableClasses."' id='".$this->tableId."'>";
        $html .= "<thead>";
        $html .= "<tr>";
        $html .=  $this->RenderHaders($headers, $html);
        $html .= "</tr>";
        $html .= "</thead>";
        $html .= "<tbody>";
        foreach ($array as $key => $value) {
            $this->targetKey = $key;
            if(is_object($value)){
                $value = $value->toArray();
            }
            $this->tableData[] = $value;
            $html .= "<tr>";
            foreach ($value as $key2 => $value2) {
                if (!is_array($value2)) {
                    $this->currentValueKey = $key2;
                    $html .= $this->renderTableCell($value2);
                }
            }
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "</table>";
        return $html;
    }


    //function to render table cell value
    function renderTableCell($value){
        $this->currentValue = $value;
        $value = $this->renderTableCellValue($value);
        $html = "";
        $html .= $value;
        return $html;
    }

    function renderTableCellValue($value)
    {
        if (array_key_exists($this->currentValueKey, $this->fieldMap)) {
            if ($this->isHidden($this->currentValueKey)) {
                return "";
            }
            $value = $this->getValueByField($value);
            $value = $this->getValueByFunction($value);
            if(isset($this->fieldMap[$this->currentValueKey]['icon'])){
                $value = $this->getIcon($this->fieldMap[$this->currentValueKey]['icon'], $value);
            }
            $value = $this->getCombinedValues($value);
            $value = $this->appendValues($value);

        }
        return "<td>" . $value . "</td>";
    }





    // function to extract headers table of array
    function extract_headers_table_from_array_2($array){
        $headers = [];
        foreach ($array as $key => $value) {
            $headers[] = $key;
        }
        return $headers;
        }
    // function to render table from array






    /**
     * @param $functionName
     * @param $value
     * @return mixed
     */
    private function getValue($functionName, $value, $params = [])
    {
        if($functionName !== null && $functionName !== ""){
            if (method_exists($this, $functionName)) {
                $value = $this->$functionName($value, $params);
            } elseif (is_callable($functionName)) {
                $value = call_user_func_array($functionName, [$value, $params]);
            }
        }

        return $value;
    }


    /**
     * @param $value
     * @return mixed
     */
    private function getValueByFunction($value)
    {
        if (array_key_exists("functionName", $this->fieldMap[$this->currentValueKey])) {
            $functionName = $this->fieldMap[$this->currentValueKey]["functionName"];
            if ($functionName !== null && !is_array($functionName)) {
                $value = $this->getValue($functionName, $value);
            }
            if ($functionName != null && is_array($functionName)) {
                $value = $this->getValue($this->getFirstKey($functionName), $value,$functionName[$this->getFirstKey($functionName)]);
            }
        }
        return $value;
    }

    private function link($value, $params = [])
    {
        $value = $this->Html->link($value, $this->formatLinkParams($params['url'],['escape' => false]));
        return $value;
    }

    //function to format cakephp link params
    private function formatLinkParams($params){
        if (array_key_exists("parameter", $params )) {
            $fieldName = $params['parameter'];
            $arrayPointer = explode(".", $fieldName);
            $newValue = $this->tableData[$this->targetKey][$arrayPointer[0]][$arrayPointer[1]];
            $params[] = $newValue ;
        }
        return $params;
    }


    //function to return the first key of an array as string
    function getFirstKey($array){
        $keys = array_keys($array);
        return $keys[0];
    }




    /**
     * @param $value
     * @return mixed
     */
    private function getValueByField($value)
    {
        if (array_key_exists("fieldName", $this->fieldMap[$this->currentValueKey])) {
            $fieldName = $this->fieldMap[$this->currentValueKey]["fieldName"];
            $arrayPointer = explode(".", $fieldName);
            $newValue = $this->tableData[$this->targetKey][$arrayPointer[0]][$arrayPointer[1]];
            $value = $newValue;
        }
        return $value;
    }
    /**
     * @param array $headers
     * @param string $html
     * @return string
     */
    private function RenderHaders(array $headers, string $html): string
    {
        foreach ($headers as $key => $value) {
            $html .= $this->renderHeaderField($value);
        }
        return $html;
    }


    //TODO display value as avatar or image
    // TODO display value as link [DONE]
    // TODO display value as link with tooltip
    // TODO display value as link with tooltip and icon
    // TODO display value as link with tooltip and icon and color
    // TODO display multiple values as bootstrap card
    // TODO combine multiple values as bootstrap card
    // TODO combine multiple values as one single value
    /**
     * @param $value
     * @return string
     */
    private function renderHeaderField($value): string
    {
        if(array_key_exists($value, $this->fieldMap)){
            if ($this->isHidden($value)) {
                return "";
            }
            if ($this->isRenamed($value)) {
                $value = $this->getRenamedValue($value);
            }
        }
        return "<th>" . $this->camelCase($value) . "</th>";
    }
    //function to check if field is set to hidden
    private function isHidden($fieldName){
        if (array_key_exists("hidden", $this->fieldMap[$fieldName])) {
            return $this->fieldMap[$fieldName]["hidden"];
        }
        return false;
    }

    //function to check if field is set to renamed
    private function isRenamed($fieldName){
        if (array_key_exists("rename", $this->fieldMap[$fieldName])) {
            return true;
        }
        return false;
    }

    //function to get renamed value
    private function getRenamedValue($fieldName){
        if (array_key_exists("rename", $this->fieldMap[$fieldName])) {
            return $this->fieldMap[$fieldName]["rename"];
        }
        return $fieldName;
    }

    // function to return combined values
    private function getCombinedValues($value, $saparator = " - "){
        if (array_key_exists("combine", $this->fieldMap[$this->currentValueKey])) {
            $combine = $this->fieldMap[$this->currentValueKey]["combine"];
            $combineValue = "";
            foreach ($combine as $key => $value) {
                if (is_array($value)){
                    if ($key == $this->lastKey($combine)) {
                        $combineValue .= $this->proccessFieldArray($value,$key);
                    } else {
                        $combineValue .= $this->proccessFieldArray($value,$key) . $saparator;
                    }
                } else{
                    if ($key == $this->lastKey($combine)) {
                        $combineValue .= $this->tableData[$this->targetKey][$value];
                    } else {
                        $combineValue .=  $this->tableData[$this->targetKey][$value] . $saparator;
                    }
                }
            }
            $value = $combineValue;
        }
        return $value;
    }

    //function to append values to main value
    private function appendValues($value, $saparator = " - "){
        if (array_key_exists("append", $this->fieldMap[$this->currentValueKey])) {
            $append = $this->fieldMap[$this->currentValueKey]["append"];
            $appendValue = "";
            $appendValue .= $value . $saparator;
            foreach ($append as $key => $value) {
                if (is_array($value)){
                    if ($key == $this->lastKey($append)) {
                        $appendValue .= $this->proccessFieldArray($value,$key);
                    } else {
                        $appendValue .= $this->proccessFieldArray($value,$key) . $saparator;
                    }
                } else{
                    if ($key == $this->lastKey($append)) {
                        $appendValue .= $this->tableData[$this->targetKey][$value];
                    } else {
                        $appendValue .=  $this->tableData[$this->targetKey][$value] . $saparator;
                    }
                }

            }
            $value = $appendValue;
        }
        return $value;
    }

    // function to return last key of an array
    private function lastKey($array){
        $keys = array_keys($array);
        return $keys[count($keys) - 1];
    }
    //function to proccess field array passed from array map
    private function proccessFieldArray($array, $key){
        $value = $this->tableData[$this->targetKey][$key];
        if(isset($array['functionName'])){
            $value = $this->getValue($array['functionName'], $value);
            return $value;
        }
        if(isset($array['icon'])){
            $value = $this->getIcon($array['icon'], $value);
            return $value;
        }
        return "";
    }
    //function to get icon prefixed to the value
    private function getIcon($icon, $value){
        return " <i class='fa fa-" . $icon . "'></i> " . $value;
    }
    //function to style the combined values
    private function styleCombinedValues($value, $saparator = " - "){
        if (array_key_exists("combine_style", $this->fieldMap[$this->currentValueKey])) {
            $style = $this->fieldMap[$this->currentValueKey]["style"];
            $value = "<span class='" . $style . "'>" . $value . "</span>";
        }
        return $value;
    }

    // function to generate png avatar from two letters
    private function getAvatar($value){
        // convert value to string
        $value = (string)$value;
        $avatar = "";
        if (strlen($value) > 1) {
            $avatar = substr($value, 0, 2);
        } else {
            $avatar = $value;
        }
        return "<img src='https://www.gravatar.com/avatar/" . md5(strtolower(trim($avatar))) . "?s=32&d=identicon&r=PG' alt='" . $avatar . "'>";
    }












}
