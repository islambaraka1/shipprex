<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\Core\App;
use Cake\Core\Exception\Exception;
use Cake\I18n\I18n;
use Cake\View\Helper;
use Cake\View\View;

/**
 * TablesRander helper
 */
class NumberHelper extends Helper\NumberHelper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(View $View, array $config = [])
    {
//        parent::__construct($View, $config);
        $config = $this->_config;

        $engineClass = App::className("Cake\I18n\Number", 'Utility');
        if ($engineClass === null) {
            throw new Exception(sprintf('Class for %s could not be found', $config['engine']));
        }

        $this->_engine = new $engineClass($config);
    }

    /**
     * Formats a number into the correct locale format
     *
     * Options:
     *
     * - `places` - Minimum number or decimals to use, e.g 0
     * - `precision` - Maximum Number of decimal places to use, e.g. 2
     * - `pattern` - An ICU number pattern to use for formatting the number. e.g #,##0.00
     * - `locale` - The locale name to use for formatting the number, e.g. fr_FR
     * - `before` - The string to place before whole numbers, e.g. '['
     * - `after` - The string to place after decimal numbers, e.g. ']'
     *
     * @param float|string $value A floating point number.
     * @param array $options An array with options.
     * @return string Formatted number
     */
    public function format($value, array $options = []): string
    {
        if(get_option_value('datetime_field_escape_localizations') == 1) {
            $options['locale'] = 'en_US';
        }else{
            $options['locale'] = I18n::getLocale();
        }
        $formatter = parent::formatter($options);
        $options += ['before' => '', 'after' => ''];

        return $options['before'] . $formatter->format((int)$value) . $options['after'];
    }



}
