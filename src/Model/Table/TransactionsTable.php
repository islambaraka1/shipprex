<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Transactions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\PickupsTable&\Cake\ORM\Association\BelongsTo $Pickups
 *
 * @method \App\Model\Entity\Transaction newEmptyEntity()
 * @method \App\Model\Entity\Transaction newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Transaction findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
        ]);
        $this->belongsTo('Pickups', [
            'foreignKey' => 'pickup_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');



        $validator
            ->scalar('details')
            ->requirePresence('details', 'create')
            ->notEmptyString('details');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['pickup_id'], 'Pickups'));

        return $rules;
    }






    public function HandelOrder($order){
        $total = $order->type == "Refund" ? 0 : $order->cod;
        $fees = $order->fees;
        $in = $this->newEmptyEntity();
        $out = $this->newEmptyEntity();
        $in->order_id = $order->id;
        $out->order_id = $order->id;
        $in->user_id = $order->user_id;
        $out->user_id = $order->user_id;
        $in->type = "in";
        $out->type = "out";
        $out->amount = $total;
        $in->amount = "-".$order->fees;
        $out->created_by = $_SESSION['Auth']['User']['id'];
        $in->created_by = $_SESSION['Auth']['User']['id'];
        $this->save($out);
        $this->save($in);
    }

    public function get_users_transactions($userId){

    }
    public function get_user_clearnces($userId){
        $data = $this->find('all') ->
        where(['user_id' => $userId , 'type' => 'in','details'=>'Clear Balance']);
        return $data;
    }
    public function get_user_transaction_for_this_clarence($userId){
        $clearences = $this->get_user_clearnces($userId)->toArray();
        $currentKey = 0;
        $nextKey = 1;
        $prev = 0;
        $allData = [];
        foreach ($clearences as $clear) {
            // loop through the Clears
            if($currentKey  >  0){
                //do the query
                $allData[$clear->id] = $this->find('all')->where(['type' => 'in','user_id' => $userId ,
                    'id <' =>$clear->id , 'id >' =>$clearences[$prev]->id  ,'details' => ''])->toArray() ;
                $currentKey++;
                $prev++;
            } else{
               $allData[$clear->id] = $this->find('all') -> where(['type' => 'in','user_id' => $userId , 'id <' =>$clear->id ,'details' => ''])->toArray() ;
               $currentKey++;
            }
        }
        return $allData;


    }
}
