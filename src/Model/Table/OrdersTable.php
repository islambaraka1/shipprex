<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PickupsTable&\Cake\ORM\Association\BelongsTo $Pickups
 * @property \App\Model\Table\DriversTable&\Cake\ORM\Association\BelongsTo $Drivers
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\DriverPickupTable&\Cake\ORM\Association\HasMany $DriverPickup
 * @property \App\Model\Table\TransactionsTable&\Cake\ORM\Association\HasMany $Transactions
 *
 * @method \App\Model\Entity\Order newEmptyEntity()
 * @method \App\Model\Entity\Order newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('OrderExtera.Meta');

        $this->belongsTo('UsersManager.Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Pickups', [
            'foreignKey' => 'pickup_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
        ]);
        $this->hasMany('Actions', [
            'foreignKey' => 'order_id',
            'sort' => [
                'Actions.id' => 'desc',
            ],
        ]);
        $this->hasMany('DriverPickup', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'order_id',
        ]);
        $this->belongsToMany('Drivers', [
            'foreignKey' => 'order_id',
            'targetForeignKey' => 'driver_id',
            'joinTable' => 'drivers_orders',
            'saveStrategy'  =>'append'
        ]);
//        $this->belongsToMany('DriversOrders', [
//            'foreignKey' => 'order_id',
//            'targetForeignKey' => 'driver_id',
//            'joinTable' => 'drivers_orders',
//            'saveStrategy'  =>'append'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['user_id'], 'Users'));
//        $rules->add($rules->existsIn(['pickup_id'], 'Pickups'));
//        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));

        return $rules;
    }

    public function beforeFind ($event, $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !count($order)) {
            $query->order( [$this->getAlias() . '.id' => 'DESC'] );
        }
    }

    public function beforeSave(EventInterface $event, EntityInterface $entity, \ArrayObject $options){
        $entity->city = str_replace('_' , '' ,$entity->city);
        if(!isset($entity->id)){
            // new order
            $zonesTable = TableRegistry::getTableLocator()->get('Zones');
            $zonesUsersTable = TableRegistry::getTableLocator()->get('ZonesUsers');
            $zoneData = $zonesTable->findByName($entity->city)->first();
            if(isset($zoneData->default_price)){
                $entity->fees =$zoneData->default_price;
                $crossData = $zonesUsersTable->findByUserIdAndZoneId($entity->user_id , $zoneData->id )->first();
                if(isset($crossData->id)){
                    $entity->fees = $crossData->price;
                }
            }
            else{
                //fix bug for default price for courier
                // this error apply to the ticket id SHIP-392
                // this issue show up specilly in the excel sheet upload where there is no selected city or city with name thats not fir our database
                $zoneData = $zonesTable->get(get_option_value('zones_default_zone'));
                if(isset($zoneData->default_price)){
                    $entity->fees =$zoneData->default_price;
                    if(get_option_value('zones_default_zone_name_show') ==1){
                        $entity->city =$zoneData->name;
                    }
                    $crossData = $zonesUsersTable->findByUserIdAndZoneId($entity->user_id , $zoneData->id )->first();
                    if(isset($crossData->id)){
                        $entity->fees = $crossData->price;
                    }
                }
            }
        }

    }




    function actionUpdates($id,$stat,$description = null){

        $row = $this->get($id);
        if($row->statues == "Collected")
            return false;
        $row->statues = $stat;
        $action = $this->Actions->newEmptyEntity();
        $action->name = $stat;
        $action->description = ($description == null)? "$stat" : $description;
        $row->actions = [$action];
        $this->save($row);
        $event = new Event('Model.UpdatedOrderState', $this, [
            'state' => $stat,
            'order' => $id
        ]);
        $this->getEventManager()->dispatch($event);
        // if State is Delivered
        if($stat =="Collected"){
            //Do Transactions For this Order
            $transactionsTable = TableRegistry::getTableLocator()->get('Transactions');
            $transactionsTable->HandelOrder($row);
        }
        return true;
    }
    // function getDriver by order id
    function getDriver($id){
        $order = $this->get($id);
        //load drivers_order model
        $driversOrdersTable = TableRegistry::getTableLocator()->get('DriversOrders');
        //find drivers for this order
        $drivers = $driversOrdersTable->find()->where(['order_id' => $id])->toArray();
        // if its empty return null
        if(count($drivers) == 0)
            return null;
        // if its many result return last one
        if(count($drivers) > 1)
            return $drivers[count($drivers)-1];
        // if its one result return it
        return $drivers[0];
    }

    // cakephp custom finder GetOrdersByDriver
    public function findGetOrdersByDriver(Query $query, array $options)
    {

        $query->contain(['Drivers'])->matching('Drivers', function ($q) {
            return $q->where(['Drivers.id' => 34]);
        });

        return $query;
    }





}
