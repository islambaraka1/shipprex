<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Collection\Collection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Drivers Model
 *
 * @property \App\Model\Table\DriverPickupTable&\Cake\ORM\Association\HasMany $DriverPickup
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
// * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsToMany $Orders
 *
 * @method \App\Model\Entity\Driver newEmptyEntity()
 * @method \App\Model\Entity\Driver newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Driver[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Driver get($primaryKey, $options = [])
 * @method \App\Model\Entity\Driver findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Driver patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Driver[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Driver|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Driver saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Driver[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Driver[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Driver[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Driver[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DriversTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('drivers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('DriverPickup', [
            'foreignKey' => 'driver_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'driver_id',
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'driver_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'drivers_orders',
            'saveStrategy'  =>'append',

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('otp')
            ->maxLength('otp', 50)
            ->requirePresence('otp', 'create')
            ->notEmptyString('otp');

        $validator
            ->scalar('license')
            ->maxLength('license', 50)
            ->requirePresence('license', 'create')
            ->notEmptyString('license');

        $validator
            ->scalar('vehicle_num')
            ->maxLength('vehicle_num', 50)
            ->requirePresence('vehicle_num', 'create')
            ->notEmptyString('vehicle_num');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 50)
            ->requirePresence('phone', 'create')
            ->notEmptyString('phone');

        $validator
            ->scalar('note')
            ->requirePresence('note', 'create')
            ->notEmptyString('note');

        return $validator;
    }

    function getDriverNextPaidAmountOrders($driverId , $status = 'Collected'){
        //get all previously paid orders
        $transesModel = TableRegistry::getTableLocator()->get('GeneralLedger.Transes');
        $DriversOrders = TableRegistry::getTableLocator()->get('DriversOrders');
        $ordersPaid = $transesModel->find('all')->where(['driver_id'=>$driverId ])->all();
        $names = $ordersPaid->extract('order_id');
        $result = $names->toList();
        $ordersPaidCount = count($ordersPaid);
        $driverCompletedOrders = $DriversOrders->find('all')
            ->contain('Orders')
            ->where(['DriversOrders.driver_id'=>$driverId])
            ->where(['Orders.statues'=>$status]);
        if(count($ordersPaid) > 0){
            $driverCompletedOrders->where(['Orders.id NOT IN'=>$result]);
        }
        $driverCompletedOrders =  $driverCompletedOrders->toArray();
        //append driver fees if multi zone module is enabled
        if (count($driverCompletedOrders) > 0)
            $driverCompletedOrders = $this->attach_driver_fees_field_for_order($driverCompletedOrders);

        return $driverCompletedOrders;
    }

    function attach_driver_fees_field_for_order($ordersList){
        //driver data and cost
        $driverId = $ordersList[0]->driver_id ;
        $driverTable = TableRegistry::getTableLocator()->get('Drivers');
        $driver = $driverTable->get($driverId);
        //load Driver zone fees
        $driverZones = TableRegistry::getTableLocator()->get('ZonesDrivers');
        $price_list = $driverZones->get_driver_price_list($driverId);
        //not taking performance in consideration
        foreach ($ordersList as $key => $trans){
            if(isset( $price_list[$trans['order']['city']] )){
                $ordersList[$key]['order']['driver_fees'] = $price_list[$trans['order']['city']];
            }else{
                $ordersList[$key]['order']['driver_fees'] = $driver->cost;
            }
        }
        return $ordersList;
    }

    //function to get the total sum of driver fees
    function total_driver_fees($transList){
        $collection = new Collection($transList);
        $amount = $collection->sumOf('order.driver_fees');
        return $amount;
    }
}
