<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * ZonesUsers Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ZonesTable&\Cake\ORM\Association\BelongsTo $Zones
 *
 * @method \App\Model\Entity\ZonesUser newEmptyEntity()
 * @method \App\Model\Entity\ZonesUser newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ZonesUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZonesUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZonesUser findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ZonesUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesUser[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesUser|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZonesUser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZonesUser[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesUser[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesUser[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesUser[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ZonesDriversTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('zones_drivers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Zones', [
            'foreignKey' => 'zone_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('price')
            ->requirePresence('price', 'create')
            ->notEmptyString('price');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));
        $rules->add($rules->existsIn(['zone_id'], 'Zones'));

        return $rules;
    }

    public function clearAllForDriver($id){
        return $this->deleteAll(['driver_id' => $id]);
    }
    public function saveForDriver($driverId,$zonesArray){
        foreach ($zonesArray as $zoneId => $zonedata){
            $zone = $this->newEmptyEntity();
            $zone->driver_id = $driverId;
            $zone->zone_id = $zoneId;
            $zone->price = $zonedata['price'];
            $this->save($zone);
            unset($zone);
        }
    }
    public function get_driver_price_list($driverId){
        $driverZones = $this->find('all')->contain(['Zones'])->where(['ZonesDrivers.driver_id' =>$driverId])->toArray();
        if (empty($driverZones)){
            $zones = $this->find('all')->contain(['Zones'])->toArray();
            $driver = TableRegistry::getTableLocator()->get('Drivers')->get($driverId);
            $price_list = [];
            foreach ($zones as $zone){
                $price_list[trim($zone['zone']->name)] = $driver->cost;
            }
            return $price_list;
        }
        $price_list = [];
        foreach ($driverZones as $price){
            $price_list[$price['zone']->name] = $price->price;
        }
        return $price_list;
    }
}
