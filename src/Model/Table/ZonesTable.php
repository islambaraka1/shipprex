<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Zones Model
 *
 * @property \App\Model\Table\ZonesTable&\Cake\ORM\Association\BelongsTo $ParentZones
 * @property \App\Model\Table\ZonesTable&\Cake\ORM\Association\HasMany $ChildZones
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Zone newEmptyEntity()
 * @method \App\Model\Entity\Zone newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Zone[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Zone get($primaryKey, $options = [])
 * @method \App\Model\Entity\Zone findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Zone patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Zone[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Zone|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Zone saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Zone[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Zone[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Zone[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Zone[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class ZonesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('zones');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Tree');

        $this->belongsTo('ParentZones', [
            'className' => 'Zones',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('ChildZones', [
            'className' => 'Zones',
            'foreignKey' => 'parent_id',
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'zone_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'zones_users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('default_price')
            ->requirePresence('default_price', 'create')
            ->notEmptyString('default_price');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentZones'));

        return $rules;
    }

    public function findUserZonesOrReturnAllZones($userId){
        $zonesUsersTable = TableRegistry::getTableLocator()->get('ZonesUsers');
        $zonesList = $this->find('all')->toArray();
        foreach ($zonesList as $key => $zone){
            $crossData = $zonesUsersTable->findByZoneIdAndUserId($zone->id,$userId)->first();
            if( isset($crossData->price)){
                $zonesList[$key]->default_price =  $crossData->price;
            }
        }
        return $zonesList;
    }

    public function findDriverZonesOrReturnAllZones($driverId){
        $zoneDriverList = TableRegistry::getTableLocator()->get('ZonesDrivers');
        $driverTable = TableRegistry::getTableLocator()->get('Drivers');
        $cost = $driverTable->get($driverId)->cost;
        $zonesList = $this->find('all')->toArray();
        foreach ($zonesList as $key => $zone){
            $crossData = $zoneDriverList->findByZoneIdAndDriverId($zone->id,$driverId)->first();
            if( isset($crossData->price)){
                $zonesList[$key]->default_price =  $crossData->price;
            } else {
                $zonesList[$key]->default_price = $cost;
            }
        }
        return $zonesList;
    }

}
