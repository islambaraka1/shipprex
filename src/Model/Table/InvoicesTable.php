<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\Time;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Invoices Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Invoice newEmptyEntity()
 * @method \App\Model\Entity\Invoice newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Invoice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Invoice get($primaryKey, $options = [])
 * @method \App\Model\Entity\Invoice findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Invoice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoice saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoicesTable extends Table
{
    var $invoiceClearedAt;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('invoices');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UsersManager.Users', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'invoice_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->dateTime('cleared_at')
            ->allowEmptyDateTime('cleared_at');

        $validator
            ->integer('total_payout')
            ->allowEmptyString('total_payout');

        $validator
            ->integer('total_fees')
            ->allowEmptyString('total_fees');

        $validator
            ->integer('total_amount')
            ->allowEmptyString('total_amount');

        $validator
            ->integer('number_of_orders')
            ->allowEmptyString('number_of_orders');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function CreateEmptyInvoice($userId){
        $invoice = $this->newEmptyEntity();
        $invoice->name = GInvoiceName();
        $invoice->user_id = $userId;
        $this->save($invoice);
    }
    public function closeAnyOpenInvoiceForUser($userId){
        $listOfOpenInvoices = $this->find('all')->where(['user_id' => $userId , 'cleared_at IS' => null ])->toArray();
        foreach ($listOfOpenInvoices as $invoice){
            $this->CloseAnActiveInvoice($invoice->id);
        }
        $this->CreateEmptyInvoice($userId);


    }

    function CloseAnActiveInvoice($InvoiceId){
        $clearedAt = $this->invoiceClearedAt == null? new Time() : $this->invoiceClearedAt;
        $invoice = $this->get($InvoiceId);
        $amounts = $this->getInvoiceTotalSum($InvoiceId);
        $invoice->total_payout = $amounts['cod'] - $amounts['fees'] ;
        $invoice->total_fees =  $amounts['fees'] ;
        $invoice->total_amount = $amounts['cod']  ;
        $invoice->number_of_orders = $amounts['orders'] ;
        $invoice->cleared_at = $clearedAt;
        $this->save($invoice);
    }
    function getCurrentActiveInvoice($userId){
        $invoice = $this->find('all') -> where(['user_id' => $userId , 'cleared_at IS' => null ])->toArray();
        if(count($invoice) == 1){
            return $invoice[0];
        } else {
            // check for system error as many or no invoices is available
            // fix this user open invoices if more then one is created
            header('Location: '.ROOT_URL.'invoices/noOpenInvoices/'.$userId);
            die();
//            var_dump("issue with the query ");
//            var_dump($userId);
//            var_dump($invoice);
//            die();
        }
    }
    function getInvoiceTotalSum($invoiceId){
        $invoice = $this->find()->where(['Invoices.id' => $invoiceId])->contain(['Orders'])->first()->toArray();
        $totalAmount = 0;
        $totalFees = 0;
        $orderCounts = 0;
        foreach ($invoice['orders'] as $order){
              $totalFees +=  $order['fees'];
              $totalAmount +=  $order['cod'];
              $orderCounts +=  1;
        }
        return ['cod' => $totalAmount , 'fees' => $totalFees,'orders' =>$orderCounts];
    }

    function  attache_order_to_active_invoice ($orderId){
        try {
            $order = $this->Orders->get($orderId);
            $invoiceId = $this->getCurrentActiveInvoice($order->user_id)->id;
            $order->invoice_id = $invoiceId;
            $this->Orders->save($order);
            return true;
        }catch (RecordNotFoundException $exception){

        }

    }

}
