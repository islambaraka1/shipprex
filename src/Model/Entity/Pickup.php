<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pickup Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $location_id
 * @property \Cake\I18n\FrozenDate $pickup_date
 * @property string $pickup_time
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property string|null $description
 * @property string|null $statues
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\Action[] $actions
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Transaction[] $transactions
 */
class Pickup extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'location_id' => true,
        'pickup_date' => true,
        'pickup_time' => true,
        'contact_name' => true,
        'contact_phone' => true,
        'contact_email' => true,
        'description' => true,
        'statues' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'location' => true,
        'actions' => true,
        'orders' => true,
        'transactions' => true,
    ];
}
