<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $address_line
 * @property string $city
 * @property string $building
 * @property string $floor
 * @property string $apt
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Pickup[] $pickups
 */
class Location extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'address_line' => true,
        'city' => true,
        'building' => true,
        'floor' => true,
        'apt' => true,
        'contact_name' => true,
        'contact_phone' => true,
        'contact_email' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'pickups' => true,
    ];
}
