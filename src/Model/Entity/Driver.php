<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Driver Entity
 *
 * @property int $id
 * @property string $name
 * @property string $otp
 * @property string $license
 * @property string $vehicle_num
 * @property string $phone
 * @property string $note
 * @property int $created
 * @property int $modified
 *
 * @property \App\Model\Entity\DriverPickup[] $driver_pickup
 * @property \App\Model\Entity\Order[] $orders
 */
class Driver extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'otp' => true,
        'license' => true,
        'vehicle_num' => true,
        'phone' => true,
        'note' => true,
        'cost' => true,
        'created' => true,
        'modified' => true,
        'driver_pickup' => true,
        'orders' => true,
    ];
}
