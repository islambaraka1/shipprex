<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DriversOrder Entity
 *
 * @property int $id
 * @property int $driver_id
 * @property int $order_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenDate $target_day
 *
 * @property \App\Model\Entity\Driver $driver
 * @property \App\Model\Entity\Order $order
 */
class DriversOrder extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'driver_id' => true,
        'order_id' => true,
        'created' => true,
        'modified' => true,
        'target_day' => true,
        'driver' => true,
        'order' => true,
    ];
}
