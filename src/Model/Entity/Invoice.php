<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $cleared_at
 * @property int|null $total_payout
 * @property int|null $total_fees
 * @property int|null $total_amount
 * @property int|null $number_of_orders
 * @property int|null $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class Invoice extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'created' => true,
        'modified' => true,
        'cleared_at' => true,
        'total_payout' => true,
        'total_fees' => true,
        'total_amount' => true,
        'number_of_orders' => true,
        'user_id' => true,
        'user' => true,
    ];
}
