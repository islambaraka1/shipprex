<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ZonesUser Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $zone_id
 * @property int $price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Zone $zone
 */
class ZonesDriver extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'driver_id' => true,
        'zone_id' => true,
        'price' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'zone' => true,
    ];
}
