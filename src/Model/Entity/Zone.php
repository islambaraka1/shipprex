<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Zone Entity
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property int $lft
 * @property int $rght
 * @property int $default_price
 *
 * @property \App\Model\Entity\Zone $parent_zone
 * @property \App\Model\Entity\Zone[] $child_zones
 * @property \App\Model\Entity\User[] $users
 */
class Zone extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'parent_id' => true,
        'branch_id' => true,
        'lft' => true,
        'rght' => true,
        'default_price' => true,
        'parent_zone' => true,
        'child_zones' => true,
        'users' => true,
    ];
}
