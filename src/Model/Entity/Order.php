<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $pickup_id
 * @property string $type
 * @property string $reference
 * @property string $receiver_name
 * @property string $receiver_email
 * @property string $receiver_phone
 * @property string $receiver_address
 * @property string $city
 * @property bool $work_address
 * @property string $package_description
 * @property int $cod
 * @property string $notes
 * @property string $statues
 * @property int $fees
 * @property int|null $driver_id
 * @property int|null $invoice_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Invoice $incoice
 * @property \App\Model\Entity\Pickup $pickup
 * @property \App\Model\Entity\Driver[] $drivers
 * @property \App\Model\Entity\Action[] $actions
 * @property \App\Model\Entity\DriverPickup[] $driver_pickup
 * @property \App\Model\Entity\Transaction[] $transactions
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'pickup_id' => true,
        'type' => true,
        'reference' => true,
        'receiver_name' => true,
        'receiver_email' => true,
        'receiver_phone' => true,
        'receiver_address' => true,
        'city' => true,
        'work_address' => true,
        'package_description' => true,
        'cod' => true,
        'notes' => true,
        'statues' => true,
        'fees' => true,
        'driver_id' => true,
        'invoice_id' => true,
        'country' => true,
        'created' => true,
        'modified' => true,
        'patch_number' => true,
        'user' => true,
        'meta' => true,
        'invoice' => true,
        'pickup' => true,
        'drivers' => true,
        'actions' => true,
        'driver_pickup' => true,
        'transactions' => true,
        'branch_id' => true,
    ];
}
