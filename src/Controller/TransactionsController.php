<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\Event;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * Transactions Controller
 *
 * @property \App\Model\Table\TransactionsTable $Transactions
 *
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TransactionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    var $from ;
    var $to ;
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Orders', 'Pickups'],
        ];
        $transactions = $this->paginate($this->Transactions);

        $this->set(compact('transactions'));
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => ['Users', 'Orders', 'Pickups'],
        ]);

        $this->set('transaction', $transaction);
    }

    public function balance($id = null)
    {
        // convert to the new balance
        $this->redirect(['controller' => 'invoices' ,'action'=>'list',$id]);
//
//        $this->transrecet($id);
//
//        $userId =  $_SESSION['Auth']['User']['id'];
//        if ($_SESSION['Auth']['User']['group_id'] == 1 && $id != null ){
//            $userId = $id;
//        }
//
//        $this->paginate = [
//            'contain' => ['Users', 'Orders', 'Pickups'],
//        ];
//        $transactions = $this->Transactions->find('all')->contain(['Orders'])->where(['Transactions.user_id' => $userId]);
////        pr($transactions->toArray());
//        $this->set(compact('transactions'));
//        $totalOrders = $this->Transactions->Orders->find('all')
//            ->where(['Orders.user_id' => $userId])
//            ->where(['Orders.statues IN' => state_active_type ])
//            ->toArray();
//        $transactions = $this->Transactions->find('all')->where(['Transactions.user_id' => $userId]);
//        $transactions
//            ->select(['sum' => $transactions->func()->sum('amount')])
//            ->toArray();
//        $feesCosts =  $this->Transactions->find('all')->where(['Transactions.user_id' => $userId,'type'=>'in']);
//        $feesCosts
//            ->select(['sum' => $feesCosts->func()->sum('amount')])
//            ->toArray();
//        $collectedCods =  $this->Transactions->find('all')->where(['Transactions.user_id' => $userId,'type'=>'out']);
//        $collectedCods
//            ->select(['sum' => $collectedCods->func()->sum('amount')])
//            ->toArray();
//
//        $TotalReminingBalance = $transactions->toArray()[0]['sum'] +0 ;
//        $totalFeesCosts = $feesCosts->toArray()[0]['sum'] + 0 ;
//        $totalCollectedCods = $collectedCods->toArray()[0]['sum'] +0 ;
//        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));
    }

    public function dbalance($id = null){
        $this->loadModel('Invoices');
        $the_open_invoice_for_user = $this->Invoices->getCurrentActiveInvoice($id);


        $feesCosts =  $this->Transactions->Orders->find('all')->select(['sum' => 'SUM(`fees`)'])->where(['Orders.invoice_id' => $the_open_invoice_for_user->id ])->toArray();
        $collectedCods =  $this->Transactions->Orders->find('all')->select(['sum' => 'SUM(`cod`)'])->where(['Orders.invoice_id' => $the_open_invoice_for_user->id ])->toArray();


        $totalFeesCosts = $feesCosts[0]['sum'];
        $totalCollectedCods = $collectedCods[0]['sum'];
        $ent = $this->Transactions->newEmptyEntity();
        $out = $this->Transactions->newEmptyEntity();
        $ent->type = "in";
        $ent->amount = abs((int)$totalFeesCosts);
        $ent->user_id = $id;
        $ent->details = "Clear Balance";
        $ent->created_by = $_SESSION['Auth']['User']['id'];

        $out->type = 'out';
        $out->amount = $totalCollectedCods *-1;
        $out->user_id = $id;
        $out->details = "Clear Balance";
        $out->created_by = $_SESSION['Auth']['User']['id'];


        $this->Transactions->save($ent);
        $this->Transactions->save($out);
        $this->loadModel('Invoices');

        $event = new Event('Model.Transactions.ClearBalance', $this, [
            'InTransactions' => $ent,
            'OutTransaction' => $out,
            'TargetInvoice' =>  $this->Invoices->getCurrentActiveInvoice($id),
        ]);
        $this->getEventManager()->dispatch($event);

        $this->Invoices->closeAnyOpenInvoiceForUser($id);
        $this->Flash->success(__("Done"));
        $this->redirect('/transactions/balance/'.$id);

    }

    /**
     * Clear balance for a user
     * originally created in 05-05-2020
     * this method need to be changed to the new invoices system
     * @author Islam Baraka
     * @uses \App\Model\Entity\Transaction , \App\Model\Entity\Invoice
     * @updated 18-04-2022
     * @updated 31-01-2023 enhance the performance of the method
     * @version 1.8.5
     * @ticket SHIP-426 upgrade to new invoice system
     * @param $id
     * @return void
     */
    public function cbalance($id = null)
    {
        $this->loadModel('Users');
        $this->loadModel('Invoices');
        $this->set('user',$this->Users->get($id));
        $the_open_invoice_for_user = $this->Invoices->getCurrentActiveInvoice($id);
//        $this->paginate = [
//            'contain' => ['Users', 'Orders', 'Pickups'],
//        ];
////        $transactions = $this->paginate($this->Transactions->find('all')->where(['Transactions.user_id' , $id ]));
////
////        $this->set(compact('transactions'));


        $totalOrders = $this->Transactions->Orders->find('all')->where(['user_id' => $id])->count();
//        $transactions = $this->Transactions->Orders->find('all')->select(['sum' => 'SUM(`cod`)']) ->where(['Orders.invoice_id' => $the_open_invoice_for_user->id ])->toArray();
//        $transactions
//            ->select(['sum' => $transactions->func()->sum('cod')])
//            ->toArray();

//
        $feesCosts =  $this->Transactions->Orders->find('all')->select(['sum' => 'SUM(`fees`)'])->where(['Orders.invoice_id' => $the_open_invoice_for_user->id ])->toArray();
//        $feesCosts
//            ->select(['sum' => $feesCosts->func()->sum('fees')])
//            ->toArray();
        $collectedCods =  $this->Transactions->Orders->find('all')->select(['sum' => 'SUM(`cod`)'])->where(['Orders.invoice_id' => $the_open_invoice_for_user->id ])->toArray();
//        $collectedCods
//            ->select(['sum' => $collectedCods->func()->sum('cod')])
//            ->toArray();
//
        $TotalReminingBalance = $collectedCods[0]['sum'] - $feesCosts[0]['sum'];
        $totalFeesCosts = $feesCosts[0]['sum'];
        $totalCollectedCods = $collectedCods[0]['sum'];
        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transaction = $this->Transactions->newEmptyEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $orders = $this->Transactions->Orders->find('list', ['limit' => 200]);
        $pickups = $this->Transactions->Pickups->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'users', 'orders', 'pickups'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $orders = $this->Transactions->Orders->find('list', ['limit' => 200]);
        $pickups = $this->Transactions->Pickups->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'users', 'orders', 'pickups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public  function dashboard(){
        // get all system Orders and active / in active of it
        $orders_inactive = $this->Transactions->Orders->find('all')
            ->where(['Orders.statues IN' => state_unactive_types])->count();
        $orders_active = $this->Transactions->Orders->find('all')
            ->where(['Orders.statues IN' => state_active_type])->count();
        $toBePaid = $this->sum_finder(['Transactions.details !=' => 'Clear Balance']);
        $cod = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'out']);
        $toBePaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance'],['Transactions.created >' => new \DateTime('-7 days')]);
        $codPaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'out'],['Transactions.created >' => new \DateTime('-7 days')]);
        $cutoffPaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in'],['Transactions.created >' => new \DateTime('-7 days')]);
        $cutoffPaid = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in']);
        $cahrtData = $this->last_days_sum(7,['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in'] );
        $this->set(compact('orders_active','cahrtData','orders_inactive','cod','cutoffPaid','codPaidWeek','cutoffPaidWeek','orders_inactive','toBePaid','toBePaidWeek'));
    }

    /**
     * @param $conditionArray
     */

    private function last_days_sum($days,$sumConditions){
        $returner = [];
        for ($x=0;$x<$days;$x++){
            $today  = new FrozenTime("-$x days");
            $returner [$today->dayOfWeekName] = $this->sum_finder($sumConditions,['Transactions.created LIKE' => '%'.$today->toDateString().'%' ]);
        }
        return $returner;
    }
    private function sum_finder($conditionArray,$duration = [])
    {
        $transactions = $this->Transactions->find('all')->where($conditionArray)->where($duration);
        return abs((int)$transactions
            ->select(['sum' => $transactions->func()->sum('amount')])
            ->toArray()[0]->sum);;
    }

    /**
     * @param $id
     */
    public function transrecet($id): void
    {
        date_default_timezone_set('Africa/Cairo');

        //get all transactions orderd by id DESC
        $ClearanceTransactions = $this->Transactions->findAllByDetailsAndUserId('Clear Balance', $id)->toArray();
        $days = [];
        $last = count($ClearanceTransactions);
        foreach ( $ClearanceTransactions as $trans ){
            $transEnt = $this->Transactions->patchEntity($trans,[]);
            $days[($transEnt->created->getTimestamp()+7200)] = '';
        }
        $this->set('days' , $this->assignTransactionsToDate($days,$id) );

    }

    private function assignTransactionsToDate($dates,$id){
        $date_list = array_keys($dates);
        foreach ($date_list as $key => $date) {
            if($key == 0 ){
                $this->from = time()-50000000;
                $this->to = $date ;

            }else{
                $this->to = $date;
                $this->from = isset ($date_list[ $key-1 ] ) ? $date_list[ $key-1 ] : time() ;
            }
                $query = $this->Transactions->find()->contain(['Orders'])
                ->where(['Transactions.user_id' => $id])
                ->where(function ($exp, $q) {
                    return $exp->between('Transactions.created', $this->from, $this->to);
                });
            $dates[$date] = $query->toArray();
        }
        return $dates;
    }

//    public function invoiceprint
}
