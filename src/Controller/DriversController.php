<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Collection\Collection;
use Cake\I18n\FrozenDate;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;

/**
 * Drivers Controller
 *
 * @property \App\Model\Table\DriversTable $Drivers
 *
 * @method \App\Model\Entity\Driver[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DriversController extends AppController
{
    private int $total_commissions;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $drivers = $this->Drivers->find('all');

        $this->set(compact('drivers'));
        $this->insights();
    }

    private function insights(){
        $this->loadComponent('ReportsDrivers');
        $this->set('reports_data_count_of_diliveries',$this->ReportsDrivers->get_count_of_diliveries());
        $this->set('reports_data_last_days_diliveries',$this->ReportsDrivers->get_count_of_diliveries_last_days(15));
        $this->set('reports_data_all_on_routes',$this->ReportsDrivers->get_all_on_route_orders());
        $this->set('reports_data_active_drivers',$this->ReportsDrivers->get_active_drivers_last_month_count());
    }

    /**
     * View method
     *
     * @param string|null $id Driver id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadComponent('ReportsDrivers');
        $driver = $this->Drivers->get($id, [
            'contain' => ['Orders', 'DriverPickup','Orders.Users'],
        ]);
        $this->set('driver_timeline' , $this->ReportsDrivers->get_orders_timeline_by_driver($id));
        $this->set('driver_statues' , $this->ReportsDrivers->get_statues_counts($id));
        $this->set('driver_state' , $this->ReportsDrivers->get_full_driver_data($id));

        if(is_array($driver->orders)){
            $finalOrders = [];
            foreach ($driver->orders as $order){
                $order->_joinData['target_day'] = new FrozenDate($order->_joinData['target_day']);
                $key = $order->_joinData['target_day'];
                //add 5 hours to the time to change the day on day light saving
                $key = $key->toUnixString() + (60*60*5);
                $key = date('Y-m-d',$key);
                $key = strval($key);
                $finalOrders[$key][] = $order;
            }
            $this->set('ordersList',$finalOrders);
        }
        $this->set('driver', $driver);
    }

    //print all url redirect
    public function printAll($driverId){
        $day = $this->request->getQuery('day');
        $crossTable = TableRegistry::getTableLocator()->get('DriversOrders');
        $str_time = strtotime($day);
        $orders = $crossTable->find('all')->where(['driver_id' => $driverId,'target_day' => date('Y-m-d',$str_time)]);
        $ids = "";
        foreach ($orders as $order){
            $ids .= DID($order->order_id).",";
        }
        $this->redirect('/orders/newprintall/?ids='.$ids);
    }


    public function otp($id = null)
    {
        $this->viewBuilder()->setLayout('backend/guest');

        // check for the id and otp code in the database
        if ($this->request->is('post')) {
            $driver = $this->Drivers->get($id);
            if($driver->otp == $this->request->getData('otp')){
                $_SESSION['driver_id'] = $id;
                $this->Flash->success(__('OTP is correct'));
                return $this->redirect(['action' => 'online',$id]);
            }

            $this->Flash->error(__('The driver could not be found. Please, try again.'));
        }
    }
    /**
     * View method
     *
     * @param string|null $id Driver id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function online($id = null)
    {
        if (get_option_value('driver_online_sec')=="hash"){
            $id = encrypt_decrypt('decrypt',$id);
        }
        if (get_option_value('driver_online_sec')=="otp"){
            if(!isset($_SESSION['driver_id'])){
                $this->Flash->error(__('Please enter correct OTP'));
                return $this->redirect(['action' => 'otp',$id]);
            }
        }

        $this->viewBuilder()->setLayout('backend/guest');
        $this->loadComponent('ReportsDrivers');
        $driver = $this->Drivers->get($id, [
            'contain' => [
                'DriverPickup',
                'Orders' => function ($query) {
                    return $query
                        ->contain(['Users','Tags'])
                        ->limit(1500);
                }
            ]
        ]);
        $this->set('driver_timeline' , $this->ReportsDrivers->get_orders_timeline_by_driver($id));
//        $this->set('driver_timeline' , []);
//        $this->set('driver_statues' , $this->ReportsDrivers->get_statues_counts($id));
        $this->set('driver_statues' , []);
        $this->set('driver_state' , []);
        $full_driver_data = $this->ReportsDrivers->get_full_driver_data($id);
        $full_driver_data['total_delivered_orders_sum_old'] = $full_driver_data['total_delivered_orders_sum'];
        //find driver zones list
        $zones = TableRegistry::getTableLocator()->get('ZonesDrivers');
        $priceList = $zones->get_driver_price_list($id);
        $full_driver_data['total_delivered_orders_sum'] = $this->respectDriverZonePrice($full_driver_data['total_delivered_orders_sum'],$priceList,$id);
        $full_driver_data['total_delivered_orders_count'] = $this->total_commissions;

        $this->set('driver_state' , $full_driver_data);
        if(is_array($driver->orders)) {
            $finalOrders = [];
            foreach ($driver->orders as $order) {
                // cast the target day to frozen date
                $order->_joinData['target_day'] = new FrozenDate($order->_joinData['target_day']);
                $key = $order->_joinData['target_day'];
                //add 5 hours to the time to change the day on day light saving
                $key = $key->toUnixString() + (60*60*5);
                $key = date('Y-m-d',$key);
                //convert the date to string
                $key = strval($key);
                $finalOrders[$key][] = $order;
            }
            $this->set('ordersList', $finalOrders);
        }

        $this->set('driver', $driver);
        I18n::setLocale('ar');
    }

    private function statics($driverId){
        // get all drivers unique
    }
    public function print($id){
        $day = $_GET['day'];
        $this->viewBuilder()->setLayout('backend/print');
        $driver = $this->Drivers->get($id, [
            'contain' => ['Orders', 'DriverPickup','Orders.Users'],
        ]);
        if(is_array($driver->orders)){
            $finalOrders = [];
            foreach ($driver->orders as $order){
                $order->_joinData['target_day'] = new FrozenDate($order->_joinData['target_day']);
                $key = $order->_joinData['target_day'];
                //add 5 hours to the time to change the day on day light saving
                $key = $key->toUnixString() + (60*60*5);
                $key = date('Y-m-d',$key);
                $key = strval($key);
                $finalOrders[$key][] = $order;
            }
            $this->set('ordersList',$finalOrders[$day]);
        }
        $this->set('driver', $driver);

    }

    public function assign(){
        // Select Driver Name Select Orders by Id **
        // Ajax Preview **
        // Append Ajax Order to the cross_table
        // Check if not already exist and save it
        // print orders slap on the same page
        // view orders by date on the view page
        // print page slap on view pages
        // update the orders actions on the moment driver took it
        $types = json_decode(get_option_value('driver_assign_show_status'));
        $driver = $this->Drivers->find('list', ['limit' => 200]);
        $orders = $this->Drivers->Orders->find('all')->where(['Orders.statues IN'=> $types])->toArray();
        $orderList=[];
        foreach ($orders as $order ){
            $orderList[$order->id] = DID($order->id).'-'.$order->receiver_name;
        }


        if ($this->request->is(['patch', 'post', 'put'])) {
            $playOnData = $this->request->getData();
            $orders_list = $playOnData['orders']['_ids'];
            foreach ($orders_list as $key => $id) {
                //unassigned before assign to other drivers
                if (get_option_value('driver_unassigned_before_re_assign') == 1) {
                    $this->forceUnAssign($id);
                }
            }
            $driver = $this->Drivers->get($this->request->getData()['id'],[
                'contain' => ['Orders']
            ]);

            foreach ($orders_list as $key => $id){
                //unassigned before assign to other drivers
                if(get_option_value('driver_unassigned_before_re_assign') == 1){
                    $this->forceUnAssign($id);
                }
                $newOrders[] = [
                    'id'=> $id,
                    '_joinData' => [
                        'target_day' => $playOnData['orders']['_joinData']['target_day']
                    ]
                ];
            }
            $playOnData['orders'] = $newOrders;

//            $driver = $this->Drivers->patchEntity($driver, $this->request->getData());
            $driver = $this->Drivers->patchEntity(
                $driver,
                $playOnData,
                ['associated' => ['Orders._joinData']]
            );

            if ($this->Drivers->save($driver,    ['associated' => ['Orders._joinData']] )) {
                $this->Flash->success(__('The driver has been saved.'));
                return $this->redirect(['action' => 'view',$driver->id]);
            }
            $this->Flash->error(__('The driver could not be saved. Please, try again.'));
        }


        $this->set(compact('driver', 'orders','orderList'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driver = $this->Drivers->newEmptyEntity();
        if ($this->request->is('post')) {
            $driver = $this->Drivers->patchEntity($driver, $this->request->getData());
            if ($this->Drivers->save($driver)) {
                $this->Flash->success(__('The driver has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver could not be saved. Please, try again.'));
        }
        $orders = $this->Drivers->Orders->find('list', ['limit' => 200]);
        $this->set(compact('driver', 'orders'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Driver id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $driver = $this->Drivers->get($id, [
            'contain' => ['Orders'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $driver = $this->Drivers->patchEntity($driver, $this->request->getData());
            if ($this->Drivers->save($driver)) {
                $this->Flash->success(__('The driver has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver could not be saved. Please, try again.'));
        }
        $orders = $this->Drivers->Orders->find('list', ['limit' => 200]);
        $this->set(compact('driver', 'orders'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Driver id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driver = $this->Drivers->get($id);
        if ($this->Drivers->delete($driver)) {
            $this->Flash->success(__('The driver has been deleted.'));
        } else {
            $this->Flash->error(__('The driver could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function unassigned($orderId,$driverId)
    {
        $this->forceUnAssign($orderId, $driverId);
        if( $this->request->is(['ajax']) ){
            $this->disableAutoRender();
            echo "unassigned";
        }else{
            $this->Flash->success(__('The driver has been unassigned.'));
            return $this->redirect(['action' => 'index','controller' => 'drivers' ]);        }
    }

    /**
     * @param $orderId
     * @param $driverId
     */
    private function forceUnAssign($orderId, $driverId = null): void
    {

        $this->loadModel('DriversOrders');
        $conditions = ['order_id' => $orderId];
        if($driverId != null)
            $conditions['driver_id'] = $driverId;
        $this->DriversOrders->deleteAll($conditions);
    }

    public function zones($driverId){
        $this->loadModel('Zones');
        $this->loadModel('ZonesDrivers');
        $this->set('zones',$this->Zones->findDriverZonesOrReturnAllZones($driverId));
        if($this->request->is('post')){
            $this->ZonesDrivers->clearAllForDriver($driverId);
            $this->ZonesDrivers->saveForDriver($driverId,$this->request->getData()['zone']);
            $this->Flash->success(__('Zones cost saved'));
            $this->redirect('/drivers/index');
        }
    }

    private function respectDriverZonePrice(mixed $total_delivered_orders_sum, $priceList, $driverId)
    {
        //check if driver has zone price
        if($priceList != null || $priceList != []){
            $sentAsZonePrice = 0;
            $total_delivered_orders_sum = 0;
            $driver = $this->Drivers->get($driverId);
            $model = TableRegistry::getTableLocator()->get('DriversOrders');
            $orders =  $model->find()->contain('Orders') ->
            where(['DriversOrders.driver_id'=>$driverId,'Orders.statues'=>'Delivered'])->toArray();
            foreach ($orders as $order){
                $price_for_order = (int)$priceList[trim($order->order->city)];
                if(!isset($priceList[trim($order->order->city)])){
                    $price_for_order = $driver->cost;
                }
                $sentAsZonePrice += $price_for_order;
                $total_delivered_orders_sum += $order->order->cod - $price_for_order;
            }
        }
        $this->total_commissions = $sentAsZonePrice;
        return $total_delivered_orders_sum;
    }

}
