<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Excel Controller
 *
 *
 * @method \App\Model\Entity\Excel[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExcelController extends AppController
{
    function reader($file,$type = "list"){
        Configure::write('debug' , false );
        $this->disableAutoRender();
        $targetFile = ROOT.DS.'webroot'.DS."excelsups".DS.$file;
        $spreadsheet = IOFactory::load($targetFile);
        $data = array(1,$spreadsheet->getActiveSheet()->toArray(null,true,true,true));
        $highestRow = $spreadsheet->getActiveSheet() ->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getActiveSheet() ->getHighestColumn(); // e.g 'F'
        $dataOfData = $data[1];
        $dataOfData = $this->filterEmptyValues($dataOfData);
        $return =  $this->Hooks->apply_filters( 'Excel_filter_reader_values',$this->getJson_encode($file, $dataOfData),$dataOfData);
        return $this->response->withType('text/html')->withStringBody($return);

    }

    public function createOrders ($file,$userid=1){
//        Configure::write('debug' , false );
        $this->disableAutoRender();
        $targetFile = ROOT.DS.'webroot'.DS."excelsups".DS.$file;
        $spreadsheet = IOFactory::load($targetFile);
        $data = array(1,$spreadsheet->getActiveSheet()->toArray(null,true,true,true));
        $highestRow = $spreadsheet->getActiveSheet() ->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getActiveSheet() ->getHighestColumn(); // e.g 'F'
        $dataOfData = $data[1];
        $dataOfData = $this->filterEmptyValues($dataOfData);
        $toBeSaved = $this->Hooks->apply_filters( 'Excel_filter_data_to_be_saved',$this->convertExcelIndexToMainIndexs($dataOfData));
        //saving
        $this->save_date($toBeSaved, $this->Hooks->apply_filters( 'user_id_on_excel_save',$userid));
        $this->redirect('/orders/list');
    }



    private function convertExcelIndexToMainIndexs($arr){
        //get main indexes
        $indexes = $this->handelHeadersOfArray($this->GetMainIndexes($arr));
        // remove first row
        unset($arr[1]);
        // assign new index to the old ones
        foreach ($arr as $key => $val){

            foreach($val as $k => $v){
                if(isset($indexes[  $k ])){
                    $newKey = $indexes [ $k ];
                    unset($arr[$key][  $k ]);
                    $arr[$key][  $newKey ] =  $v;
                }
            }
        }
        return $arr;
        // return the new structred array
    }

    private function handelHeadersOfArray($header){
        foreach($header as $key =>$val){
            $val = str_replace(' ','_',$val);
            $header[$key] = $val;
        }
        return $header;
    }
    private function GetMainIndexes($arr){
        return $arr[1];
    }
    private  function filterEmptyValues($arr){
        $returner = [];
        foreach ($arr as $key=> $row){
            $savedRow = [] ;
            foreach($row as $keys => $values ){
                if($values != ""){
                    $savedRow[$keys] = $values;
                }
            }
            $returner[$key] = $savedRow;
        }
        return $returner;
    }

    /**
     * @param $file
     * @param array $dataOfData
     * @return false|string
     */
    private function getJson_encode($file, array $dataOfData)
    {
        return json_encode([
            'file' => $file,
            'data' => $this->convertExcelIndexToMainIndexs($dataOfData),
            'values' => $this->handelHeadersOfArray($this->GetMainIndexes($dataOfData)),
        ]);
    }

    /**
     * @param $toBeSaved
     * @param int $userid
     */
    private function save_date($toBeSaved, $userid): void
    {
        $this->loadModel('Orders');

        // Generate a random 8-character patch number
        $patchNumber = bin2hex(random_bytes(4));

        // Start a transaction
        $this->Orders->getConnection()->begin();

        foreach ($toBeSaved as $record) {
            $name = isset($record['Name']) ? $record['Name'] : '';
            $phone = isset($record['Phone']) ? $record['Phone'] : '';
            $address = isset($record['Address']) ? $record['Address'] : '';
            $city = isset($record['City']) ? $record['City'] : 'Cairo';
            $country = isset($record['Country']) ? $record['Country'] : 'Egypt';
            $cod = isset($record['Cost']) ? $record['Cost'] : 0;
            $ref = isset($record['Ref']) ? $record['Ref'] : "";
            $description = isset($record['Description']) ? $record['Description'] : "";
            $ent = $this->Orders->newEmptyEntity();
            $ent->user_id = $userid;
            $ent->receiver_name = $name;
            $ent->receiver_phone = $phone;
            $ent->receiver_address = $address;
            $ent->city = trim($city);
            $cod = (string)$cod;
            $ent->cod = trim($cod);
            $ent->type = "Forward";
            $ent->package_description = $description;
            $ent->reference = $ref;

            // Set the patch_number field of the entity
            $ent->patch_number = $patchNumber;
            $ent->country = $country;

            $this->Orders->save($ent);
            $this->Hooks->do_action('excel_single_product_saved',$ent,$record);
            unset($ent);
        }

        // Commit the transaction
        $this->Orders->getConnection()->commit();
    }


}
