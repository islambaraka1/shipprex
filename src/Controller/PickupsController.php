<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Pickups Controller
 *
 * @property \App\Model\Table\PickupsTable $Pickups
 *
 * @method \App\Model\Entity\Pickup[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PickupsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Locations'],
        ];
        $pickups = $this->paginate($this->Pickups);

        if($_SESSION['Auth']['User']['group_id'] == 3){
            $pickups = $this->paginate($this->Pickups->find('all')->where(['Pickups.user_id' => $_SESSION['Auth']['User']['id']]));
        }
        $this->set(compact('pickups'));
    }

    /**
     * View method
     *
     * @param string|null $id Pickup id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pickup = $this->Pickups->get($id, [
            'contain' => ['Users', 'Locations', 'Actions', 'Orders', 'Transactions'],
        ]);

        $this->set('pickup', $pickup);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pickup = $this->Pickups->newEmptyEntity();
        if ($this->request->is('post')) {
            $pickup = $this->Pickups->patchEntity($pickup, $this->request->getData());
            if ($this->Pickups->save($pickup)) {
                $this->Flash->success(__('The pickup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pickup could not be saved. Please, try again.'));
        }
        $users = $this->Pickups->Users->find('list', ['limit' => 200]);
        $locations = $this->Pickups->Locations->find('list', ['limit' => 200]);
        $this->set(compact('pickup', 'users', 'locations'));
    }

    public function locations($id){
        $this->disableAutoRender();
        $this->loadModel('Locations');
        echo json_encode($this->Locations->get($id));
        if ($this->request->is('ajax')) {
//            var_dump("Test");
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Pickup id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pickup = $this->Pickups->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pickup = $this->Pickups->patchEntity($pickup, $this->request->getData());
            if ($this->Pickups->save($pickup)) {
                $this->Flash->success(__('The pickup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pickup could not be saved. Please, try again.'));
        }
        $users = $this->Pickups->Users->find('list', ['limit' => 200]);
        $locations = $this->Pickups->Locations->find('list', ['limit' => 200]);
        $this->set(compact('pickup', 'users', 'locations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pickup id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pickup = $this->Pickups->get($id);
        if ($this->Pickups->delete($pickup)) {
            $this->Flash->success(__('The pickup has been deleted.'));
        } else {
            $this->Flash->error(__('The pickup could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
