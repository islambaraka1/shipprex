<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Reports component
 */
class ReportsComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    protected $model;
    function setTheTargetModel ($model){
        $this->model = TableRegistry::getTableLocator()->get($model);
    }

    function get_count(array $conditions,$secondModel = null){
        if($secondModel == null)
            $secondModel = [];
        return $this->model->find()->contain($secondModel)->where($conditions)->count();
    }
    function get_sum_of_field($field,$conditions,$secondModel = null){
        if($secondModel == null)
            $secondModel = [];
        return $this->model->find()->contain($secondModel)->where($conditions)->sumOf($field);
    }

    function get_avg_of_field($field,$conditions){
        $query = $this->model->find();
        $query->select(['avg_'.$field => $query->func()->avg($field)])
            ->where($conditions)
            ->first();
        $return = 0;
        if (isset($query->toArray()['0']->toArray()['avg_'.$field]))
            $return = $query->toArray()['0']->toArray()['avg_'.$field];
        return $return;
    }

    function get_timeline_data($total_of_field, $conditions, $type = 'sum', $start = null, $duration = 7, $timeField = 'created') {
        /** @var Query $query */
        $query = $this->model->find();
        $query->select([$timeField, $total_of_field]);

        if (!empty($conditions)) {
            $query->where($conditions);
        }

        if (!empty($start)) {
            $query->andWhere([$timeField . ' >=' => $start]);
        }

        $query->andWhere([$timeField . ' <=' => date('Y-m-d', strtotime($start . ' + ' . $duration . ' days'))]);

        switch ($type) {
            case 'sum':
                $query->select([$total_of_field => $query->func()->sum($total_of_field)]);
                break;
            case 'count':
                $query->select([$total_of_field => $query->func()->count($total_of_field)]);
                break;
            case 'avg':
                $query->select([$total_of_field => $query->func()->avg($total_of_field)]);
                break;
            default:
                throw new \InvalidArgumentException('Invalid type parameter');
                break;
        }
        $createdYear = $query->func()->date_format([
            $timeField => 'identifier',
            "'%Y-%m-%d'" => 'literal'
        ]);
        $query->group([$createdYear]);
        $result = $query->toArray();
        // map the result to be $result[date(y-m-d)] = value
        $result = array_map(function ($item) use ($timeField, $total_of_field) {
            return [
                'date' => $item->$timeField->format('Y-m-d'),
                'value' => $item->$total_of_field
            ];
        }, $result);
        // index the result by date
        $result = array_combine(array_column($result, 'date'), array_column($result, 'value'));
        return $result;
    }

    function get_values_of_feild ($field,$conditions=[] ,$limit = 10000){
        $connection = ConnectionManager::get('default');
        $lowerModelName = $this->model->getTable();
        $where = $this->conditions_to_where_statment($conditions);
        $results = $connection
            ->execute(
                "select $field, count($field) AS total , (COUNT(*) / (SELECT COUNT(*) FROM orders)) * 100 as percentage from $lowerModelName $where group by $field order by total desc LIMIT $limit"
                )
            ->fetchAll('assoc');
        return $results;
    }

    private function  conditions_to_where_statment($conditions){
        if(!empty($conditions)) {
            $return = "WHERE ";
            $nextIndex = 1;
            foreach ($conditions as $key => $value) {
                $return .= " $key = $value ";
                if(isset($conditions[$nextIndex]))
                    $return .="AND ";
                $nextIndex++;
            }
            return $return;
        }
    }

    function best_of($field,$conditions,$return_number =5,$contain=[] ){
       return $this->model->find('all')->where($conditions)->contain($contain)->order([$field => 'DESC'])->limit($return_number)->toArray();
    }

    function best_of_by_other_model($secondModel,$avg_field,$sum_field,$count_field = 'id',$order_by=count){
        $connection = ConnectionManager::get('default');
        $modelTable = $this->model->getTable();
        $results = $connection
            ->execute(
                "
            SELECT $modelTable.*, AVG($secondModel.cod) AS model_avg , SUM($secondModel.cod) AS model_sum,count($secondModel.user_id) AS model_count FROM $modelTable
            LEFT JOIN $secondModel ON $modelTable.id = $secondModel.user_id GROUP BY $modelTable.id ORDER BY model_".$order_by." DESC
 "
            )
            ->fetchAll('assoc');
        return $results;
    }

    /**
     * @param $days
     * @return false|string
     */
    public function getDate($days)
    {
        return date('Y-m-d', (time() - (86400 * $days)));
    }

}
