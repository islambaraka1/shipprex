<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Collection\Collection;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * DataTableAjax component
 */
class DataTableAjaxComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    var  $controller;
    var  $request;
    var  $response;
    var  $paginate;
    var  $model;
    var  $fields;
    var  $search;
    var  $order;
    var  $conditions;
    var  $contain;
    var $columnIndex;
    var $rowperpage;
    var $columnName;
    var $columnSortOrder;
    var $searchValue;
    var $draw;
    var $row;
    var $searchFields;
    var $TemFieldName;
    var $useDriverSearch;
    var $driverName;


    /**
     * @return void
     * This method set the global setting of the search which include
     * the model ,fileds , order ,fields which are the columns names and so on
     */
    public function safeSetter()
    {
        $this->draw = $_POST['draw'];
        $this->row = $_POST['start'];
        $this->rowperpage = $_POST['length']; // Rows display per page
        $this->columnIndex = $_POST['order'][0]['column']; // Column index
        $this->columnName = $_POST['columns'][$this->columnIndex]['data']; // Column name
        $this->fields = $_POST['columns'];// Column name
        foreach ($this->fields as $key => $field){
            if($field['data'] == 'drivers.0.name'){
                if($field['search']['value'] != ''){
                    $this->useDriverSearch = true;
                    $this->driverName = $this->removeRegex($field['search']['value']);
                }
                unset($this->fields[$key]);
            }
        }
        $this->columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $this->searchValue= $_POST['search']['value'] ; // Search value
    }
    /**
     * @return array - return the data of the table
     * This is the main method of the component which is responsible for the search and pagination
     */
    public function handelDataTableAjaxRequest($model,$contains = [] , $appended_conditions = [])
    {
        // handel the request on the model
        $this->model = $model;
        $this->safeSetter();

        $this->setSearchFields();
        $search = $this->searchHandler();
        $search = $this->filtersHandler($search);
        $search = $this->append_conditions($appended_conditions,$search);
//        Log::write('debug',json_encode($search));
//        var_dump($search);
//        die();
        $totalRecords =$model->find('all')->where($appended_conditions)->count(); // Total number of records
        // handel the search for driver case
        if($this->useDriverSearch === true){
            $totalRecordwithFilter = $model->find('all')->contain($contains)->where($search)->matching('Drivers', function ($q) {
                return $q->where(['Drivers.id' => $this->driverName]);
            })->count();
            $finderData = $model->find('all')->contain($contains)->where($search)->matching('Drivers', function ($q) {
                return $q->where(['Drivers.id' => $this->driverName]);
            })->limit($this->rowperpage)->offset($this->row)->toArray(); ;
        } else {
            $totalRecordwithFilter = $model->find('all')->contain($contains)->where($search)->count();
            $finderData = $model->find('all')->contain($contains)->where($search)->limit($this->rowperpage)->offset($this->row)->toArray(); ;
        }


        $response = array(
            "draw" => intval($this->draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $finderData
        );
        return $response;
    }
    /**
     * @return array - a condition array for the search
     * this method is used for the search array to be placed in the condition array
     * the default search is the search value and the search fields are the fields which are in the search with OR statment wrapped in the array
     * the filtring method is used to preduce an and statment for the search fields
     * this method only prepare the search array for the condition array
     */
    function setSearchFields(){
        //convert the column name to the field name
        $this->searchFields = array();
        foreach ($this->fields as $field){
            //if the field is a relation field
            if($field['searchable'] == 'true'){
                $field = $field['data'] ;
                if(strpos($field,'.') !== false) {
                    $field = explode('.', $field);
                    $field[0] = $this->pluralize($field[0]);
                    $this->searchFields[] = $field[0] . '.' . $field[1];
                }

                $this->searchFields[] = $field;
            }
        }
    }

    /**
     * @return array[] - return the search array for the condition array
     * this method is used for the search array to be placed in the condition array
     * @depends setSearchFields()
     */
    function searchHandler(){
        $searchCondition = [];
        if($this->searchValue != ''){

            foreach($this->searchFields as $field){
                if(!is_array($field)) {
                    $searchCondition = $this->getSearchCondition($field, $searchCondition);
                }
            }
        }
        return ['OR' => $searchCondition];
    }


    //function to convert singluer to plural with the help of inflector
    function pluralize($singular){
        return Inflector::camelize(Inflector::pluralize($singular));
    }

    /**
     * @param $field
     * @param array $searchCondition
     * @return array
     * this method determin the search condition for the search fields only which may be a string or a number condiftion matching
     */
    private function getSearchCondition($field, array $searchCondition): array
    {
        if($field == 'drivers.0.name'){
            $this->useDriverSearch = true;
            $this->driverName = $this->searchValue;
            return [];
        }
        if(strpos($field,'.') !== false){
            $field = explode('.',$field);
            $alias = $this->pluralize($field[0]);
            $field = $field[1];
        }else{
            $alias = $this->model->getAlias();
            $field = $field;
        }

        $modelRow = TableRegistry::getTableLocator()->get($alias);
        $fieldType = $modelRow->getSchema()->getColumnType($field);
        if ($fieldType == 'string' || $fieldType == 'text') {
            $searchCondition[$alias . '.' . $field . ' LIKE'] = '%' . $this->searchValue . '%';
        } elseif ($fieldType == 'datetime') {
            if($this->isJson($this->searchValue)){
                $decodedValue = json_decode($this->searchValue,true);
                if(isset($decodedValue['start'])){
                    $searchCondition[$alias . '.' . $field . ' >='] = $decodedValue['start'];
                    $searchCondition[$alias . '.' . $field . ' <='] = $decodedValue['end'];
                }
            } else {
                $searchCondition[$alias . '.' . $field] = $this->searchValue;
            }
        } else {
            // check if this intval is returning a number of just converted to 0
            if (intval($this->searchValue) != 0 ) {
                // if the field is id then use DID and UID for the search
                if ($field == 'id' && $alias == 'Orders') {
                    $searchCondition[$alias . '.' . $field ] = UNDID($this->searchValue);
                } else{
                    $searchCondition[$alias . '.' . $field] = intval($this->searchValue);
                }
            }
        }
        return $searchCondition;
    }

    //function to handel the search for driver name by matching rule
    function driverNameSearch($searchCondition){
        var_dump($searchCondition);
    }

    function filtersHandler($search){
        $andCondition = [];
        foreach ($this->fields as $field){
            if($field['searchable'] == 'true'){
                //check if there was a search value
                if($field['search']['value'] != ''){
                    $this->searchValue = trim($this->removeRegex($field['search']['value']));
                    $andCondition = $this->getSearchCondition($field['data'],$andCondition);
                }
            }
        }
        $search['AND'] = $andCondition;
        return $search;
    }

    //function to remove regex from the search value
    function removeRegex($searchValue){
        $searchValue = str_replace('^','',$searchValue);
        $searchValue = str_replace('$','',$searchValue);
        return $searchValue;
    }

    function append_conditions($condition,$search){
        if(!empty($search)){
            $condition = array_merge_recursive($condition,$search);
        }
        return $condition;
    }

    //function to check if the string passed is a json string or not
    function isJson($string) {
        json_decode(strval($string));
        return (json_last_error() == JSON_ERROR_NONE);
    }
    // function to create a search array from the json string


    function search_column_fields($term,$field,$modelName){
        //if this field contain relation then get the relation model name
        if(strpos($field,'.') !== false){
            $field = explode('.',$field);
            $modelName = $this->pluralize($field[0]);
            $field = $field[1];
        }
        if($modelName == "Users"){
            return $this->handel_search_for_usernames($term);
        }
        if($modelName == "Drivers"){
            return $this->handel_search_for_driver_name($term);
        }
        $model = TableRegistry::getTableLocator()->get($modelName);
        $fieldType = $model->getSchema()->getColumnType($field);
        if($fieldType == 'string'){
            $searchCondition = [$field . ' LIKE' => '%' . $term . '%'];
        }elseif($fieldType == 'datetime'){
            $searchCondition = [$field . ' >=' => $term];
        }else{
            $searchCondition = [$field => $term];
        }
        // fix the users for a companies conditions
        $groupTable = TableRegistry::getTableLocator()->get('Groups');
        $group = $groupTable->find()->where(['id' => $_SESSION['Auth']['User']['group_id']])->first();

        if(isset($_SESSION['Auth']['User']['id']) && $_SESSION['Auth']['User']['group_id'] == 3 || $group->is_companies == 1){
            $searchCondition['user_id'] = $this->getController()->Hooks->apply_filters('get_user_id_for_companies',$_SESSION['Auth']['User']['id']);
        }
//        $searchCondition['AND'] = [$modelName . '.user_id' => 0];

        $result = $model->find('all')->select([$field])->distinct()->where($searchCondition)->toArray();
        $this->TemFieldName = $field;
        $collection = new Collection($result);
        $result = $collection->map(function ($item, $key) {
            $field = $this->TemFieldName;
            return ['id' => $item->$field, 'text' => $item->$field];
        });
        return $result;
    }

    function handel_search_for_usernames($term){
        $model = TableRegistry::getTableLocator()->get("Users");
        $searchCondition = ['username LIKE' => '%' . $term . '%'];
        // fix the users for a companies conditions
        $groupTable = TableRegistry::getTableLocator()->get('Groups');
        $group = $groupTable->find()->where(['id' => $_SESSION['Auth']['User']['group_id']])->first();
        if($group->is_companies == 1){
            $searchCondition= ['id' => $this->getController()->Hooks->apply_filters('get_user_id_for_companies',$_SESSION['Auth']['User']['id'])];
        }
        if(isset($_SESSION['Auth']['User']['id']) && $_SESSION['Auth']['User']['group_id'] == 3){
            $searchCondition= ['id' => $this->getController()->Hooks->apply_filters('get_user_id_for_companies',$_SESSION['Auth']['User']['id'])];
        }
        $result = $model->find('all')->where($searchCondition)->toArray();
        $collection = new Collection($result);
        $result = $collection->map(function ($item, $key) {
            return ['id' => $item->username, 'text' => $item->username];
        });
        return $result;
    }

    function handel_search_for_driver_name($term){
        $model = TableRegistry::getTableLocator()->get("Drivers");
        $searchCondition = ['name LIKE' => '%' . $term . '%'];
//        if(isset($_SESSION['Auth']['User']['id']) && $_SESSION['Auth']['User']['group_id'] == 3){
//            $searchCondition= ['id' => $_SESSION['Auth']['User']['id']];
//        }
        $result = $model->find('all')->where($searchCondition)->toArray();
        $collection = new Collection($result);
        $result = $collection->map(function ($item, $key) {
            return ['id' => $item->id, 'text' => $item->name];
        });
        return $result;
    }

    //function to check the depth of the array
    function array_depth(array $array) {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->array_depth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }


}
