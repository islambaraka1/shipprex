<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Front component
 */
class FrontComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array<string, mixed>
     */
    protected $_defaultConfig = [];

    public function sendEvent($event_name, $data)
    {
        $_SESSION['FrontFlash'] = $data['message'];
        $_SESSION['front_callback'][$event_name] = $data;
        $_SESSION['front_callback'][$event_name]['data'] = $data['data']->toArray();

    }

    public function sendError($message){
        $_SESSION['FrontFlash'] = $message;
        $_SESSION['flash_type'] = "error";

    }

}
