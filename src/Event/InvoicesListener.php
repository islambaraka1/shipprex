<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


class InvoicesListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Orders.afterSave' => 'OrdersSaved',
            'Model.Orders.afterRemove' => 'OrdersRemoved',
            'Model.Users.afterSave' => 'UserSaved',
        ];
    }

    public function OrdersSaved($event, $order)
    {
        if($order->invoice_id === null && $order->statues == "Collected"){
            //update invoice id to the latest un cleared invoice
            $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
            $ordersTable = TableRegistry::getTableLocator()->get('Orders');
            $newOrder = $ordersTable->get($order->id);
            $newOrder->invoice_id = $invoicesTable->getCurrentActiveInvoice($order->user_id)->id;
            $ordersTable->save($newOrder);
        }
        // handel terminated orders on the termination action
        if (get_option_value('order_invoices_should_collect_terminated') == 1){
            if($order->invoice_id === null && $order->statues == "Terminated"){
                //update invoice id to the latest un cleared invoice
                $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
                $ordersTable = TableRegistry::getTableLocator()->get('Orders');
                $newOrder = $ordersTable->get($order->id);
                $newOrder->invoice_id = $invoicesTable->getCurrentActiveInvoice($order->user_id)->id;
                $newFees = 0;
                if( get_option_value('order_invoices_terminated_use_the_current_fees') == "Use default Fees"){
                    $newFees = get_option_value('order_invoices_terminated_default_fees');
                }
                if( get_option_value('order_invoices_terminated_use_the_current_fees') == "Use Order Fees"){
                    $newFees = $order->fees;
                }
                $newOrder->cod = 0;
                $newOrder->fees = $newFees;
                if (get_option_value('order_invoices_terminated_capture_the_old_cod') == 1)
                    $newOrder->notes = $order->notes ." ORDER OLD COD : ". $order->cod;
                $ordersTable->save($newOrder);
            }
        }

    }

    public function OrdersRemoved($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }

    public function UserSaved($event, $user){
        if ($user->isNew()) {
            // Create the first open invoice
            $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
            $invoicesTable->CreateEmptyInvoice($user->id);
        }
    }
}


?>
