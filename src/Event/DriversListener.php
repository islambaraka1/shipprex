<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;


class DriversListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
//            'Model.Orders.afterSave' => 'afterSave',
//            'Model.Orders.afterRemove' => 'afterRemove',
        ];
    }

    public function afterSave($event, $order)
    {
        // add new action change statues of the order
        $ordersTable = TableRegistry::getTableLocator()->get('Orders');
        $driver = $ordersTable->Drivers->get($order->driver_id);

        $message = "Assigned to driver : $driver->name [ $driver->phone]  ";
        $ordersTable->actionUpdates($order->order_id,"On route",$message);
    }

    public function afterRemove($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }
}


?>
