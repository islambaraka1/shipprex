<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


class ActionsListener implements EventListenerInterface
{



    public function implementedEvents() :array
    {
        return [
            'OrderExtera.OrderTagged' => 'TagsLinked',
            'Model.afterSave' => 'order_is_saved',
        ];
    }

    public function TagsLinked($event, $data)
    {
        $data = $event->getData();
        $tagTable = TableRegistry::getTableLocator()->get('OrderExtera.Tags');
        $tag = $tagTable->get($data['tagId']);
        //insert new action for this order with tagged message
        $actionsTable = TableRegistry::getTableLocator()->get('Actions');
        $actionsTable->add_action($data['orderId'], 'Tagged', 'Tagged with ' . $tag->name);
    }


    public function order_is_saved($event, $data): void
    {
        $subject = $event->getSubject();
        if($subject->getAlias() == 'Orders') {
            $data = $event->getData();
            $order = $data['entity'];
            //is this a new order
            if($order->isNew()) {
                //insert new action for this order with new order message
                $this->add_order_creation_action($order);
            }
            else{
                if($order->isDirty('cod')) {
                    //insert new action for this order with new order message
                    $this->order_cod_changed($order);
                }
            }
        }
    }


    //function to add action to the action log of order when the COD is changed and who changed it
    public function order_cod_changed($orderEntity)
    {
        $actionsTable = TableRegistry::getTableLocator()->get('Actions');
        $actionsTable->add_action($orderEntity->id, 'COD Changed', 'COD Changed to ' . $orderEntity->cod. ' Original value was : '.$orderEntity->getOriginal('cod'));
    }

    public function add_order_creation_action($orderEntity)
    {
        $actionsTable = TableRegistry::getTableLocator()->get('Actions');
        $actionsTable->add_action($orderEntity->id, 'New Order', 'New Order Created');
    }


}


?>
