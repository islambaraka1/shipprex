<?php
namespace App\Event;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


class OrdersListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Orders.afterSave' => 'afterSave',
            'Model.Orders.afterRemove' => 'afterRemove',
            'Model.Orders.addNewComment' => 'afterComment',
            'Model.Comments.addNewComment' => 'afterComments',
        ];
    }

    public function afterSave($event, $order)
    {
//        pr($order);
//        die();
    }

    public function afterRemove($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }

    public function afterComment($event,$order){
        $tableAction  = TableRegistry::getTableLocator()->get('Actions');
        $entity = $tableAction->newEmptyEntity();
        $entity->name = "New Comment";
        $entity->description = $order->notes ;
        $entity->order_id = $order->id ;
        $tableAction->save($entity);
    }

    public function afterComments($event,$comment){
        $tableAction  = TableRegistry::getTableLocator()->get('Actions');
        $entity = $tableAction->newEmptyEntity();
        $entity->name = "New Comment";
        $entity->description = $comment->description ;
        $entity->order_id = $comment->order_id ;
        $tableAction->save($entity);
    }

}


?>
