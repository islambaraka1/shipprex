<?php
declare(strict_types=1);

namespace GeneralLedger\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Transes Model
 *
 * @property \GeneralLedger\Model\Table\BanksTable&\Cake\ORM\Association\BelongsTo $Banks
 * @property \GeneralLedger\Model\Table\InvoicesTable&\Cake\ORM\Association\BelongsTo $Invoices
 * @property \GeneralLedger\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \GeneralLedger\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \GeneralLedger\Model\Entity\Transe newEmptyEntity()
 * @method \GeneralLedger\Model\Entity\Transe newEntity(array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[] newEntities(array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Transe get($primaryKey, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Transe|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Transe[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gl_transes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id',
            'joinType' => 'LEFT',
            'className' => 'GeneralLedger.Banks',
        ]);

        $this->belongsTo('FromBank', [
            'foreignKey' => 'from_bank',
            'joinType' => 'LEFT',
            'className' => 'GeneralLedger.Banks',
            'propertyName' => 'FromBank'
        ]);




        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'className' => 'Invoices',
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'LEFT',
            'className' => 'GeneralLedger.Categories',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
            'className' => 'UsersManager.Users',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'LEFT',
            'className' => 'Orders',
        ]);
        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
            'joinType' => 'LEFT',
            'className' => 'Drivers',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['bank_id'], 'Banks'));
//        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));
//        $rules->add($rules->existsIn(['category_id'], 'Categories'));
//        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    function add_cod_transaction($order){
        $trans = $this->newEmptyEntity();
        $trans->category_id = 4;
        $trans->bank_id = 2;
        $trans->amount = $order->cod;
        $trans->user_id = 1;
        $trans->name = "Order Payment";
        $trans->description = "Order : " . DID($order->id);
        $this->save($trans);

    }


    function add_driver_fees_transaction($driverId,$orderID,$fees,$moveOutFromCOD = false){

        $trans = $this->newEmptyEntity();
        $trans->category_id = 4;
        $trans->bank_id =5 ;
        $trans->amount = $fees;
        $trans->user_id = 1;
        $trans->driver_id = $driverId;
        $trans->order_id = $orderID;
        $trans->name = "Driver Fees";
        $trans->description = "Order : " . DID($orderID);
        $this->save($trans);
        if($moveOutFromCOD){
            //move out the same amount
            $codTrans = $this->newEmptyEntity();
            $codTrans->category_id = 4;
            $codTrans->bank_id =2 ;
            $codTrans->amount = -1 * abs((int)$fees) ;
            $codTrans->user_id = 1;
            $codTrans->driver_id = null;
            $codTrans->order_id = null;
            $codTrans->name = "[MOVE OUT] Driver Fees ";
            $codTrans->description = "For Order : " . DID($orderID);
            $this->save($codTrans);
        }
        $event = new Event('Model.Transes.DriverTransactionsIsDone', $this, [
            'Transaction' => [
                'driverId' => $driverId,
                'orderId' => $orderID,
                'fees' => $fees,
                'moveFromCod' => $moveOutFromCOD,
                ]
        ] );
        $this->getEventManager()->dispatch($event);
    }
    function move_from_bank_to_another ($trans){
        $copiedTrans = $this->newEmptyEntity();
        //add transaction to the bank
        $this->save($trans);
        // add transaction from a bank
        $copiedTrans->amount = -1 * abs((int)$trans->amount) ;
        $copiedTrans->bank_id = $trans->from_bank ;
        $copiedTrans->from_bank = null ;
        $copiedTrans->category_id = $trans->category_id ;
        $copiedTrans->name = "[MOVE OUT]".$trans->name ;
        $copiedTrans->description = "[MOVE OUT]".$trans->description ;
        $this->save($copiedTrans);
        return true;
    }

    function reset_bank_to_zero ($bankId){
        $upToDateCollections = $this->Banks->get($bankId)->current_balance;

        $copiedTrans = $this->newEmptyEntity();
        // add transaction from a bank
        $copiedTrans->amount = -1 * abs((int)$upToDateCollections) ;
        $copiedTrans->bank_id = $bankId ;
        $copiedTrans->from_bank = null ;
        $copiedTrans->category_id = 1 ;
        $copiedTrans->name = "[CLEAR OUT]";
        $copiedTrans->description = "[CLEAR OUT]" ;
        $this->save($copiedTrans);
        return true;
    }
    function cod_burn_for_invoice($outTransaction,$inTransaction, $targetInvoice){
        $burnTrans = $this->newEmptyEntity();
        $burnTrans->amount = $outTransaction->amount;
        $burnTrans->user_id = $outTransaction->user_id;
        $burnTrans->bank_id = GL_CONFIG_PROFIT_BANK;
        $burnTrans->name = __("COD Invoice Transaction");
        $burnTrans->description = __("COD Invoice Transaction");
        $burnTrans->invoice_id = $targetInvoice->id;
        $profitTrans = $this->newEmptyEntity();
        $profitTrans->amount = $inTransaction->amount;
        $profitTrans->user_id = $inTransaction->user_id;
        $profitTrans->bank_id =  GL_CONFIG_COD_BANK;
        $profitTrans->name = __("COD Invoice Transaction");
        $profitTrans->description = __("COD Invoice Transaction");
        $profitTrans->invoice_id = $targetInvoice->id;
        $this->save($burnTrans);
        $this->save($profitTrans);
    }
}
