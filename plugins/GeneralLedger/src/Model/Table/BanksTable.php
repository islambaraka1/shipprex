<?php
declare(strict_types=1);

namespace GeneralLedger\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Banks Model
 *
 * @property \GeneralLedger\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \GeneralLedger\Model\Entity\Bank newEmptyEntity()
 * @method \GeneralLedger\Model\Entity\Bank newEntity(array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[] newEntities(array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Bank get($primaryKey, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \GeneralLedger\Model\Entity\Bank|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \GeneralLedger\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BanksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gl_banks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'UsersManager.Users',
        ]);

        $this->hasMany('Transes', [
            'foreignKey' => 'bank_id',
            'joinType' => 'LEFT',
            'className' => 'GeneralLedger.Transes',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('minimum_amount')
            ->maxLength('minimum_amount', 50)
            ->requirePresence('minimum_amount', 'create')
            ->notEmptyString('minimum_amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
