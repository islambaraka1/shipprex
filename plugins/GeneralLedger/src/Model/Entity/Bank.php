<?php
declare(strict_types=1);

namespace GeneralLedger\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Bank Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property int $user_id
 * @property string $minimum_amount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \GeneralLedger\Model\Entity\User $user
 */
class Bank extends Entity
{
    protected $_virtual = ['current_balance'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'type' => true,
        'user_id' => true,
        'minimum_amount' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
    ];

    protected function _getCurrentBalance()
    {
        $transesModel = TableRegistry::getTableLocator()->get('GeneralLedger.Transes');
        $balance = $transesModel->find('all')->where(['bank_id' => $this->id])->sumOf('amount');
        return $balance;
    }
}
