<?php
declare(strict_types=1);

namespace GeneralLedger\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transe Entity
 *
 * @property int $id
 * @property float $amount
 * @property int $bank_id
 * @property int|null $invoice_id
 * @property string $description
 * @property string $name
 * @property int $category_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \GeneralLedger\Model\Entity\Bank $bank
 * @property \GeneralLedger\Model\Entity\Invoice $invoice
 * @property \GeneralLedger\Model\Entity\Category $category
 * @property \GeneralLedger\Model\Entity\User $user
 * @property int order_id
 * @property int driver_id
 */
class Transe extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'bank_id' => true,
        'invoice_id' => true,
        'description' => true,
        'name' => true,
        'category_id' => true,
        'user_id' => true,
        'from_bank' => true,
        'to_bank' => true,
        'driver_id' => true,
        'order_id' => true,
        'created' => true,
        'modified' => true,
        'bank' => true,
        'invoice' => true,
        'category' => true,
        'user' => true,
        'fromBank' => true,
    ];
}
