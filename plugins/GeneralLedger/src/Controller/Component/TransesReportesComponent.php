<?php
declare(strict_types=1);

namespace GeneralLedger\Controller\Component;

use App\Controller\Component\ReportsComponent;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;

/**
 * TransesReportes component
 */
class TransesReportesComponent extends ReportsComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->setTheTargetModel('GeneralLedger.Transes') ;
    }

    public function get_full_profit_sums(){
        $amounts_profit = $this->get_sum_of_field('amount',['Transes.bank_id' => 3]);
        $amounts_drivers = $this->get_sum_of_field('amount',['Transes.bank_id' => 5]);
        return $amounts_profit - $amounts_drivers;
    }
    public function get_full_income_sums(){
        $amounts_profit = $this->get_sum_of_field('amount',['Transes.bank_id' => 3]);
        return $amounts_profit;
    }
    public function get_full_drivers_sums(){
        $amounts_drivers = $this->get_sum_of_field('amount',['Transes.bank_id' => 5]);
        return $amounts_drivers;
    }

    function get_profit_time_line( ){
        $conditions = ['Transes.bank_id IN' =>[3,5]];
        $duration = get_option_value('reports_duration_for_profit_timeline');
        $profit_timeline = $this->get_timeline_data('amount', ['Transes.bank_id' => 3], 'sum', null, $duration);
        $date_of_first_accour = new Time( array_key_first((array)$profit_timeline));
        $all = [
            'profits'   => $profit_timeline,
            'drivers'   =>$this->get_timeline_data('amount' , ['Transes.bank_id' =>5],'sum', $date_of_first_accour, $duration),
            'count'   =>$this->get_timeline_data('amount' , $conditions,'sum', $date_of_first_accour, $duration),
        ];

        $enhanced_return =[];
        foreach ($all['count'] as $date => $value){
            $enhanced_return[$date] = [
                'revenue'       =>$value,
                'drivers_fees'       =>isset($all['drivers'][$date])?$all['drivers'][$date]:0 ,
                'profit_margins'     =>$value - $all['drivers'][$date],
            ];
        }
        return $enhanced_return ;
    }

}
