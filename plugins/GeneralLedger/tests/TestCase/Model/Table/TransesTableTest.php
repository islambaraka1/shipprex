<?php
declare(strict_types=1);

namespace GeneralLedger\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use GeneralLedger\Model\Table\TransesTable;

/**
 * GeneralLedger\Model\Table\TransesTable Test Case
 */
class TransesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \GeneralLedger\Model\Table\TransesTable
     */
    protected $Transes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.GeneralLedger.Transes',
        'plugin.GeneralLedger.Banks',
        'plugin.GeneralLedger.Invoices',
        'plugin.GeneralLedger.Categories',
        'plugin.GeneralLedger.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Transes') ? [] : ['className' => TransesTable::class];
        $this->Transes = TableRegistry::getTableLocator()->get('Transes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Transes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
