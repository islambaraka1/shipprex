<?php
declare(strict_types=1);

namespace GeneralLedger\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use GeneralLedger\Controller\Component\TransesReportesComponent;

/**
 * GeneralLedger\Controller\Component\TransesReportesComponent Test Case
 */
class TransesReportesComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \GeneralLedger\Controller\Component\TransesReportesComponent
     */
    protected $TransesReportes;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->TransesReportes = new TransesReportesComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TransesReportes);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
