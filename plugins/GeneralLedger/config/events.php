<?php
if (!function_exists('drivers_control_view_buttons3')) {
    $hooks->add_action('drivers_control_view_buttons', 'drivers_control_view_buttons3');
    function drivers_control_view_buttons3()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        $driverId = $hooks->currentView->getRequest()->getParam('pass')[0];
        echo '
        <div class="col">
            <a class="btn btn-primary" href="' . ROOT_URL . 'general-ledger/transes/driver_balance/' . $driverId . '">Driver Balance <i class="fa fa-check-circle"></i> </a>
        </div>
    ';
    }
}


$hooks->create_new_root_menu('general_ledger','General Ledger','#','university',[1,4]);
$hooks->add_sub_menu_item_to_root('general_ledger','Payment Categories','general-ledger/categories',[1,4]);
$hooks->add_sub_menu_item_to_root('general_ledger','Add Category','general-ledger/categories/add',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Bank List','general-ledger/banks',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Add Bank','general-ledger/banks/add',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Transactions List','general-ledger/transes',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Net Profits','general-ledger/transes/profits',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Daily Net Profits','general-ledger/transes/net_profit_per_day',[1]);
$hooks->add_sub_menu_item_to_root('general_ledger','Add Transaction ','general-ledger/transes/add',[1,4]);

