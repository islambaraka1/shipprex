<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $bank
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Bank'), ['action' => 'edit', $bank->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Bank'), ['action' => 'delete', $bank->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bank->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Banks'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Bank'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="banks view large-9 medium-8 columns content">
    <h3><?= h($bank->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($bank->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Type') ?></th>
                <td><?= h($bank->type) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $bank->has('user') ? $this->Html->link($bank->user->username, ['controller' => 'Users', 'action' => 'view', $bank->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Minimum Amount') ?></th>
                <td><?= h($bank->minimum_amount) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($bank->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($bank->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($bank->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($bank->description)); ?>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
            <th scope="col"><?= $this->Paginator->sort('balance_after') ?></th>
            <th scope="col"><?= $this->Paginator->sort('invoice_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('description') ?></th>
            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $balance = 0;

        foreach ($bank['transes'] as $transe) :
            $balance += $transe->amount;
            ?>
            <tr>
                <td><?= $this->Number->format($transe->id) ?></td>
                <td><?= $this->Number->format($transe->amount) ?></td>
                <td><?= $this->Number->format($balance) ?></td>
                <td><?=  $this->Html->link($transe->invoice_id, ['controller' => 'Invoices', 'action' => 'view', $transe->invoice_id,'plugin'=>null]); ?></td>
                <td><?= h($transe->description) ?></td>
                <td><?= h($transe->name) ?></td>
                <td><?= $transe->has('category') ? $this->Html->link($transe->category->name, ['controller' => 'Categories', 'action' => 'view', $transe->category->id]) : '' ?></td>
                <td><?= $transe->has('user') ? $this->Html->link($transe->user->username, ['controller' => 'Users', 'action' => 'view', $transe->user->id]) : '' ?></td>
                <td><?= h($transe->created) ?></td>
                <td><?= h($transe->modified) ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
