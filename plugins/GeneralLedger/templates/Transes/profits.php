<div class="row">
    <section class="col-lg-3 connectedSortable">
<?php
// add count widget for all the profits
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'success',
    'size' => '12',
    'count' => $profit_count ,
    'title' => __('Profits Total'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => __('From the start'),
]);

echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'warning',
    'size' => '12',
    'count' => $income_count ,
    'title' => __('Invoices Income'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => __('From the start'),
]);

echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'danger',
    'size' => '12',
    'count' => $drivers_count ,
    'title' => __('Drivers Fees'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => __('From the start'),
]);
// add time line widget for profits
?>
    </section>
    <section class="col-lg-9 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanv',
            'height' => '600',
            'title' => 'Orders Since account creation  ',
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'bar',
            'cahrtData' => $profit_timeline,
            'scale' =>SUSTEM_CURRENCY
        ]);

        ?>
    </section>

</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
        <th scope="col"><?= $this->Paginator->sort('balance_after') ?></th>
        <th scope="col"><?= $this->Paginator->sort('invoice_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('description') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
        $balance = 0;
        foreach ($transactions as $transe) :

            $transe->amount = $transe->bank_id === 5 ? -$transe->amount: $transe->amount;
            $balance += $transe->amount;

        ?>
        <tr>
            <td><?= $this->Number->format($transe->id) ?></td>
            <td><?= $this->Number->format($transe->amount) ?></td>
            <td> <b><?= $this->Number->format($balance) ?></b></td>
            <td><?= $this->Number->format($transe->invoice_id) ?></td>
            <td><?= h($transe->description) ?></td>
            <td><?= h($transe->name) ?></td>
            <td><?= $transe->has('category') ? $this->Html->link($transe->category->name, ['controller' => 'Categories', 'action' => 'view', $transe->category->id]) : '' ?></td>
            <td><?= $transe->has('user') ? $this->Html->link($transe->user->username, ['controller' => 'Users', 'action' => 'view', $transe->user->id]) : '' ?></td>
            <td><?= h($transe->created) ?></td>
            <td><?= h($transe->modified) ?></td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
