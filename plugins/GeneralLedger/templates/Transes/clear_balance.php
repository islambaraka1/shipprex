<h1> <?=__('Clear balance for driver')?>  : #<?=$driver->id;?> - <?=$driver->name;?></h1>
<h1> <?=__('Driver Fees are :')?> <?=$driver->cost;?> <?= SUSTEM_CURRENCY . __(' per order')?> </h1>
<div class="row">
    <?php
    //        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'count' => $sum_of_collections - $paid_amount,
        'title' => __('Collect from driver'),
        'icon' => 'money-check-alt',
        'sub_msg' => 'More Info',
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'count' => abs((int)$billableOrders),
        'title' => __('Billable orders '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'orange',
        'count' => $paid_amount,
        'title' => __('Total Payment'),
        'icon' => 'wallet',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'dark',
        'count' => $sum_of_collections,
        'title' => __('Total Collections '),
        'icon' => 'wallet',
        'sub_msg' => __('including the driver amount'),
    ]);
    ?>

</div>
<div class="">
    <h3><?=__('Everything is good')?></h3>
    <br>
    <a href="<?=ROOT_URL?>general-ledger/transes/clear_driver_balance/<?=$driver->id?>" class="btn btn-primary btn-lg text-light"><?=__('Proceed')?> <i class="fa fa-check-circle"></i></a>

</div>

<div class="row">
    <table class="table table-bordered table-striped">
        <thead>
            <th>ID</th>
            <th>reciver name</th>
            <th>phone</th>
            <th>City</th>
            <th>cost</th>
            <th>driver fees</th>
        </thead>
        <tbody>
        <?php foreach ($ordersToBeBilled as $trans):

            $order = $trans['order'];
            ?>
            <tr>
                <td><?=DID($order->id)?></td>
                <td><?=$order->receiver_name?></td>
                <td><?=$order->receiver_phone?></td>
                <td><?=$order->city?></td>
                <td><?=$order->cod?></td>
                <td><?=$order->driver_fees?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
