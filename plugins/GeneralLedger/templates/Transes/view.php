<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $transe
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Transe'), ['action' => 'edit', $transe->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Transe'), ['action' => 'delete', $transe->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transe->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Transes'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Transe'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Banks'), ['controller' => 'Banks', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Bank'), ['controller' => 'Banks', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="transes view large-9 medium-8 columns content">
    <h3><?= h($transe->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Bank') ?></th>
                <td><?= $transe->has('bank') ? $this->Html->link($transe->bank->name, ['controller' => 'Banks', 'action' => 'view', $transe->bank->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Description') ?></th>
                <td><?= h($transe->description) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($transe->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Category') ?></th>
                <td><?= $transe->has('category') ? $this->Html->link($transe->category->name, ['controller' => 'Categories', 'action' => 'view', $transe->category->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $transe->has('user') ? $this->Html->link($transe->user->id, ['controller' => 'Users', 'action' => 'view', $transe->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($transe->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Amount') ?></th>
                <td><?= $this->Number->format($transe->amount) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Invoice Id') ?></th>
                <td><?= $this->Number->format($transe->invoice_id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($transe->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($transe->modified) ?></td>
            </tr>
        </table>
    </div>
</div>
