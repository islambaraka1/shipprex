<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $transes
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Transe'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Banks'), ['controller' => 'Banks', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Bank'), ['controller' => 'Banks', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
        <th scope="col"><?= $this->Paginator->sort('bank_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('invoice_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('description') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($transes as $transe) : ?>
        <tr>
            <td><?= $this->Number->format($transe->id) ?></td>
            <td><?= $this->Number->format($transe->amount) ?></td>
            <td><?= $transe->has('bank') ? $this->Html->link($transe->bank->name, ['controller' => 'Banks', 'action' => 'view', $transe->bank->id]) : '' ?></td>
            <td><?= $this->Number->format($transe->invoice_id) ?></td>
            <td><?= h($transe->description) ?></td>
            <td><?= h($transe->name) ?></td>
            <td><?= $transe->has('category') ? $this->Html->link($transe->category->name, ['controller' => 'Categories', 'action' => 'view', $transe->category->id]) : '' ?></td>
            <td><?= $transe->has('user') ? $this->Html->link($transe->user->username, ['controller' => 'Users', 'action' => 'view', $transe->user->id]) : '' ?></td>
            <td><?= h($transe->created) ?></td>
            <td><?= h($transe->modified) ?></td>

        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('First')) ?>
        <?= $this->Paginator->prev('< ' . __('Previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('Next') . ' >') ?>
        <?= $this->Paginator->last(__('Last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
