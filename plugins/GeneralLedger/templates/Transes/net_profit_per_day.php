<?php
/** @var  $transactions */
use Cake\Collection\Collection;

$transactions= array_reverse($transactions);
foreach ($transactions as $transeaction) : ?>
    <div class="row">
        <div class="col"><h2>Date : <?=$transeaction->day?></h2>
        </div>
        <div class="col"><h2>Amount : <?=$transeaction->total?></h2>
        </div>
        <?php
        $collection = new Collection($transeaction['transactions'] );
        $sum =  $collection->sumOf('order.cod');
        $fees =  $collection->sumOf('order.fees');

        ?>
        <div class="col"><h2>COD's : <?=$sum ?></h2>
        </div>

        <div class="col"><h2>Collected : <?=$sum - $transeaction->total?></h2>
        </div>
        <div class="col"><h2>Net profit : <?=$fees-$transeaction->total?></h2>
        </div>
    </div>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
        <th scope="col"><?= $this->Paginator->sort('driver_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('order_amount') ?></th>
        <th scope="col"><?= $this->Paginator->sort('order_fees') ?></th>
        <th scope="col"><?= $this->Paginator->sort('description') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($transeaction['transactions'] as $transe) : ?>
        <tr>
            <td><?= $this->Number->format($transe->id) ?></td>
            <td><?= $this->Number->format($transe->amount) ?></td>
            <td><?= $transe->has('driver') ? $this->Html->link($transe->driver->name, ['controller' => 'Drivers', 'action' => 'view', $transe->driver->id]) : '' ?></td>
            <td><?= $this->Number->format($transe->order->cod) ?></td>
            <td><?= $this->Number->format($transe->order->fees) ?></td>
            <td><?= h($transe->description) ?></td>
            <td><?= h($transe->name) ?></td>
            <td><?= h($transe->created) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php endforeach; ?>

