<?php
declare(strict_types=1);

namespace Stocks\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Stocks\Controller\Component\StocksComponent;

/**
 * Stocks\Controller\Component\StocksComponent Test Case
 */
class StocksComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Stocks\Controller\Component\StocksComponent
     */
    protected $Stocks;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Stocks = new StocksComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Stocks);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
