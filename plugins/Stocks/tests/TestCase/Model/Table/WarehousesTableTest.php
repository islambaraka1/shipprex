<?php
declare(strict_types=1);

namespace Stocks\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Stocks\Model\Table\WarehousesTable;

/**
 * Stocks\Model\Table\WarehousesTable Test Case
 */
class WarehousesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Stocks\Model\Table\WarehousesTable
     */
    protected $Warehouses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Stocks.Warehouses',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Warehouses') ? [] : ['className' => WarehousesTable::class];
        $this->Warehouses = TableRegistry::getTableLocator()->get('Warehouses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Warehouses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
