<?php
declare(strict_types=1);

namespace Stocks\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Stocks\Model\Table\ProductsOrdersTable;

/**
 * Stocks\Model\Table\ProductsOrdersTable Test Case
 */
class ProductsOrdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Stocks\Model\Table\ProductsOrdersTable
     */
    protected $ProductsOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Stocks.ProductsOrders',
        'plugin.Stocks.Products',
        'plugin.Stocks.Orders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductsOrders') ? [] : ['className' => ProductsOrdersTable::class];
        $this->ProductsOrders = TableRegistry::getTableLocator()->get('ProductsOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProductsOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
