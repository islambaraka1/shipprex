<?php
declare(strict_types=1);

namespace Stocks\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Stocks\Model\Table\WarehouseTable;

/**
 * Stocks\Model\Table\WarehouseTable Test Case
 */
class WarehouseTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Stocks\Model\Table\WarehouseTable
     */
    protected $Warehouse;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Stocks.Warehouse',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Warehouse') ? [] : ['className' => WarehouseTable::class];
        $this->Warehouse = TableRegistry::getTableLocator()->get('Warehouse', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Warehouse);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
