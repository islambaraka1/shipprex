<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $product
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Products'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Product'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="row">
    <?php $this->Hooks->do_action('product_single_view_top'); ?>
</div>
<div class="row">
    <div class="btn-group" role="group" aria-label="Basic example">
        <?=$this->Html->link(__('View All stock movements'),'/stocks/transactions/product/'.$product->id ,['class'=>'btn btn-primary']); ?>
        <?=$this->Html->link(__('Add New Stock'),'/stocks/transactions/add/'.$product->id,['class'=>'btn btn-primary']); ?>
        <?=$this->Html->link(__('View All Related Orders'),'/orders/'.$product->id,['class'=>'btn btn-primary']); ?>

    </div>
</div>
<div class="products view large-9 medium-8 columns content">
    <h3><?= h($product->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($product->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Photo') ?></th>
                <td><?= h($product->photo) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Barcode') ?></th>
                <td><?= h($product->barcode) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $product->has('user') ? $this->Html->link($product->user->username, ['controller' => 'Users', 'action' => 'view', $product->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($product->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Reguler Price') ?></th>
                <td><?= $this->Number->format($product->reguler_price) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Min Quantity') ?></th>
                <td><?= $this->Number->format($product->min_quantity) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('In Stock Quantity') ?></th>
                <td><?= $this->Number->format($product->current_stock) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($product->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($product->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($product->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($product->orders)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
                    <th scope="col"><?= __('Receiver Name') ?></th>
                    <th scope="col"><?= __('Receiver Phone') ?></th>
                    <th scope="col"><?= __('Receiver Address') ?></th>
                    <th scope="col"><?= __('City') ?></th>
                    <th scope="col"><?= __('Work Address') ?></th>
                    <th scope="col"><?= __('Cod') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Fees') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($product->orders as $orders): ?>
                <tr>
                    <td><?= h($orders->id) ?></td>
                    <td><?= h($orders->user_id) ?></td>
                    <td><?= h($orders->type) ?></td>
                    <td><?= h($orders->reference) ?></td>
                    <td><?= h($orders->receiver_name) ?></td>
                    <td><?= h($orders->receiver_phone) ?></td>
                    <td><?= h($orders->receiver_address) ?></td>
                    <td><?= h($orders->city) ?></td>
                    <td><?= h($orders->work_address) ?></td>
                    <td><?= h($orders->cod) ?></td>
                    <td><?= h($orders->statues) ?></td>
                    <td><?= h($orders->fees) ?></td>
                    <td><?= h($orders->created) ?></td>
                    <td><?= h($orders->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id ,'plugin'=>null], ['class' => 'btn btn-secondary']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
