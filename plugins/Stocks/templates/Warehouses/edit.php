<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $warehouse
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $warehouse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouse->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Warehouses'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="warehouses form content">
    <?= $this->Form->create($warehouse) ?>
    <fieldset>
        <legend><?= __('Edit Warehouse') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('space');
            echo $this->Form->control('location');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
