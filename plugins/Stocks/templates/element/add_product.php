<?php
    $orderId = isset($this->request->getAttribute('params')['pass'][0])?$this->request->getAttribute('params')['pass'][0]:0;
    $userId = isset($_SESSION['Auth']['User']->id)?$_SESSION['Auth']['User']->id:1;
    ?>
<div class="container">
    <br />
    <div class="card">
        <div class="card-header row">
            <div class="col-md-6">
                Products
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8">
                        <?php
                        echo $this->Form->control('products',[ 'options'=>[1,2,3],'class'=>'select2bs4','label'=>false ,'id'=>'product_list'] );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-primary btn-sm" id="add_product">
                            Add Product
                        </button>
                    </div>
                </div>


            </div>
        </div>
        <table class="table table-bordered table-hover specialCollapse" id="product_table">
            <thead>
            <tr>
                <th class="col-md-1"><small>ID</small></th>
                <th class="col-md-1"><small>barcode</small></th>
                <th class="col-md-2"><small>Name</small></th>
                <th class="col-md-2"><small>Price</small></th>
                <th class="col-md-5"><small>Quantity</small></th>
                <th class="col-md-1"><small>Total</small></th>
                <th class="col-md-1"><small>Action</small></th>
            </tr>
            </thead>
            <tbody>



            </tbody>
        </table>
        <div class="card-footer row">
            <div class="col-md-9">
                <b>Total Price :</b> <span class="total_price_holder"> 0000 </span>
            </div>
            <div class="col-md-3">
                <button class="btn btn-info" id="update_cod">update COD + shipping</button>
            </div>
        </div>
    </div>




</div>


<script>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
$(document).ready(function(){
    //handel edit actions
    if (window.location.href.indexOf("edit") > -1) {
        getCurrentOrderData();
        // get current order products
    }
    $userSelected = $('#user-id').val();
    // alert($userSelected);
    updateUserProductList($userSelected);

    $('#user-id').change(function(){
        $userSelected = $('#user-id').val();
        updateUserProductList($userSelected);

    })
    $('#add_product').click(function(e){
        $product = $('#product_list').val();
        e.preventDefault();
        addProductToTable($product)
    })
    $('#update_cod').click(function (e) {
        e.preventDefault();
        updateCocAndShippingCost();
        updateDescriptionField();
    })


})

function updateInputListners() {
    $('#product_table input').change(function () {
        updatePrices();
    })
}
function updateUserProductList($id){
    if ($id == undefined) {
        return false;
    }
    $.ajax({
        url: "<?=ROOT_URL?>stocks/products/get_user_products/"+$id,
        context: document.body
    }).done(function(data) {
        updateProductList(data)
    });
}

function updateProductList($newData){
    $('#product_list option').remove()
    for (var key in $newData) {
        if ($newData.hasOwnProperty(key)) {
            $('#product_list').append('<option value="'+key+'">'+$newData[key]+'</option>')
        }
    }
}

function getCurrentOrderData(){
    $orderId = "<?=$orderId;?>";
    // alert($orderId);
    $.ajax({
        url: "<?=ROOT_URL?>stocks/products/get_single_order_data/" + $orderId,
        context: document.body
    }).done(function (data) {
        console.log(data.products_orders)
        //update list for user id
        updateUserProductList(data.user_id);
        //add products table rows for each product and price and qty
        data.products_orders.forEach(function(row){
            addProductToTable(row.product_id,row.unit_price,row.quantity);
        })
        // update prices

    });
}
function addProductToTable($productId,price = null, qty = null){
    console.log(checkIfProductAlreadyAdded($productId));
    if(checkIfProductAlreadyAdded($productId) == true){
        alert("found updated the Qty")
        addOneToQty($productId);
    } else {
        $.ajax({
            url: "<?=ROOT_URL?>stocks/products/get_single_product_data/" + $productId,
            context: document.body
        }).done(function (data) {
            console.log(data);
            if(price === null){
                price = data.reguler_price;
            }
            if(qty === null){
                qty = 1;
            }
            $('#product_table tbody').append(
                ' <tr>\n' +
                '                <td class="product_id"><small>' + data.id + '</small></td>\n' +
                '                <td><small><img src="https://barcode.tec-it.com/barcode.ashx?data=' + data.barcode + '&code=&translate-esc=true0" /></small></td>\n' +
                '                <td><small>' + data.name + '</small></td>\n' +
                '                <td><small><input type="number" class="price_input" name="product[' + data.id + '][price]" value="' + price + '" /></small></td>\n' +
                '                <td><small><input type="number" class="qty_input" name="product[' + data.id + '][quantity]" value="'+qty+'" /></small></td>\n' +
                '                <td class="price_total"><small>' + data.reguler_price + '</small></td>\n' +
                '                <td><small>Delete</small></td>\n' +
                '            </tr>\n'
            );
            updateInputListners();
            updateTotalPrice();
        });
    }
}

function addOneToQty($productId){
    $('#product_table tbody tr').each(function () {
        $id = $(this).find('.product_id').text()
        if($productId == parseInt($id)){
            $qty = $(this).find('input.qty_input').val()
            $qty++;
            $(this).find('input.qty_input').val($qty)
            updatePrices();
        }
    })
}
function checkIfProductAlreadyAdded($productId){
    $returner = false;
    $('#product_table tbody tr').each(function () {
        $id = $(this).find('.product_id').text()
        if($productId == parseInt($id)){
            $returner =  true;
        }
    })
    return $returner;
}
function updateQty($productId){

}

function updateCocAndShippingCost() {
    $price = $('.total_price_holder').text()
    $city = $('#city').val();
    console.log($city);
    //TODO create a shipping calculator
    $('#cod').val(parseInt($price));
}

function updateDescriptionField() {
    //TODO
    // Grap all the products tables
    // insert table data inside the description field
    // text should be in this syntex
    /*
        Product_name : [QTY,unit_price],
        Product_name : [QTY,Unit_price]
     */
    $returnedText = "";
    $('#product_table tr').each(function( index ,row  ) {
        if(index > 0){
            console.log(row);
            $name = $(row).find('td:eq(2)').text();
            $qty = $(row).find('td:eq(4) input').val();
            $price = $(row).find('td:eq(3) input').val();
            $returnedText += " " +
                ""+$name+" : ["+$qty+"], \r\n";
        }
    });
    console.log($returnedText);
    $('#package-description').val($returnedText);


}

function updatePrices(){
    $('#product_table tbody tr').each(function () {
        $price = $(this).find('input.price_input').val()
        $qty = $(this).find('input.qty_input').val()
        $total = $price * $qty;
        $qty = $(this).find('td.price_total').text($total);
    })
    updateTotalPrice();
}
function updateTotalPrice(){
    $total = 0 ;
    $('#product_table tbody tr .price_total').each(function () {
        $price = $(this).text();
        $total = parseInt($total) + parseInt($price);
    })
    $(".total_price_holder").text($total);
}

<?php
    if(get_option_value("stocks_block_none_stock_orders") == 1){
        ?>
    $(document).ready(function () {
        $('#product_list').parents('form').submit(function () {
            $total_amount = $(".total_price_holder").text();
            $total_amount = parseInt($total_amount);
            console.log($total_amount);
            var optionsCount = $('#product_list').children('option').length;

            if(optionsCount > 0 && $total_amount == 0){
                alert("You can not submit order with 0 quantity of product");
                return false;
            } else {
                return true;
            }

        });

    });
<?php
    }
?>

<?php $this->Html->scriptEnd(); ?>
</script>

