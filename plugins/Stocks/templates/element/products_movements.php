<?php
$data = $this->request->getData();
?>
<br />
<div class="card">
    <div class="card-header">
        <?php if($print != true){ ?>
        <div class="row">
            <div class="col-md-10">
                <h3>Products per selected duration</h3>
            </div>

            <div class="col-md-2">
                <a  href="<?=ROOT_URL?>/stocks/transactions/print_movements/<?=$data['user_id']?>/<?=$data['warehouse_id']?>/<?=$data['start_date']?>/<?=$data['end_date']?>"
                    target ='_blank' class="btn btn-primary align-self-md-stretch" ><?=__('Print')?> </a>

            </div>
        </div>
        <?php } ?>
    </div>
    <table class="table table-striped" width="100%">
        <thead>
        <tr>
            <th scope="col"><?= __('product name') ?></th>
            <th scope="col"><?= __('Shipped In') ?></th>
            <th scope="col"><?= __('Shipped Out') ?></th>

            <th scope="col"><?= __('Current Stock ') ?></th>
            <?php  foreach($status as $key => $statue ){ ?>
                <th scope="col" style="font-weight: lighter; font-size: 8px;word-break: break-word;"><?= $statue ?></th>
            <?php } ?>
            <th scope="col"><?= __('Total Items') ?></th>

        </tr>
        </thead>
        <tbody>

        <?php
        /*
         *
(
    [64] => Array
        (
            [MoveIn] => 360
            [MoveOut] => 207
            [statues] => Array
                (
                    [Collected] => 207
                )

            [balance] => 153
        )

)
         */
        $start_stock = 0;
        $end_stock = 0;
        $orders_quantity = 0;
        $order_money = 0;
        foreach($products as $pid => $product){

            $start_stock    += $product['product']['start_stock'];
            $end_stock      += $product['product']['end_stock'];
            $start_end_q    =$product['product']['start_stock'] + $product['product']['end_stock'];
            $orders_quantity += $product['product']['orders_quantity'];
            $order_money    += $product['product']['order_money'];
            ?>
        <tr>
            <td scope="col"><?= $product['product']['name'] ?></td>
            <td scope="col"><?= $product['MoveIn'] ?></td>
            <td scope="col"><?= $product['MoveOut'] ?></td>
            <td scope="col"><?= $product['balance'] ?></td>
            <?php
            $total_count = 0;
            foreach($status as $key => $statue ){

                $count = isset($product['statues'][$statue])?$product['statues'][$statue]:0;
                $total_count += $count;
                ?>
                <td scope="col"><?= $count ?></td>
            <?php } ?>
            <td scope="col"><b><?= $total_count ?></b></td>

        </tr>
        <?php } ?>

        <tr style="font-weight: bold">
            <td scope="col"><?= __('Total') ?></td>
            <td scope="col"><?= $start_stock ?></td>
            <td scope="col"><?= $end_stock + $start_stock ?></td>
            <td scope="col"><?= $orders_quantity ?></td>
        </tr>

        </tbody>

    </table>
</div>

