
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= __('id') ?></th>
        <th scope="col"><?= __('warehouse_id') ?></th>
        <th scope="col"><?= __('amount') ?></th>
        <th scope="col"><?= __('product_id') ?></th>
        <th scope="col"><?= __('user_id') ?></th>
        <th scope="col"><?= __('order_id') ?></th>
        <th scope="col"><?= __('type') ?></th>
        <th scope="col"><?= __('created') ?></th>
        <th scope="col"><?= __('modified') ?></th>
        <th scope="col" class="actions"><?php echo $hide_actions == true?'' : __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($transactions as $transaction) : ?>
        <tr>
            <td><?= $this->Number->format($transaction->id) ?></td>
            <td><?= $transaction->has('warehouse') ? $this->Html->link($transaction->warehouse->name, ['controller' => 'Warehouses', 'action' => 'view', $transaction->warehouse->id]) : '' ?></td>
            <td><?= $this->Number->format($transaction->amount) ?></td>
            <td><?= $transaction->has('product') ? $this->Html->link($transaction->product->name, ['controller' => 'Products', 'action' => 'view', $transaction->product->id]) : '' ?></td>
            <td><?= $transaction->has('user') ? $this->Html->link($transaction->user->id, ['controller' => 'Users', 'action' => 'view', $transaction->user->id]) : '' ?></td>
            <td><?= h($transaction->order_id) ?></td>
            <td><?= h($transaction->type) ?></td>
            <td><?= h($transaction->created) ?></td>
            <td><?= h($transaction->modified) ?></td>
            <?php if($hide_actions != true) { ?>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $transaction->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $transaction->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $transaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transaction->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
            <?php } ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
