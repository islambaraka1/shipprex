<div class="container">
    <br />
    <div class="card">
        <div class="card-header row">
            <div class="col-md-6">
                <?=__('Products')?>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8">
                        <?php
                        echo $this->Form->control('products',[ 'options'=>[1,2,3],'class'=>'select2bs4','label'=>false ,'id'=>'product_list'] );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-primary btn-sm" id="add_product">
                            <?=__('Add Product')?>
                        </button>
                    </div>
                </div>


            </div>
        </div>
        <table class="table table-bordered table-hover specialCollapse" id="product_table">
            <thead>
            <tr>
                <th class="col-md-1"><small><?=__('ID')?></small></th>
                <th class="col-md-1"><small><?=__('barcode')?></small></th>
                <th class="col-md-2"><small><?=__('Name')?></small></th>
                <th class="col-md-2"><small><?=__('Price')?></small></th>
                <th class="col-md-5"><small><?=__('Quantity')?></small></th>
                <th class="col-md-1"><small><?=__('Total')?></small></th>
                <th class="col-md-1"><small><?=__('Action')?></small></th>
            </tr>
            </thead>
            <tbody>



            </tbody>
        </table>
        <div class="card-footer row">
            <div class="col-md-9">
                <b>Total Price :</b> <span class="total_price_holder"> 0000 </span>
            </div>
            <div class="col-md-3">
<!--                <button class="btn btn-info" id="update_cod">--><?//=__('update COD + shipping')?><!--</button>-->
            </div>
        </div>
    </div>




</div>


<script>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
$(document).ready(function(){
    $userSelected = $('#user-id').val();
    updateUserProductList($userSelected);

    $('#user-id').change(function(){
        $userSelected = $('#user-id').val();
        updateUserProductList($userSelected);

    })
    $('#add_product').click(function(e){
        $product = $('#product_list').val();
        e.preventDefault();
        addProductToTable($product)
    })
    $('#update_cod').click(function (e) {
        e.preventDefault();
        updateCocAndShippingCost();
    })


})

function updateInputListners() {
    $('#product_table input').change(function () {
        updatePrices();
    })
}
function updateUserProductList($id){
    $.ajax({
        url: "<?=ROOT_URL?>stocks/products/get_user_products/"+$id,
        context: document.body
    }).done(function(data) {
        updateProductList(data)
    });
}

function updateProductList($newData){
    $('#product_list option').remove()
    for (var key in $newData) {
        if ($newData.hasOwnProperty(key)) {
            $('#product_list').append('<option value="'+key+'">'+$newData[key]+'</option>')
        }
    }
}

function addProductToTable($productId){
    console.log(checkIfProductAlreadyAdded($productId));
    if(checkIfProductAlreadyAdded($productId) == true){
        alert("found updated the Qty")
        addOneToQty($productId);
    } else {
        $.ajax({
            url: "<?=ROOT_URL?>stocks/products/get_single_product_data/" + $productId,
            context: document.body
        }).done(function (data) {
            console.log(data);
            $('#product_table tbody').append(
                ' <tr>\n' +
                '                <td class="product_id"><small>' + data.id + '</small></td>\n' +
                '                <td><small><img src="https://barcode.tec-it.com/barcode.ashx?data=' + data.barcode + '&code=&translate-esc=true0" /></small></td>\n' +
                '                <td><small>' + data.name + '</small></td>\n' +
                '                <td><small><input type="number" class="price_input" disabled name="product[' + data.id + '][price]" value="' + data.reguler_price + '" /></small></td>\n' +
                '                <td><small><input type="number" class="qty_input" name="product[' + data.id + '][quantity]" value="1" /></small></td>\n' +
                '                <td class="price_total"><small>' + data.reguler_price + '</small></td>\n' +
                '                <td><small><?=__("Delete")?></small></td>\n' +
                '            </tr>\n'
            );
            updateInputListners();
            updateTotalPrice();
        });
    }
}

function addOneToQty($productId){
    $('#product_table tbody tr').each(function () {
        $id = $(this).find('.product_id').text()
        if($productId == parseInt($id)){
            $qty = $(this).find('input.qty_input').val()
            $qty++;
            $(this).find('input.qty_input').val($qty)
            updatePrices();
        }
    })
}
function checkIfProductAlreadyAdded($productId){
    $returner = false;
    $('#product_table tbody tr').each(function () {
        $id = $(this).find('.product_id').text()
        if($productId == parseInt($id)){
            $returner =  true;
        }
    })
    return $returner;
}
function updateQty($productId){

}

function updateCocAndShippingCost() {
    $price = $('.total_price_holder').text()
    $city = $('#city').val();
    console.log($city);
    //TODO create a shipping calculator
    $('#cod').val(parseInt($price));
}

function updatePrices(){
    $('#product_table tbody tr').each(function () {
        $price = $(this).find('input.price_input').val()
        $qty = $(this).find('input.qty_input').val()
        $total = $price * $qty;
        $qty = $(this).find('td.price_total').text($total);
    })
    updateTotalPrice();
}
function updateTotalPrice(){
    $total = 0 ;
    $('#product_table tbody tr .price_total').each(function () {
        $price = $(this).text();
        $total = parseInt($total) + parseInt($price);
    })
    $(".total_price_holder").text($total);
}

<?php $this->Html->scriptEnd(); ?>
</script>

