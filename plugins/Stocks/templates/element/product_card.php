<div class="">
    <div class="row">
        <div class="col-md-3">
            <img src="<?=$this->FileUploaded->getFile($product)?>" alt="" style="max-height:110px;" class="prodcut_img" />
        </div>
        <div class="col-md-6">
            <h3><?=$product->name?> </h3>
            <br />
            <p><?=$product->description?></p>
        </div>
        <div class="col-md-3">
            </br><img
                src='https://barcode.tec-it.com/barcode.ashx?data=<?=$product->barcode?>&code=Code128&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=72&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&modulewidth=150' alt=''/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped">

                    <tr>
                        <th scope="row"><?= __('Product name') ?></th>
                        <td><?= h($product->name) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Description') ?></th>
                        <td><?= h($product->description) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Product Barcode') ?></th>
                        <td><?= h($product->barcode) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Seller Name') ?></th>
                        <td><?= h($product->user->username) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Product Total') ?></th>
                        <td><?= h($product->current_stock) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Product Price') ?></th>
                        <td><?= h($product->reguler_price) ?></td>
                    </tr>


                </table>
            </div>

        </div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped">

                    <tr>
                        <th scope="row"><?= __('Current Stock Price') ?></th>
                        <td><?= ($product->current_stock * $product->reguler_price ) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Number of related orders') ?></th>
                        <td><?= h($product->number_of_orders) ?></td>
                    </tr>

                    <tr>
                        <th scope="row"><?= __('Orders per day') ?></th>
                        <td><?= h($product->barcode) ?></td>
                    </tr>


                    <tr>
                        <th scope="row"><?= __('Created at ') ?></th>
                        <td><?= h($product->created) ?></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</div>
