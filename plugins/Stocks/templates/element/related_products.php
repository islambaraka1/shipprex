
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= __('product_id') ?></th>
        <th scope="col"><?= __('order_id') ?></th>
        <th scope="col"><?= __('amount') ?></th>
        <th scope="col"><?= __('unit price') ?></th>
        <th scope="col"><?= __('Total price') ?></th>
        <th scope="col"><?= __('created') ?></th>
        <th scope="col"><?= __('modified') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orders as $order) : ?>
        <tr>
            <td><?= $order->has('product') ? $order->product->name : '' ?></td>
            <td><?= $order->has('order') ? $order->order->id : '' ?></td>
            <td><?= $this->Number->format($order->quantity) ?></td>
            <td><?= h($order->unit_price) ?></td>
            <td><?= h($order->total_price) ?></td>
            <td><?= h($order->created) ?></td>
            <td><?= h($order->modified) ?></td>


        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
