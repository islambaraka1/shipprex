<?php
$templateHalf =[
    'inputContainer' => '<div class="form-group col-md-3 {{type}}{{required}}">{{content}}</div>',
];
$user_field_options = [
    'options' => $users,
    'templates'=>$templateHalf];

if($_SESSION['Auth']['User']['group_id'] == 1 ){
    $user_field_options['type'] = 'select';
}else{
    $user_field_options['type'] = 'hidden';
    $user_field_options['value'] = $_SESSION['Auth']['User']['id'];
}
?>
<div class="container">
    <div class="transactions form content">
        <?= $this->Form->create() ?>
        <?php         echo "<div class='row'>"; ?>
        <?= $this->Form->control('user_id',$user_field_options ); ?>
        <?= $this->Form->control('warehouse_id', ['options' => $warehouses ,'type'=>'select' ,'templates'=>$templateHalf]); ?>
        <?= $this->Form->control('start_date',['type'=>'date','templates'=>$templateHalf]); ?>
        <?= $this->Form->control('end_date',['type'=>'date', 'templates'=>$templateHalf]); ?>
        <?php         echo "</div>"; ?>
        <?= $this->Form->control('status',['type'=>'select','multiple' => 'checkbox', 'options' =>  types_for_state]); ?>

        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php if(isset($transactions)){ ?>
<div class="container">
    <?php echo $this->element('Stocks.products_movements',['products' =>$products ,'print'=>false , 'status'=>$status ]); ?>
    <?php echo $this->element('Stocks.transactions_table',['transactions' =>$transactions , 'hide_actions' =>true]); ?>
</div>
<?php } ?>



<script>
    <?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
$(document).ready(function(){
     $('#end-date').change(function(){
         // check for the value of start date
         $startDate = new Date($('#start-date').val());
         $endDate = new Date($('#end-date').val());
         if($endDate > $startDate){

         }else{
             alert('Error in dates');
             $('#start-date').val('');
             $('#end-date').val('');
         }
     })

 })

    <?php $this->Html->scriptEnd(); ?>
</script>
