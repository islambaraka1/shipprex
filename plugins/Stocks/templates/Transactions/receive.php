<div class="container">
    <div class="transactions form content">
        <?= $this->Form->create($transaction) ?>

        <?= $this->Form->control('user_id', ['options' => $users,'type'=>'select','value' => $_SESSION['Auth']['User']['id']]); ?>
        <?= $this->Form->control('warehouse_id', ['options' => $warehouses ,'type'=>'select' ]); ?>
        <?= $this->element('Stocks.add_product_transactions'); ?>
        <?= $this->Form->control('description'); ?>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
