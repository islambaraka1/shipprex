<?php
    $product = [];
    foreach ($items as $key => $item){
        if (!empty($item)){
            foreach($item as $transactions){
                $product_id = $transactions['product_id'];
                if (isset($product[$product_id])){
                    //update quantity
                    $product[$product_id]['product']['quantity'] += $transactions['amount'];
                } else {
                    $product[$product_id]['product'] =  $transactions['product'];
                    $product[$product_id]['product']['quantity'] += $transactions['amount'];
                }
            }
        }
    }
?>
<div class="container">
    <div class="table-responsive">
        <?php if(!empty($product)){ ?>
        <table class="table table-striped">
            <?php foreach ($product as $item): ?>
                <tr>
                    <th scope="row"><?= $item['product']['name'] ?></th>
                    <td><?= abs((int)$item['product']['quantity']) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php }else{ ?>
            <p><?=__("Sorry no items to display")?></p>
        <?php } ?>
    </div>
</div>
