<?php
declare(strict_types=1);

namespace Stocks\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Stocks\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Stocks\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsToMany $Orders
 *
 * @method \Stocks\Model\Entity\Product newEmptyEntity()
 * @method \Stocks\Model\Entity\Product newEntity(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Product get($primaryKey, $options = [])
 * @method \Stocks\Model\Entity\Product findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Stocks\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Product[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stok_products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'photo' => [
                "path" => "webroot{DS}uploads{DS}Products{DS}{field}{DS}{time}{DS}",
            ],
        ]);
//        $this->addBehavior('MultiBranches.MultiBranch',[
//            "options"=>["prefix"=>"stok"]
//        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'UsersManager.Users',
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            'className' => 'Stocks.Transactions',
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'stok_products_orders',
            'className' => 'Orders',
        ]);
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');


        $validator
            ->scalar('barcode')
            ->maxLength('barcode', 255)
            ->requirePresence('barcode', 'create')
            ->notEmptyString('barcode');

        $validator
            ->numeric('reguler_price')
            ->requirePresence('reguler_price', 'create')
            ->notEmptyString('reguler_price');

        $validator
            ->integer('min_quantity')
            ->requirePresence('min_quantity', 'create')
            ->notEmptyString('min_quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
