<?php
declare(strict_types=1);

namespace Stocks\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductsOrders Model
 *
 * @property \Stocks\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \Stocks\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \Stocks\Model\Entity\ProductsOrder newEmptyEntity()
 * @method \Stocks\Model\Entity\ProductsOrder newEntity(array $data, array $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[] newEntities(array $data, array $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder get($primaryKey, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\ProductsOrder[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsOrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stok_products_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            'className' => 'Stocks.Products',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'className' => 'Stocks.Orders',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('quantity')
            ->allowEmptyString('quantity');

        $validator
            ->numeric('unit_price')
            ->allowEmptyString('unit_price');

        $validator
            ->numeric('total_price')
            ->allowEmptyString('total_price');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }
}
