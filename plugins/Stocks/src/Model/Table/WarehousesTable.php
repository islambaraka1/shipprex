<?php
declare(strict_types=1);

namespace Stocks\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Warehouses Model
 *
 * @method \Stocks\Model\Entity\Warehouse newEmptyEntity()
 * @method \Stocks\Model\Entity\Warehouse newEntity(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Warehouse[] newEntities(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Warehouse get($primaryKey, $options = [])
 * @method \Stocks\Model\Entity\Warehouse findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Stocks\Model\Entity\Warehouse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Warehouse[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Warehouse|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Warehouse saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Warehouse[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Warehouse[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Warehouse[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Warehouse[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehousesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stok_warehouse');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('space')
            ->maxLength('space', 50)
            ->requirePresence('space', 'create')
            ->notEmptyString('space');

        $validator
            ->scalar('location')
            ->maxLength('location', 255)
            ->requirePresence('location', 'create')
            ->notEmptyString('location');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        return $validator;
    }

    // function to transfer amount from one warehouse to another
    public function transfer($from, $to, $productId ,$amount)
    {
        $TransactionsTable = TableRegistry::getTableLocator()->get('Stocks.Transactions');
        $product = $TransactionsTable->Products->get($productId);
        $transaction = $TransactionsTable->move_out($productId,$amount,$product->user_id,$from);
        if ($transaction) {
            $transaction = $TransactionsTable->move_in($productId,$amount,$product->user_id,$to);
            if ($transaction) {
                return true;
            }
        }
        return false;
    }
}
