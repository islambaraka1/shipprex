<?php
declare(strict_types=1);

namespace Stocks\Model\Table;

use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Transactions Model
 *
 * @property \Stocks\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \Stocks\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \Stocks\Model\Entity\Transaction newEmptyEntity()
 * @method \Stocks\Model\Entity\Transaction newEntity(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \Stocks\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \Stocks\Model\Entity\Transaction findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Stocks\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Transaction[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Stocks\Model\Entity\Transaction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Transaction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Stocks\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Stocks\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stok_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            'className' => 'Stocks.Products',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'LEFT',
            'className' => 'Orders',
        ]);
        $this->belongsTo('Warehouses', [
            'foreignKey' => 'warehouse_id',
            'joinType' => 'INNER',
            'className' => 'Stocks.Warehouses',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'UsersManager.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('type')
            ->maxLength('type', 50)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }


    public function addOrderTransactions($orderId,$productId,$qty){
        $entity = $this->newEmptyEntity();
        $entity->order_id = $orderId;
        $entity->warehouse_id = get_option_value('stocks_default_warehouse');
        $entity->product_id = $productId;
        // get user id from order data
        $OrderTable = TableRegistry::getTableLocator()->get('Orders');
        $order = $OrderTable->get($orderId);
        $entity->user_id = $order->user_id;
        $entity->amount = -$qty;
        $entity->type = "MoveOut";
        $entity->description = "Amount for order : ".DID($orderId);
        return $this->save($entity);
    }

    public function addOrUpdateOrderTransactions($orderId,$productId,$qty){
        $finder = $this->find()->where(['order_id' =>$orderId , 'product_id'=>$productId,'pending IN' => [1,0]])->first();
        $entity = $finder === null ?  $this->newEmptyEntity() : $finder;
        $entity->order_id = $orderId;
        $entity->warehouse_id = get_option_value('stocks_default_warehouse');
        $entity->product_id = $productId;
        // get user id from order data
        $OrderTable = TableRegistry::getTableLocator()->get('Orders');
        $order = $OrderTable->get($orderId);
        $entity->user_id = $order->user_id;
        $entity->amount = -$qty;
        $entity->type = "MoveOut";
        $entity->description = "Amount for order : ".DID($orderId);
        return $this->save($entity);
    }

    public function getStockOfProductTillDate($productId,$date){
        return  $this->find('all')->where(['product_id'=>$productId,'created <'=>$date])->sumOf('amount');
    }
    public function getStockOfProductAfterDate($productId,$date){
        return  $this->find('all')->where(['product_id'=>$productId,'created >='=>$date])->sumOf('amount');
    }

    public function beforeFind(EventInterface $event, Query $query, \ArrayObject $options, $primary)
    {
        $sql = $query->sql();
        //pending in
        $pendingPattern = '/\b`?pending`?\s+in\b/i';

        if (preg_match($pendingPattern, $sql)) {
            return $query;
        } else {
            return $query->where(['Transactions.pending' => 0]);
        }
    }
    public function get_all_order_transactions($orderId,$contain = []){
        $transes = $this->find()->where(['Transactions.order_id' =>$orderId ,'pending IN' => [1,0]])->contain($contain)->all()->toArray();
        return $transes;
    }

    public function change_pending_for_order($orderID,$pending){
        $transes = $this->get_all_order_transactions($orderID);
        foreach ($transes as $trans){
            $entity = $trans;
            $entity->pending = $pending;
            $this->save($entity);
            unset($entity);
        }
    }
    public function activeTransactions($orderId){
        $this->change_pending_for_order($orderId,0);
        return true;
    }

    public function unActiveTransactions($orderId){
        $this->change_pending_for_order($orderId,1);
        return true;
    }

    public function move_in($productId,$amount,$userId , $warehouseId = 1 ,$description = '' ,$type = 'MoveIn'){
        $entity = $this->newEmptyEntity();
        $entity->warehouse_id = $warehouseId;
        $entity->product_id = $productId;
        $entity->user_id = $userId;
        $entity->amount = $amount;
        $entity->type = $type;
        $entity->pending = 0;
        $entity->description = $description;
        return $this->save($entity);
    }

    public function move_out($productId,$amount,$userId , $warehouseId = 1 ,$description = '' ,$type = 'MoveOut'){
        $entity = $this->newEmptyEntity();
        $entity->warehouse_id = $warehouseId;
        $entity->product_id = $productId;
        $entity->user_id = $userId;
        $entity->amount = -$amount;
        $entity->type = $type;
        $entity->pending = 0;
        $entity->description = $description;
        return $this->save($entity);
    }

}
