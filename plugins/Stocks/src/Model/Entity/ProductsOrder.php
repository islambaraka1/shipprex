<?php
declare(strict_types=1);

namespace Stocks\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductsOrder Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $order_id
 * @property int|null $quantity
 * @property float|null $unit_price
 * @property float|null $total_price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Stocks\Model\Entity\Product $product
 * @property \Stocks\Model\Entity\Order $order
 */
class ProductsOrder extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'order_id' => true,
        'quantity' => true,
        'unit_price' => true,
        'total_price' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'order' => true,
    ];
}
