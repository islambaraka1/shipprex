<?php
declare(strict_types=1);

namespace Stocks\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $photo
 * @property string $barcode
 * @property float $reguler_price
 * @property int $min_quantity
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Stocks\Model\Entity\User $user
 * @property \Stocks\Model\Entity\Order[] $orders
 */
class Product extends Entity
{
    protected $_virtual = ['current_stock','number_of_orders'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'photo' => true,
        'dir' => true,
        'barcode' => true,
        'reguler_price' => true,
        'min_quantity' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'orders' => true,
        'transactions' => true,
    ];
    protected function _getCurrentStock()
    {
        if($this->id != null) {
            $transesModel = TableRegistry::getTableLocator()->get('Stocks.Transactions');
            $balance = $transesModel->find('all')->where(['product_id' => $this->id])->sumOf('amount');
            return $balance;
        }
        return 0;
    }
    protected function _getNumberOfOrders()
    {
        if($this->id != null){
            $productsTable = TableRegistry::getTableLocator()->get('stok_products_orders');
            $balance = $productsTable->find('all')->where(['product_id' => $this->id])->count();
            return $balance;
        }
        return 0;
    }
}
