<?php
declare(strict_types=1);

namespace Stocks\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $id
 * @property int $amount
 * @property int $product_id
 * @property int $user_id
 * @property string $description
 * @property string $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Stocks\Model\Entity\Product $product
 * @property \Stocks\Model\Entity\User $user
 */
class Transaction extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'product_id' => true,
        'user_id' => true,
        'order_id' => true,
        'description' => true,
        'warehouse_id' => true,
        'type' => true,
        'pending' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'user' => true,
    ];
}
