<?php
declare(strict_types=1);

namespace Stocks\Controller\Component;

use Cake\Collection\Collection;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Stocks component
 * this component is used to get the stock of a product
 * and along with this component are responsible for every logical operation of anything related to the Stocks
 */
class StocksComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    var $controller;
    var $TransactionsTable;
    protected $currentData;

    // init the component
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
        $this->TransactionsTable = TableRegistry::getTableLocator()->get('Stocks.Transactions');

    }


    function get_stocks_transactions($user_id , $start_date,$end_date){
       $data =  $this->TransactionsTable->find('all')->contain(['Users','Products','Warehouses','Orders'])->where([
            'Transactions.user_id' => $user_id,
//            'Transactions.created >=' =>  $start_date,
//            'Transactions.created <=' => $end_date
        ])->toArray();
       $this->currentData = $data;
       return $data;
    }
/*
 * @bug : the move in move out statues are not consistent :/
 * we need to develop some other way than consider move in / out as the main source of truth
 * some cases no entry for the in products and no entries for the out products
 */
    function products_insights(){
        if(count($this->currentData) > 0){
            $data = $this->currentData;
            $products = (new Collection($data))->groupBy('product_id')->toArray();

            $products = array_map(function($product){
                $product = (new Collection($product))->groupBy('type')->toArray();
                if(isset($product['MoveIn'])){
                    $product['product'] = (new Collection($product['MoveIn']))->extract('product')->first();
                }else {
                    $product['product'] = (new Collection($product['MoveOut']))->extract('product')->first();
                }
                if(!isset($product['MoveOut'])){
                    $product['MoveOut'] = [];
                }
                $product['statues'] = (new Collection($product['MoveOut']))->filter(function ($item) {
                    // Keep only items that have 'order' key and 'statues' key inside 'order' and 'statues' is not null
                    return isset($item['order']) && isset($item['order']['statues']) && !is_null($item['order']['statues']);
                })
                    ->groupBy('order.statues')->map(function ($value, $key) {
                    $countOfAmount = (new Collection($value))->sumOf('amount');
                    //convert negative to positive number
                    $countOfAmount = $countOfAmount * -1;
                    return $countOfAmount;
                })->toArray();
                $product['MoveIn'] = isset($product['MoveIn']) ? (new Collection($product['MoveIn']))->sumOf('amount'):0;
                $product['MoveOut'] = isset($product['MoveOut']) ? (new Collection($product['MoveOut']))->sumOf('amount'):0;
                // convert negative to positive
                $product['MoveOut'] = $product['MoveOut'] * -1;
                $product['balance'] = $product['MoveIn'] - $product['MoveOut'];
                return $product;
            },$products);
            return $products;
        }
    }



    function add_transaction_on_product_creation($entity){

    }

    function check_current_stock_from_transactions($product_id){
        $TransactionsTable = TableRegistry::getTableLocator()->get('Stocks.Transactions');
        $amount = $TransactionsTable->find('all')->where(['Transactions.product_id' => $product_id])->sumOf('amount');
        return $amount;
    }

    function transfer_amount_of_product_between_warehouses($from,$to,$productId,$amount){
        $wareHouseTable = TableRegistry::getTableLocator()->get('Stocks.Warehouses');
        $checkAmount = $this->check_current_stock_from_transactions($productId);
        //check if the check amount is greater than the amount to transfer
        if($checkAmount >= $amount){
            //check if the from warehouse is the same as the to warehouse
            if($from != $to){
                if($wareHouseTable->transfer($from, $to, $productId ,$amount)){
                        $this->controller->Flash->success(__('The amount has been transfered successfully'));
                        return true;
                } else {
                    $this->controller->Flash->error(__('The amount could not be transfered. Please, try again.'));
                    return false;
                }
            } else {
                //check if the from warehouse is the same as the to warehouse
                $this->controller->Flash->error(__('You can not transfer from the same warehouse to the same warehouse'));
                return false;
            }
        }
        else {
            //throw exception
            $this->controller->Flash->error(__('You can not transfer more than the current stock, current stock is : '.$checkAmount));
            return false;
        }
    }


}
