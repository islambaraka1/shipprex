<?php
declare(strict_types=1);

namespace Stocks\Controller;

use Cake\ORM\TableRegistry;

/**
 * Transactions Controller
 *
 * @property \Stocks\Model\Table\TransactionsTable $Transactions
 *
 * @method \Stocks\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TransactionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    var $status = [] ;
    public function index()
    {
        $this->paginate = [
            'contain' => ['Warehouses', 'Products', 'Users'],
        ];
        $transactions = $this->paginate($this->Transactions);

        $this->set(compact('transactions'));
    }

    /**
     * @param $id
     */
    public function product($id){
        $this->viewBuilder()->addHelpers(['FileUploaded']);
        $product = $this->Transactions->Products->get($id,['contain'=>'Users']);
        $transactions = $this->paginate($this->Transactions->find('all')
            ->contain(['Warehouses', 'Products', 'Users'])->where(['Transactions.product_id'=>$id]));
        $this->set(compact('transactions','product'));
    }

    public function viewOrderItems($day,$driverId){
        // get this driver orders for this day
        $driverCrossTable = TableRegistry::getTableLocator()->get('DriversOrders');
        $orders = $driverCrossTable->find('all')->where(['driver_id'=>$driverId,'target_day'=>date('Y-m-d', (int) $day)])->toArray();
        $items = [];
        foreach ($orders as $key =>$order){
         $items[] = $this->Transactions->get_all_order_transactions($order->order_id,['Products']);
        }
        $this->set('items', $items);
    }

    public function receive(){
        $users = $this->Transactions->Users->find('list');
        $warehouses = $this->Transactions->Warehouses->find('list');
        $transaction = $this->Transactions->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->getRequest()->getData();
            $products = $data['product'];
            unset ($data['product']);
            foreach ($products as $id => $productData){
                $transaction = $this->Transactions->newEmptyEntity();
                $data['type'] = 'MoveIn';
                $data['pending'] = 0;
                $data['product_id'] = $id;
                $data['amount'] = $productData['quantity'];
                $transaction = $this->Transactions->patchEntity($transaction, $data);
                $this->Transactions->save($transaction);
            }
            return $this->redirect(['action' => 'index']);
        }

        $this->set(compact('users','transaction','warehouses'));

    }

    public function refundStocks(){
        $users = $this->Transactions->Users->find('list');
        $warehouses = $this->Transactions->Warehouses->find('list');
        $transaction = $this->Transactions->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->getRequest()->getData();
            $products = $data['product'];
            unset ($data['product']);
            foreach ($products as $id => $productData){
                $transaction = $this->Transactions->newEmptyEntity();
                $data['type'] = 'MoveOut';
                $data['pending'] = 0;
                $data['product_id'] = $id;
                $num = $productData['quantity'];
                $data['amount'] = -$num;
                $transaction = $this->Transactions->patchEntity($transaction, $data);
                $this->Transactions->save($transaction);
            }
            return $this->redirect(['action' => 'index']);
        }

        $this->set(compact('users','transaction','warehouses'));

    }
    public function invoice(){
        $users = $this->Transactions->Users->find('list');
        $warehouses = $this->Transactions->Warehouses->find('list');
        $transaction = $this->Transactions->newEmptyEntity();
        if ($this->request->is('post')) {
            if(empty($_POST['status'])){
                $this->Flash->error(__('Please select any status to generate report'));
            }else{
                $this->loadComponent('Stocks.Stocks');
                $transactions = $this->Stocks->get_stocks_transactions( $_POST['user_id'],$_POST['start_date'],$_POST['end_date']);
                $this->status = array_combine($_POST['status'],$_POST['status']);
                $_SESSION['print_status'] = array_combine($_POST['status'],$_POST['status']);
                $this->set('transactions',$transactions);
                $this->set('products',$this->Stocks->products_insights());
                $this->set('status',$this->status);
            }
        }
        $this->set(compact('users','transaction','warehouses'));
    }

    function productsInsights($transaction){
        $tableCross = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
        $products = [];
        foreach ($transaction as $transaction){
            if(!isset($products[$transaction->product_id])){
                $products[$transaction->product_id] = [];
                $products[$transaction->product_id]['product'] = $this->Transactions->Products->get($transaction->product_id)->toArray();
                $products[$transaction->product_id]['product']['orders_quantity'] = 0;
                $products[$transaction->product_id]['product']['order_money'] = 0;
                $products[$transaction->product_id]['product']['start_stock'] = $this->Transactions->getStockOfProductTillDate($transaction->product_id,$transaction->created);
                $products[$transaction->product_id]['product']['end_stock'] = $this->Transactions->getStockOfProductAfterDate($transaction->product_id,$transaction->created);
                $products[$transaction->product_id]['product']['status'] = [];
            }
            $products[$transaction->product_id][] = $transaction;
            if($transaction->amount < 0 && $transaction->order_id != null){
                $finder = $tableCross->find('all')->where(['product_id' => $transaction->product_id , 'order_id' =>$transaction->order_id])->first();
                $num =  0 - $transaction->amount;
                $products[$transaction->product_id]['product']['orders_quantity'] = $products[$transaction->product_id]['product']['orders_quantity']  + $num;
                $products[$transaction->product_id]['product']['order_money'] = $products[$transaction->product_id]['product']['order_money']  + $finder->total_price;

                $products[$transaction->product_id]['product']['status'][$transaction->order->statues]['qty'] =
                   isset($products[$transaction->product_id]['product']['status'][$transaction->order->statues]['qty'])?
                       $products[$transaction->product_id]['product']['status'][$transaction->order->statues]['qty'] + $num:$num;
                $products[$transaction->product_id]['product']['status'][$transaction->order->statues]['money'] =
                    isset($products[$transaction->product_id]['product']['status'][$transaction->order->statues]['money'])?
                        $products[$transaction->product_id]['product']['status'][$transaction->order->statues]['money']  + $finder->total_price: $finder->total_price;
            }
        }
        return $products;
    }



    public function printMovements($user,$wear , $start,$end){
        $this->viewBuilder()->setLayout('backend/print');
        $users = $this->Transactions->Users->get($user);
        $warehouses = $this->Transactions->Warehouses->get($wear);
        $transaction = $this->Transactions->newEmptyEntity();
            $transactions = $this->Transactions->find('all')->contain(['Users','Products','Warehouses','Orders'])->where([
                'Transactions.user_id' => $user,
                'Transactions.created >=' =>  $start,
                'Transactions.created <=' => $end
            ])->toArray();
        $this->status =  $_SESSION['print_status'] ;
        $this->set('transactions',$transactions);
        $this->set('products',$this->productsInsights($transactions));
        $this->set('status',$this->status);

        $this->set(compact('users','transaction','warehouses','start','end'));
    }



    public function warehouse($id){
        $warehouse = $this->Transactions->Warehouses->get($id);
        $transactions = $this->paginate($this->Transactions->find('all')
            ->contain(['Warehouses', 'Products', 'Users'])->where(['Transactions.warehouse_id'=>$id]));
        $this->set(compact('transactions','warehouse'));
    }
    public function seller($id){
        $user = $this->Transactions->Users->get($id);
        $transactions = $this->paginate($this->Transactions->find('all')
            ->contain(['Warehouses', 'Products', 'Users'])->where(['Transactions.user_id'=>$id]));
        $this->set(compact('transactions','user'));
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => ['Warehouses', 'Products', 'Users'],
        ]);

        $this->set('transaction', $transaction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($productId = null)
    {
        $transaction = $this->Transactions->newEmptyEntity();
        if($productId != null){
            $product = $this->Transactions->Products->get($productId);
            $transaction->product_id = $productId;
            $transaction->user_id = $product->user_id;
        }
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            $transaction->pending = 0;
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect('/stocks/products/view/'.$productId);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $warehouses = $this->Transactions->Warehouses->find('list', ['limit' => 200]);
        $products = $this->Transactions->Products->find('list', ['limit' => 200]);
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'warehouses', 'products', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $warehouses = $this->Transactions->Warehouses->find('list', ['limit' => 200]);
        $products = $this->Transactions->Products->find('list', ['limit' => 200]);
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'warehouses', 'products', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
