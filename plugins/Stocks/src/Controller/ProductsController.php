<?php
declare(strict_types=1);

namespace Stocks\Controller;

/**
 * Products Controller
 *
 * @property \Stocks\Model\Table\ProductsTable $Products
 *
 * @method \Stocks\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        //if user is customer return only his products
        if($this->Auth->user('group_id') == '3'){
            $this->paginate = [
                'contain' => ['Users'],
                'conditions' => ['Products.user_id' => $this->Auth->user('id')]
            ];
        }

        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Users', 'Orders','Transactions'],
        ]);

        $this->set('product', $product);
    }

    /**
     * @param null $id
     */
    public function orders($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Users', 'Orders','Transactions'],
        ]);

        $this->set('product', $product);
    }

    /**
     * @param $user
     * @return \Cake\Http\Response|null
     */
    public function getUserProducts ($user){
        $this->disableAutoRender();
            $products = $this->Products->find('list')->where(['user_id'=>$user])->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode((array) $products));
    }

    /**
     * @param $id
     * @return \Cake\Http\Response|null
     */
    public function getSingleProductData ($id){
        $this->disableAutoRender();
        $products = $this->Products->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($products));
    }

    /**
     * @param $id
     * @return \Cake\Http\Response|null
     */
    public function getSingleOrderData ($id){
        $this->disableAutoRender();
        $this->Products->Orders->addAssociations(['hasMany' => ['Stocks.ProductsOrders']]);
        $products = $this->Products->Orders->get($id,['contain'=>['ProductsOrders','ProductsOrders.Products']]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($products));
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEmptyEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $users = $this->Products->Users->find('list', ['limit' => 200]);
//        $orders = $this->Products->Orders->find('list', ['limit' => 200]);
        $this->set(compact('product', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Orders'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $users = $this->Products->Users->find('list', ['limit' => 200]);
//        $orders = $this->Products->Orders->find('list', ['limit' => 200]);
        $this->set(compact('product', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
