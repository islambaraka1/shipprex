<?php
use Cake\Core\Configure;

Configure::write('Settings.Stocks', []);
easy_new_setting('Settings.Stocks','out_statues','Which statues to count the product out',"checkbox",json_encode([]),types_for_state);
easy_new_setting('Settings.Stocks','in_statues','Which statues to count the product Back to stock',"checkbox",json_encode([]),types_for_state);
easy_new_setting('Settings.Stocks','count_collected_only','Count only collected orders as out in the invoices',
    "on_off",0);
easy_new_setting('Settings.Stocks','allow_negative_stock','Allow product Stock to have a negative number (not recommended)',"on_off",0);
easy_new_setting('Settings.Stocks','product_excel_template','Link for the product excel ',"text_input",'',);

$wearTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Stocks.Warehouses');
$wearhouses = $wearTable->find('list')->toArray();
easy_new_setting('Settings.Stocks','stocks_default_warehouse','what is the default warehouse to be used ',"select",null,$wearhouses,false,0);
easy_new_setting('Settings.Stocks','stocks_block_none_stock_orders','Block stock available users form none stock orders  ',"on_off",0);

