<?php

use Cake\ORM\TableRegistry;

$hooks = \App\Hooks\Hooks::getInstance();
if (!function_exists('order_add_form_before_description')) {
    $hooks->add_action('order_add_form_before_description', 'order_add_form_before_description');
    function order_add_form_before_description()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        echo $hooks->currentView->element('Stocks.add_product');
    }
}

if (!function_exists('add_driver_view_button')) {
    $hooks->add_action('driver_view_daily_buttons', 'add_driver_view_button');
    function add_driver_view_button($day)
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        $driverId = $hooks->currentView->getRequest()->getParam('pass')[0] ;
       echo $hooks->currentView->Html->link(_('View items'),"#",
           ['class'=>"btn btn-primary align-self-md-end modal-opener",
               "data-modalName" => __("Reheated Stock"),
               "data-url" => ROOT_URL."stocks/transactions/view-order-items/$day/$driverId"]);
    }
}


if (!function_exists('saveOrderProducts')) {
    $hooks->add_action('excel_single_product_saved', 'saveOrderProducts');
    function saveOrderProducts($entity,$record)
    {
       for($x=1;$x<=10;$x++){
           $fieldName = 'product_'.$x;
           if(isset($record[$fieldName])){
               if(!isset($record[$fieldName.'_qty']))
                   $record[$fieldName.'_qty'] = 0;
               if(!isset($record[$fieldName.'_cost']))
                   $record[$fieldName.'_cost'] = 0;
               save_order_data($entity->id,$entity->user_id,$record[$fieldName],$record[$fieldName.'_qty'],$record[$fieldName.'_cost']);
           }
       }
    }
}

if (!function_exists('orders_view_under_details_order_stock')) {
    $hooks->add_action('orders_view_under_details', 'orders_view_under_details_order_stock');
    function orders_view_under_details_order_stock()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        echo "<h3> ".__('Order Items')."</h3>";
        $table = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
        $orderId = $hooks->currentView->get('order')->id;
        $related = $table->find('all')->contain(['Orders','Products'])->where(['ProductsOrders.order_id'=>$orderId]);
        echo $hooks->currentView->element('Stocks.related_products',['orders'=>$related]);
    }
}

function save_order_data($order_id,$userId,$name,$qty,$price){

    //need updates
    $product = findProductIdByName($name,$userId);
    if(isset($product->id)){
        $table = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
        $entity = $table->newEmptyEntity();
        $entity->product_id = $product->id;
        $entity->order_id = $order_id;
        $entity->quantity = $qty>0?$qty:1;
        $entity->unit_price = $price>0?$price:$product->reguler_price;
        $entity->total_price = $entity->quantity * $entity->unit_price;
        $table->save($entity);
    }else{
        //product not found
    }
}
function findProductIdByName($name,$userId){
    $name = trim($name);
    $table = TableRegistry::getTableLocator()->get('Stocks.Products');
    // find product by name or by barcode
    return $table->find('all')
        ->where(['OR' => [['name LIKE' => '%'.$name.'%'], ['barcode LIKE' => '%'.$name.'%']],'user_id' => $userId])
        ->first();
}


//if (!function_exists('orderUploadExcelFile')) {
//    $hooks->add_filter('Excel_filter_reader_values', 'orderUploadExcelFile');
//    function orderUploadExcelFile($data,$arg1)
//    {
//        //edit the reader file if we want to do so
//
////        $data = json_decode($data);
////       var_dump($data->data);
////       var_dump($arg1);
////       die();
//    }
//}


$tableOrders = TableRegistry::getTableLocator()->get('Orders');

$tableOrders->getEventManager()->on('Model.Order.afterPlace',  function($event, $entity){
    $eventData = $event->getData();
    $cross = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
    if(isset($eventData['data']['product'])){
        foreach ($eventData['data']['product'] as $productId => $data){
            //save order products data
            $entity = $cross->newEmptyEntity();
            $entity->order_id =$eventData['order']->id ;
            $entity->product_id = $productId ;
            $entity->quantity = $data['quantity'] ;
            $entity->unit_price = $data['price'] ;
            $entity->total_price = ($data['quantity'] * $data['price']) ;
            $cross->save($entity);
        }
    }
});

$tableOrders->getEventManager()->on('Model.Order.afterUpdate',  function($event, $entity){
    $eventData = $event->getData();
    $cross = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
    if(!empty($eventData['data']['product'])){
        foreach ($eventData['data']['product'] as $productId => $data){
            //save order products data
            $productRow = $cross->find('all')->where(['order_id'=>$eventData['order']->id , 'product_id'=>$productId])->first();
            if(isset($productRow->id)){
                $entity = $productRow;
                $entity->order_id =$eventData['order']->id ;
                $entity->product_id = $productId ;
                $entity->quantity = $data['quantity'] ;
                $entity->unit_price = $data['price'] ;
                $entity->total_price = ($data['quantity'] * $data['price']) ;
                $cross->save($entity);
            }else{
                $entity = $cross->newEmptyEntity();
                $entity->order_id =$eventData['order']->id ;
                $entity->product_id = $productId ;
                $entity->quantity = $data['quantity'] ;
                $entity->unit_price = $data['price'] ;
                $entity->total_price = ($data['quantity'] * $data['price']) ;
                $cross->save($entity);
            }
        }

    }
});

//Add product Transaction only if it not exiect  and the value for order status match the target statues

$crossTable = TableRegistry::getTableLocator()->get('Stocks.ProductsOrders');
$crossTable->getEventManager()->on('Model.afterSave',  function($event, $entity){
    // add the transaction for this product to assign it on order
    //if this was already exist in the transaction just update dont create
    $transactions = TableRegistry::getTableLocator()->get('Stocks.Transactions');
    $transes = $transactions->get_all_order_transactions($entity->order_id);
    if(count($transes) == 0 ){
        $result = $transactions->addOrderTransactions($entity->order_id,$entity->product_id,$entity->quantity);
    }
    else{
        $result = $transactions->addOrUpdateOrderTransactions($entity->order_id,$entity->product_id,$entity->quantity);
    }
});

$ordersTable = TableRegistry::getTableLocator()->get('Orders');

$ordersTable->getEventManager()->on('Model.UpdatedOrderState',  function($event, $entity)  {
    //
    $out_statues =json_decode( get_option_value('out_statues'));
    $in_statues =json_decode( get_option_value('in_statues'));
    $current_update = $event->getData()['state'];
    $current_id = $event->getData()['order'];
    $transactionsTable = TableRegistry::getTableLocator()->get('Stocks.Transactions');
    if (in_array($current_update, $out_statues)){
        $transactionsTable->activeTransactions($current_id);
    }
    if (in_array($current_update, $in_statues)){
        $transactionsTable->unActiveTransactions($current_id);
    }
});




$hooks->create_new_root_menu('stocks','Stock','#','archive',[1,4,3]);
$hooks->add_sub_menu_item_to_root('stocks','Warehouses','stocks/warehouses',[1,4]);
$hooks->add_sub_menu_item_to_root('stocks','Add Warehouse','stocks/warehouses/add',[1]);
$hooks->add_sub_menu_item_to_root('stocks','Products','stocks/products',[1,4,3]);
$hooks->add_sub_menu_item_to_root('stocks','Add Products','stocks/products/add',[1,4]);
$hooks->add_sub_menu_item_to_root('stocks','receive stock','stocks/transactions/receive',[1,4]);
$hooks->add_sub_menu_item_to_root('stocks','Transactions','stocks/transactions',[1,4]);
$hooks->add_sub_menu_item_to_root('stocks','Stock Movements','stocks/transactions/invoice',[1,4]);
$hooks->add_sub_menu_item_to_root('stocks','Refund Stock','stocks/transactions/refund_stocks',[1,4]);

