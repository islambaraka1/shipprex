<?php

declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;

$hooks = \App\Hooks\Hooks::getInstance();
if (!function_exists('after_add_driver')) {
    function sms_menu()
    {
        $hooks = \App\Hooks\Hooks::getInstance();

        $menu = '';
        $hasTree = "has-treeview";
        $item_icon = "<i class=\"right fas fa-angle-left\"></i>";
        $menu .= '
            <li class="nav-item ' . $hasTree . ' ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            ' . __('SMS Gateway') . '
                            ' . $item_icon . '

                        </p>
                    </a>';
        $menu .= ' <ul class="nav nav-treeview">  ';

        $hooks->do_action('top_of_sms_menu');

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/categories" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Payment Categories') . '</p>
                            </a>
                        </li>';


        $menu .= $hooks->apply_filters('bottom_of_sms_menu', '');

        $menu .= '</ul>';
        echo $menu;
    }
    $hooks->add_action('after_main_menu','sms_menu');

}

TableRegistry::getTableLocator()->get('Orders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $SmsListener = new \Sms\Event\SmsListener();
        $SmsListener->OrdersAfterSave($event,$entity);
    });

$hooks = \App\Hooks\Hooks::getInstance();
$hooks->add_action('action_test','testing_funciton_hello');
function testing_funciton_hello($test){
    echo "Hi : ".$test;
}
$hooks->add_action('Orders_List_afterTable','Orders_List_afterTable');
function Orders_List_afterTable(){
    $view = new \App\View\AppView();
    echo $view->element('backend/datatablejs');
}

$hooks->add_verify('system_show_data_table');

function example_callback( $example ) {
   if(1==2){
       return $example;
   } else
       return "";
}
$hooks->add_filter( 'example_filter', 'example_callback' );
//$hooks->remove_verify('system_show_data_table','system_show_data_table');

