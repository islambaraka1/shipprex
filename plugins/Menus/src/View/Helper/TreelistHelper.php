<?php
declare(strict_types=1);

namespace Menus\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Treelist helper
 */
class TreelistHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    public function generateUls($array){
        $returer = '';
        foreach ($array as $row){
            $row = (object) $row;
            $hasTree = !empty($row->children) ? "has-treeview" : "";
            $item_icon = !empty($row->children) ? "<i class=\"right fas fa-angle-left\"></i>" : "";
            $row->name= trim($row->name);
            $returer .='
            <li class="nav-item '.$hasTree.' ">
                    <a href="'.ROOT_URL.$row->url.'" class="nav-link">
                        <i class="nav-icon fas fa-'.$row->icon.'"></i>
                        <p>
                            '.__(trim($row->name)).'
                            '.$item_icon.'

                        </p>
                    </a>';
            if(!empty($row->children)){
                $returer .=' <ul class="nav nav-treeview">  ';
                foreach ($row->children as $sub){
                    $sub = (object) $sub ;
                    $returer .= '<li class="nav-item">
                            <a href="'.ROOT_URL.$sub->url.'" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>'.__(trim($sub->name)).'</p>
                            </a>
                        </li>';
                }
                $returer .='</ul>';
            }
            $returer .='
            </li>
                    ';



        }
        return $returer;
    }
    public function generateSettingMenu($array){
        $returer = '';
//        $returer .='
//            <li class="nav-item has-treeview">
//                    <a href="'.ROOT_URL.'/settings/config/global_setting/" class="nav-link">
//                        <i class="nav-icon fas fa-globe"></i>
//                        <p>
//                            '.__("Global").'
//                        </p>
//                    </a>';
//
//        $returer .='
//            </li>
//                    ';
        return $returer;
    }
}
