<?php
declare(strict_types=1);

namespace Menus\Model\Table;

use Cake\Collection\Collection;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menuitems Model
 *
 * @property \Menus\Model\Table\MenuitemsTable&\Cake\ORM\Association\BelongsTo $ParentMenuitems
 * @property \Menus\Model\Table\MenusTable&\Cake\ORM\Association\BelongsTo $Menus
 * @property \Menus\Model\Table\MenuitemsTable&\Cake\ORM\Association\HasMany $ChildMenuitems
 *
 * @method \Menus\Model\Entity\Menuitem newEmptyEntity()
 * @method \Menus\Model\Entity\Menuitem newEntity(array $data, array $options = [])
 * @method \Menus\Model\Entity\Menuitem[] newEntities(array $data, array $options = [])
 * @method \Menus\Model\Entity\Menuitem get($primaryKey, $options = [])
 * @method \Menus\Model\Entity\Menuitem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Menus\Model\Entity\Menuitem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Menus\Model\Entity\Menuitem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Menus\Model\Entity\Menuitem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Menus\Model\Entity\Menuitem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Menus\Model\Entity\Menuitem[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menuitem[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menuitem[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menuitem[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class MenuitemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('menuitems');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree', [
            'recoverOrder' => ['sorting' => 'ASC'],
        ]);
        $this->belongsTo('ParentMenuitems', [
            'className' => 'Menus.Menuitems',
            'foreignKey' => 'parent_id',
        ]);
        $this->belongsTo('Menus', [
            'foreignKey' => 'menu_id',
            'joinType' => 'INNER',
            'className' => 'Menus.Menus',
        ]);
        $this->hasMany('ChildMenuitems', [
            'className' => 'Menus.Menuitems',
            'foreignKey' => 'parent_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->requirePresence('url', 'create')
            ->notEmptyString('url');

        $validator
            ->scalar('icon')
            ->maxLength('icon', 255)
            ->allowEmptyString('icon');

        $validator
            ->boolean('badge')
            ->requirePresence('badge', 'create')
            ->notEmptyString('badge');

        $validator
            ->scalar('count_model')
            ->maxLength('count_model', 255)
            ->allowEmptyString('count_model');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['parent_id'], 'ParentMenuitems'));
        $rules->add($rules->existsIn(['menu_id'], 'Menus'));

        return $rules;
    }

    public function fixZeroParentIdToNull(){
        $this->updateAll(['parent_id' => null], ['parent_id' => 0]);
    }
    public function deleteMenuItems($menuId){
        $this->deleteAll(['menu_id' => $menuId]);
    }
    public function saveAllAjax($data,$menuId){
        $sorting = 1;
        //loop through the data and set sorting and parent_id if not set using cakephp collection class
        $data = (new Collection($data))->map(function ($item) use (&$sorting,&$menuId) {
            $item['sorting'] = $sorting++;
            $item['menu_id'] = $menuId;
            $item['badge'] = 0;
            if(!isset($item['parent_id'])){
                $item['parent_id'] = 0;
            }
            //if the item has children, loop through them and set the parent_id, sorting and menu_id
            if(isset($item['children'])){
                $item['children'] = (new Collection($item['children']))->map(function ($child) use (&$sorting,&$menuId,$item) {
                    $child['sorting'] = $sorting++;
                    $child['menu_id'] = $menuId;
                    $child['badge'] = 0;
                    $child['parent_id'] = $item['id'];
                    return $child;
                })->toArray();
            }
            return $item;
        })->toArray();
        // loop through the data and children and save them using the save method
        foreach($data as $item){
            $children = $item['children'];
            unset($item['children']);
            $this->updateOrCreate($item);
            if(isset($children)){
                foreach($children as $child){
                    $this->updateOrCreate($child);
                }
            }
        }

        $this->recover();

        return true;
    }

    public function updateOrCreate($entity){
        if(isset($entity['id']) && $entity['id'] > 0) {
            //update case - get the entity and set the new values
            $entity_to_save = $this->get($entity['id']);
            $entity_to_save = $this->patchEntity($entity_to_save, $entity);
            $this->save($entity_to_save);
        } else {
            //create case - create a new entity
            $entity = $this->newEntity($entity);
            $this->save($entity);
        }
    }
}
