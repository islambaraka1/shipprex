<?php
declare(strict_types=1);

namespace Menus\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menus Model
 *
 * @property \Menus\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \Menus\Model\Table\MenuitemsTable&\Cake\ORM\Association\HasMany $Menuitems
 *
 * @method \Menus\Model\Entity\Menu newEmptyEntity()
 * @method \Menus\Model\Entity\Menu newEntity(array $data, array $options = [])
 * @method \Menus\Model\Entity\Menu[] newEntities(array $data, array $options = [])
 * @method \Menus\Model\Entity\Menu get($primaryKey, $options = [])
 * @method \Menus\Model\Entity\Menu findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Menus\Model\Entity\Menu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Menus\Model\Entity\Menu[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Menus\Model\Entity\Menu|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Menus\Model\Entity\Menu saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Menus\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Menus\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MenusTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('menus');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER',
            'className' => 'Menus.Groups',
        ]);
        $this->hasMany('Menuitems', [
            'foreignKey' => 'menu_id',
            'className' => 'Menus.Menuitems',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('locations')
            ->maxLength('locations', 255)
            ->requirePresence('locations', 'create')
            ->notEmptyString('locations');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }
}
