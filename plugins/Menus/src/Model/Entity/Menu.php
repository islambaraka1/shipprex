<?php
declare(strict_types=1);

namespace Menus\Model\Entity;

use Cake\ORM\Entity;

/**
 * Menu Entity
 *
 * @property int $id
 * @property string $name
 * @property int $group_id
 * @property string $locations
 *
 * @property \Menus\Model\Entity\Group $group
 * @property \Menus\Model\Entity\Menuitem[] $menuitems
 */
class Menu extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'group_id' => true,
        'locations' => true,
        'group' => true,
        'menuitems' => true,
    ];
}
