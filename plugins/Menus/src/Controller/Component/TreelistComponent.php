<?php
declare(strict_types=1);

namespace Menus\Controller\Component;

use App\Hooks\Hooks;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;

/**
 * Treelist component
 */
class TreelistComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    var $heplers = ['Menus.Treelist'];
    protected $_defaultConfig = [];
    function setmenu(){
        $hooks = Hooks::getInstance();
        $this->getController()->viewBuilder()->addHelpers(['Menus.Treelist']);
        $this->Menus = TableRegistry::getTableLocator()->get('Menus.Menus');
        $group = isset($_SESSION['Auth']['User']['group_id'])?$_SESSION['Auth']['User']['group_id']:0;
        $group = $this->getController()->Hooks->apply_filters('menu_group_id',$group);
        $list = $this->Menus->Menuitems->find('threaded')->order('lft')->contain('Menus')->where(['Menus.group_id' => $group])->toArray();
        $additional = $hooks->get_all_menu_array($group);
        if(!empty($additional)){
            $list = array_merge($list,$additional);
        }
        $this->getController()->set('Array_for_menu',$list);
    }
}
