<?php
declare(strict_types=1);

namespace Menus\Controller;

/**
 * Menus Controller
 *
 * @property \Menus\Model\Table\MenusTable $Menus
 *
 * @method \Menus\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MenusController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Groups'],
        ];
        $menus = $this->paginate($this->Menus);

        $this->set(compact('menus'));
    }

    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => ['Groups', 'Menuitems'],
        ]);

        $this->set('menu', $menu);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menu = $this->Menus->newEmptyEntity();
        if ($this->request->is('post')) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $groups = $this->Menus->Groups->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'groups'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $groups = $this->Menus->Groups->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'groups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->Menus->Menuitems->deleteMenuItems($id);
            $this->Flash->success(__('The menu has been deleted.'));
        } else {
            $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function duplicate($id = null): \Cake\Http\Response|null
    {
        $this->Menus->Menuitems->fixZeroParentIdToNull();
        $menu = $this->Menus->get($id);
        $newMenu = $this->Menus->newEmptyEntity();
        $newMenu = $this->Menus->patchEntity($newMenu, $menu->toArray());
        $newMenu->name = $newMenu->name . ' - Copy';
        $newMenu->id = null;
        if ($this->Menus->save($newMenu)) {
            $this->clone_root_menuItems($menu->id, $newMenu->id);
            $this->Flash->success(__('The menu has been duplicated.'));
        } else {
            $this->Flash->error(__('The menu could not be duplicated. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    private function clone_root_menuItems($oldMenu,$newMenu){
        //condition for Menuitems.parent_id is null or 0

        $menuItems = $this->Menus->Menuitems->find('all', [
            'conditions' => ['Menuitems.menu_id' => $oldMenu, 'Menuitems.parent_id IS' => null ],
            'contain' => ['ChildMenuitems']
        ]);
        foreach ($menuItems as $menuItem) {
            $newMenuItem = $this->Menus->Menuitems->newEmptyEntity();
            $newMenuItem = $this->Menus->Menuitems->patchEntity($newMenuItem, $menuItem->toArray());
            unset($newMenuItem->child_menuitems);
            $newMenuItem->id = null;
            $newMenuItem->menu_id = $newMenu;
            $newMenuItem->parent_id = null;
            $this->Menus->Menuitems->save($newMenuItem);
            $this->clone_menuItems($menuItem,$newMenuItem);
        }
    }

    private function clone_menuItems(mixed $menuItem, \Menus\Model\Entity\Menuitem|\Cake\Datasource\EntityInterface $newMenuItem)
    {
        if (isset($menuItem->child_menuitems)) {
            foreach ($menuItem->child_menuitems as $childMenuItem) {
                $newChildMenuItem = $this->Menus->Menuitems->newEmptyEntity();
                $newChildMenuItem = $this->Menus->Menuitems->patchEntity($newChildMenuItem, $childMenuItem->toArray());
                $newChildMenuItem->id = null;
                $newChildMenuItem->menu_id = $newMenuItem->menu_id;
                $newChildMenuItem->parent_id = $newMenuItem->id;
                unset($newChildMenuItem->lft);
                unset($newChildMenuItem->rght);
                $this->Menus->Menuitems->save($newChildMenuItem);
            }
        } else {
            return true;
        }
    }
}
