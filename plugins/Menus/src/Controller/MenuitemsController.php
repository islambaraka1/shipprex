<?php
declare(strict_types=1);

namespace Menus\Controller;

use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use ReflectionClass;
use ReflectionMethod;

/**
 * Menuitems Controller
 *
 * @property \Menus\Model\Table\MenuitemsTable $Menuitems
 *
 * @method \Menus\Model\Entity\Menuitem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MenuitemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    var $heplers = ['Treelist'];
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentMenuitems', 'Menus'],
        ];
        $menuitems = $this->paginate($this->Menuitems);

        $this->set(compact('menuitems'));
    }

    public function tree($menu = null){
        $conditions = [];
        if($menu){
            $conditions = ['Menuitems.menu_id' => $menu];
        }
        $list = $this->Menuitems->find('threaded')->order('lft')->where($conditions)->toArray();
        $this->set('arr',$list);
        // provide the view with all menus avaliable in the system
        $menus = $this->Menuitems->Menus->find('all')->contain('Groups')->toArray();
        //convert $menus to list of options [id => name + group name] using cakephp's collection
        $menus = (new Collection($menus))->combine('id', function($menu){
            return $menu->name . ' (' . $menu->group->name . ')';
        })->toArray();
        $this->set(compact('menus'));
        //provide the system with all groups in the system
        $groupsTable = TableRegistry::getTableLocator()->get('Groups');
        $groups = $groupsTable->find('list', ['limit' => 200]);
        $this->set(compact('groups'));
        $this->set('menu_id',$menu);
    }


    public function saveSingleMenuItem(){
        //if request is post then save the data
        if($this->request->is('post')){
            $data = $this->request->getData();
            $data['badge'] = 0;
            $data['sorting'] = 100;
            $menuitem = $this->Menuitems->newEmptyEntity();
            $menuitem = $this->Menuitems->patchEntity($menuitem, $data);
            if($this->Menuitems->save($menuitem)){
                $this->set('menuitem',$menuitem);
            }else{
                $this->Flash->error(__('The menuitem could not be saved. Please, try again.'));
            }
        }
    }


    public function deleteSingleMenuItem() {
        $id = $this->request->getData('menu_item_id');
        // Find the menu item to delete
        $menuitem = $this->Menuitems->get($id);
        if ($this->Menuitems->delete($menuitem)) {
            $result = [
                'status' => 'success',
                'message' => __('The menu item has been deleted.')
            ];
        } else {
            $result = [
                'status' => 'error',
                'message' => __('The menu item could not be deleted. Please, try again.')
            ];
        }
        //return the result as json
        return $this->response->withType('application/json')->withStringBody(json_encode($result));
    }

    /**
     * View method
     *
     * @param string|null $id Menuitem id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menuitem = $this->Menuitems->get($id, [
            'contain' => ['ParentMenuitems', 'Menus', 'ChildMenuitems'],
        ]);

        $this->set('menuitem', $menuitem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menuitem = $this->Menuitems->newEmptyEntity();
        if ($this->request->is('post')) {
            $menuitem = $this->Menuitems->patchEntity($menuitem, $this->request->getData());
            if ($this->Menuitems->save($menuitem)) {
                $this->Flash->success(__('The menuitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menuitem could not be saved. Please, try again.'));
        }
        $parentMenuitems = $this->Menuitems->ParentMenuitems->find('list', ['limit' => 200]);
        $menus = $this->Menuitems->Menus->find('list', ['limit' => 200]);
        $this->set(compact('menuitem', 'parentMenuitems', 'menus'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Menuitem id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menuitem = $this->Menuitems->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menuitem = $this->Menuitems->patchEntity($menuitem, $this->request->getData());
            if ($this->Menuitems->save($menuitem)) {
                $this->Flash->success(__('The menuitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menuitem could not be saved. Please, try again.'));
        }
        $parentMenuitems = $this->Menuitems->ParentMenuitems->find('list', ['limit' => 200]);
        $menus = $this->Menuitems->Menus->find('list', ['limit' => 200]);
        $this->set(compact('menuitem', 'parentMenuitems', 'menus'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Menuitem id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menuitem = $this->Menuitems->get($id);
        if ($this->Menuitems->delete($menuitem)) {
            $this->Flash->success(__('The menuitem has been deleted.'));
        } else {
            $this->Flash->error(__('The menuitem could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getListOfPlugins(){
        // Get the list of plugins from the cakephp app from the plugins folder
        $plugins = scandir(ROOT . DS . 'plugins');
        $plugins = array_diff($plugins, array('.', '..'));
        $plugins = array_values($plugins);
        // push as the first element the core plugin
        array_unshift($plugins, 'Core');
        array_unshift($plugins, 'Select a plugin');
        //return $plugins as json response for cakephp ajax call
        return $this->response->withType('application/json')->withStringBody(json_encode($plugins));
    }
    public function getListOfControllers($pluginName=null){
        //check if plugin name is passed as query string
        if($pluginName==null){
            $pluginName = $this->request->getQuery('plugin');
        }
        if($pluginName != 'Core'){
            // Get the list of controllers from the cakephp app from the plugins folder
            $controllers = scandir(ROOT . DS . 'plugins' . DS . $pluginName . DS . 'src' . DS . 'Controller');
            $controllers = array_diff($controllers, array('.', '..'));
            $controllers = array_diff($controllers, array('Component', 'PagesController.php', 'AppController.php','Controller.zip','backend'));
            $controllers = array_values($controllers);
        } elseif($pluginName == 'Core'){
            // Get the list of controllers from the cakephp app from the plugins folder
            $controllers = scandir(ROOT . DS . 'src' . DS . 'Controller');
            $controllers = array_diff($controllers, array('.', '..'));
            // remove folders that are not controllers
            $controllers = array_diff($controllers, array('Component', 'PagesController.php', 'AppController.php','Controller.zip','backend'));
            $controllers = array_values($controllers);
        }
        //return $controllers without .php extension as json response for cakephp ajax call
        foreach ($controllers as $key => $value) {
            $controllers[$key] = str_replace('.php', '', $value);
        }
        array_unshift($controllers, 'Select a controller');

        //return $controllers as json response for cakephp ajax call
        return $this->response->withType('application/json')->withStringBody(json_encode($controllers));
    }

    /**
     * Get the list of actions from the controller as public methods only (not private or protected).
     *
     * @param  string $pluginName   Name of the plugin.
     * @param  string $controllerName   Name of the controller.
     *
     * @return JSON    JSON response of list of actions.
     */
    public function getListOfActions($pluginName=null,$controllerName=null){
        //check if plugin name and controller name is passed as query string
        if($pluginName==null){
            $pluginName = $this->request->getQuery('plugin');
        }
        if($controllerName==null){
            $controllerName = $this->request->getQuery('controller');
        }
        if($pluginName != 'Core'){
            // Get the list of actions from the controller as public methods only (not private or protected)
            $actions = $this->extractActionsPublic($pluginName . DS .  'Controller' . DS . $controllerName);
        } else {
            // Get the list of actions from the controller as public methods only (not private or protected)
            $actions = $this->extractActionsPublic( 'App\Controller' . DS . $controllerName);
        }
        array_unshift($actions, 'Select a action');
        //return $actions as json response for cakephp ajax call
        return $this->response->withType('application/json')->withStringBody(json_encode($actions));
    }
    /**
     * Extract the list of actions from the specified plugin and controller.
     *
     * @param  string $className   Name of the class.
     *
     * @return array   List of actions.
     */
    private function extractActionsPublic($className){
        $controllerName = $className;
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results = [];
        $ignoreList = ['beforeRender','beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                array_push($results, $action->name);
            }
        }
        return $results;
    }

    /**
     * Make necessary changes to the classname so it can be used with the ReflectionClass.
     *
     * @param  string $classname   Name of the class.
     *
     * @return string   Modified classname.
     */
    private function handelClassName($classname){
        $classname = str_replace('/','\\',$classname);
        $classname = str_replace('\\src','',$classname);
        $classname = str_replace('\\plugins\\','',$classname);
        return $classname;
    }
    public function saveFullMenuObject(){
        //if request is post and ajax
        if ($this->request->is('post') && $this->request->is('ajax')) {
            //get the menu object from the request
            $menuObject = $this->request->getData('data.menuObject');
            $this->Menuitems->saveAllAjax($menuObject,$this->request->getData('data.menu_id'));
            return $this->response->withType('application/json')->withStringBody(json_encode('success'));
        } else {
            //return error message as json response for cakephp ajax call
            return $this->response->withType('application/json')->withStringBody(json_encode('error'));
        }
    }

}
