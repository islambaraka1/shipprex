<div class="">
<!--    left side bar section   -->
    <div id="top_bar" class="row">
        <div class="card-header">
            <h3 class="card-title text-center"><?=__('Available menus')?> </h3>
<!--     bootstrap check box to enable sorting        -->
            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 450px;">
<!--                  display a select box with the menus available-->
                    <?= $this->Form->select('menu_id', $menus, ['label' => false, 'class' => 'form-control float-right', 'id' => 'menu_id', 'default' => $menu_id, 'empty' => 'Select a menu']) ?>
                    <div class="input-group-append">
                        <button type="submit" id="load_menu" class="btn btn-default"><i class="fas fa-redo"></i></button>
                    </div>
                    <div class="input-group-append">
                        <button type="button" id="sort_mode" class="btn btn-default"><i class="fas fa-sort-amount-down"></i></button>
                    </div>
                </div>
<!--                create a toggle for sort mode -->
                <div class="input-group input-group-sm" style="width: 150px;">

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-center">Menu</h3>
                </div>
                <div class="card-body">
                    <div class="create_menu">
                        <p><?=__('Use this form to add a new menu item to the selected menu')?>:</p>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->Form->control('name',['placeholder'=>'name','label'=>false ,'value'=>''])?>
                                <?= $this->Form->control('icon',['placeholder'=>'icon','value'=>'','label'=>false ,'class'=>'icon-picker'])?>
                            </div>
                            <div class="col-md-10">
                                <?= $this->Form->control('url',['placeholder'=>'Url','label'=>false,'value'=>'' , 'id'=>'main_url_left'])?>
                            </div>
                            <div class="col-md-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-s" data-toggle="modal" data-change="main_url_left" data-target="#link_builder">
                                    <i class="fas fa-link"></i>
                                </button>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->button(__('Create'),['class'=>'btn btn-primary','id'=>'add_menu_item'])?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 ">
            <div class="accordion dd" id="accordionExample">

                <ul class="dd-list" id="root_menu_selector">
                    <?php foreach ($arr as $root_item): ?>
                    <li class="dd-item" data-id="<?=$root_item->id?>">
                        <?= $this->element('Menus.menu_card', ['item' => $root_item]) ?>
                        <?php if (!empty($root_item['children'])): ?>
                        <ul class="dd-list">
                            <?php foreach ($root_item['children'] as $child_item): ?>
                            <li class="dd-item" data-id="<?=$child_item->id?>">
                                <?= $this->element('Menus.menu_card', ['item' => $child_item]) ?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
<!--    cakephp save button -->
    <div class="row">
        <div class="col-md-12">
            <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary float-right','id'=>'main_save']) ?>
        </div>
    </div>
</div>


<?= $this->element('Menus.menu_link_builder') ?>


<style>
    .picker{
        cursor: move;
    }
    .menu-tree-custom ul li{
        list-style: none;

    }
    .menu-tree-custom ul{
        list-style-type: none;
        overflow: hidden;
    }
    .fade:not(.show) {
        opacity: 1!important;
    }

    /**
     * Nestable
     */

    .dd { position: relative; display: block; margin: 0; padding: 0;  list-style: none; font-size: 13px; line-height: 20px; }

    .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
    .dd-list .dd-list { padding-left: 30px; }
    .dd-collapsed .dd-list { display: none; }

    .dd-item,
    .dd-empty,
    .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

    .dd-handle { display: block;  margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd-handle:hover { color: #2ea8e5; background: #fff; }

    .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
    .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
    .dd-item > button[data-action="collapse"]:before { content: '-'; }

    .dd-placeholder,
    .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
    .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
        background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-size: 60px 60px;
        background-position: 0 0, 30px 30px;
    }

    .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
    .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
    .dd-dragel .dd-handle {
        -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
        box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
    }

    /**
     * Nestable Extras
     */

    .nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

    #nestable-menu { padding: 0; margin: 20px 0; }

    #nestable-output,
    #nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

    #nestable2 .dd-handle {
        color: #fff;
        border: 1px solid #999;
        background: #bbb;
        background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
        background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
        background:         linear-gradient(top, #bbb 0%, #999 100%);
    }
    #nestable2 .dd-handle:hover { background: #bbb; }
    #nestable2 .dd-item > button:before { color: #fff; }

    @media only screen and (min-width: 700px) {

        .dd { float: left; width: 100%; }
        .dd + .dd { margin-left: 2%; }

    }

    .dd-hover > .dd-handle { background: #2ea8e5 !important; }

    /**
     * Nestable Draggable Handles
     */

    .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd3-content:hover { color: #2ea8e5; background: #fff; }

    .dd-dragel > .dd3-item > .dd3-content { margin: 0; }

    .dd3-item > button { margin-left: 30px; }

    .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
        border: 1px solid #aaa;
        background: #ddd;
        background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:         linear-gradient(top, #ddd 0%, #bbb 100%);
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
    .dd3-handle:hover { background: #ddd; }

    /**
     * Socialite
     */

    .socialite { display: block; float: left; height: 35px; }
</style>
<link rel="stylesheet" href="https://itsjavi.com/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css">

<script>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
    $(document).ready(function() {
        //delete menu item action
        $('.delete-menu').click(function() {
            //show a confirm message to the user
            var r = confirm("Are you sure you want to delete this menu item?");
            if (r == false) {
                return false;
            }
            var menu_item_id = $(this).attr('data-id');
            var url = '<?php echo $this->Url->build(['controller' => 'menuitems', 'action' => 'deleteSingleMenuItem','plugin'=>'Menus']); ?>' ;
            var data = {
                'menu_item_id': menu_item_id,
            }
            // get the .dd-item element with data-id = menu_item_id and remove it
            var toBeRemoved = $('.dd-item[data-id="' + menu_item_id + '"]');
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                success: function(data) {
                    console.log(data);
                    if (data.status == 'success') {
                        $(toBeRemoved).remove();
                    }
                }
            });
        });
        $('.create_menu').find('.icon-picker').iconpicker();
        //add menu item
        $('#add_menu_item').click(function() {
            //send all of the menu item data to save menu item action in menu controller and get the new menu item html and append it to the menu items list
            var url = window.location.href;
            var menu_id = url.substring(url.lastIndexOf('/') + 1);
            var url = '<?php echo $this->Url->build(['controller' => 'menuitems', 'action' => 'saveSingleMenuItem','plugin'=>'Menus']); ?>' ;
            var data = {
                'name': $('.create_menu #name').val(),
                'url': $('.create_menu #main_url_left').val(),
                'icon': $('.create_menu #icon').val(),
                'menu_id': menu_id
            }
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                success: function(response) {
                    $('#root_menu_selector').append(response).find('.icon-picker').iconpicker();

                }
            });
        });
        $('#load_menu').click(function (){
            var menu_id = $('#menu_id').val();
            var url = '<?php echo $this->Url->build(['controller' => 'menuitems', 'action' => 'tree','plugin'=>'Menus']); ?>'+"/"+menu_id;
            // write me a redirect to url code
            window.location.href = url;
            });

        // when the link data-target="#link_builder" is clicked set var current collapse to match the parent collapse id of the clicked link
        var current_collapse = '';
        var change_element = '';
        // the link selector is data-target="#link_builder"
        $('[data-target="#link_builder"]').click(function(){
            current_collapse = $(this).parent().parent().parent().parent().attr('id');
            var cahnge = $(this).attr('data-change');
            if (cahnge != undefined) {
                change_element = cahnge;
            } else {
                change_element = '';
            }
        });
        // $('.icon-picker').iconpicker(); when the collapse is open
        $('.collapse').on('shown.bs.collapse', function () {
            $(this).find('.icon-picker').iconpicker();
        });
        $('#link_builder').on('show.bs.modal', function (event) {
            loadAjaxJsonIntoSelect($('#link_builder #plugin'),'<?= $this->Url->build(['plugin' => 'Menus', 'controller' => 'Menuitems', 'action' => 'getListOfPlugins']) ?>');
            $('#link_builder #plugin').on('change', function (event) {
                loadAjaxJsonIntoSelect($('#link_builder #controller'),'<?= $this->Url->build(['plugin' => 'Menus', 'controller' => 'Menuitems', 'action' => 'getListOfControllers']) ?>?plugin='+$(this).val());
                $('#link_builder #controller').on('change', function (event) {
                    loadAjaxJsonIntoSelect($('#link_builder #action'),'<?= $this->Url->build(['plugin' => 'Menus', 'controller' => 'Menuitems', 'action' => 'getListOfActions']) ?>?plugin='+$('#link_builder #plugin').val()+'&controller='+$(this).val());
                });
            });
        })
        //save new link in the input field
        $('#link_builder #save').on('click', function (event) {
            $url = $('#link_builder #plugin').val()+'/'+$('#link_builder #controller').val()+'/'+$('#link_builder #action').val();
            //remove string Core and Controller from the url
            $url = $url.replace('Core','');
            $url = $url.replace('Controller','');
            //add / in the beginning of the url if it is not there
            if($url.charAt(0) != '/'){
                $url = '/'+$url;
            }
            if (change_element != '') {
                $('#'+change_element).val($url);
            } else {
                $('#'+current_collapse).find('input[name="url"]').val($url);
            }
            //select the current show collapse
            $('.collapse .show').css('border','1px solid green');
            $('#link_builder').modal('hide');
            $('#link_builder #plugin').val('');
            $('#link_builder #controller').val('');
            $('#link_builder #action').val('');
            $('#link_builder #plugin').off('change');
            $('#link_builder #controller').off('change');

        })



        $('#sort_mode').click(function() {
           //add toggle class for the sort_mode btn and check if we are on selected mode or not
              $(this).toggleClass('btn-success');
                if($(this).hasClass('btn-success')) {
                    $('.dd li').find('.card').addClass('dd-handle');
                } else {
                    // destroy the attachmenet of the nestable
                    $('.dd li').find('.card').removeClass('dd-handle');
                }
        });
        // save the menu
        $('#main_save').click(function (){
            var $sort = $('.dd').nestable('serialize');
            var $menuObject = convertUlToJson($('#root_menu_selector'));
            //get menu id from the url as the last / is the menu id
            var url = window.location.href;
            var menu_id = url.substring(url.lastIndexOf('/') + 1);
            var url = '<?php echo $this->Url->build(['controller' => 'menuitems', 'action' => 'saveFullMenuObject','plugin'=>'Menus']); ?>' ;
            var data = {
                'menuObject': $menuObject,
                'menu_id': menu_id
            };
            $.ajax({
               url: url,
               type: 'POST',
               data: {data:data},
               success: function (data) {
                   console.log(data);
               }
            });
        });
        // convert nested list to json
        function convertUlToJson(ul) {
            var result = [];
            // var lis = ul.children('li'); limit the search to the first level only and not the nested ones
            var lis = ul.children('li');
            console.log(lis);
            lis.each(function(index, li) {
                var $li = $(li);
                var children = $li.children('ul');
                //handel the case when the li has no children dont create Item
                var item = {
                    id: $li.attr('data-id'),
                    name: $li.find('#name').val(),
                    url: $li.find('#url').val(),
                    icon: $li.find('#icon').val(),
                };
                if (children.length > 0) {
                    item.children = convertUlToJson(children);
                }
                result.push(item);
            });
            return result;
        }



        $('.dd').nestable({
            listNodeName    : 'ul',
            expandBtnHTML   : false,
            collapseBtnHTML : false,
        });

    });
<?php $this->Html->scriptEnd(); ?>
</script>
