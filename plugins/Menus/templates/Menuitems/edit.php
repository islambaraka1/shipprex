<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $menuitem
 * @var \App\Model\Entity\ParentMenuitem[]|\Cake\Collection\CollectionInterface $parentMenuitems
 * @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $menus
 * @var \App\Model\Entity\ChildMenuitem[]|\Cake\Collection\CollectionInterface $childMenuitems
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menuitem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuitem->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menuitems'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Parent Menuitems'), ['controller' => 'Menuitems', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Parent Menuitem'), ['controller' => 'Menuitems', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menus'), ['controller' => 'Menus', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Menu'), ['controller' => 'Menus', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="menuitems form content">
    <?= $this->Form->create($menuitem) ?>
    <fieldset>
        <legend><?= __('Edit Menuitem') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('url');
            echo $this->Form->control('parent_id', ['options' => $parentMenuitems, 'empty' => true]);
            echo $this->Form->control('lft');
            echo $this->Form->control('rght');
            echo $this->Form->control('menu_id', ['options' => $menus]);
            echo $this->Form->control('icon');
            echo $this->Form->control('badge');
            echo $this->Form->control('count_model');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
