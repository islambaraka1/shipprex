<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $menuitem
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Menuitem'), ['action' => 'edit', $menuitem->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Menuitem'), ['action' => 'delete', $menuitem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuitem->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menuitems'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Menuitem'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Parent Menuitems'), ['controller' => 'Menuitems', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Parent Menuitem'), ['controller' => 'Menuitems', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menus'), ['controller' => 'Menus', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Menu'), ['controller' => 'Menus', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Child Menuitems'), ['controller' => 'Menuitems', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Child Menuitem'), ['controller' => 'Menuitems', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="menuitems view large-9 medium-8 columns content">
    <h3><?= h($menuitem->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($menuitem->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Url') ?></th>
                <td><?= h($menuitem->url) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Parent Menuitem') ?></th>
                <td><?= $menuitem->has('parent_menuitem') ? $this->Html->link($menuitem->parent_menuitem->name, ['controller' => 'Menuitems', 'action' => 'view', $menuitem->parent_menuitem->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Menu') ?></th>
                <td><?= $menuitem->has('menu') ? $this->Html->link($menuitem->menu->name, ['controller' => 'Menus', 'action' => 'view', $menuitem->menu->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Icon') ?></th>
                <td><?= h($menuitem->icon) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Count Model') ?></th>
                <td><?= h($menuitem->count_model) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($menuitem->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Lft') ?></th>
                <td><?= $this->Number->format($menuitem->lft) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Rght') ?></th>
                <td><?= $this->Number->format($menuitem->rght) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($menuitem->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($menuitem->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Badge') ?></th>
                <td><?= $menuitem->badge ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Menuitems') ?></h4>
        <?php if (!empty($menuitem->child_menuitems)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Url') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Lft') ?></th>
                    <th scope="col"><?= __('Rght') ?></th>
                    <th scope="col"><?= __('Menu Id') ?></th>
                    <th scope="col"><?= __('Icon') ?></th>
                    <th scope="col"><?= __('Badge') ?></th>
                    <th scope="col"><?= __('Count Model') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($menuitem->child_menuitems as $childMenuitems): ?>
                <tr>
                    <td><?= h($childMenuitems->id) ?></td>
                    <td><?= h($childMenuitems->name) ?></td>
                    <td><?= h($childMenuitems->url) ?></td>
                    <td><?= h($childMenuitems->parent_id) ?></td>
                    <td><?= h($childMenuitems->lft) ?></td>
                    <td><?= h($childMenuitems->rght) ?></td>
                    <td><?= h($childMenuitems->menu_id) ?></td>
                    <td><?= h($childMenuitems->icon) ?></td>
                    <td><?= h($childMenuitems->badge) ?></td>
                    <td><?= h($childMenuitems->count_model) ?></td>
                    <td><?= h($childMenuitems->created) ?></td>
                    <td><?= h($childMenuitems->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Menuitems', 'action' => 'view', $childMenuitems->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Menuitems', 'action' => 'edit', $childMenuitems->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Menuitems', 'action' => 'delete', $childMenuitems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childMenuitems->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
