<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $menuitems
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Menuitem'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menus'), ['controller' => 'Menus', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Menu'), ['controller' => 'Menus', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('url') ?></th>
        <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('menu_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('icon') ?></th>
        <th scope="col"><?= $this->Paginator->sort('badge') ?></th>
        <th scope="col"><?= $this->Paginator->sort('count_model') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($menuitems as $menuitem) : ?>
        <tr>
            <td><?= $this->Number->format($menuitem->id) ?></td>
            <td><?= h($menuitem->name) ?></td>
            <td><?= h($menuitem->url) ?></td>
            <td><?= $menuitem->has('parent_menuitem') ? $this->Html->link($menuitem->parent_menuitem->name, ['controller' => 'Menuitems', 'action' => 'view', $menuitem->parent_menuitem->id]) : '' ?></td>
            <td><?= $menuitem->has('menu') ? $this->Html->link($menuitem->menu->name, ['controller' => 'Menus', 'action' => 'view', $menuitem->menu->id]) : '' ?></td>
            <td><?= h($menuitem->icon) ?></td>
            <td><?= h($menuitem->badge) ?></td>
            <td><?= h($menuitem->count_model) ?></td>
            <td><?= h($menuitem->created) ?></td>
            <td><?= h($menuitem->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $menuitem->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menuitem->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menuitem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuitem->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('First')) ?>
        <?= $this->Paginator->prev('< ' . __('Previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('Next') . ' >') ?>
        <?= $this->Paginator->last(__('Last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
