<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $menus
 */
?>
<div class="row ">
    <?= $this->Html->link(__('Add Menus'), ['action' => 'add' ,'controller'=>'menus','plugin'=>'Menus'], ['class' => 'btn btn-primary m-1']) ?>
    <?= $this->Html->link(__('Add Menu items '), ['action' => 'add' ,'controller'=>'menuitems','plugin'=>'Menus'], ['class' => 'btn btn-primary m-1']) ?>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('group_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('locations') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($menus as $menu) : ?>
        <tr>
            <td><?= $this->Number->format($menu->id) ?></td>
            <td><?= h($menu->name) ?></td>
            <td><?= $menu->has('group') ? $this->Html->link($menu->group->name, ['controller' => 'Groups', 'action' => 'view', $menu->group->id]) : '' ?></td>
            <td><?= h($menu->locations) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $menu->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menu->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Duplicate'), ['action' => 'duplicate', $menu->id], ['title' => __('duplicate'), 'class' => 'btn btn-primary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}? and all of its menu Items', $menu->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('First')) ?>
        <?= $this->Paginator->prev('< ' . __('Previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('Next') . ' >') ?>
        <?= $this->Paginator->last(__('Last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
