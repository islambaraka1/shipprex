<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $menu
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Menu'), ['action' => 'edit', $menu->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Menu'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menu->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menus'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Menu'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Menuitems'), ['controller' => 'Menuitems', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Menuitem'), ['controller' => 'Menuitems', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="menus view large-9 medium-8 columns content">
    <h3><?= h($menu->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($menu->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Group') ?></th>
                <td><?= $menu->has('group') ? $this->Html->link($menu->group->name, ['controller' => 'Groups', 'action' => 'view', $menu->group->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Locations') ?></th>
                <td><?= h($menu->locations) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($menu->id) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Menuitems') ?></h4>
        <?php if (!empty($menu->menuitems)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Url') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Lft') ?></th>
                    <th scope="col"><?= __('Rght') ?></th>
                    <th scope="col"><?= __('Menu Id') ?></th>
                    <th scope="col"><?= __('Icon') ?></th>
                    <th scope="col"><?= __('Badge') ?></th>
                    <th scope="col"><?= __('Count Model') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($menu->menuitems as $menuitems): ?>
                <tr>
                    <td><?= h($menuitems->id) ?></td>
                    <td><?= h($menuitems->name) ?></td>
                    <td><?= h($menuitems->url) ?></td>
                    <td><?= h($menuitems->parent_id) ?></td>
                    <td><?= h($menuitems->lft) ?></td>
                    <td><?= h($menuitems->rght) ?></td>
                    <td><?= h($menuitems->menu_id) ?></td>
                    <td><?= h($menuitems->icon) ?></td>
                    <td><?= h($menuitems->badge) ?></td>
                    <td><?= h($menuitems->count_model) ?></td>
                    <td><?= h($menuitems->created) ?></td>
                    <td><?= h($menuitems->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Menuitems', 'action' => 'view', $menuitems->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Menuitems', 'action' => 'edit', $menuitems->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Menuitems', 'action' => 'delete', $menuitems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuitems->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
