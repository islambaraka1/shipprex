<div class="card">
    <div class="card-header" id="heading-<?=$item->id?>">
        <div class="row">
            <div class="d-flex w-100">
                <h2 class="mb-0 flex-grow-1">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse-<?=$item->id?>" aria-expanded="false" aria-controls="collapse-<?=$item->id?>">
                        <?=$item->name?>
                    </button>
                </h2>
                <button class="btn btn-danger btn-xs delete-menu"  data-id="<?=$item->id?>" data-name="<?=$item->name?>">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="collapse-<?=$item->id?>" class="collapse" aria-labelledby="heading-<?=$item->id?>" data-parent="#accordionExample">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('name',['label'=>'name','value'=>$item->name])?>
                    <?= $this->Form->control('icon',['label'=>'icon','value'=>$item->icon,'class'=>'icon-picker'])?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('url',['label'=>'Url','value'=>$item->url])?>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#link_builder">
                        Launch
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
