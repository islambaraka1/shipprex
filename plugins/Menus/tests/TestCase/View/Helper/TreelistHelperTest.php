<?php
declare(strict_types=1);

namespace Menus\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Menus\View\Helper\TreelistHelper;

/**
 * Menus\View\Helper\TreelistHelper Test Case
 */
class TreelistHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Menus\View\Helper\TreelistHelper
     */
    protected $Treelist;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Treelist = new TreelistHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Treelist);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
