<?php
declare(strict_types=1);

namespace Menus\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Menus\Controller\Component\TreelistComponent;

/**
 * Menus\Controller\Component\TreelistComponent Test Case
 */
class TreelistComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Menus\Controller\Component\TreelistComponent
     */
    protected $Treelist;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Treelist = new TreelistComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Treelist);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
