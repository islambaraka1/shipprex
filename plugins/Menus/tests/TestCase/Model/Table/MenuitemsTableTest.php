<?php
declare(strict_types=1);

namespace Menus\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Menus\Model\Table\MenuitemsTable;

/**
 * Menus\Model\Table\MenuitemsTable Test Case
 */
class MenuitemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Menus\Model\Table\MenuitemsTable
     */
    protected $Menuitems;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Menus.Menuitems',
        'plugin.Menus.Menus',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Menuitems') ? [] : ['className' => MenuitemsTable::class];
        $this->Menuitems = TableRegistry::getTableLocator()->get('Menuitems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Menuitems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
