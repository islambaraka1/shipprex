
<div class="row " style="display: none">
<?= $this->Html->link(__('New User'), ['action' => 'add'], ['class' => 'btn btn-primary m-1']) ?>
<?= $this->Html->link(__('List Groups'), ['action' => 'index' ,'controller'=>'groups'], ['class' => 'btn btn-primary m-1']) ?>
<?= $this->Html->link(__('New Groups'), ['action' => 'add' ,'controller'=>'groups'], ['class' => 'btn btn-primary m-1']) ?>
<?= $this->Html->link(__('Permissions'), ['action' => 'viewPremissions' ,'controller'=>'users'], ['class' => 'btn btn-primary m-1']) ?>
<?= $this->Html->link(__('Menus'), ['action' => 'index' ,'controller'=>'menus','plugin'=>'Menus'], ['class' => 'btn btn-primary m-1']) ?>
<?= $this->Html->link(__('Menu items '), ['action' => 'index' ,'controller'=>'menuitems','plugin'=>'Menus'], ['class' => 'btn btn-primary m-1']) ?>
</div>

<div class="row">

            <?php
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'yellow',
                'size' => 3,

                'count' => $users_list_full_data['all_users_count'],
                'title' => __('All Users  '),
                'icon' => 'truck',
                'sub_msg' => __('Users from the system creation'),
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'blue',
                'size' => 3,
                'count' => $users_list_full_data['all_users_active'],
                'title' => __('Active Users'),
                'icon' => 'truck',
                'sub_msg' => __('Users with orders last 7 days ') ,
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'cyan',
                'size' => 3,
                'count' => $users_list_full_data['all_users_last_day'],
                'title' => __('Today Active Users '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => __('last 24 hours active users  :'),
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'red',
                'size' => 3,

                'count' => $totalBalance,
                'title' => __('Upcoming Invoice  '),
                'icon' => 'info-circle',
                'sub_msg' => __('Sum Amount of upcoming invoice  '),
            ]);
            //
            ?>
</div>
<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/data_table',[
        'color' => 'green',
        'size' => '6',
        'table_data' => $users_trade_level,
        'title' => __('Users Levels '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Users grouped by orders COD ') ,

    ]);


    ?>

    <section class="col-lg-6 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanv',
            'height' => '400',
            'title' => __('Users Accusations   '),
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'bar',
            'cahrtData' => $users_timeline,
            'scale' =>'Users'
        ]);

        ?>
    </section>
</div>
<table class="table table-striped" id="example1">
    <thead>
    <tr>
        <th scope="col"><?= __('id') ?></th>
        <th scope="col"><?= __('username') ?></th>
        <th scope="col"><?= __('email') ?></th>
        <th scope="col"><?= __('Phone') ?></th>
        <th scope="col"><?= __('Upcoming invoice') ?></th>
        <th scope="col"><?= __('group_id') ?></th>
        <th scope="col"><?= __('created') ?></th>
        <th scope="col"><?= __('modified') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
        <tr>
            <td><?= $this->Number->format($user->id) ?></td>
            <td><?= h($user->username) ?></td>
            <td><?= h($user->email) ?></td>
            <td><?= h($user->phone) ?></td>
            <td><?= $usersBalances[$user->id]['cod_sum'] ?></td>
            <td><?= $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
            <td><?= h($user->created) ?></td>
            <td><?= h($user->modified) ?></td>


            <td class="actions">
                <div class="dropdown" style="display: inline-block">
                    <button id="dLabel" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?=__('Balance')?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dLabel">
                        <?= $this->Html->link(__('Clear Balance'), ['plugin'=>null,'controller'=>'transactions', 'action' => 'cbalance', $user->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                        <?= $this->Html->link(__('View Balance '), ['plugin'=>null,'controller'=>'transactions', 'action' => 'balance', $user->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn btn-secondary']) ?>


                    </div>

                </div>
                <div class="dropdown" style="display: inline-block" >
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Zones'), ['action' => 'zones', $user->id], ['title' => __('Zones'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit Password'), ['action' => 'edit-password', $user->id], ['title' => __('Password'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                        <?php
                        if($user->active == 0){
                            echo $this->Html->link(__('Active account'), ['action' => 'active', $user->id],
                                ['class' => 'dropdown-item btn btn-secondary']);
                        } else {
                            echo $this->Html->link(__('suspend account'), ['action' => 'suspend', $user->id],
                                ['class' => 'dropdown-item btn btn-secondary']);

                        }
                        ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'title' => __('Delete'), 'class' => 'dropdown-item btn btn-danger d-none']) ?>


                    </div>
                </div>
            </td>


        <?php endforeach; ?>
    </tbody>
</table>
<?=$this->element('backend/datatablejs')?>
