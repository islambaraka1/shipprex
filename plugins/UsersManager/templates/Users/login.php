<div class="login-box">
    <div class="login-logo">
        <img src="<?=SETTING_KEY['global']['site_settings_']['site_logo_'];?>" class="" />
        <br />
        <a href="#"><b><?= __("Welcome Back"); ?> <br> <?= __("To Business"); ?>  </a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">
                <?= __('Please enter your username and password') ?>
            </p>

            <?= $this->Form->create() ?>
                    <?= $this->Form->control('email') ?>

                    <?= $this->Form->control('password') ?>

                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                <?= __("Remember Me"); ?>
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <?= $this->Form->button(__('Login')); ?>
                    </div>
                    <!-- /.col -->
                </div>
            <?= $this->Form->end() ?>


            <p class="mb-1">
                <a href="<?=ROOT_URL?>/users-manager/users/forgetpassword"><?= __("I forgot my password"); ?></a>
            </p>
            <p class="mb-0">
                <?=$this->Html->link('Register',['action'=>'register'])?>
<!--                <a href="" class="text-center">Register a new membership</a>-->
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<div class="users form">
    <fieldset>
    </fieldset>

</div>
