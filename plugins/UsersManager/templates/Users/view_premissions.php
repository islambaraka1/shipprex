<?php
    function safeName ($name) {
        return str_replace('\\' , '_' , $name);
    }
function check_permisssion_checker($permissions,$currentPermission){
        foreach ($permissions as $prem){
            if($prem->controller == $currentPermission['controller'] && $prem->action == $currentPermission['action'] && $prem->group->name == $currentPermission['group'] ){
//                var_dump($prem->permission);
                return $prem->permission =="false"?false:true;
            }
        }
        return false;
}
    ?>
<div class="row">

    <div class="col-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <?php
                foreach ($allController as $controller){
                    echo '            <a class="nav-link" id="v-pills-profile-tab"
            data-toggle="pill" href="#v-pills-'.safeName($controller['name']).'" role="tab" aria-controls="v-pills-'.safeName($controller['name']).'"
             aria-selected="false">'.$controller['name'].' </a>
';
                }
            ?>

        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">

            <?php
            foreach ($allController as $controller){
                echo '
                            <div class="tab-pane fade" id="v-pills-'.safeName($controller['name']).'" role="tabpanel" aria-labelledby="v-pills-'.safeName($controller['name']).'-tab">
                                ';
                echo $this->element('UsersManager.user_action_tables',['actions' => $controller['actions'] ,'groups' => $groups ,
                    'controller' => safeName($controller['name']) ]);
                ?>

                        <?php echo '
                            </div>

                ';
            }
            ?>

        </div>
    </div>
</div>
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>

</style>
<?php
echo $this->Html->script('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js', ['block' => 'scriptBottom']);

$this->Html->scriptStart(['block' => 'scriptcode']);
$linkUrl = ROOT_URL .'/users-manager/users/ajaxsave/';
echo "
   $(function() {
   $('.loading-img').hide();
   function updatePremissions(controller,action,group,value,clonElement){
      console.log( action , group , value );
      $.ajax({
          method: 'POST',
          url: '$linkUrl',
          data: { action:action , group_id : group , permission : value ,  controller:controller },
          beforeSend: function(){
            // Show image container
            $(clonElement).parent().parent().find('.loading-img').show();
            
           },
        })
        
          .done(function( msg ) {
            $(clonElement).parent().parent().find('.loading-img').hide();
          });
   }
    $('.premissionCheck').change(function() {
    clonElement = $(this);
    action = $(this).attr('data-action');
    controller = $(this).attr('data-controller');
    value = $(this).prop('checked');
    group = $(this).attr('value');
     updatePremissions( controller , action , group , value ,clonElement);
    })
  })
";
$this->Html->scriptEnd();
?>

