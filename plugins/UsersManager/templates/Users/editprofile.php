<div class="row ">
    <?= $this->Html->link(__('Change Password'), ['action' => 'change-my-password'], ['class' => 'btn btn-primary m-1']) ?>
</div>
<div class="users form content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit Profile') ?></legend>
        <?php
            echo $this->Form->control('id');
            echo $this->Form->control('username');
            // echo $this->Form->control('password');
            echo $this->Form->control('email');
            // echo $this->Form->control('group_id', ['options' => $groups]);
            echo $this->Form->control('full_name');
            echo $this->Form->control('address');
            echo $this->Form->control('city');
            echo $this->Form->control('phone');
                ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
