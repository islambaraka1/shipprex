<div class="users form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Change your password') ?></legend>
        <p><?=__('Please type your old password and new password')?></p>
        <?php
         echo $this->Form->control('old_password',['type'=>'password']);
         echo $this->Form->control('new_password',['type'=>'password']);
         echo $this->Form->control('confirm_password',['type'=>'password']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
