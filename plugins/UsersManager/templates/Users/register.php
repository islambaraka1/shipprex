<div class="login-box">
    <div class="login-logo">
        <img src="<?=SETTING_KEY['global']['site_settings_']['site_logo_'];?>" class="" />
<br />
        <a href="#"><b><?=PROJECT_NAME?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">
                <?= __('Please Enter your data') ?>
            </p>

            <?= $this->Form->create() ?>
            <?= $this->Form->control('username') ?>
            <?= $this->Form->control('full_name') ?>
            <?= $this->Form->control('phone') ?>
            <?= $this->Form->control('address') ?>
            <?= $this->Form->control('city') ?>
            <?= $this->Form->control('email') ?>
            <?= $this->Form->control('password') ?>

            <div class="row">
                <div class="col-8">

                </div>
                <!-- /.col -->
                <div class="col-4">
                    <?= $this->Form->button(__('Register')); ?>
                </div>
                <!-- /.col -->
            </div>
            <?= $this->Form->end() ?>



            <p class="mb-0">
                <?=$this->Html->link(__('Login'),['action'=>'login'])?>
                <!--                <a href="" class="text-center">Register a new membership</a>-->
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<div class="users form">
    <fieldset>
    </fieldset>

</div>
