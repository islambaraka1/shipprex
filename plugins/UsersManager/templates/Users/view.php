<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $user
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Profile Fields'), ['controller' => 'ProfileFields', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Profile Field'), ['controller' => 'ProfileFields', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="row">
    <div class="col-md-6">
        <div class="row">

            <?php
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'yellow',
                'size' => 6,

                'count' => $orders_count,
                'title' => __('Total Orders '),
                'icon' => 'truck',
                'sub_msg' => __('All Orders for this user  '),
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'green',
                'size' => 6,

                'count' => $order_full_counts['total_orders_count'],
                'title' => __('Active Orders '),
                'icon' => 'truck',
                'sub_msg' => __('All Orders for this user  '),
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'blue',
                'size' => 6,
                'count' => $inactive_order_full_counts['total_orders_amount_sum'],
                'title' => __('Orders Sum'),
                'icon' => 'truck',
                'sub_msg' => __('All Orders') ,
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'dark',
                'size' => 6,
                'count' => $inactive_order_full_counts['total_orders_fees_sum'],
                'title' => __('Orders Fees'),
                'icon' => 'truck',
                'sub_msg' => __('All Orders') ,
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'cyan',
                'size' => 6,
                    'count' => $order_full_counts['total_orders_amount_sum'],
                'title' => __('Total UpComing COD '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => __('Active Orders : '),
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'maroon',
                'size' => 6,
                'count' => $order_full_counts['total_orders_fees_sum'],
                'title' => __('Total UpComing Fees '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => __('For Active Orders'),
            ]);

            //
            ?>
        </div>
    </div>
    <?php
    echo $this->element('Utils.backend/widgets/data_table',[
        'color' => 'green',
        'size' => '6',
        'table_data' => $statues_table,
        'title' => __('Order Statues '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('Order Status List ') ,

    ]);


    ?>
</div>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Username') ?></th>
                <td><?= h($user->username) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Password') ?></th>
                <td><?= h($user->password) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Group') ?></th>
                <td><?= $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Login Token') ?></th>
                <td><?= h($user->login_token) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('phone') ?></th>
                <td><?= h($user->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('address') ?></th>
                <td><?= h($user->address) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Full name ') ?></th>
                <td><?= h($user->full_name) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('City') ?></th>
                <td><?= h($user->city) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= h($user->id) ?></td>
            </tr>


            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($user->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Profile Fields') ?></h4>
        <?php if (!empty($user->pfields)): ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Type') ?></th>
                        <th scope="col"><?= __('Options Values') ?></th>
                        <th scope="col"><?= __('Icon') ?></th>
                        <th scope="col"><?= __('Group Id') ?></th>
                        <th scope="col"><?= __('Front Back') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($user->pfields as $profileFields):
//                    pr($profileFields);
                        ?>
                        <tr>
                            <td><?= h($profileFields->id) ?></td>
                            <td><?= h($profileFields->name) ?></td>
                            <td><?= h($profileFields->type) ?></td>
                            <td><?= h($profileFields->_joinData->val) ?></td>
                            <td><?= h($profileFields->icon) ?></td>
                            <td><?= h($profileFields->group_id) ?></td>
                            <td><?= h($profileFields->front_back) ?></td>
                            <td><?= h($profileFields->created) ?></td>
                            <td><?= h($profileFields->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'ProfileFields', 'action' => 'view', $profileFields->id], ['class' => 'btn btn-secondary']) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'ProfileFields', 'action' => 'edit', $profileFields->id], ['class' => 'btn btn-secondary']) ?>
                                <?= $this->Form->postLink( __('Delete'), ['controller' => 'ProfileFields', 'action' => 'delete', $profileFields->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profileFields->id), 'class' => 'btn btn-danger']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
