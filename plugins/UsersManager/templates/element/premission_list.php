<?php

foreach ($groups as $key=> $group):
    $checker =  check_permisssion_checker($permissions,[
            'controller' => $controller,
            'action' => $action,
            'group' => $group,
        ]) ? "checked" : "";
?>
     <label class="checkbox-inline">
        <input class="premissionCheck" type="checkbox" data-action="<?=$action?>"
               data-controller="<?=$controller?>" <?=$checker?> value="<?=$key?>" data-toggle="toggle" data-width="50"> <?=$group?>
         <div class="loading-img">
             Looding
         </div>
    </label><br />
<?php endforeach; ?>
