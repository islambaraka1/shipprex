<?php
    $controller = array_key_first((array)$actions);
?>

<table class="table ">
    <thead>
    <tr>
        <th scope="col">Action</th>
        <th scope="col">Premissions</th>

    </tr>
    </thead>
    <tbody>
    <?php foreach($actions[$controller] as $key => $action): ?>
    <tr>
        <th scope="row"><?=$action?></th>
        <td>
            <?=$this->element('premission_list',['groups' => $groups , 'action' => $action , 'controller' => $controller ]);?>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
