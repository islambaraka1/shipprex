<?php
declare(strict_types=1);

namespace UsersManager\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use UsersManager\Model\Behavior\CutomeFieldBehavior;

/**
 * UsersManager\Model\Behavior\CutomeFieldBehavior Test Case
 */
class CutomeFieldBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \UsersManager\Model\Behavior\CutomeFieldBehavior
     */
    protected $CutomeField;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->CutomeField = new CutomeFieldBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CutomeField);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
