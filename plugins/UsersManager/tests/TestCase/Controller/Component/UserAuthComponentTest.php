<?php
declare(strict_types=1);

namespace UsersManager\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use UsersManager\Controller\Component\UserAuthComponent;

/**
 * UsersManager\Controller\Component\UserAuthComponent Test Case
 */
class UserAuthComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \UsersManager\Controller\Component\UserAuthComponent
     */
    protected $UserAuth;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->UserAuth = new UserAuthComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->UserAuth);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
