<?php
declare(strict_types=1);

namespace UsersManager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;

/**
 * UserAuth component
 */
class UserAuthComponent extends Component
{
    var $exptionList=['isAdmin','isRedirect','isApi','isPage' ];
    //http://localhost:8765//users-manager/users/verfiy/289071
    var $allowedUrls = ['users/login','users/logout','users/register','users/verify','users/forgetpassword','users/resetpass'];
    var $allowMe = false;
    var $logedInGroup;
    var $currentController;
    var $currentAction;
    var $currentPlugin;
    var $configuration = [
        'adminGroup'    => 1,
    ];
    var $Controller;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $components = ['Auth','Front'];
    function beforeFilter(EventInterface $event){
//        $this->Types = TYPES_CUSTOM_FIELD;

    }
    public function startup(Event $event){
        $this->Controller = $this->_registry->getController();

    }

    function loadModel($modelName){
        $this->$modelName = TableRegistry::get($modelName);
    }
    function checkPremissions(){

        $this->Controller = $this->Controller != null ? $this->Controller :  $this->_registry->getController()  ;
        $this->currentController = $this->getController()->getRequest()->getParam('controller');
        $this->currentAction = $this->getController()->getRequest()->getParam('action');
        $this->currentPlugin = $this->getController()->getRequest()->getParam('plugin');

        $this->logedInGroup = $this->Auth->user('group_id') !== null ? $this->Auth->user('group_id') : 0;
        $this->handelRedirect( $this->validatePremissions() );
    }

    function validatePremissions(){
        $premssionsTable = TableRegistry::getTableLocator()->get('UsersManager.Permissions');
        $premssions = $premssionsTable->find()->where(['group_id' => $this->logedInGroup ])->all()->toArray();

        $returner = false;
        foreach($premssions as $pre){
            $preCtrl =explode('\\', $pre->controller);
            if( $preCtrl[0] == $this->currentPlugin && $preCtrl[2] == $this->currentController &&
                $pre->action == $this->currentAction && $pre->permission =="true"){
                $returner = true;
            }

        }

        $returner = $this->exptionRunner($returner);
        return $returner;
    }
    function handelRedirect($state){
        if(!$state){
            $this->Controller->redirect('/users-manager/users/access-denaied/');
            $this->Front->sendError(__('You dont have access to this action .'));
        }
        if(!isset($_SESSION['Auth']['User']['id']) && $state == false){
            $this->Controller->Flash->error('Session timeout');
            $this->Controller->redirect('/users-manager/users/login/');
            header('Location: '.ROOT_URL);
            die();
        }
    }
    function exptionRunner($returner){
        foreach($this->exptionList as $func){
            if($this->$func())
                $returner = true;
        }
        return $returner;
    }

    function isAdmin(){
        return $this->logedInGroup == $this->configuration['adminGroup'] ?true :false;
    }
    function isRedirect(){
        if( $this->currentController == "Users" &&
            $this->currentAction == "accessDenaied" ){
            if(!isset($_SESSION['Auth']['User'])){
                $this->Controller->redirect('/users-manager/users/login' );
                return;
            }
            return true;
        }
        return false;
    }
    function isPage(){
        foreach($this->allowedUrls as $url){
            $url = explode('/',$url) ;
            if(isset($this->currentController)){
                if( strtolower ($this->currentController) == $url[0] &&
                    strtolower ($this->currentAction )==  $url[1] ){
                    return true;
                }
            }
        }
        return false;
    }
    function isApi(){
        return $this->getController()->getRequest()->getParam("plugin") == "Apiv1"  ? true : false;
    }
    function isAllowed(){
        return $this->allowMe;
    }



}
