<?php
declare(strict_types=1);

namespace UsersManager\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property int $group_id
 * @property string $login_token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \UsersManager\Model\Entity\Group $group
 * @property \UsersManager\Model\Entity\Pfield[] $profile_fields
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'email' => true,
        'group_id' => true,
        'phone' => true,
        'address' => true,
        'city' => true,
        'full_name' => true,
        'login_token' => true,
        'email_verfied' => true,
        'active' => true,
        'created' => true,
        'api_access' => true,
        'modified' => true,
        'group' => true,
        'pfields' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password','api_access'
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
