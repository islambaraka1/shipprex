<?php
declare(strict_types=1);

namespace UsersManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Groups Model
 *
 * @property \UsersManager\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $ParentGroups
 * @property \UsersManager\Model\Table\GroupsTable&\Cake\ORM\Association\HasMany $ChildGroups
 * @property \UsersManager\Model\Table\PermissionsTable&\Cake\ORM\Association\HasMany $Permissions
 * @property \UsersManager\Model\Table\PfiledsTable&\Cake\ORM\Association\HasMany $ProfileFields
 * @property \UsersManager\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 *
 * @method \UsersManager\Model\Entity\Group newEmptyEntity()
 * @method \UsersManager\Model\Entity\Group newEntity(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Group[] newEntities(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Group get($primaryKey, $options = [])
 * @method \UsersManager\Model\Entity\Group findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \UsersManager\Model\Entity\Group patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Group[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Group|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\Group saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GroupsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('groups');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ParentGroups', [
            'className' => 'UsersManager.Groups',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('ChildGroups', [
            'className' => 'UsersManager.Groups',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('Permissions', [
            'foreignKey' => 'group_id',
            'className' => 'UsersManager.Permissions',
        ]);
        $this->hasMany('ProfileFields', [
            'foreignKey' => 'group_id',
            'className' => 'UsersManager.ProfileFields',
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'group_id',
            'className' => 'UsersManager.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentGroups'));

        return $rules;
    }
}
