<?php
declare(strict_types=1);

namespace UsersManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \UsersManager\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \UsersManager\Model\Table\PfieldsTable&\Cake\ORM\Association\BelongsToMany $ProfileFields
 *
 * @method \UsersManager\Model\Entity\User newEmptyEntity()
 * @method \UsersManager\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\User get($primaryKey, $options = [])
 * @method \UsersManager\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \UsersManager\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER',
            'className' => 'UsersManager.Groups',
        ]);
        $this->belongsToMany('Pfields', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'profile_field_id',
            'joinTable' => 'pfields_users',
            'className' => 'UsersManager.Pfields',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

//        $validator
//            ->scalar('login_token')
//            ->maxLength('login_token', 255)
//            ->requirePresence('login_token', 'create')
//            ->notEmptyString('login_token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }

    public function check_code($code){
        return $this->findByLoginToken($code)->first();
    }
    public function change_password_reset($code,$newPass){
        $row = $this->check_code($code);
        if(isset($row->id)){
            $row->password = $newPass;
            $row->login_token = '';
            $this->save($row);
            return true;
        }
        return false ;
    }
    public function verify_activate($user){
        $user->login_token = '';
        $user->email_verfied = 1;
        $user->active = 1;
        $this->save($user);
        return true;
    }

    public function updateLoginTokens($user){
        $user->login_token = rand(111111111,9999999999);
        $this->save($user);
        return $user;
    }
    public function generate_api_access_token($user){
        $user->api_access = generateRandomString(128);
        $this->save($user);
        return $user;
    }
}
