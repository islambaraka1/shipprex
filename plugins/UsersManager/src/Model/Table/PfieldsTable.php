<?php
declare(strict_types=1);

namespace UsersManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProfileFields Model
 *
 * @property \UsersManager\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \UsersManager\Model\Table\UsersTable&\Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \UsersManager\Model\Entity\Pfield newEmptyEntity()
 * @method \UsersManager\Model\Entity\Pfield newEntity(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Pfield[] newEntities(array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Pfield get($primaryKey, $options = [])
 * @method \UsersManager\Model\Entity\Pfield findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \UsersManager\Model\Entity\Pfield patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Pfield[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \UsersManager\Model\Entity\Pfield|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\Pfield saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UsersManager\Model\Entity\Pfield[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Pfield[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Pfield[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \UsersManager\Model\Entity\Pfield[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PfieldsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('pfields');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('UsersManager.CutomeField');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'LEFT',
            'className' => 'UsersManager.Groups',
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'profile_field_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'pfields_users',
            'className' => 'UsersManager.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 50)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');





        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }
}
