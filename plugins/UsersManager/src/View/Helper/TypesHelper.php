<?php
declare(strict_types=1);

namespace UsersManager\View\Helper;

use Cake\Collection\Collection;
use Cake\View\Helper;
use Cake\View\View;

/**
 * Types helper
 */
class TypesHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'Model' => 'ProfileFields'
    ];
    public $helpers = ['Html','Form','Utils.Uploader'];
    var $c = 0;
    var $currentName;
    /**
     * @var Helper|void|null
     */
    private $values;
    /**
     * @var Helper|void|null
     */
    private $checkId;

    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->_defaultConfig = $config;
    }

    /*
     * Trace issue : Fix the types
     * issue points :
     *      1- the types are not having values **
     *      2- the types are not built for editing --> save on edit **
     *      3- the types are handling the save structure --> need to pass the structure
     *      4- checkboxes handling
     *      5- on / off elmeent **
     *      6- image upload handling the value of an image
     *      7- color form element
     *      8- Slider form element
     * -------------- Next Gen ------------
     *      1- add icons
     *      2- add styles
     *      3- label icon
     *
     */

    public function renderUserProfileForm($customFields,$types,$values = null){
        $this->values = $values;
        foreach ($customFields as $field){
            $this->currentName = $this->_defaultConfig['Model'] . "." . $this->c . '.' . "";
          $type = $types[$field->type];
            switch ($type){
                case "checkbox":
                    $this->handelCheckBox($field,$type);
                    break;
                case "on_off":
                    $this->handelOnOff($field,$type);
                    break;
                case "image":
                    $this->handelFileUpload($field);
                    break;
                case "file":
                    $this->handelFileUpload($field);
                    break;

                default:
                    $this->handelSimple($field,$type);
            }
            $this->c++;
        }
        return "";
    }
    public function renderFormFields($customFields,$types){
        foreach ($customFields as $field){
            $this->currentName = $this->_defaultConfig['Model'] . "." . $this->c . '.' . "";
          $type = $types[$field->type];
            switch ($type){
                case "checkbox":
                    $this->handelCheckBox($field,$type);
                    break;
                case "image":
                    $this->handelFileUpload($field);
                    break;
                case "file":
                    $this->handelFileUpload($field);
                    break;

                default:
                    $this->handelSimple($field,$type);
            }
            $this->c++;
        }
        return "";
    }

    private function handelSimple($field,$type)
    {
        $id = $this->currentName."id";
        $targetValue = isset($field->stored_val)? $field->stored_val: $this->handelValues($field->id);
        $options = $this->hasOptions($field) ? $this->extractOptionsArray($field) : [];
        echo $this->Form->control($id,['type' => 'hidden' , 'value' => $field->id]);
        echo $this->Form->control($this->currentName."_joinData.val",[
            'label' => $field->name ,
            'type'  => $type,
            'options' => $options,
            'value' => $targetValue
        ]);
    }


    private function extractOptionsArray($field){
        $options = explode('
',$field->options_values);
        foreach ($options as $key => $opt ){
            $options[trim($opt)]=trim($opt);
            unset($options[$key]);
        }
        return $options;

    }
    private function hasOptions($field)
    {
        return $field->options_values == "" ? false : true  ;
    }

    private function handelCheckBox($field, $type)
    {
        $options = $this->hasOptions($field) ? $this->extractOptionsArray($field) : [];
        echo $this->Form->control($this->currentName.".id",['type' => 'hidden' , 'value' => $field->id]);
        echo $this->Form->label($field->name);

        foreach ($options as $key => $opt){
            echo $this->Form->control($this->currentName."_joinData.val[]",[
                'label' => $opt ,
                'type'  => 'checkbox',
                'value' => $opt,
                'hiddenField' => false
            ]);
        }

    }

    private function handelImageUpload($field, $type)
    {
        //TODO impliment handel image upload form element
    }

    private function handelFileUpload($field)
    {
        $targetValue = isset($field->stored_val)? $field->stored_val: $this->handelValues($field->id);
        echo $this->Form->control($this->currentName."id",['type' => 'hidden' , 'value' => $field->id]);
        $this->Uploader->inputUploader(['name'=> $this->currentName."_joinData.val" ,'label' => $field->name ,'current_img' => $targetValue]);
    }

    private function handelValues($fieldId){

//        $this->checkId = $fieldId;
//
//        $collection = new Collection($this->values);
//        $targetField = $collection->filter(function ($value, $key, $iterator) {
//            return $value->id == $this->checkId;
//        });
//        $val = $targetField->first()->_joinData->val;
        return "";
    }

    private function handelOnOff($field, $type)
    {
        $id = $this->currentName."id";
        $name = $this->_defaultConfig['Model'] . "[" . $this->c . '][_joinData][val]' ;
        echo $this->Form->control($id,['type' => 'hidden' , 'value' => $field->id]);
        echo '<div class="custom-control custom-switch">
  <input type="checkbox" name="'.$name.'" class="custom-control-input" id="customSwitch'.$field->id.'">
  <label class="custom-control-label" for="customSwitch'.$field->id.'">'.$field->name.'</label>
</div>';
    }


}
