<?php
namespace UsersManager\Event;

use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\Mailer\Email;


class UserListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Users.afterRegister' => 'afterRegister',
            'Model.Users.afterRemove' => 'afterRemove',
        ];
    }

    public function afterRegister($event, $user)
    {
        Log::write('debug', $user['username'] . ' Register Called.');
        die();
        /*
        $email = new Email('default');
        $email->setFrom(['support@example.com' => 'Example Site'])
            ->setTo('backend@example.com')
            ->setSubject('New User Registration - ' . $user['email'])
            ->send('User has registered in your application');
        */
    }

    public function afterRemove($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }
}

?>
