<div class="col-lg-<?=$size?>">
    <div class="card bg-<?=$color?>" style="max-height: <?=isset($max_height) ?$max_height."px":"auto";?>; overflow-y: scroll;">
        <div class="card-header border-transparent">
            <h3 class="card-title"><?=$title?></h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <div class="table-responsive">

                <?php echo $this->Utils->generateTable($table_data,['thead' => true,'tbody' => true]);
                ?>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
<!--            <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>-->
<!--            <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">--><?//=$sub_msg?><!--</a>-->
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
</div>
