<?php
$size = isset($size)?$size:3;
$url = isset($url)?$url:"#";
?>
<div class="col-lg-<?=$size?> col-6">
    <!-- small box -->
    <div class="small-box bg-<?=$color?>">
        <div class="inner">
            <h3><?=$count?></h3>

            <p><?=$title?></p>
        </div>
        <div class="icon">
            <i class="fa fa-<?=$icon?>"></i>
        </div>
        <a href="<?=$url?>" class="small-box-footer"><?=$sub_msg?> <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
