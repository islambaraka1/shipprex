    <!-- Custom tabs (Charts with tabs)-->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-<?=$icon?> mr-1"></i>
                <?=$title?>
            </h3>
            <div class="card-tools">

            </div>
        </div><!-- /.card-header -->
        <div class="card-body">
                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="revenue-chart"
                     style="position: relative; height:<?=$height?>px;">
                    <canvas id="<?=$canv_id?>" ></canvas>
                </div>

        </div><!-- /.card-body -->
    </div>
    <!-- /.card -->

    <?php
    $this->Html->scriptStart(['block' => 'scriptcode']);
    ?>
        $(document).ready(function (){
            var ctx = document.getElementById('<?=$canv_id?>').getContext('2d');
            ctx.height = <?=$height?>;

            var colors = ['#322e2f50','#e2d810','#d9138a','#12a4d9','cyan'];
            var myChart = new Chart(ctx, {
                type: '<?=$chart_type?>',
                data: {
                    labels: JSON.parse('<?=json_encode( array_keys($cahrtData) )?>') ,
                    datasets: [
                <?php
                $values = array_values($cahrtData);
                $dataset = [];
                foreach ($values as $key => $record){
                    foreach ($record as $type => $value){
                        $dataset[$type][$key] = $value;
                    }
                }
                $count = 0;
                foreach ($dataset as $line => $numbers){
                    ?>
                    {
                    label: '# of <?=$line?> ',
                    data: JSON.parse('<?=json_encode( $numbers )?>'),
                    backgroundColor:colors[<?=$count?>],
                    borderWidth: 3
                    },
                <?php
                $count++;
                }
                ?>

    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        })
    <?php
    $this->Html->scriptEnd();
    ?>

