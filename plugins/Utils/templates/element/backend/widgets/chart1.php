    <!-- Custom tabs (Charts with tabs)-->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-<?=$icon?> mr-1"></i>
                <?=$title?>
            </h3>
            <div class="card-tools">

            </div>
        </div><!-- /.card-header -->
        <div class="card-body">
                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="revenue-chart"
                     style="position: relative; height:48vh;">
                    <canvas id="<?=$canv_id?>" ></canvas>
                </div>

        </div><!-- /.card-body -->
    </div>
    <!-- /.card -->

    <?php
    $this->Html->scriptStart(['block' => 'scriptcode']);
    ?>
        $(document).ready(function (){
            var ctx = document.getElementById('<?=$canv_id?>').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: JSON.parse('<?=json_encode( array_keys($cahrtData) )?>') ,
                    datasets: [
                    {
                        label: '# of <?=$scale?> ',
                        data: JSON.parse('<?=json_encode( array_values($cahrtData) )?>'),
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        })
    <?php
    $this->Html->scriptEnd();
    ?>

