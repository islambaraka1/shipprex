<div class="info-box mb-3 bg-<?=$color?>">
    <span class="info-box-icon"><i class="fas fa-<?=$icon?>"></i></span>

    <div class="info-box-content">
        <span class="info-box-text"><?= __($title)?></span>
        <span class="info-box-number"><?=$count?></span>
    </div>
    <!-- /.info-box-content -->
</div>
