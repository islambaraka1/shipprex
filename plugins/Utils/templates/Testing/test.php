<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @var \App\Model\Entity\Group[]|\Cake\Collection\CollectionInterface $groups
 * @var \App\Model\Entity\ProfileField[]|\Cake\Collection\CollectionInterface $profileFields
 */
?>

    <div class="users form content">
        <?= $this->Form->create([]) ?>
        <fieldset>
            <legend><?= __('Add somthing ') ?></legend>

            <?php
            echo $this->Uploader->inputUploader(['name' => 'islam.baraka.image','folder' => 'baraka_web']);
            echo $this->Uploader->inputUploader(['name' => 'islam.baraka.Cv']);
            echo $this->Uploader->inputUploader(['name' => 'islam.baraka.Test']);
            echo $this->Uploader->inputUploader(['name' => 'islam.baraka.hello']);
            echo $this->Uploader->inputTframe(['name' => 'islam.baraka.hello','model'=>'users']);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
