<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $translation
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Translation'), ['action' => 'edit', $translation->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Translation'), ['action' => 'delete', $translation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $translation->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Translations'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Translation'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="translations view large-9 medium-8 columns content">
    <h3><?= h($translation->id) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Domain') ?></th>
                <td><?= h($translation->domain) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Locale') ?></th>
                <td><?= h($translation->locale) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Singular') ?></th>
                <td><?= h($translation->singular) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Plural') ?></th>
                <td><?= h($translation->plural) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Context') ?></th>
                <td><?= h($translation->context) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Value 0') ?></th>
                <td><?= h($translation->value_0) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Value 1') ?></th>
                <td><?= h($translation->value_1) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Value 2') ?></th>
                <td><?= h($translation->value_2) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($translation->id) ?></td>
            </tr>
        </table>
    </div>
</div>
