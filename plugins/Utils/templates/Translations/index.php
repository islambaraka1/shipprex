<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $translations
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Translation'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>
<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'purple',
        'count' => count(LANG_LIST),
        'title' => __('Number of languages'),
        'icon' => 'globe',
        'sub_msg' => __('List texts'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'dark',
        'count' => $langsTexts,
        'title' => __('Number of phrases'),
        'icon' => 'globe',
        'sub_msg' => __('List texts'),
    ]);
    foreach ($langData as $lang){
        echo $this->element('Utils.backend/widgets/count_block',[
            'color' => 'blue',
            'count' => $lang['un_translated'],
            'title' => __('Untranslated '.$lang['name'].' Text'),
            'icon' => 'language',
            'sub_msg' => __('List texts'),
        ]);
    }


    ?>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('domain') ?></th>
        <th scope="col"><?= $this->Paginator->sort('locale') ?></th>
        <th scope="col"><?= $this->Paginator->sort('singular') ?></th>
        <th scope="col"><?= $this->Paginator->sort('plural') ?></th>
        <th scope="col"><?= $this->Paginator->sort('context') ?></th>
        <th scope="col"><?= $this->Paginator->sort('value_0') ?></th>
        <th scope="col"><?= $this->Paginator->sort('value_1') ?></th>
        <th scope="col"><?= $this->Paginator->sort('value_2') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($translations as $translation) : ?>
        <tr>
            <td><?= $this->Number->format($translation->id) ?></td>
            <td><?= h($translation->domain) ?></td>
            <td><?= h($translation->locale) ?></td>
            <td><?= h($translation->singular) ?></td>
            <td><?= h($translation->plural) ?></td>
            <td><?= h($translation->context) ?></td>
            <td><?= h($translation->value_0) ?></td>
            <td><?= h($translation->value_1) ?></td>
            <td><?= h($translation->value_2) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $translation->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $translation->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $translation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $translation->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('First')) ?>
        <?= $this->Paginator->prev('< ' . __('Previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('Next') . ' >') ?>
        <?= $this->Paginator->last(__('Last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
