
function elfinder($id,$connectorURL){
    $('<div \>').dialog({modal: true, width: "60%", title: "Select your file", zIndex: 9999999999999,
        create: function(event, ui) {
            $(this).elfinder({
                resizable: false,
                url: $connectorURL,
                commandsOptions: {
                    getfile: {
                        oncomplete: 'destroy'
                    }
                },
                ui:['stat','toolbar'],
                uiOptions : {
                    // toolbar configuration
                    toolbar: [
                        ['back', 'forward'],
                        // ['reload'],
                        // ['home', 'up'],
                        ['mkdir', 'mkfile', 'upload'],

                    ],
                },
                getFileCallback: function(file) {
                    console.log(file);
                    document.getElementById('input-'+$id).value = file.url;
                    $('#showimg-'+$id).attr('src' , file.url);
                    $('#showimg-'+$id).show();
                    jQuery('.ui-icon-closethick').click();
                }
            }).elfinder('instance')
        }
    });
}


function InjectElfinder($inputId,$connectorURL){
    $(document).ready(function(){
        $(".image-input").hide();
        $('#elfinder-' + $inputId).click(function(){
            elfinder($inputId,$connectorURL);
        })
    })
}