<?php
declare(strict_types=1);

namespace Utils\Controller;

/**
 * Testing Controller
 *
 *
 * @method \Utils\Model\Entity\Testing[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TestingController extends AppController
{
   public function test(){
       $this->viewBuilder()->addHelpers(['Utils.Uploader']);
   }
}
