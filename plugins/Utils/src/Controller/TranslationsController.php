<?php
declare(strict_types=1);

namespace Utils\Controller;
use Cake\Routing\Router;
use Google\Cloud\Translate\V2\TranslateClient;

/**
 * Translations Controller
 *
 * @property \Utils\Model\Table\TranslationsTable $Translations
 *
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TranslationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $translations = $this->paginate($this->Translations);
        $langsTexts = $this->Translations->find('all')->count();
        $langData = [];
        foreach (LANG_LIST as $key => $lang){
            $langData[$key] = [
                'name' =>$lang,
                'count'=>$this->Translations->find('all')->where(['locale'=>$key])->count(),
                'un_translated'=>$this->Translations->find('all')->where(['locale'=>$key , 'value_0 IS'=> NULL ])->count()
                ];
        }
        $this->set(compact('translations','langsTexts','langData'));
    }

    function changelang($lang){
        $_SESSION['lang'] = $lang;
        $this->redirect(Router::url( $this->referer(), true ) );
    }

    public function autotranslate($lang){
        $this->disableAutoRender();
        $translate = new TranslateClient([
            'key' => 'AIzaSyBmnTMfIF6gnMSedCK2O1o0EhOZQs8cKII'
        ]);
        $texts = $this->Translations->find('all')->where(['locale'=>$lang , 'value_0 IS'=> NULL ])->toArray();
        foreach ($texts as $record){
            $result = $translate->translate($record->singular, [
                'target' => 'ar'
            ]);
            $record->value_0 = $result['text'];
            $this->Translations->save($record);
        }
        $this->redirect(['action' => 'index']);


    }

    /**
     * View method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $translation = $this->Translations->get($id, [
            'contain' => [],
        ]);

        $this->set('translation', $translation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $translation = $this->Translations->newEmptyEntity();
        if ($this->request->is('post')) {
            $translation = $this->Translations->patchEntity($translation, $this->request->getData());
            if ($this->Translations->save($translation)) {
                $this->Flash->success(__('The translation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The translation could not be saved. Please, try again.'));
        }
        $this->set(compact('translation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $translation = $this->Translations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $translation = $this->Translations->patchEntity($translation, $this->request->getData());
            if ($this->Translations->save($translation)) {
                $this->Flash->success(__('The translation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The translation could not be saved. Please, try again.'));
        }
        $this->set(compact('translation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $translation = $this->Translations->get($id);
        if ($this->Translations->delete($translation)) {
            $this->Flash->success(__('The translation has been deleted.'));
        } else {
            $this->Flash->error(__('The translation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
