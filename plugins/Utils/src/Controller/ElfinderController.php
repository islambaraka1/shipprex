<?php
declare(strict_types=1);

namespace Utils\Controller;

//use
use elFinder;
use elFinderConnector;

/**
 * Elfinder Controller
 *
 *
 * @method \Utils\Model\Entity\Elfinder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ElfinderController extends AppController
{
    public function uploadiframe($foldername = ''){
        $this->viewBuilder()->setLayout('ajax');
    }
    public function showElfinder(){
        $this->viewBuilder()->setLayout('ajax');
    }
    public function connector($openDir = ''){
        if($_GET['foldername'])
            $openDir = $_GET['foldername'];
        $this->disableAutoRender();
        require ROOT.DS.'plugins'.DS.'Utils'.DS.'webroot'.DS.'elfinder'.DS.'php'.DS.'autoload.php';
        //make dir if not exict
        if (!file_exists(WWW_ROOT.'files/'.$openDir)) {
            mkdir(WWW_ROOT.'files/'.$openDir, 0755, true);
        }
        elFinder::$netDrivers['ftp'] = 'FTP';
        function access($attr, $path, $data, $volume, $isDir, $relpath) {
            $basename = basename($path);
            return $basename[0] === '.'                  // if file/folder begins with '.' (dot)
            && strlen($relpath) !== 1           // but with out volume root
                ? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
                :  null;                                 // else elFinder decide it itself
        }

        $opts = array(
            // 'debug' => true,
            'roots' => array(
                // Items volume
                array(
                    'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
                    'path'          => WWW_ROOT.'files/'.$openDir,                 // path to files (REQUIRED)
                    'URL'           =>  ROOT_URL.'files/'.$openDir, // URL to files (REQUIRED)
                    'trashHash'     => 't1_Lw',                     // elFinder's hash of trash folder
                    'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
                    'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
                    'uploadAllow'   => array('image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon', 'text/plain'), // Mimetype `image` and `text/plain` allowed to upload
                    'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
                    'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
                ),
                // Trash volume
                array(
                    'id'            => '1',
                    'driver'        => 'Trash',
                    'path'          => '../files/.trash/',
                    'tmbURL'        => dirname($_SERVER['PHP_SELF']) . '/../files/.trash/.tmb/',
                    'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
                    'uploadDeny'    => array('all'),                // Recomend the same settings as the original volume that uses the trash
                    'uploadAllow'   => array('image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon', 'text/plain'), // Same as above
                    'uploadOrder'   => array('deny', 'allow'),      // Same as above
                    'accessControl' => 'access',                    // Same as above
                ),
            )
        );
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();

    }

}
