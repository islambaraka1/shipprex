<?php
declare(strict_types=1);

namespace Utils\View\Helper;

use Cake\Collection\Collection;
use Cake\View\Helper;
use Cake\View\View;

/**
 * Utils helper
 */
class UtilsHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    var $defaultOptionsTable = [
        'main_tag'  =>'table',
        'main_class'=>'table m-0',
        'main_id'=>'',
        'thead'=>false,
        'header_tag'=>'th',
        'header_tr_class'=>'',
        'header_tag_class'=>'',
        'tbody'=>false,
        'body_td_class'=> '',
        'body_tr_class'=> ''
    ];
    var $output ='';
    public function generateTable($records,$options = [] ){
        if($records != null){
            $this->output = '';
            $options = array_merge($this->defaultOptionsTable,$options);
            $this->output .= "<{$options['main_tag']} class='{$options['main_class']}' id='{$options['main_id']}'>";
            $zero_record = !is_array($records[0])?$records[0]->toArray():$records[0];
            $this->extract_headers($zero_record,$options);
            $this->output .= $options['tbody'] ? "<tbody>":"";

            foreach ($records as $record){
                $this->output .= "<tr class='{$options['body_tr_class']}'>";
                $row = !is_array($record)?$record->toArray():$record;
                foreach ($row as $key => $rval){
                    $this->output .="<td class='{$options['body_td_class']}'>";
                    $this->output .= $rval;

                    $this->output .="</td>";
                }
                $this->output .= "</tr>";

            }

            $this->output .= $options['tbody'] ? "</tbody>":"";
            $this->output .= "</{$options['main_tag']}>";
            return $this->output;
        }
    }
    private function extract_headers($record,$options=[]){
        $headers = $this->extract_headers_array($record);
        $this->output .= $options['thead'] ? "<thead>":"";
        $this->output .= "<tr class='{$options['header_tr_class']}'>";
        foreach ($headers as $header){
            $this->output .= "<th class='{$options['body_td_class']}'>";
            $this->output .= __($header);
            $this->output .= "</th>";
        }
        $this->output .= "</tr>";
        $this->output .= $options['thead'] ? "</thead>":"";

    }
    private function extract_headers_array($record){
        $collection = new Collection($record);
        $new = $collection->map(function ($value, $key) {
            return $key;
        });
       return $new->toArray();
    }
}
