<?php
namespace Utils\Event;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use Google\Cloud\Translate\V2\TranslateClient;


class TranslationsListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Translations.newWord' => 'TranslationNewWord',
        ];
    }

    public function TranslationNewWord($event, $word)
    {
        $_SESSION['keywords']++;
        $_SESSION['texts'][] = $word;
        if(!empty(SETTING_KEY['translations']['auto_translate']['google_api_key_'])){
            $translate = new TranslateClient([
                'key' => SETTING_KEY['translations']['auto_translate']['google_api_key_']
            ]);
            $TransModel = TableRegistry::getTableLocator()->get('Utils.Translations');
            // check for auto translate settings
            $langs = ['en','ar'];
            foreach ($langs as $lang){
                if($lang !='en'){
                    $result = $translate->translate($word, [
                        'target' => $lang
                    ]);
                }
                $entity = $TransModel->newEmptyEntity();
                $entity->domain = 'default';
                $entity->locale = $lang;
                $entity->singular = $word;
                $entity->value_0 = isset($result['text'])?$result['text']:$word;
                $TransModel->save($entity);
            }
        }
    }


}
?>
