<?php
declare(strict_types=1);

namespace Utils\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Utils\View\Helper\UtilsHelper;

/**
 * Utils\View\Helper\UtilsHelper Test Case
 */
class UtilsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Utils\View\Helper\UtilsHelper
     */
    protected $Utils;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Utils = new UtilsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Utils);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
