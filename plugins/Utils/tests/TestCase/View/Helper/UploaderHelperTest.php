<?php
declare(strict_types=1);

namespace Utils\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Utils\View\Helper\UploaderHelper;

/**
 * Utils\View\Helper\UploaderHelper Test Case
 */
class UploaderHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Utils\View\Helper\UploaderHelper
     */
    protected $Uploader;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Uploader = new UploaderHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Uploader);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
