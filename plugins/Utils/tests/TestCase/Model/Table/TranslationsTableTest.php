<?php
declare(strict_types=1);

namespace Utils\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Utils\Model\Table\TranslationsTable;

/**
 * Utils\Model\Table\TranslationsTable Test Case
 */
class TranslationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Utils\Model\Table\TranslationsTable
     */
    protected $Translations;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Utils.Translations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Translations') ? [] : ['className' => TranslationsTable::class];
        $this->Translations = TableRegistry::getTableLocator()->get('Translations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Translations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
