<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
//
//TableRegistry::getTableLocator()->get('Orders')
//    ->getEventManager()
//    ->on('Model.afterSave', function($event, $entity)
//    {
//        $SmsListener = new \Sms\Event\SmsListener();
//        $SmsListener->OrdersAfterSave($event,$entity);
//    });


define("GL_CONFIG_MAIN_BANK" , 1);
define("GL_CONFIG_COD_BANK" , 2);
define("GL_CONFIG_PROFIT_BANK" , 3);
$hooks = \App\Hooks\Hooks::getInstance();
if (!function_exists('after_add_driver')) {
    $hooks->add_action('drivers_form_add_after', 'after_add_driver');
    function after_add_driver()
    {
        $view = new \App\View\AppView();
        echo $view->Form->control('cost', ["type" => 'number']);
    }
}
/*
 * Create the configuration buttons
 * Add a clear balance for active viewed driver
 */

//TODO Check why it's crruopt the test cases
if (!function_exists('drivers_control_view_buttons2')) {
    $hooks->add_action('drivers_control_view_buttons', 'drivers_control_view_buttons2');
    function drivers_control_view_buttons2()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        $driverId = $hooks->currentView->getRequest()->getParam('pass')[0];
        echo '
        <div class="col">
            <a class="btn btn-primary" href="' . ROOT_URL . 'general-ledger/transes/clear_balance/' . $driverId . '">Clear Balance <i class="fa fa-check-circle"></i> </a>
        </div>
    ';
    }
}
if (!function_exists('after_edit_driver')) {
    $hooks->add_action('drivers_form_edit_after', 'after_edit_driver');
    function after_edit_driver()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        echo $hooks->currentView->Form->control('cost', ["type" => 'number']);
    }
}
if (!function_exists('add_driver_cost_col')) {
    $hooks->add_action('drivers_index_body_col', 'add_driver_cost_col');
    function add_driver_cost_col($driver)
    {
        echo "<td> $driver->cost </td>";
    }
}
if (!function_exists('add_driver_cost_header')) {
    $hooks->add_action('drivers_index_head_col', 'add_driver_cost_header');
    function add_driver_cost_header()
    {
        echo '        <th scope="col">' . __("Driver Fees") . ' </th>';
    }
}
if (!function_exists('after_main_menu')) {
    $hooks->add_action('after_main_menu', 'after_main_menu');
    function after_main_menu()
    {
        $hooks = \App\Hooks\Hooks::getInstance();

        $menu = '';
        $hasTree = "has-treeview";
        $item_icon = "<i class=\"right fas fa-angle-left\"></i>";
        $menu .= '
            <li class="nav-item ' . $hasTree . ' ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            ' . __('General Ledger') . '
                            ' . $item_icon . '

                        </p>
                    </a>';
        $menu .= ' <ul class="nav nav-treeview">  ';

        $hooks->do_action('top_of_gl_menu');

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/categories" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Payment Categories') . '</p>
                            </a>
                        </li>';

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/categories/add" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Add Category') . '</p>
                            </a>
                        </li>';

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/banks" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Bank List ') . '</p>
                            </a>
                        </li>';

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/banks/add" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Add Bank') . '</p>
                            </a>
                        </li>';

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/transes" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Transactions List ') . '</p>
                            </a>
                        </li>';

        $menu .= '<li class="nav-item">
                            <a href="' . ROOT_URL . '/general-ledger/transes/add" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>' . __('Add Transaction') . '</p>
                            </a>
                        </li>';

        $menu .= $hooks->apply_filters('bottom_of_gl_menu', '');

        $menu .= '</ul>';
        echo $menu;
    }
}
//$hooks->add_action('Orders_List_afterTable','Orders_List_afterTable');
//function Orders_List_afterTable(){
//    $view = new \App\View\AppView();
//    echo $view->element('backend/datatablejs');
//}
//
//$hooks->add_verify('system_show_data_table');
//
//function example_callback( $example ) {
//   if(1==2){
//       return $example;
//   } else
//       return "";
//}
//$hooks->add_filter( 'example_filter', 'example_callback' );
////$hooks->remove_verify('system_show_data_table','system_show_data_table');




EventManager::instance()->on(
    'Model.Order.UpdatedOrderState',
    function ($event){
        $stateString = $event->getData()['state'];
        if($stateString == "Collected"){
            $transModel = TableRegistry::getTableLocator()->get("GeneralLedger.Transes");
            $transModel->add_cod_transaction($event->getData()['order']);
        }
    }
);
EventManager::instance()->on(
    'Model.Transactions.ClearBalance',
    function ($event){
        $data = $event->getData();
        $transModel = TableRegistry::getTableLocator()->get("GeneralLedger.Transes");
        $transModel->cod_burn_for_invoice($event->getData()['InTransactions'],$event->getData()['OutTransaction'],$event->getData()['TargetInvoice']);
    }
);


//$aCallback =
