<?php
declare(strict_types=1);

namespace Apiv1\Controller;

/**
 * Users Controller
 *
 *
 * @method \Apiv1\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users)->toArray();
        return $this->custom_pages($users,'Users');
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('UsersManager.Users');
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            // ad the default user group to sellers
            $user->group_id = 3;
            //activation baypass code
            if(isset($this->request->getData()['bypass_activation']) && $this->request->getData()['bypass_activation'] == "true") {
                $user->active = 1;
                $user->email_verfied = 1;
            }
            if ($this->Users->save($user)) {
                return $this->returnData($user);
            }
            return $this->returnError("error users_001",$user->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('UsersManager.Users');
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            //remove password modification and activate users
            if(isset($this->request->getData()['password'])){
                return $this->returnError("error users_003 [Cant change user password through this api call]");
            }
            if(isset($this->request->getData()['active'])){
                return $this->returnError("error users_003 [Cant change user active through this api call]");
            }
            if(isset($this->request->getData()['email_verfied'])){
                return $this->returnError("error users_003 [Cant change user email_verfied through this api call]");
            }
            if(isset($this->request->getData()['group_id'])){
                return $this->returnError("error users_003 [Cant change user group_id through this api call]");
            }
            if ($this->Users->save($user)) {
                return $this->returnData($user);
            }
            return $this->returnError("error users_002",$user->getErrors());
        }
        $this->set(compact('user'));
    }




    /**
     * Status method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function status($id = null){
        $this->loadModel('UsersManager.Users');
        try {
            //user found
            $user = $this->Users->get($id);
            return $this->returnData(['user_is_active' => $user->active]);
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException  $exception){
            return $this->returnError('error user not found ');
        }
    }


    /**
     * Status method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function forceActive($id = null){
        $this->loadModel('UsersManager.Users');
        try {
            //user found
            $user = $this->Users->get($id);
            $user->active = 1;
            $user->email_verfied = 1;
            if ($this->Users->save($user)) {
                return $this->returnData($user);
            }
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException  $exception){
            return $this->returnError('error user not found ');
        }
    }


 /**
     * generate API access token method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function generateAccessToken($id = null){
        $this->loadModel('UsersManager.Users');
        try {
            //user found
            $user = $this->Users->get($id);
            if ($this->Users->generate_api_access_token($user) ) {
                return $this->returnData($user->get('api_access'));
            }
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException  $exception){
            return $this->returnError('error user not found ');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
