<?php
declare(strict_types=1);

namespace Apiv1\Controller;

/**
 * Orders Controller
 *
 *
 * @method \Apiv1\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $orders = $this->paginate($this->Orders)->toArray();
        return $this->custom_pages($orders,'Orders');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function listSellerOrders($id=null)
    {
        $this->loadModel('Users');
        try {
            //user found
            $userData = $this->Users->get($id);
            $orders = $this->paginate($this->Orders->find('all')->where(['user_id'=>$id]))->toArray();
            return $this->custom_pages($orders,'Orders');
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException  $exception){
            return $this->returnError('error Order not found ');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Users','Drivers'],
        ]);

        return $this->returnData($order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEmptyEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                return $this->returnData($order);
            }
            return $this->returnError("error orders_001",$order->getErrors());
        }
        return $this->returnError("error Post is required as a method");

    }

    /**
     * checkStatus method
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function checkStatus($id = null)
    {
        try {
            //user found
            $order = $this->Orders->get($id);
            return $this->returnData(['status' => $order->statues]);
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException  $exception){
            return $this->returnError('error Order not found ');
        }

    }
    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // to do protect field of none admin modifications
        $order = $this->Orders->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                return $this->returnData($order);
            }
            return $this->returnError("error orders_002",$order->getErrors());
        }
        return $this->returnError("Error a post put patch methods are allowed only" );
    }



     /**
     * search for order method
     *
     * @param string|null $keyword Order keyword.
     * @return \Cake\Http\Response|null JSON format response .
     *
     */
    public function search($keyword = null)
    {
        $this->loadModel('Orders');
        if (is_numeric( $keyword)) {
            $id = UNDID($keyword);
            $results = $this->Orders->find('all')->where([
                'id' =>$id
            ]);
            if(isset($results->toArray()[0]['id'])){
                $orders = $this->paginate($results)->toArray();
                return $this->custom_pages($orders,'Orders');
            }
        }
        $results = $this->Orders->find('all')->where([
            [
                'OR' =>
                    [
                        'receiver_name LIKE' => '%'.$keyword.'%' ,
                        'receiver_phone LIKE' => '%'.$keyword.'%' ,
                        'package_description LIKE' => '%'.$keyword.'%' ,
                        'notes LIKE' => '%'.$keyword.'%' ,
                    ]
            ]
        ]);
        $orders = $this->paginate($results)->toArray();
        return $this->custom_pages($orders,'Orders');
    }


    /**
     * Filter for order method
     *
     * @param string|null POST Filters array filter[field_name] = value LIKE
     * @return \Cake\Http\Response|null JSON format response .
     *
     */
    public function filters(){
        if ($this->request->is(['post'])) {
            if(isset( $this->request->getData()['filters'] )){
                $filters = $this->request->getData()['filters'];
                $query = $this->Orders->find('all');
                $conditions = [];
                foreach ($filters as $key => $value){
                    $conditions[$key." LIKE"] = '%'.$value."%";
                }
                $query->where($conditions);
                $orders = $this->paginate($query)->toArray();
                return $this->custom_pages($orders,'Orders');
            }else{
                return $this->returnError("Error filters dont exicet in your call!" );
            }
        }
        return $this->returnError("Error a post  methods are allowed only" );
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
