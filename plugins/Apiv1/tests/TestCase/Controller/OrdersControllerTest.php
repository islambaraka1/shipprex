<?php
declare(strict_types=1);

namespace Apiv1\Test\TestCase\Controller;

use Apiv1\Controller\OrdersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * Apiv1\Controller\OrdersController Test Case
 *
 * @uses \Apiv1\Controller\OrdersController
 */
class OrdersControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Apiv1.Orders',
        'plugin.Apiv1.Users',
        'plugin.Apiv1.Invoices',
        'plugin.Apiv1.Pickups',
        'plugin.Apiv1.Drivers',
        'plugin.Apiv1.Actions',
        'plugin.Apiv1.DriverPickup',
        'plugin.Apiv1.Transactions',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
