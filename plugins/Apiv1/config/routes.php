<?php
use Cake\Routing\Route\DashedRoute;

$routes->plugin(
    'Apiv1',
    ['path' => '/api-v1'],
    function ($routes) {
        $routes->setRouteClass(DashedRoute::class);

        $routes->connect('/:controller/:action/*');
    }
);
