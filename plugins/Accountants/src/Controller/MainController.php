<?php
declare(strict_types=1);

namespace Accountants\Controller;

use Cake\I18n\FrozenTime;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Main Controller
 *
 *
 * @method \Accountants\Model\Entity\Transactions[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MainController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {


    }

    /**
     *  view function for the dashboard of the accountant system
     * @todo create widget of how many orders on route right now
     * @todo how many orders returned to warehouse for today
     * @todo how many orders collected by today
     * @todo how much is the collected money for today
     * @todo how many is the profit for today
     * @todo how many is the orders cancelled by today
     *
     *
     */
    public  function dashboard(){
        $this->loadComponent('ReportsOrders');
        // get all system Orders and active / in active of it
        $this->loadModel( 'Transactions');
        $orders_inactive = $this->Transactions->Orders->find('all')
            ->where(['Orders.statues IN' => state_unactive_types])->count();
        $orders_active = $this->Transactions->Orders->find('all')
            ->where(['Orders.statues IN' => state_active_type])->count();
        $toBePaid = $this->sum_finder(['Transactions.details !=' => 'Clear Balance']);
        $cod = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'out']);
        $days = '-'.get_option_value('accountant_widgets_days');
        $this->loadModel('Actions');
        $number_of_delivered_orders = $this->Actions->find('all')->where([
            'Actions.name'=> 'Delivered'
        ])->where(['Actions.created >' => new \DateTime('' . $days . ' days')])->groupBy('order_id')->count();

        $number_of_on_route_orders = $this->Actions->find('all')->where([
            'Actions.name'=> 'On route'
        ])->where(['Actions.created >' => new \DateTime('' . $days . ' days')])->groupBy('order_id')->count();

        $number_of_collected_orders = $this->Actions->find('all')->where([
            'Actions.name'=> 'Collected'
        ])->where(['Actions.created >' => new \DateTime('' . $days . ' days')])->groupBy('order_id')->count();

        $fullOrdersCoveredByThisData = $this->Actions->find('all')->where([
            'Actions.name IN'=> ['Collected','On route','Delivered']
        ])->where(['Actions.created >' => new \DateTime('' . $days . ' days')])
            ->contain(['Orders','Orders.Drivers'])
            ->toArray();

        $this->loadModel('GeneralLedger.Banks');
        $upToDateCollections = $this->Banks->get(6)->current_balance;
        $this->set('statues_table' , $this->ReportsOrders->get_statues_counts());
        $this->set('full_orders' , $fullOrdersCoveredByThisData );




        $toBePaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance'],['Transactions.created >' => new \DateTime('' . $days . ' days')]);
        $codPaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'out'],['Transactions.created >' => new \DateTime('' . $days . ' days')]);
        $cutoffPaidWeek = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in'],['Transactions.created >' => new \DateTime('' . $days . ' days')]);
        $cutoffPaid = $this->sum_finder(['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in']);
        $cahrtData = $this->last_days_sum(7,['Transactions.details !=' => 'Clear Balance' , 'Transactions.type' => 'in'] );
        $this->set(compact('orders_active','cahrtData','orders_inactive','cod','cutoffPaid','codPaidWeek','cutoffPaidWeek','orders_inactive','toBePaid','toBePaidWeek'));
        $this->set(compact('upToDateCollections','number_of_delivered_orders','number_of_on_route_orders','number_of_collected_orders','days'));
    }

    /**
     * @param $days
     * @param $sumConditions
     * @return array
     */
    private function last_days_sum($days,$sumConditions){
        $returner = [];
        for ($x=0;$x<$days;$x++){
            $today  = new FrozenTime("-$x days");
            $returner [$today->dayOfWeekName] = $this->sum_finder($sumConditions,['Transactions.created LIKE' => '%'.$today->toDateString().'%' ]);
        }
        return $returner;
    }
    private function sum_finder($conditionArray,$duration = [])
    {
        $this->loadModel( 'Transactions');
        $transactions = $this->Transactions->find('all')->where($conditionArray)->where($duration);
        return abs((int)$transactions
            ->select(['sum' => $transactions->func()->sum('amount')])
            ->toArray()[0]->sum);;
    }


    public function closing(){
        $this->loadModel('GeneralLedger.Banks');
        $upToDateCollections = $this->Banks->get(6)->current_balance;
        $this->set('total',$upToDateCollections);
    }

    public function clear(){
        $this->loadModel('GeneralLedger.Banks');
        $this->loadModel('GeneralLedger.Transes');
        if($this->Transes->reset_bank_to_zero(get_option_value('accountant_bank_daily'))){
            $this->Flash->success(__('The bank was reset.'));
            return $this->redirect(['action' => 'dashboard']);
        }


    }



    function balance(){
        $transactionsModel = TableRegistry::getTableLocator()->get('GeneralLedger.Transes');
        $transactions = $transactionsModel->find('all')->where(['bank_id' => 6])->toArray();
        $this->set('transactions' , $transactions);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $main = $this->Main->newEmptyEntity();
        if ($this->request->is('post')) {
            $main = $this->Main->patchEntity($main, $this->request->getData());
            if ($this->Main->save($main)) {
                $this->Flash->success(__('The main has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The main could not be saved. Please, try again.'));
        }
        $this->set(compact('main'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Main id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $main = $this->Main->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $main = $this->Main->patchEntity($main, $this->request->getData());
            if ($this->Main->save($main)) {
                $this->Flash->success(__('The main has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The main could not be saved. Please, try again.'));
        }
        $this->set(compact('main'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Main id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $main = $this->Main->get($id);
        if ($this->Main->delete($main)) {
            $this->Flash->success(__('The main has been deleted.'));
        } else {
            $this->Flash->error(__('The main could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
