<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use Cake\Controller\Controller;

$hooks = \App\Hooks\Hooks::getInstance();


if(get_option_value('accountant_daily_bank_activated') == 1){
    $transactionTable = TableRegistry::getTableLocator()->get('GeneralLedger.Transes');

    $transactionTable->getEventManager()
        ->on('Model.Transes.DriverTransactionsIsDone', function($event, $entity)
        {
            // get order by id
            $ordersTable = TableRegistry::getTableLocator()->get('Orders');
            $order = $ordersTable->get($entity['orderId']);
            // add transaction to daily cod if the option is enabled
            $transactionTable = TableRegistry::getTableLocator()->get('GeneralLedger.Transes');
            $trans = $transactionTable->newEmptyEntity();
            $trans->category_id = 4;
            $trans->bank_id =get_option_value('accountant_bank_daily') ;
            $trans->amount = $order->cod - $entity['fees'];
            $trans->user_id = 1;
            $trans->driver_id = $entity['driverId'];
            $trans->order_id = $entity['orderId'];
            $trans->name = "Driver Fees";
            $trans->description = "Order : " . DID($entity['orderId']);
            $transactionTable->save($trans);


        });

}

if(get_option_value('accountant_redirect_url') != null){

    $transactionTable = TableRegistry::getTableLocator()->get('UsersManager.Users');

    $transactionTable->getEventManager()
        ->on('Model.Users.DidLoggedIn', function($event, $entity)
        {
            if(get_option_value('accountant_label_user_group') == $entity['group_id']){
                header("Location: ".ROOT_URL.get_option_value('accountant_redirect_url'));
                die();
            }
        });

}



$hooks->create_new_root_menu('accountant','Accountant','#','money-bill',[1,4]);
$hooks->add_sub_menu_item_to_root('accountant','Dashboard','accountants/main/dashboard',[1,4]);
$hooks->add_sub_menu_item_to_root('accountant','Close Day','accountants/main/closing',[1]);
$hooks->add_sub_menu_item_to_root('accountant','Daily transactions','accountants/main/balance',[1,4]);

