<?php
use Cake\Core\Configure;
//
//\Cake\Core\Configure::write('Settings.testing.duration', '+2 minutes');
//
//
//$test = Configure::read('Settings');
//var_dump($test);
//die();

// create the setting array



Configure::write('Settings.Accountant', []);
$table = \Cake\ORM\TableRegistry::getTableLocator()->get('UsersManager.Groups');
$groupList = $table->find('list')->toArray();
easy_new_setting('Settings.Accountant','accountant_label_user_group','Please select the main user group act as Accountant',"select"
,4, $groupList,'',false);

easy_new_setting('Settings.Accountant','accountant_daily_bank_activated','turn on / off the daily bank activity',"on_off"
,0);

$tableBanks = \Cake\ORM\TableRegistry::getTableLocator()->get('GeneralLedger.Banks');
$bankList = $tableBanks->find('list')->toArray();

easy_new_setting('Settings.Accountant','accountant_bank_daily','Please select the daily bank ',"select"
,1, $bankList,'',false);

easy_new_setting('Settings.Accountant','accountant_widgets_days','Accountant dashboard number of days in the past for all queries',"number"
    ,1, null ,'changing this will affect the widgets counters');


easy_new_setting('Settings.Accountant','accountant_redirect_url','Accountant dashboard redirect',"select"
    ,'accountants/main/dashboard', [
        'accountants/main/dashboard',
        'general-ledger/banks'
    ] ,'changing this will affect the login redirect action for accountant');


//
//easy_new_setting('Settings.Settings','allow_driver_screen','Allow the driver access link',$type="on_off");
//
//// Orders Options
//easy_new_setting('Settings.Orders','columns_view_in_the_dashboard','Select Columns in the dashboard view for (active orders)',
//    $type="checkbox",'',get_columns_names('Orders'));
//easy_new_setting('Settings.Orders','allow_delete_for_orders','Turn this on will allow admin to delete the orders(which is not recommended)',
//    $type="on_off",'0');
//// orders Print options
//easy_new_setting('Settings.Orders','print_labels','Control the printing of Invoices ',"label");
//easy_new_setting('Settings.Orders','print_show_sender_name','Show Sender name in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_sender_city','Show Sender city in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_sender_phone','Show Sender phone in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_sender_ref','Show Sender reference in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_receiver_name','Show Receiver name in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_receiver_email','Show Receiver email in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_receiver_phone','Show Receiver phone in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_receiver_address','Show Receiver address in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_ship_type','Show package shipping type in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_ship_notes','Show package shipping notes in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_ship_description','Show package description in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','print_show_signature','Show signature in the print slip',"on_off",'0');
//easy_new_setting('Settings.Orders','excel_template','Upload the excel file as a template',"label");
//easy_new_setting('Settings.Orders','excel_template_file','Upload the excel file as a template',"upload");
//
//$setting = get_setting_array('Settings.Settings');
////$test = Configure::read('Settings');
//$table = \Cake\ORM\TableRegistry::getTableLocator()->get('Settings.Options');
////var_dump($table->create_new_option_or_fail('testing','Settings','Hello world'));
////var_dump($table->create_or_update_option('testin22gssss','Settings','Hello worldss'));
////var_dump($table->get_feature_id_by_name('Settings'));
////var_dump($table->get_option_record('testings'));
////var_dump($table->get_option_value('testing'));
////var_dump($setting);
////die();
