<h1> <?=__('Clear balance for this day')?> </h1>
<div class="row">
    <?php
    //        $this->set(compact('totalOrders','totalCollectedCods','totalFeesCosts','TotalReminingBalance'));

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'count' => $total,
        'title' => __('Cash in hand'),
        'icon' => 'money-check-alt',
        'sub_msg' => 'More Info',
    ]);


    ?>

</div>
<div class="">
    <h3><?=__('Click here to start new day')?></h3>
    <br>
    <a href="<?=ROOT_URL?>accountants/main/clear/" class="btn btn-primary btn-lg text-light">
        <?=__('Proceed')?> <i class="fa fa-check-circle"></i></a>

</div>
