<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'size' => 2,
        'count' => $orders_active,
        'title' => __('Active Orders'),
        'icon' => 'map',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'size' => 2,
        'count' => $orders_inactive,
        'title' => __('InActive Orders'),
        'icon' => 'map',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'size' => 2,
        'count' => $number_of_delivered_orders,
        'title' => __('Number of delivered orders'),
        'icon' => 'money-check-alt',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'orange',
        'size' => 2,
        'count' => $number_of_on_route_orders,
        'title' => __('Number of on routes orders'),
        'icon' => 'wallet',
        'sub_msg' => __('More Info'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'green',
        'size' => 2,
        'count' => $number_of_collected_orders,
        'title' => __('Number collected orders'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'lightblue',
        'size' => 2,

        'count' => $upToDateCollections,
        'title' => __('Cash on hand'),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => __('More Info'),
    ]);
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'orange',
//        'count' => $codPaidWeek,
//        'title' => __('Last week COD'),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => __('More Info'),
//    ]);
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'green',
//        'count' => $cutoffPaidWeek,
//        'title' => __('Last week profits'),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => __('More Info'),
//    ]);


    ?>
</div>
<div class="row">
    <section class="col-lg-7 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart1',[
            'canv_id' => 'chartscanv',
            'count' => '120',
            'title' => __('last 7 days of activities'),
            'icon' => 'file-invoice-dollar',
            'sub_msg' => __('More Info'),
            'chart_data' => $cahrtData,
            'scale' => SUSTEM_CURRENCY
        ]);

        ?>
    </section>
    <section class="col-lg-5 connectedSortable">
        <?php
            echo $this->element('Utils.backend/widgets/data_table',[
                'color' => 'green',
                'size' => '12',
                'table_data' => $statues_table,
                'title' => __('Order Statues '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Order Status List ' ,
            ]);
        ?>
    </section>
</div>

<div class="row">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th scope="col"><?=__('ID')?></th>
                <th scope="col"><?=__('reference')?></th>
                <th scope="col"><?=__('receiver name')?></th>
                <th scope="col"><?=__('Driver name')?></th>
                <th scope="col"><?=__('Status')?></th>
                <th scope="col"><?=__('COD')?></th>
                <th scope="col"><?=__('Fees')?></th>
                <th scope="col"><?=__('Cost')?></th>
            </tr>
        </thead>
    <tbody>
    <?php foreach ($full_orders as $order) : ?>
        <tr>
            <td scope="col"><?=DID($order->order->id)?></td>
            <td scope="col"><?=$order->order->reference?></td>
            <td scope="col"><?=$order->order->receiver_name?></td>
            <td scope="col"><?= isset($order->order->drivers[0])?$order->order->drivers[0]->name:__('No driver known')?></td>
            <td scope="col" class="status-element"><?= HSTT($order->order->statues)?></td>
            <td scope="col"><?=$order->order->cod?></td>
            <td scope="col"><?=$order->order->fees?></td>
            <td scope="col"><?=isset($order->order->drivers[0])?$order->order->drivers[0]->cost:__('No driver known') ?></td>

        </tr>
    <?php endforeach;?>
    </tbody>
    </table>
</div>
