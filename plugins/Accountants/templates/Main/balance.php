    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
            <th scope="col"><?= $this->Paginator->sort('balance_after') ?></th>
            <th scope="col"><?= $this->Paginator->sort('description') ?></th>
            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $balanceAfter = 0;
        foreach ($transactions as $transe) :
            $style = $transe->name == "[CLEAR OUT]" ? "background:cyan;border:2px solid red;color:#000":"";
            if( $transe->name == "[CLEAR OUT]"){
                $balanceAfter = 0;
            }
            else{
                $balanceAfter += $transe->amount;
            }
            ?>
            <tr style="<?=$style?>">
                <td><?= $this->Number->format($transe->id) ?></td>
                <td><?= $this->Number->format($transe->amount) ?></td>
                <td><b><?= $this->Number->format($balanceAfter) ?></b></td>
                <td><?= h($transe->description) ?></td>
                <td><?= h($transe->name) ?></td>
                <td><?= $transe->has('category') ? $this->Html->link($transe->category->name, ['controller' => 'Categories', 'action' => 'view', $transe->category->id]) : '' ?></td>
                <td><?= $transe->has('user') ? $this->Html->link($transe->user->username, ['controller' => 'Users', 'action' => 'view', $transe->user->id]) : '' ?></td>
                <td><?= h($transe->created) ?></td>
                <td><?= h($transe->modified) ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

