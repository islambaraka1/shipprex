<?php

use Cake\Core\Configure;

Configure::write('Settings.Search', []);

easy_new_setting('Settings.Search','allow_search_on_global','Allow the global search ',$type="on_off");
easy_new_setting('Settings.Search','min_amount_of_characters','Minimum count of character to start searching',$type="number",3,null,'Recommended grater than 3');
