<div class="container">
    <div class="alert alert-primary alert-dismissible fade show d-block flex-fill" role="alert m-5">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        <strong><?=__("There is a delegations for today")?></strong> <?=count($delegations)?> .
        <?php
        echo $this->Html->link(__('Today\'s Delegates'), ['action' => 'index', 'controller' => 'Delegations', 'plugin' => 'Delegations', '?date='.date("Y-m-d")], ['class' => 'btn btn-warning ajax_form_open_modal']);
        ?>
    </div>
</div>
