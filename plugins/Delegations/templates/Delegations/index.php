<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $delegations
 */
?>

<div class="row">
    <?php
    //create filter by date form
    echo $this->Form->create($filter, ['type' => 'get']);
    echo $this->Form->control('date', ['type'=>'date','label' => __('Filter by date'), 'class' => 'form-control']);
    echo $this->Form->button(__('Filter'), ['class' => 'btn btn-primary']);
    echo $this->Form->end();
    ?>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('delegation_date') ?></th>
        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($delegations as $delegation) : ?>
        <tr>
            <td><?= $this->Number->format($delegation->id) ?></td>
            <td><?= h($delegation->delegation_date) ?></td>
            <td><?= $delegation->has('user') ? $this->Html->link($delegation->user->username, ['controller' => 'Users', 'action' => 'view', $delegation->user->id,'plugin'=>'UsersManager']) : '' ?></td>
            <td><?= $delegation->has('order') ? $this->Html->link(DID($delegation->order->id), ['controller' => 'Orders', 'action' => 'view', $delegation->order->id,'plugin'=>null]) : '' ?></td>
            <td><?= h($delegation->created) ?></td>
            <td><?= h($delegation->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $delegation->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $delegation->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $delegation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $delegation->id), 'title' => __('Delete'), 'class' => 'btn btn-danger']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('First')) ?>
        <?= $this->Paginator->prev('< ' . __('Previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('Next') . ' >') ?>
        <?= $this->Paginator->last(__('Last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
