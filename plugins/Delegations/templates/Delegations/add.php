<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>

<div class="delegations form content">
    <?= $this->Form->create($delegation) ?>
    <fieldset>
        <legend><?= __('Add Delegation') ?></legend>
        <?php
            echo $this->Form->control('delegation_date');
            echo $this->Form->control('description');
            $orderIdFromUrl = $this->request->getParam('pass.0');
            $attrs = $orderIdFromUrl > 0 ? ['value' => $orderIdFromUrl,'type'=>'hidden'] : ['options' => $orders];
            echo $this->Form->control('order_id', $attrs);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
