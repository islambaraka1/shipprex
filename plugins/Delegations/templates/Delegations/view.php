<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Delegation'), ['action' => 'edit', $delegation->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Delegation'), ['action' => 'delete', $delegation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $delegation->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Delegations'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Delegation'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="delegations view large-9 medium-8 columns content">
    <h3><?= h($delegation->id) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $delegation->has('user') ? $this->Html->link($delegation->user->username, ['controller' => 'Users', 'action' => 'view', $delegation->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Order') ?></th>
                <td><?= $delegation->has('order') ? $this->Html->link($delegation->order->id, ['controller' => 'Orders', 'action' => 'view', $delegation->order->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($delegation->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Delegation Date') ?></th>
                <td><?= h($delegation->delegation_date) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($delegation->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($delegation->modified) ?></td>
            </tr>
        </table>
    </div>
</div>
