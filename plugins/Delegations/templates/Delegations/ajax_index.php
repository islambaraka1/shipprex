
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?=__('delegation_date') ?></th>
        <th scope="col"><?=__('order_id') ?></th>
        <th scope="col"><?=__('created') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($delegations as $delegation) : ?>
        <tr>
            <td><?= h($delegation->delegation_date) ?></td>
            <td><?= $delegation->has('order') ? $this->Html->link(DID($delegation->order->id), ['controller' => 'Orders', 'action' => 'view', $delegation->order->id,'plugin'=>null],
            ['style'=>'color:darkblue']) : '' ?></td>
            <td><?= h($delegation->created) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
