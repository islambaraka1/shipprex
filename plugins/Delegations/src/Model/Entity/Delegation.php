<?php
declare(strict_types=1);

namespace Delegations\Model\Entity;

use Cake\ORM\Entity;

/**
 * Delegation Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $delegation_date
 * @property int $user_id
 * @property int $order_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Delegations\Model\Entity\User $user
 * @property \Delegations\Model\Entity\Order $order
 */
class Delegation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'delegation_date' => true,
        'description' => true,
        'user_id' => true,
        'order_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'order' => true,
    ];
}
