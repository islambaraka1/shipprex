<?php
declare(strict_types=1);

namespace Delegations\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Delegations Model
 *
 * @property \Delegations\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Delegations\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \Delegations\Model\Entity\Delegation newEmptyEntity()
 * @method \Delegations\Model\Entity\Delegation newEntity(array $data, array $options = [])
 * @method \Delegations\Model\Entity\Delegation[] newEntities(array $data, array $options = [])
 * @method \Delegations\Model\Entity\Delegation get($primaryKey, $options = [])
 * @method \Delegations\Model\Entity\Delegation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Delegations\Model\Entity\Delegation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Delegations\Model\Entity\Delegation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Delegations\Model\Entity\Delegation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Delegations\Model\Entity\Delegation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Delegations\Model\Entity\Delegation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Delegations\Model\Entity\Delegation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Delegations\Model\Entity\Delegation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Delegations\Model\Entity\Delegation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DelegationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('dlgt_delegations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UsersManager.Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('delegation_date')
            ->requirePresence('delegation_date', 'create')
            ->notEmptyDate('delegation_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    //function so set default user_id to current user
    public function beforeSave($event, $entity, $options)
    {
        //check if it's a new record
        if ($entity->isNew()) {
            $entity->user_id = $_SESSION['Auth']['User']['id'];
        }
        return true;
    }

    //afterSave function to update order status to delegated status
    public function afterSave($event, $entity, $options)
    {
        $order = $this->Orders->get($entity->order_id);
        $order->statues = get_option_value('delegations_order_change_statues');
        if(get_option_value('delegations_mark_order_note_field') == 1){
            $order->notes = '[DELEGATED TO : '.$entity->delegation_date.']';
        }
        $this->Orders->save($order);
        $this->createAction($entity);
    }
    //create action record of the new delegation
    public function createAction( $entity)
    {
        $actionTable = TableRegistry::getTableLocator()->get('Actions');
        $action = $actionTable->newEmptyEntity();
        $action->order_id = $entity->order_id;
        $action->name = 'Delegated';
        $action->description = "Order Delegated To : ".$entity->delegation_date;
        $actionTable->save($action);
    }
}
