<?php
declare(strict_types=1);

namespace Delegations\Controller;

/**
 * Delegations Controller
 *
 * @property \Delegations\Model\Table\DelegationsTable $Delegations
 *
 * @method \Delegations\Model\Entity\Delegation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DelegationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Orders'],
        ];
        $finder = $this->Delegations->find('all');
        if (isset($_GET['date'] ) && $_GET['date'] != '') {
            $finder->where(['Delegations.delegation_date' => $_GET['date']]);
        }
        $delegations = $this->paginate($finder);
        $filter = $this->request->getQuery('filter');
        $this->set(compact('delegations','filter'));
        // if it was ajax request use ajax_index view
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->setTemplate('ajax_index');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Delegation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $delegation = $this->Delegations->get($id, [
            'contain' => ['Users', 'Orders'],
        ]);

        $this->set('delegation', $delegation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $delegation = $this->Delegations->newEmptyEntity();
        if ($this->request->is('post')) {
            $delegation = $this->Delegations->patchEntity($delegation, $this->request->getData());
            if ($this->Delegations->save($delegation)) {
                $this->Flash->success(__('The delegation has been saved.'));
                //check if the post was ajaxtype
                if($this->request->is('ajax')) {
                    return $this->redirect(['action' => 'ajaxsuccess']);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delegation could not be saved. Please, try again.'));
        }
        $orders = $this->Delegations->Orders->find('list', ['limit' => 200]);
        $this->set(compact('delegation', 'orders'));
    }

    public function bulkDelegate()
    {
        $delegation = $this->Delegations->newEmptyEntity();
        if ($this->request->is('post')) {
            $saveData = $this->massCreateDelegations($this->request->getData());
            if ($this->Delegations->saveMany($saveData)) {
                $this->Flash->success(__('The delegation has been saved.'));
                //check if the post was ajaxtype
                if($this->request->is('ajax')) {
                    return $this->redirect(['action' => 'ajaxsuccess']);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delegation could not be saved. Please, try again.'));
        }
        $ids = $this->handelIdsVar();
        $condition = !empty($ids) ? ['id IN' => $ids] : ['id' => 0 ];
        $orders = $this->Orders->find('all')->where($condition)->toArray();
        $orderList=[];
        foreach ($orders as $order ){
            $orderList[$order->id] = DID($order->id).'-'.$order->receiver_name;
        }
        $this->set(compact('delegation', 'orderList'));
    }

    private function massCreateDelegations($data){
        $delegations = [];
        $delegationData = $data;
        foreach ($data['orders']['_ids'] as $orderId ) {
            $delegation = $this->Delegations->newEmptyEntity();
            $delegation->order_id = $orderId;
            $delegation->delegation_date = $delegationData['delegation_date'];
            $delegation->description = $delegationData['description'];
            $delegations[] = $delegation;
        }
        return $delegations;
    }



    function ajaxsuccess(){
        $this->viewBuilder()->setLayout('ajax');

    }

    /**
     * Edit method
     *
     * @param string|null $id Delegation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $delegation = $this->Delegations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $delegation = $this->Delegations->patchEntity($delegation, $this->request->getData());
            if ($this->Delegations->save($delegation)) {
                $this->Flash->success(__('The delegation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delegation could not be saved. Please, try again.'));
        }
        $users = $this->Delegations->Users->find('list', ['limit' => 200]);
        $orders = $this->Delegations->Orders->find('list', ['limit' => 200]);
        $this->set(compact('delegation', 'users', 'orders'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Delegation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $delegation = $this->Delegations->get($id);
        if ($this->Delegations->delete($delegation)) {
            $this->Flash->success(__('The delegation has been deleted.'));
        } else {
            $this->Flash->error(__('The delegation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
