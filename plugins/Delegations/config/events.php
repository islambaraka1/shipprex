<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use Cake\Controller\Controller;

$hooks = \App\Hooks\Hooks::getInstance();


// if setting is enabled for this plugin then add the listener
if (get_option_value('delegations_active_state') == 1) {

    if (!function_exists('delegation_buttons_add')) {
        $hooks->add_action('orders_list_top_buttons', 'delegation_buttons_add');
        function delegation_buttons_add()
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            echo $hooks->currentView->Html->link(__('List Delegated orders'), ['action' => 'index', 'controller' => 'Delegations', 'plugin' => 'Delegations'], ['class' => 'btn btn-primary']);

        }
    }

    if (!function_exists('delegation_action_button_in_order_row')) {
        $hooks->add_action('orders_action_buttons_list', 'delegation_action_button_in_order_row');
        function delegation_action_button_in_order_row($order)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            echo $hooks->currentView->Html->link(__('Delegate this order'), ['action' => 'add', 'controller' => 'Delegations', 'plugin' => 'Delegations', $order], ['class' => 'btn btn-primary ajax_form_open_modal']);

        }
    }

    //add menu items for this plugin
    $hooks->create_new_root_menu('delegations','Delegations','#','tasks',[1]);
    $hooks->add_sub_menu_item_to_root('delegations','List Delegations','delegations/delegations/',[1]);
    $hooks->add_sub_menu_item_to_root('delegations','Today Delegations','delegations/delegations/?date='.date("Y-m-d"),[1]);
    $hooks->add_sub_menu_item_to_root('delegations','Bulk Delegations','delegations/delegations/bulk_delegate',[1]);




    //show modal with todays delegations for one time only per user session
    if (!function_exists('delegation_modal')) {
        $hooks->add_action('before_orders_list_top_row', 'delegation_modal');
        function delegation_modal()
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $view = $hooks->currentView;
            // get today's delegations from the modal Delegations
            $delegationsModel = TableRegistry::getTableLocator()->get('Delegations.Delegations');
            $delegations = $delegationsModel->find('all')->where(['Delegations.delegation_date' => date("Y-m-d")])->toArray();
            if(count($delegations)>0){
                echo $view->element('Delegations.delegation_modal',['delegations' => $delegations]);
            }
        }
    }

    //add bulk delegation action to the bulk action dropdown
    if (!function_exists('delegation_bulk_action')) {
        $hooks->add_action('orders_bulk_action_dropdown', 'delegation_bulk_action');
        function delegation_bulk_action()
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $view = $hooks->currentView;
           echo"{  text: '".__('Delegate selected for a day')."',
                                action: function () {
                var rows = table.rows( { selected: true } ).data();
                                    selectedCallback(rows,'delegations/delegations/bulk_delegate');
                                }
                            },
                            ";

        }
    }

}
