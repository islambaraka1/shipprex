<?php
use Cake\Core\Configure;




Configure::write('Settings.Delegations', []);

easy_new_setting('Settings.Delegations','delegations_active_state','Delegation feature on or off','on_off',0);
easy_new_setting('Settings.Delegations','delegations_order_change_statues','Delegated orders new status','select',0,types_for_state);
easy_new_setting('Settings.Delegations','delegations_mark_order_note_field','Change the delegated order notes field to [DELEGATED TO : DATE]','on_off',0);

