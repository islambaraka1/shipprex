<?php
declare(strict_types=1);

namespace Delegations\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Delegations\Model\Table\DelegationsTable;

/**
 * Delegations\Model\Table\DelegationsTable Test Case
 */
class DelegationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Delegations\Model\Table\DelegationsTable
     */
    protected $Delegations;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Delegations.Delegations',
        'plugin.Delegations.Users',
        'plugin.Delegations.Orders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Delegations') ? [] : ['className' => DelegationsTable::class];
        $this->Delegations = TableRegistry::getTableLocator()->get('Delegations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Delegations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
