<?php
// this file is to be removed from future version - only created to fix the SENA Issue of country code
//TODO the migration for orders table to add country code
$countries = [
    'Saudi Arabia' => 'Saudi Arabia',
    'Kuwait' => 'Kuwait',
    'United Arab Emirates' => 'United Arab Emirates',
];
// add the country to the orders entity before find and save


// add the country to the orders create form
$hooks = \App\Hooks\Hooks::getInstance();
$hooks->add_action('orders_add_form_before_end', 'addCountryToOrderForm');
$hooks->add_action('orders_edit_form_before_end', 'addCountryToOrderForm');
function addCountryToOrderForm()
{
    $hooks = \App\Hooks\Hooks::getInstance();
    $countries = [
        'Saudi Arabia' => 'Saudi Arabia',
        'Kuwait' => 'Kuwait',
        'United Arab Emirates' => 'United Arab Emirates',
    ];
    echo $hooks->currentView->Form->control('country', ['options' => $countries]);
}

// add the country to the orders edit form

// add the country to the orders view form
$hooks->add_action('view_order_after_other_info', 'addCountryToOrderViewForm');
function addCountryToOrderViewForm($order)
{
    $hooks = \App\Hooks\Hooks::getInstance();
    echo "<h2>$order->country</h2>";
}

// add the country to the orders list view
$hooks->add_action('orders_list_view_table_after_meta', 'addCountryToOrderListHeader');
function addCountryToOrderListHeader()
{
    $hooks = \App\Hooks\Hooks::getInstance();
    echo checkbox_show_if_option_equels('columns_view_in_the_dashboard','country',
        '<th scope="col" data-column="country" data-searchable="true">Country</th>');
}

// add the country to the search logic and form filter

// add the country to the order excel import

