<?php

use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use OrderExtera\Controller\Component\TemplatesServiceComponent;

$hooks = \App\Hooks\Hooks::getInstance();

if(get_option_value('whats_app_enable') == 1){
    //create a new content with all of the available templates
    $TemplatesTable = TableRegistry::getTableLocator()->get('OrderExtera.Templates');
    $templates = $TemplatesTable->find('all')->toArray();
    define('AVAILABLE_TEMPLATES', $templates);
}

// add the button for WhatsApp Messages
if (!function_exists('WhatsAppMessages')) {
    if(get_option_value('whats_app_enable') == 1)
        $hooks->add_action('driver_online_order_actions_container', 'WhatsAppMessages');
    if(get_option_value('whats_app_enable_for_admin') == 1)
        $hooks->add_action('orders_action_after_buttons_list', 'WhatsAppMessages');
    function WhatsAppMessages($order)
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        $TemplateComponent = new TemplatesServiceComponent(new ComponentRegistry());
        $templates = $TemplateComponent->GetWhatsLinkForOrder($order);
        $newTempArray =[];
        foreach ($templates as $name =>$template) {
            $newTempArray[] = [
                'title' => $name,
                'url' => $template,
            ];
        }
        echo $hooks->currentView->Misc->generate_custom_dropdown_button('WhatsApp', 'whatsapp', $newTempArray, false);

    }
}
