<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use Cake\Controller\Controller;

$hooks = \App\Hooks\Hooks::getInstance();
$events = EventManager::instance();

include 'phone_events.php';
//only add the association if the table exists and the tags option is enabled in the config
$orders = TableRegistry::getTableLocator()->get('Orders');
$orders->addAssociations([
    'belongsToMany' => [
        'Tags' => [
            'className' => 'OrderExtera.Tags',
            'joinTable' => 'tags_orders',
            'foreignKey' => 'order_id',
            'targetForeignKey' => 'tag_id',
            'propertyName' => 'tags',
            'saveStrategy' => 'replace',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]
    ]
]);

$orders->addAssociations([
    'hasMany' => [
        'Meta' => [
            'className' => 'OrderExtera.Meta',
            'foreignKey' => 'model_key',
            'conditions' => [
                'Meta.model' => 'Orders',
            ],
            'finder' => 'MetaKeyValue',
        ],
    ],
]);

// the business logic of extra weight
$events->on('Orders.UpdatedOrderState',  function($event, $entity,$extra_weight) {
    $tableOrders = TableRegistry::getTableLocator()->get('Orders');
    $tableOrders->add_edit_meta('Orders','extra weight',$entity->id,$extra_weight);
    // add support for free upto - SHIP-222
    $free_upto = get_option_value('extra_weight_free_up_to');
    $extra_cost = 0;
    if($extra_weight > $free_upto){
        $extra_cost = $extra_weight * get_option_value('extra_weight_cost_per_extra_kg');
    }
    // update order cost fields with the extra cost
    $effected_fields = get_option_value('extra_weight_cost_added_to');
    if ($effected_fields == "COD"){
        $entity->cod = $entity->cod + $extra_cost;
    }elseif ($effected_fields == "Fees"){
        $entity->fees = $entity->fees + $extra_cost;
    }elseif ($effected_fields == "Both"){
        $entity->cod = $entity->cod + $extra_cost;
        $entity->fees = $entity->fees + $extra_cost;
    }
    //support note change if enabled
    if(get_option_value('extra_weight_added_note_for_extra_kg') == 1){
        $entity->notes = $entity->notes . ' - ' . $extra_cost . ' extra weight cost';
    }
    $tableOrders->save($entity);
});

// Extra weight settings
if(get_option_value('extra_weight_enable') == 1){
    easy_add_custom_field('Orders','extra weight','number',0,
        get_option_value('extra_weight_added_to_create_form'),
        get_option_value('extra_weight_added_to_edit_form'),
        get_option_value('extra_weight_show_in_list_view'),1,['policy'=>1,'slip'=>1,'print'=>1]);



    // add the button for feature of partial delivered
    if (!function_exists('extra_weight_add_button')) {
        $hooks->add_action('orders_action_buttons_list', 'extra_weight_add_button');
        function extra_weight_add_button($id)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            echo $hooks->currentView->Html->link(__('Extra Weight'), ['action' => 'add', 'controller' => 'extra',
                'plugin' => 'OrderExtera', $id],['class' => 'dropdown-item btn btn-secondary btn-sm ajax_form_open_modal']);
        }
    }

    // the business logic of extra weight
    $events->on('Orders.extra_weighted',  function($event, $entity,$extra_weight) {
        $tableOrders = TableRegistry::getTableLocator()->get('Orders');
        $tableOrders->add_edit_meta('Orders','extra weight',$entity->id,$extra_weight);
        // add support for free upto - SHIP-222
        $free_upto = get_option_value('extra_weight_free_up_to');
        $extra_cost = 0;
        if($extra_weight > $free_upto){
            $extra_cost = $extra_weight * get_option_value('extra_weight_cost_per_extra_kg');
        }
        // update order cost fields with the extra cost
        $effected_fields = get_option_value('extra_weight_cost_added_to');
        if ($effected_fields == "COD"){
            $entity->cod = $entity->cod + $extra_cost;
        }elseif ($effected_fields == "Fees"){
            $entity->fees = $entity->fees + $extra_cost;
        }elseif ($effected_fields == "Both"){
            $entity->cod = $entity->cod + $extra_cost;
            $entity->fees = $entity->fees + $extra_cost;
        }
        //support note change if enabled
        if(get_option_value('extra_weight_added_note_for_extra_kg') == 1){
            $entity->notes = $entity->notes . ' - ' . $extra_cost . ' extra weight cost';
        }
        $tableOrders->save($entity);
    });


}


if(get_option_value('partial_module_enable') == 1){

    easy_add_custom_field('Orders','partially','on_off',0,0,1,1,1,['policy'=>1,'slip'=>1,'print'=>1]);
    // add the button for feature of partial delivered
    if (!function_exists('partial_add_buttons')) {
        $hooks->add_action('driver_view_action_status_update', 'partial_add_buttons');
        $hooks->add_action('orders_action_buttons_list', 'partial_add_buttons');
        // add driver workflow
        if(get_option_value('partial_add_driver_partial_workflow') == 1){
            $hooks->add_action('driver_online_order_actions', 'partial_add_buttons');
        }
        function partial_add_buttons($id)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            echo $hooks->currentView->Html->link(__('Partial Delivery'), ['action' => 'add', 'controller' => 'Partial',
                'plugin' => 'OrderExtera', $id],['class' => 'dropdown-item btn btn-secondary btn-sm ajax_form_open_modal']);
        }
    }
    //add the form element for creating partial orders
    if (!function_exists('orders_partially_form')) {
        $hooks->add_action('orders_add_form_before_end', 'orders_partially_form');
        function orders_partially_form($id=null)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $fields = get_meta_array('Orders');
            $hooks->currentView->SettingFields->setting = "Meta";
            if(is_array($fields)){
                foreach ($fields as $key=> $field){
                    if($field['show_in_add'] == 1){
                       echo  $hooks->currentView->SettingFields->generate_html_output_for_field($key,$fields[$key]);
                    }
                }
            }
        }
    }
    // the edit form for partially orders
    if (!function_exists('orders_partially_form_edit')) {
        $hooks->add_action('orders_edit_form_before_end', 'orders_partially_form_edit');
        function orders_partially_form_edit($order)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $fields = get_meta_array('Orders');
            $hooks->currentView->SettingFields->setting = "Meta";
            if(is_array($fields)){
                foreach ($fields as $key=> $field){
                    if($field['show_in_edit'] == 1){
                        $fields[$key]['field']['value']= getMetaByName($order->meta,$key)??$field['default'];
                       echo  $hooks->currentView->SettingFields->generate_html_output_for_field($key,$fields[$key]);
                    }
                }
            }
        }
    }

    function getMetaByName(array $meta,$name):mixed {
        //search the array for the name
        foreach ($meta as $item){
            if($item['name'] == $name){
                return $item->toArray()['value'];
            }
        }
        return [];
    }



    if (!function_exists('display_orders_custom_fields')) {
        $hooks->add_action('view_Order_Driver_Info', 'display_orders_custom_fields');
        function display_orders_custom_fields($order=null)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $fields = get_meta_array('Orders');
            echo  $hooks->currentView->element('OrderExtera.detailed_meta', ['metas' => $fields,'order'=>$order]);
        }

    }

    if (!function_exists('display_orders_custom_fields_in_print')) {
        $hooks->add_action('orders_print_under_bills_info', 'display_orders_custom_fields_in_print');
        function display_orders_custom_fields_in_print($order=null)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $fields = get_meta_array('Orders');
            echo  $hooks->currentView->element('OrderExtera.text_meta', ['metas' => $fields,'order'=>$order]);
        }

    }

    if (!function_exists('display_custom_fields_under_id_seller_print')) {
        $hooks->add_action('invoices_print_under_id', 'display_custom_fields_under_id_seller_print');
        function display_custom_fields_under_id_seller_print($order=null)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            $fields = get_meta_array('Orders');
            echo  $hooks->currentView->element('OrderExtera.text_meta', ['metas' => $fields,'order'=>$order]);
        }
    }



    // trigger after the event model were called right after the partially delivered status

    $events->on('Orders.partially_delivered',  function($event, $entity){
        $tableOrders = TableRegistry::getTableLocator()->get('Orders');
        $tableOrders->add_edit_meta('Orders','partially',$entity->id,1);
        //add action for the order as partially delivered
        if (get_option_value('broadcast_notifications_on_partial_delivered')==1){
            $notificationTable = TableRegistry::getTableLocator()->get('Notifictions.Notifications');
            $notificationTable->sendNotificationForAdminAndSellerOfOrder($entity,__('Partially Delivered'));
        }
    });



}

//add button for tagging the order in online view for driver dashboard
if (!function_exists('add_tag_button')) {
    $hooks->add_action('driver_online_order_actions', 'add_tag_button');
    function add_tag_button($id)
    {
//        $hooks = \App\Hooks\Hooks::getInstance();
//        $tagsTable = TableRegistry::getTableLocator()->get('OrderExtera.Tags');
//        $tags = $tagsTable->find()->all()->toArray();
//        foreach ($tags as $tag) {
//            echo $hooks->currentView->Html->link($tag->name, ['action' => 'add_tag', 'controller' => 'Tags',
//                'plugin' => 'OrderExtera', $id, $tag->id], ['class' => 'dropdown-item btn btn-secondary btn-sm']);
//        }
    }
}


//enable disable the tags for driver
if(get_option_value('tags_enable_for_driver_online') == 1){
    //add custom filter
    $hooks->add_filter('update_order_link', 'update_link');
    function update_link($data)
    {
        $data['controller'] = 'Tags';
        $data['action'] = 'updatenew';
        $data['plugin'] = 'OrderExtera';
        return $data;
    }
    $hooks->add_filter('update_order_link_ajax_implementation', 'update_link_class');
    function update_link_class($data)
    {
        $data = 'dropdown-item btn btn-secondary btn-sm ajax_form_open_modal';
        return $data;
    }
}


//add the tagging options to admin view for orders
if (!function_exists('add_tag_button_admin') && get_option_value('tags_enable_for_admin') == 1){
    $hooks->add_action('orders_action_after_buttons_list', 'add_tag_button_admin');
    function add_tag_button_admin($id)
    {
        $id = $id->id;
        // handel the way we should display the tags
        $hooks = \App\Hooks\Hooks::getInstance();
        $tagsTable = TableRegistry::getTableLocator()->get('OrderExtera.Tags');
        $tags = $tagsTable->find()->all()->toArray();
        $tags_url = [];
        foreach ($tags as $tag) {
            $tags_url[] = ['title' => $tag->name, 'url' => $hooks->currentView->Url->build(['action' => 'add-tag', 'controller' => 'Tags',
                'plugin' => 'OrderExtera', $id, $tag->id])];
        }
        echo $hooks->currentView->Misc->generate_custom_dropdown_button('Tags','tags', $tags_url);
    }
}

//add the tagging to order detailed view
if (!function_exists('add_tag_on_order_detailed_view')) {
    $hooks->add_action('view_order_after_receiver_details', 'add_tag_on_order_detailed_view');
    function add_tag_on_order_detailed_view($order)
    {
        // handel the way we should display the tags
        $hooks = \App\Hooks\Hooks::getInstance();
        $orderTable = TableRegistry::getTableLocator()->get('Orders');
        $orderWithTags = $orderTable->find()->contain(['Tags'])->where(['Orders.id' => $order->id])->first();


        echo $hooks->currentView->Misc->displayTags($orderWithTags->tags);
    }
}





//
//
//$hooks->create_new_root_menu('accountant','Accountant','#','money-bill',[1,4]);
//$hooks->add_sub_menu_item_to_root('accountant','Dashboard','accountants/main/dashboard',[1,4]);
//$hooks->add_sub_menu_item_to_root('accountant','Close Day','accountants/main/closing',[1]);
//$hooks->add_sub_menu_item_to_root('accountant','Daily transactions','accountants/main/balance',[1,4]);
//
//add the tagging to order detailed view
if (!function_exists('add_settings_menu')) {
    $hooks->add_action('settings_menu_patching', 'add_settings_menu');
    function add_settings_menu()
    {
        // handel the way we should display the tags
        $hooks = \App\Hooks\Hooks::getInstance();
        $hooks->add_sub_menu_item_to_root('settings','Templates Management ','order-extera/templates/',[1]);


    }
}




if(get_option_value('tags_enable_for_driver_online') ==1) {
//Tags to be added to status change
    if (!function_exists('add_tags_under_status')) {
        $hooks->add_filter('status_button_driver', 'add_tags_under_status');
        function add_tags_under_status($statusHtml, $order)
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            return $statusHtml . '<br> ' . $hooks->currentView->Misc->displayTags($order->tags);
        }
    }
}
