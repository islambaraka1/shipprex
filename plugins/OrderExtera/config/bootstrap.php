<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
$country_code =  get_option_value('intl_local_country_code')?get_option_value('intl_local_country_code'):"EG";
define("DEFINED_COUNTRY_CODE",$country_code);
require_once 'settings.php';
require_once 'events.php';
require_once 'country.php';
