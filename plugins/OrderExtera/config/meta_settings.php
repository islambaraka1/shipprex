<?php
use Cake\Core\Configure;
//    easy_add_custom_field('Orders','partially','on_off',0,1,1,1,['policy'=>1,'slip'=>1]);
Configure::write('Meta_map',[]);


/**
 * this method is a shortcut for adding a new custom fields for any model
 * @param $model
 * @param $key
 * @param $type
 * @param $default
 * @param $show_in_add
 * @param $edit
 * @param $view_table
 * @param $detailed
 * @param $print_array
 * @param $options
 * @param $help
 * @param $parent_tab
 * @return void "true if the setting added / false if not"
 */
function easy_add_custom_field($model,$key,$type,$default,$show_in_add,$edit,$view_table,$detailed,$print_array=[],$fixOptopn= true,$options = null,$help = ''){
    $data = [
        $key  =>[
            'name'                  => __($key),
            'slug'                  => $key,
            'value'                 => $default,
            'default'               => $default,
            'fixOptions'            => $fixOptopn,
            'show_in_add'           => $show_in_add,
            'show_in_edit'          => $edit,
            'show_in_view_table'    => $view_table,
            'show_in_view_detail'   => $detailed,
            'print_in'              => $print_array,
            'field'     =>[
                'type'      =>$type,
                'field_help'=>__($help),
                'options' => $options,
                'on_click'  =>null,
                'class'     =>null,
                'id'        =>'',
                'parent_tab'=>null,
                'value'      => $default
            ]
        ]
    ];
    set_new_meta($model,$data);
}

/**
 * this is a method for appending custom fields in the Configuration
 * @param $metaPath
 * @param $metaArray
 * @return bool
 */
function set_new_meta($metaPath,$metaArray){
    $target = Configure::read('Meta_map.'.$metaPath);
    if($target === null){
        Configure::write('Meta_map.'.$metaPath,[]);
        $target = Configure::read('Meta_map.'.$metaPath);
    }
    $target = array_merge($target,$metaArray);
    Configure::write('Meta_map.'.$metaPath,$target);
    return true;
}

/**
 * read the meta array for any model
 * @param $metaPath
 * @return array|false[]|mixed
 */
function get_meta_array($metaPath){
    return Configure::read('Meta_map.'.$metaPath);
}

function render_meta_fields($metaPath,$entity,$options = []){
    $metaArray = get_meta_array($metaPath);
    $html = '';
    if($metaArray != null){
        foreach ($metaArray as $key => $meta){
            $html .= render_meta_table_cell($key,$meta,$entity,$options);
        }
    }
    return $html;
}

function render_meta_columns($metaPath){
    $metaArray = get_meta_array($metaPath);
    $html = '';
    if($metaArray != null){
        foreach ($metaArray as $key => $meta){
            $html .= render_meta_column($key,$meta);
        }
    }
    return $html;
}

function render_meta_column($key,$meta){
    $html = '';
    if($meta['show_in_view_table'] == 1){
        $html .= '<th scope="col" data-searchable="false" data-column="meta.'.$key.'">'.__($meta['name']).'</th>';
    }
    return $html;
}

function render_meta_table_cell($key,$meta,$entity,$options = []){
    $html = '';
    if($meta['show_in_view_table'] == 1){
        if(isset($entity->meta[$key])){
            $html .= '<td>'.render_icon($entity->meta[$key]).'</td>';
        } else {
            $html .= '<td>N/A</td>';
        }
    }
    return $html;
}

//function ro conver 0,1 to icon for the table
function render_icon($value){
    if($value == 1){
        return '<i class="fa fa-check-circle" style="color: green"></i>';
    }elseif($value == 0 || $value == null || $value == ''){
        return '<i class="fa fa-times-circle" style="color: red"></i>';
    }else{
        return $value;
    }
}


