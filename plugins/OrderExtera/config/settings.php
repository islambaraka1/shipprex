<?php
use Cake\Core\Configure;


Configure::write('Settings.Extras', []);
////////// partial payment options //////////

// create label for the setting
easy_new_setting('Settings.Extras','partial_label','Partial delivery options  ',"label");
easy_new_setting('Settings.Extras','partial_module_enable','Enable or Disable the partial delivery orders',"on_off",0);
easy_new_setting('Settings.Extras','partial_status','what default status for partial delivery',"select",0,types_for_state);
easy_new_setting('Settings.Extras','partial_add_partial_statues',
    'Add Partial delivered status',"on_off",0,null,"this option cancel the default status and add new status for partial delivery");

easy_new_setting('Settings.Extras','partial_add_driver_partial_workflow','Add Partials delivered workflow for driver' ,"on_off",0,
    null,"this option add new workflow for partial delivery for the driver dashboard ");
require_once 'meta_settings.php';



// NNotification settings
easy_new_setting('Settings.Extras','broadcast_notifications_on_partial_delivered','Send Notifications for the seller/admin on driver making ',"on_off",0);



// Extra weight settings
easy_new_setting('Settings.Extras','extra_weight_label','Extra weight setup options  ',"label");
easy_new_setting('Settings.Extras','extra_weight_enable','Enable or Disable the Extra weight for orders',"on_off",0);
easy_new_setting('Settings.Extras','extra_weight_added_to_create_form','Show extra weight on create new order',"on_off",0,null,
    "this option will show the extra weight field on the create new order form - this wont affect the cod or fees fields");
easy_new_setting('Settings.Extras','extra_weight_added_to_edit_form','Show extra weight on Edit order',"on_off",0,null,
    "this option will show the extra weight field on the EDIT order form - this wont affect the cod or fees fields");
easy_new_setting('Settings.Extras','extra_weight_show_in_list_view','Display extra weight inside list orders table ',"on_off",0);
easy_new_setting('Settings.Extras','extra_weight_cost_per_extra_kg','Cost per extra kg',"number",0);
easy_new_setting('Settings.Extras','extra_weight_cost_added_to','Extra Weight cost should affect COD/FEES column or both',"select",null,[
    'cod'=>'COD',
    'fees'=>'Fees',
    'both'=>'Both'
]);
easy_new_setting('Settings.Extras','extra_weight_free_up_to','Free up to Kg',"number",0,null,'Just a information for the customer on orders creation');
easy_new_setting('Settings.Extras','extra_weight_added_note_for_extra_kg','should include the updated extra weight on notes filed',"on_off",0);



////Tags settings
easy_new_setting('Settings.Extras','tags_label','Tags options  ',"label");
easy_new_setting('Settings.Extras','tags_enable_for_driver_online','Enable or Disable the Tags for driver view',"on_off",0);
easy_new_setting('Settings.Extras','tags_enable_for_admin','Enable or Disable the Tags for admin view',"on_off",0);
easy_new_setting('Settings.Extras','tags_enable_for_admin_type','Type of tagging action for admin',"select",null,[
    'update'=>'Display in the Update Order dropdown',
    'tag_button'=>'Display Tagging Button'
],'',false);
easy_new_setting('Settings.Extras','tags_display_style','Tags should display as:',"select",null,[
    'sub_status'=>'Sub Status',
    'notes_append'=>'Append to the notes',
    'separate_field'=>'Separate field'
],'',false);
easy_new_setting('Settings.Extras','tags_enable_for_admin_bulk','Enable or Disable the Tags for admin bulk actions',"on_off",0);
easy_new_setting('Settings.Extras','tags_enable_for_seller','Enable or Disable the Tags for seller view',"on_off",0);


easy_new_setting('Settings.Extras','tags_enable_for_driver_view','Allow admin to see Tags on orders for driver view ',"on_off");


//WHATSAPP

easy_new_setting('Settings.Extras','whats_app_label','Whatsapp options',"label");
easy_new_setting('Settings.Extras','whats_app_enable','Enable or Disable the Whatsapp',"on_off",0,null,
    "this option will enable the whatsapp button on the driver dashboard");
easy_new_setting('Settings.Extras','whats_app_enable_for_admin','Enable or Disable the Whatsapp for admin',"on_off",0,null,
    "this option will enable the whatsapp button on the admin dashboard");
