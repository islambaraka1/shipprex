<?php
namespace OrderExtera\Automation;
abstract class AutomationHandler
{
    var $controller;
    var $model;
    var $handler_starting_event;
    var $handler_affecting_entity;
    var $handler_name;
    var $handler_description;
    function __construct($controller = null, $model = null) {
//        $this->controller = $controller;
    }

    function getHandlerName() {
        return $this->handler_name;
    }

    function getHandlerDescription() {
        return $this->handler_description;
    }

    abstract function run($entity, $config);

}
