<?php
namespace OrderExtera\Automation\Handlers;

use OrderExtera\Automation\AutomationHandler;

class TagHandler extends AutomationHandler
{
    var $handler_starting_event = 'order_link_tag';
    var $handler_affecting_entity = 'Order';
    var $handler_name = 'TagHandler';
    var $handler_description = 'TagHandler';
    function run($order, $config) {
        return true;
    }
}
