<?php
/*
 * @var \App\View\AppView $this
 * @var $metas
 */
?>

<h2><?=__('Custom fields')?></h2>
<div class="table-responsive" style="max-height: 250px;overflow-y: scroll">


<table class="table table-striped ">
    <thead class="thead-default">
    <tr>
        <th><?=__('Name')?></th>
        <th><?=__('Value')?></th>
    </tr>
    </thead>
    <tbody>
<?php
if(is_array($metas)){
    foreach ($metas as $meta) {
        if($meta['show_in_view_detail'] == 1){
            echo $this->element('OrderExtera.meta_row', ['meta' => $meta,'order'=>$order]);
        }
    }
}
?>

    </tbody>
</table>
</div>
