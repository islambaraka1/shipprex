<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $template
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Templates'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="templates form content">
    <?= $this->Form->create($template) ?>
    <fieldset>
        <legend><?= __('Add Template') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type', ['options' => $availableTypes]);
            echo $this->Form->control('content',['help'=>"
            <p>Use the following tags to add order data to the template</p>
            {order_id} - Order ID<br>
            {receiver_name} - Receiver Name<br>
            {receiver_phone} - Receiver Phone<br>
            {receiver_address} - Receiver Address<br>
            {cod} - COD<br>
            {seller} - Seller Name<br>
            {seller.phone} - Seller Phone<br>
            {driver} - Driver Name<br>
            {driver.phone} - Driver Phone<br>
            {company} - Company Name<br>
            "]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
