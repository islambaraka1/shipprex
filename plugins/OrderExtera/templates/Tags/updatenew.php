<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $partial
 */
$orderIdFromUrl = $this->request->getParam('pass.0');
$orderState = $this->request->getParam('pass.1');
?>

<div class="delegations form content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading"><?= __('Update Order') ?></h4>
            <p><?=__("The following order ")."#".DID($orderIdFromUrl)." ".__("Is going to be changed to ").$orderState?></p>
            <p class="mb-0"><?=__("Please select the sub reason as a tag for the order ")?></p>
        </div>
        <?php
        echo $this->Form->control('tags',['label'=>__('Sub Tag'),
            'type'=>'select' ,'options' => $tags,'empty' => true,'class'=>'forceShow']);
        $attrs = $orderIdFromUrl > 0 ? ['value' => $orderIdFromUrl,'type'=>'hidden'] : ['options' => $orders];
        echo $this->Form->control('order_id', $attrs);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
