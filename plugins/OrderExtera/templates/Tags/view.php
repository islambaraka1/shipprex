<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $tag
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Tag'), ['action' => 'edit', $tag->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Tag'), ['action' => 'delete', $tag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tag->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Tags'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Tag'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="tags view large-9 medium-8 columns content">
    <h3><?= h($tag->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($tag->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Slug') ?></th>
                <td><?= h($tag->slug) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Parent Status') ?></th>
                <td><?= h($tag->parent_status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Automation Handler') ?></th>
                <td><?= h($tag->automation_handler) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($tag->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($tag->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($tag->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($tag->orders)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Pickup Id') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
                    <th scope="col"><?= __('Receiver Name') ?></th>
                    <th scope="col"><?= __('Receiver Email') ?></th>
                    <th scope="col"><?= __('Receiver Phone') ?></th>
                    <th scope="col"><?= __('Receiver Address') ?></th>
                    <th scope="col"><?= __('City') ?></th>
                    <th scope="col"><?= __('Work Address') ?></th>
                    <th scope="col"><?= __('Package Description') ?></th>
                    <th scope="col"><?= __('Cod') ?></th>
                    <th scope="col"><?= __('Notes') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Fees') ?></th>
                    <th scope="col"><?= __('Driver Id') ?></th>
                    <th scope="col"><?= __('Invoice Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($tag->orders as $orders): ?>
                <tr>
                    <td><?= h($orders->id) ?></td>
                    <td><?= h($orders->user_id) ?></td>
                    <td><?= h($orders->pickup_id) ?></td>
                    <td><?= h($orders->type) ?></td>
                    <td><?= h($orders->reference) ?></td>
                    <td><?= h($orders->receiver_name) ?></td>
                    <td><?= h($orders->receiver_email) ?></td>
                    <td><?= h($orders->receiver_phone) ?></td>
                    <td><?= h($orders->receiver_address) ?></td>
                    <td><?= h($orders->city) ?></td>
                    <td><?= h($orders->work_address) ?></td>
                    <td><?= h($orders->package_description) ?></td>
                    <td><?= h($orders->cod) ?></td>
                    <td><?= h($orders->notes) ?></td>
                    <td><?= h($orders->statues) ?></td>
                    <td><?= h($orders->fees) ?></td>
                    <td><?= h($orders->driver_id) ?></td>
                    <td><?= h($orders->invoice_id) ?></td>
                    <td><?= h($orders->created) ?></td>
                    <td><?= h($orders->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
