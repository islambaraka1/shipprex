<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $delegation
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $partial
 */
?>

<div class="delegations form content">
    <?= $this->Form->create($partial) ?>
    <fieldset>
        <legend><?= __('Partial delivered') ?></legend>
        <?php
            echo $this->Form->control('cod',['type' => 'number','label'=>__('New COD')]);
            echo $this->Form->control('notes',['label'=>__('Reason for review')]);
            $orderIdFromUrl = $this->request->getParam('pass.0');
            $attrs = $orderIdFromUrl > 0 ? ['value' => $orderIdFromUrl,'type'=>'hidden'] : ['options' => $orders];
            echo $this->Form->control('order_id', $attrs);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
