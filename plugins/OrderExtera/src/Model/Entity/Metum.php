<?php
declare(strict_types=1);

namespace OrderExtera\Model\Entity;

use Cake\ORM\Entity;

/**
 * Metum Entity
 *
 * @property int $id
 * @property string $model
 * @property string $name
 * @property string $value
 * @property int $user_id
 * @property int $model_key
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 *
 * @property \OrderExtera\Model\Entity\User $user
 */
class Metum extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'model' => true,
        'name' => true,
        'value' => true,
        'user_id' => true,
        'model_key' => true,
        'created_at' => true,
        'modified_at' => true,
        'user' => true,
    ];
}
