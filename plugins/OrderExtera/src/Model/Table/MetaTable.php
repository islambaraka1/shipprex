<?php
declare(strict_types=1);

namespace OrderExtera\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Meta Model
 *
 * @property \OrderExtera\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \OrderExtera\Model\Entity\Metum newEmptyEntity()
 * @method \OrderExtera\Model\Entity\Metum newEntity(array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Metum[] newEntities(array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Metum get($primaryKey, $options = [])
 * @method \OrderExtera\Model\Entity\Metum findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \OrderExtera\Model\Entity\Metum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Metum[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Metum|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OrderExtera\Model\Entity\Metum saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OrderExtera\Model\Entity\Metum[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Metum[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Metum[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Metum[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MetaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('meta');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'OrderExtera.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->requirePresence('model', 'create')
            ->notEmptyString('model');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->requirePresence('value', 'create')
            ->notEmptyString('value');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {

        return $rules;
    }


    public function findMetaKeyValue(Query $query, array $options)
    {
        return $query->formatResults(function ($results) {
            return   $results->map(function ($row, $key) {
//                    if($row != null){
//                        $meta = [];
//                        $meta[$row->name] = $row->value;
//                    }
                    return $row;

                });
        });
    }

}
