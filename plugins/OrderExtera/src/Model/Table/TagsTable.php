<?php
declare(strict_types=1);

namespace OrderExtera\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Tags Model
 *
 * @property \OrderExtera\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsToMany $Orders
 *
 * @method \OrderExtera\Model\Entity\Tag newEmptyEntity()
 * @method \OrderExtera\Model\Entity\Tag newEntity(array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Tag[] newEntities(array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Tag get($primaryKey, $options = [])
 * @method \OrderExtera\Model\Entity\Tag findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \OrderExtera\Model\Entity\Tag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Tag[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \OrderExtera\Model\Entity\Tag|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OrderExtera\Model\Entity\Tag saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OrderExtera\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \OrderExtera\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TagsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tags');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Orders', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'tags_orders',
            'className' => 'OrderExtera.Orders',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmptyString('slug');

        $validator
            ->scalar('parent_status')
            ->maxLength('parent_status', 255)
            ->allowEmptyString('parent_status');

        return $validator;
    }

    function linkToOrder($tagId, $orderId)
    {
        $crossTable = TableRegistry::getTableLocator()->get('OrderExtera.TagsOrders');
        $crossTable->addBehavior('Timestamp');
        $entity = $crossTable->newEmptyEntity();
        $entity->tag_id = $tagId;
        $entity->order_id = $orderId;
        if ($crossTable->findOrCreate(['tag_id' => $tagId, 'order_id' => $orderId])) {
            //brodcast event OrderTagged
            $this->getEventManager()->dispatch(new \Cake\Event\Event('OrderExtera.OrderTagged', $this, ['tagId' => $tagId, 'orderId' => $orderId]));
            return $entity;
        }
        return false;
    }
}
