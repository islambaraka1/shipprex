<?php
declare(strict_types=1);

namespace OrderExtera\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
/**
 * Meta behavior
 */
class MetaBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(Table $table, array $config = [])
    {
        parent::__construct($table, $config);
        $table->addAssociations([
            'hasMany' => [
                'Meta' => [
                    'className' => 'OrderExtera.Meta',
                    'foreignKey' => 'model_key',
                    'conditions' => [
                        'Meta.model' => $table->getRegistryAlias(),
                    ],
                ],
            ],
        ]);
    }

//    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options, bool $primary)
//    {
//
//        //check if string contains 'SELECT 1 AS existing FROM orders'
//        // FIX other models should be implimented here
//        if (strpos($query->sql(), $this->_table->getRegistryAlias().'.id') !== false AND strpos($query->sql(), 'SELECT 1 AS existing FROM orders') === false) {
//            $query->contain(['Meta']);
//            $query->formatResults(function ($results) {
//                return $results->map(function ($row) {
//                    if($row != null){
//                        $meta = [];
//                        foreach ($row->meta as $m) {
//                            $meta[$m->name] = $m->value;
//                        }
//                        $row->set('meta', $meta);
//                        return $row;
//                    }
//
//                });
//            });
//        }
//
//    }


    function afterSave(EventInterface $event, EntityInterface $entity, ArrayObject $options)
    {
        if (isset($_POST['Meta']) && is_array($_POST['Meta'])) {
            $meta = $_POST['Meta'];
            $metaTable = TableRegistry::getTableLocator()->get('OrderExtera.Meta');
            foreach ($meta as $key => $value) {
                $this->add_edit_meta($this->_table->getRegistryAlias(), $key , $entity->id, $value);
            }
        }
    }

    function get_record_meta($model,$key){
        $meta = TableRegistry::getTableLocator()->get('Meta');
        $meta = $meta->find()->where(['model' => $model, 'model_key' => $key])->all();

        return $meta;
    }

    /**
     * @desc Save meta data or update an old one if it exists
     * This method should be the only place to update create a meta data record
     * @param $model
     * @param $key
     * @param $recordId
     * @param $value
     * @return mixed
     */

    function add_edit_meta($model,$key,$recordId,$value = null){
        $metaTable = TableRegistry::getTableLocator()->get('OrderExtera.Meta');
        $meta = $metaTable->find()->where(['model' => $model, 'model_key' => $recordId,'name' =>$key])->first();
        if($meta == null){
            $meta = $metaTable->newEmptyEntity();
            $meta->model = $model;
            $meta->model_key = $recordId;
            $meta->name = $key;
            $meta->value = $value;
            $meta->created_at = date('Y-m-d H:i:s');
            $meta->modified_at = date('Y-m-d H:i:s');
            $meta->user_id = $_SESSION['Auth']['User']['id'];
            $meta = $metaTable->save($meta);
        }else{
            $meta->modified_at = date('Y-m-d H:i:s');
            $meta->user_id = $_SESSION['Auth']['User']['id'];
            $meta->value = $value;
            $meta = $metaTable->save($meta);
        }
        return $meta;
    }


}
