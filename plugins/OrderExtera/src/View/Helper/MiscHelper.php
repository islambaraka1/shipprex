<?php
declare(strict_types=1);

namespace OrderExtera\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Misc helper
 */
class MiscHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    function generate_custom_dropdown_button($title,$icon,$action_list,$useAjax = true){
        $html = '<div class="dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-'.$icon.'"></i> '.$title.'
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
        foreach($action_list as $action){
            $class = $useAjax ? 'ajaxinLink' : '';
            $html .= '<a class="dropdown-item '.$class.'" href="'.$action['url'].'">'.$action['title'].'</a>';
        }
        $html .= '</div>
            </div>';
        return $html;
    }


    function displayTags($tags){
        $html = '';
        foreach($tags as $tag){
            //display the tag as badge with icon of font awsom and the tag name
            $html .= '<span class="badge badge-secondary"><i class="fa fa-tag"></i> '.$tag->name.'</span> ';
        }
        return $html;
    }



}
