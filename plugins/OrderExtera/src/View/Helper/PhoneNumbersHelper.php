<?php
declare(strict_types=1);

namespace OrderExtera\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use libphonenumber\PhoneNumberUtil;


/**
 * PhoneNumbers helper
 */
class PhoneNumbersHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    function phoneIt($input){
        //extract phone number out of text string
        $inputEn = $this->convertAnyArabicNumbers($input);
        //if phone number is too long, it's probably not a phone number
        if(!PhoneNumberUtil::isViablePhoneNumber($inputEn)){
            return $input;
        }
        if(strlen($inputEn) > 15){
            return $input;
        }
        try {
            $phone = $this->extractPhone($inputEn);
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $phone = $phoneUtil->parse((int) $phone, DEFINED_COUNTRY_CODE);
            $phone = $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::E164);
            return "<a href='tel:$phone'>$phone</a>";
        } catch (\Exception $e) {
            return $input;
        }
//        return $input;
    }

    function convertAnyArabicNumbers($input){
        $arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
        $english = array('0','1','2','3','4','5','6','7','8','9');
        return str_replace($arabic, $english, $input);
    }

    function extractPhone($input){
        //extract phone number out of text string
        $phone_number = preg_replace('/[^0-9]/', '', $input);
        return $phone_number;
    }

}
