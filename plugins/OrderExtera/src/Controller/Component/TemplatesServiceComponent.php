<?php
declare(strict_types=1);

namespace OrderExtera\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use libphonenumber\PhoneNumberUtil;

/**
 * TemplatesService component
 */
class TemplatesServiceComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    /**
     * @var /App/OrderExtrea/Model/Table/TemplatesTable
     *
     */
    private $Templates;
    public $availableTypes = [
        'SMS' => 'SMS',
        'EMAIL' => 'EMAIL',
        'PUSH' => 'PUSH',
        'WHATSAPP' => 'WHATSAPP',
    ];

    public function getTemplates()
    {
        return AVAILABLE_TEMPLATES ?? [];
    }

    // slug the name
    public function slugify($text)
    {
       return Text::slug($text);
    }

    public function getTemplateBySlug($slug)
    {
        $templates = AVAILABLE_TEMPLATES ?? [];
        foreach ($templates as $template) {
            if($template->slug == $slug){
                return $template;
            }
        }
        return null;
    }
    public function getTemplatesByType($type)
    {
        $templates = AVAILABLE_TEMPLATES ?? [];
        $return = [];
        foreach ($templates as $template) {
            if($template->type == $type){
                $return[] = $template;
            }
        }
        return $return;
    }

    //functions to add order data to the template
    public function addOrderData($template, $order)
    {
        $orderEntity = $order;
        $template = str_replace('{order_id}', (string) DID($orderEntity->id), $template);
        $template = str_replace('{receiver_name}', (string) $orderEntity->receiver_name, $template);
        $template = str_replace('{receiver_phone}',(string)  $orderEntity->receiver_phone, $template);
        $template = str_replace('{receiver_address}',(string) $orderEntity->receiver_address, $template);
        $template = str_replace('{cod}', (string) $orderEntity->cod, $template);
        $template = str_replace('{seller}', (string) $orderEntity->user->username, $template);
        $template = str_replace('{seller.phone}', (string) $orderEntity->user->phone, $template);
        $template = str_replace('{driver}', (string) $orderEntity->drivers[0]->name, $template);
        $template = str_replace('{driver.phone}', (string) $orderEntity->drivers[0]->phone, $template);
        $template = str_replace('{company}', (string) SETTING_KEY['global']['site_settings_']['site_name'], $template);
        return $template;
    }

    function HandelTemplateMessage($template, $orderId)
    {
        $template = $this->addOrderData($template, $orderId);
        return $template;
    }

    function GetWhatsLinkForOrder($order)
    {

        $orderEntity =$order;
        $template = $this->getTemplatesByType('WHATSAPP');
        $templates = [];
        foreach ($template as $temp) {
            $content = $this->HandelTemplateMessage($temp->content, $order);
            $link = "https://api.whatsapp.com/send?phone=" . $this->convertPhoneToWhats($orderEntity->receiver_phone) . "&text=" . urlencode($content);
            $templates[$temp->name] = $link;
        }
        return $templates;
    }
    //function to convert phone number to egyptian wahtsapp number
    function convertPhoneToWhats($phone)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        //if $phone is gretter than 15 digits return it as it is
        if (strlen($phone) > 15) {
            return $phone;
        }
        if(!PhoneNumberUtil::isViablePhoneNumber($phone)){
            return $phone;
        }
        $phone = $phoneUtil->parse($phone, DEFINED_COUNTRY_CODE);
        $phone = $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::E164);
        return $phone;
    }



}
