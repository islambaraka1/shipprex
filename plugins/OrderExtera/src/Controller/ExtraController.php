<?php
declare(strict_types=1);

namespace OrderExtera\Controller;

use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\ORM\TableRegistry;

/**
 * Partial Controller
 *
 *
 * @method \OrderExtera\Model\Entity\Partial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExtraController extends AppController
{

    public function add($order_id = null)
    {
        $this->loadModel('Orders');
        $order = $this->Orders->get($order_id);
        $this->set('partial',$order);
        if (isset($_POST['extra_weight'])) {
            $event = new Event('Orders.extra_weighted', $this, ['order' => $order, 'extra_weight' => $_POST['extra_weight']]);
            $this->getEventManager()->dispatch($event);
            return $this->redirect(['plugin' => 'Delegations' ,'controller' => 'Delegations', 'action' => 'ajaxsuccess']);
        }
    }


    private function partial_the_order($id){
        $this->loadModel('Orders');
        $order = $this->Orders->get($id);
        $order->statues = get_option_value('partial_status');
        $order->cod = $this->request->getData('cod');
        $order->notes = 'Partial delivered' . $this->request->getData('notes') . ' - ' . $this->request->getData('cod') . ' old cod' . $order->cod;
        $this->Orders->save($order);
        $event = new Event('Orders.partially_delivered', $this, ['order' => $order]);
        $this->getEventManager()->dispatch($event);
    }
    private function save_order_action_to_partialy_delivered($order_id,$new_cod,$description,$old_cod = 0 ){
        $this->loadModel('Actions');
        $orderTable = TableRegistry::getTableLocator()->get('Orders');
        $driverTable = TableRegistry::getTableLocator()->get('Drivers');
        $action = $this->Actions->newEmptyEntity();
        $action->order_id = $order_id;
        $action->name = 'Partial Delivery';
        $driver = $orderTable->getDriver($order_id);
        $driver = $driverTable->get($driver->driver_id)->name;
        $action->description = $description . ' New COD: ' . $new_cod . ' Old COD: ' . $old_cod . ' Driver : ' .$driver;
        $this->Actions->save($action);
    }

}
