<?php
declare(strict_types=1);

namespace OrderExtera\Controller;

use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\ORM\TableRegistry;

/**
 * Partial Controller
 *
 *
 * @method \OrderExtera\Model\Entity\Partial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\FrontComponent|\Cake\Controller\Component\FrontComponent|\Cake\Datasource\RepositoryInterface|\DebugKit\Controller\Component\FrontComponent|\Front|\FrontComponent|null $Front
 */
class PartialController extends AppController
{

    public function add($order_id = null)
    {
        $this->loadModel('Orders');
        $order = $this->Orders->get($order_id);
        $this->set('partial',$order);
        if (isset($_POST['cod'])) {
              $new_cod = $this->request->getData('cod');
              $description = $this->request->getData('notes');
              $old_cod = $order->cod;
              if($this->save_order_action_to_partialy_delivered($order_id,$new_cod,$description,$old_cod)){
                  $this->Flash->success(__('The order has been saved.'));
                  // test the server events here
                  $order->cod = $new_cod;
                  $this->Front->sendEvent('update_record', ['id' => $order_id, 'message' => __('The order has been saved.'),
                      'data' => $order]);
              }
            $this->partial_the_order($order_id);
            return $this->redirect(['plugin' => 'Delegations' ,'controller' => 'Delegations', 'action' => 'ajaxsuccess']);
        }
    }


    private function partial_the_order($id){
        $this->loadModel('Orders');
        $order = $this->Orders->get($id);
        $order->statues = get_option_value('partial_status');
        $order->notes = 'Partial delivered ' . $this->request->getData('notes') . ' - ' . $this->request->getData('cod') . ' old cod ' . $order->cod;
        $order->cod = $this->request->getData('cod');
        $this->Orders->save($order);
        $event = new Event('Orders.partially_delivered', $this, ['order' => $order]);
        $this->getEventManager()->dispatch($event);
    }
    private function save_order_action_to_partialy_delivered($order_id,$new_cod,$description,$old_cod = 0 ){
        $this->loadModel('Actions');
        $orderTable = TableRegistry::getTableLocator()->get('Orders');
        $driverTable = TableRegistry::getTableLocator()->get('Drivers');
        $action = $this->Actions->newEmptyEntity();
        $action->order_id = $order_id;
        $action->name = 'Partial Delivery';
        $driver = $orderTable->getDriver($order_id);
        if(!isset($driver->driver_id)){
            $this->Flash->error(__('cant have partial orders without driver assigned to it'));
            return false;
        }
        $driver = $driverTable->get($driver->driver_id)->name;
        $action->description = $description . ' New COD: ' . $new_cod . ' Old COD: ' . $old_cod . ' Driver : ' .$driver;
        $this->Actions->save($action);
        return true;
    }

}
