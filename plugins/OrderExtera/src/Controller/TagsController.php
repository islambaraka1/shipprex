<?php
declare(strict_types=1);

namespace OrderExtera\Controller;

use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use OrderExtera\Controller\Component\TagsServicesComponent;

/**
 * Tags Controller
 *
 * @property \OrderExtera\Model\Table\TagsTable $Tags
 * @property TagsServicesComponent $TagsServices
 *
 * @method \OrderExtera\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TagsController extends AppController
{
    function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
        $this->loadComponent('OrderExtera.TagsServices');
        $this->set('available_handlers', $this->TagsServices->getAvalibleHandlers());

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $tags = $this->paginate($this->Tags);

        $this->set(compact('tags'));
    }

    public function updatenew($id,$stat){
        $OrdersTable = TableRegistry::getTableLocator()->get('Orders');
        $orderData = $OrdersTable->get($id);
        $this->set('order', $orderData);
        $tagsTable = TableRegistry::getTableLocator()->get('OrderExtera.Tags');
        $tags = $tagsTable->find('list')->where(['Tags.parent_status'=>$stat])->toArray();
        $this->set('tags',$tags);
        if($this->request->is(['post','put']) ){
            //load the TagService
            $this->loadComponent('OrderExtera.TagsServices');
            if($this->request->getData('tags') > 0 ){
                if($this->TagsServices->linkOrdersToTag($this->request->getData('tags'),$id)){
                    $OrdersTable->actionUpdates($id,$stat);
                    $orderDataAfterUpdate = $OrdersTable->get($id);
                    $orderDataAfterUpdate->statues = HSTT($orderDataAfterUpdate->statues);
                    $this->Front->sendEvent('handel_update_record_status', ['id' => $id, 'message' => __('The order has been saved.'),
                        'data' => $orderDataAfterUpdate]);

                    return $this->redirect('/delegations/delegations/ajaxsuccess');
                }
            }
            else{
                $OrdersTable->actionUpdates($id,$stat);
                $orderDataAfterUpdate = $OrdersTable->get($id);
                $orderDataAfterUpdate->statues = HSTT($orderDataAfterUpdate->statues);
                $this->Front->sendEvent('handel_update_record_status', ['id' => $id, 'message' => __('The order has been saved.'),
                    'data' => $orderDataAfterUpdate]);

                return $this->redirect('/delegations/delegations/ajaxsuccess');
            }
        }
    }


    /*
     * get tag list which is under the order status
     */

    public function getTagList($stat)
    {
        $tagsTable = TableRegistry::getTableLocator()->get('OrderExtera.Tags');
        $tags = $tagsTable->find('list')->where(['Tags.parent_status' => $stat])->toArray();
        //return as json response
        return $this->response->withType('application/json')->withStringBody(json_encode($tags));

    }

    function addTag($orderID,$tagId){
        $this->loadComponent('OrderExtera.TagsServices');
        if($this->TagsServices->linkOrdersToTag($tagId,$orderID)){
            if( $this->request->is(['ajax']) ){
                $OrdersTable = TableRegistry::getTableLocator()->get('Orders');
                $tag = $this->Tags->get($tagId);
                return $this->response->withType('text/html')->withStringBody(HSTT($OrdersTable->get($orderID)->statues).'<br>'.$tag->name);
            }else{
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * View method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Orders'],
        ]);

        $this->set('tag', $tag);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tag = $this->Tags->newEmptyEntity();
        if ($this->request->is('post')) {
            $tag = $this->Tags->patchEntity($tag, $this->request->getData());
            $tag->slug = Text::slug($tag->name);
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__('The tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tag could not be saved. Please, try again.'));
        }
        $orders = $this->Tags->Orders->find('list', ['limit' => 200]);
        $this->set(compact('tag', 'orders'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Orders'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->Tags->patchEntity($tag, $this->request->getData());
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__('The tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tag could not be saved. Please, try again.'));
        }
        $orders = $this->Tags->Orders->find('list', ['limit' => 200]);
        $this->set(compact('tag', 'orders'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tag = $this->Tags->get($id);
        if ($this->Tags->delete($tag)) {
            $this->Flash->success(__('The tag has been deleted.'));
        } else {
            $this->Flash->error(__('The tag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
