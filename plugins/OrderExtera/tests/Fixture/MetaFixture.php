<?php
declare(strict_types=1);

namespace OrderExtera\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MetaFixture
 */
class MetaFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'meta';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'model' => 'Lorem ipsum dolor sit amet',
                'name' => 'Lorem ipsum dolor sit amet',
                'value' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'user_id' => 1,
                'model_key' => 1,
                'created_at' => '2023-05-13 11:01:21',
                'modified_at' => '2023-05-13 11:01:21',
            ],
        ];
        parent::init();
    }
}
