<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use OrderExtera\Model\Table\MetaTable;

/**
 * OrderExtera\Model\Table\MetaTable Test Case
 */
class MetaTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\Model\Table\MetaTable
     */
    protected $Meta;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'plugin.OrderExtera.Meta',
        'plugin.OrderExtera.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Meta') ? [] : ['className' => MetaTable::class];
        $this->Meta = $this->getTableLocator()->get('Meta', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Meta);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \OrderExtera\Model\Table\MetaTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \OrderExtera\Model\Table\MetaTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
