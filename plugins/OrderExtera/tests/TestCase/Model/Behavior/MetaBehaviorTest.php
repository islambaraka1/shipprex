<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use OrderExtera\Model\Behavior\MetaBehavior;

/**
 * OrderExtera\Model\Behavior\MetaBehavior Test Case
 */
class MetaBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\Model\Behavior\MetaBehavior
     */
    protected $Meta;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->Meta = new MetaBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Meta);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
