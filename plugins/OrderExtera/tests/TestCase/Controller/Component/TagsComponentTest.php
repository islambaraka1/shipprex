<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use OrderExtera\Controller\Component\TagsComponent;

/**
 * OrderExtera\Controller\Component\TagsComponent Test Case
 */
class TagsComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\Controller\Component\TagsComponent
     */
    protected $Tags;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Tags = new TagsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Tags);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
