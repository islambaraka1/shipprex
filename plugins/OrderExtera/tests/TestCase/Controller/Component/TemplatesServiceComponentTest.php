<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use OrderExtera\Controller\Component\TemplatesServiceComponent;

/**
 * OrderExtera\Controller\Component\TemplatesServiceComponent Test Case
 */
class TemplatesServiceComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\Controller\Component\TemplatesServiceComponent
     */
    protected $TemplatesService;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->TemplatesService = new TemplatesServiceComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TemplatesService);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
