<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use OrderExtera\View\Helper\PhoneNumbersHelper;

/**
 * OrderExtera\View\Helper\PhoneNumbersHelper Test Case
 */
class PhoneNumbersHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\View\Helper\PhoneNumbersHelper
     */
    protected $PhoneNumbers;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->PhoneNumbers = new PhoneNumbersHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PhoneNumbers);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
