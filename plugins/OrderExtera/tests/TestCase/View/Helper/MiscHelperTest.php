<?php
declare(strict_types=1);

namespace OrderExtera\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use OrderExtera\View\Helper\MiscHelper;

/**
 * OrderExtera\View\Helper\MiscHelper Test Case
 */
class MiscHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OrderExtera\View\Helper\MiscHelper
     */
    protected $Misc;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Misc = new MiscHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Misc);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
