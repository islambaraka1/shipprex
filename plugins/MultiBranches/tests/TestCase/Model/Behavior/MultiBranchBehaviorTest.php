<?php
declare(strict_types=1);

namespace MultiBranches\Test\TestCase\Model\Behavior;

use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use MultiBranches\Model\Behavior\MultiBranchBehavior;

/**
 * MultiBranches\Model\Behavior\MultiBranchBehavior Test Case
 */
class MultiBranchBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \MultiBranches\Model\Behavior\MultiBranchBehavior
     */
    protected $MultiBranch;

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $table = new Table();
        $this->MultiBranch = new MultiBranchBehavior($table);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->MultiBranch);

        parent::tearDown();
    }
}
