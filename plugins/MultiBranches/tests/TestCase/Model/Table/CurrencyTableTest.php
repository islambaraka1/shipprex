<?php
declare(strict_types=1);

namespace MultiBranches\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use MultiBranches\Model\Table\CurrencyTable;

/**
 * MultiBranches\Model\Table\CurrencyTable Test Case
 */
class CurrencyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \MultiBranches\Model\Table\CurrencyTable
     */
    protected $Currency;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'plugin.MultiBranches.Currency',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Currency') ? [] : ['className' => CurrencyTable::class];
        $this->Currency = $this->getTableLocator()->get('Currency', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Currency);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \MultiBranches\Model\Table\CurrencyTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
