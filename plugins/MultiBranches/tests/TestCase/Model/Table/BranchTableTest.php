<?php
declare(strict_types=1);

namespace MultiBranches\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use MultiBranches\Model\Table\BranchTable;

/**
 * MultiBranches\Model\Table\BranchTable Test Case
 */
class BranchTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \MultiBranches\Model\Table\BranchTable
     */
    protected $Branch;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'plugin.MultiBranches.Branch',
        'plugin.MultiBranches.GlBanks',
        'plugin.MultiBranches.MlbCurrencies',
        'plugin.MultiBranches.Users',
        'plugin.MultiBranches.Zones',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Branch') ? [] : ['className' => BranchTable::class];
        $this->Branch = $this->getTableLocator()->get('Branch', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Branch);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \MultiBranches\Model\Table\BranchTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \MultiBranches\Model\Table\BranchTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
