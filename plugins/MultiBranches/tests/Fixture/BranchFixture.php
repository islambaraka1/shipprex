<?php
declare(strict_types=1);

namespace MultiBranches\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BranchFixture
 */
class BranchFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'mlb_branches';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'location' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'bank_id' => 1,
                'currency_id' => 1,
                'user_id' => 1,
                'zone_id' => 1,
                'warehouse_id' => 1,
                'contact_info' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'open_at' => '16:59:45',
                'closed_at' => '16:59:45',
                'delivery_fees' => 1.5,
                'payment_method' => 'Lorem ipsum dolor sit amet',
                'inventory' => 1,
                'created' => '2023-02-18 16:59:45',
                'modified' => '2023-02-18 16:59:45',
            ],
        ];
        parent::init();
    }
}
