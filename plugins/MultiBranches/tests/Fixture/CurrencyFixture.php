<?php
declare(strict_types=1);

namespace MultiBranches\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CurrencyFixture
 */
class CurrencyFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'mlb_currencies';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'code' => 'Lo',
                'created' => '2023-02-18 15:03:52',
                'modified' => '2023-02-18 15:03:52',
            ],
        ];
        parent::init();
    }
}
