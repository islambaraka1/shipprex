<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Features seed.
 */
class FeaturesSeed extends AbstractSeed
{
    use Cake\ORM\Locator\LocatorAwareTrait;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            "name" => "MultiBranches",
            "plugin" => "MultiBranches",
            "active" => 1,
            "version" => "1.6.0",
        ];
//        $article = $this->fetchTable('Articles')->newEmptyEntity();
//
//        $article = $this->fetchTable('Articles')->where('plugin','!=',$data['plugin']);

//
//        $table = $this->table('features');
//        $table->where('plugin','!=',$data['plugin'])->insert($data)->save();
    }
}
