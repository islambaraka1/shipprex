<?php

use MultiBranches\Enums\MultiBranchesSettingEnum;
use MultiBranches\Services\BranchService;

if (get_option_value(MultiBranchesSettingEnum::ENABLE_PLUGIN)) {
    $hooks->create_new_root_menu('branches', 'Branches', '#', 'store', [1]);
    $hooks->add_sub_menu_item_to_root('branches', 'List Branches', '/multi-branches/branch', [1]);
    $hooks->add_sub_menu_item_to_root('branches', 'Add Branch', '/multi-branches/branch/add', [1]);
    $hooks->add_sub_menu_item_to_root('branches', 'List Currency', '/multi-branches/currency', [1]);
    $hooks->add_sub_menu_item_to_root('branches', 'Add Currency', '/multi-branches/currency/add', [1]);
//$hooks->add_sub_menu_item_to_root('branches', 'Choose Branch', '/multi-branches/branch/choose', [1]);

    if (!function_exists('add_branches_dropdown')) {
        $hooks->add_action('before_main_menu', 'add_branches_dropdown');
        function add_branches_dropdown()
        {

            if (MultiBranchesSettingEnum::ENABLE_PLUGIN && $_SESSION['Auth']['User']['group_id'] == 1) {
                $branchService = new BranchService();
                $select = '<select class="form form-control" id="branches_select" onchange="getSelectedBranch(this);" >
                    <a>  <option value="0"  selected>' . __("All Branches") . '</option></a>';
                foreach ($branchService->buildBranchesHtmlOptions() as $option) {
                    $select .= $option;

                }

                $select .= '</select>';
                echo $select;
            }
        }

    }
}



