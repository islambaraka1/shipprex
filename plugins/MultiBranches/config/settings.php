<?php
use Cake\Core\Configure;
use Settings\Enums\SettingsEnum;
use MultiBranches\Enums\MultiBranchesSettingEnum;


Configure::write(MultiBranchesSettingEnum::PATH, []);


easy_new_setting(MultiBranchesSettingEnum::PATH,MultiBranchesSettingEnum::ENABLE_PLUGIN,'Enable Multi-Branch',SettingsEnum::ON_OFF_FILED);
easy_new_setting(MultiBranchesSettingEnum::PATH,MultiBranchesSettingEnum::CURRENCIES,'Enable Different Currencies For each branch',SettingsEnum::ON_OFF_FILED);
easy_new_setting(MultiBranchesSettingEnum::PATH,MultiBranchesSettingEnum::INVENTORIES,'Enable Different Inventory For each branch',SettingsEnum::ON_OFF_FILED);
easy_new_setting(MultiBranchesSettingEnum::PATH,MultiBranchesSettingEnum::BANKS,'Enable Different Financial Banks For each branch',SettingsEnum::ON_OFF_FILED);
easy_new_setting(MultiBranchesSettingEnum::PATH,MultiBranchesSettingEnum::USER_GROUP,'Branch Management User Group',SettingsEnum::SELECT,'khaleds',MultiBranchesSettingEnum::getGroups());
