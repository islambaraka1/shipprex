<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use Cake\Controller\Controller;
use MultiBranches\Enums\MultiBranchesSettingEnum;

$hooks = \App\Hooks\Hooks::getInstance();
if (get_option_value(MultiBranchesSettingEnum::ENABLE_PLUGIN)) {

    $zonesTable = TableRegistry::getTableLocator()->get("Zones");
    $zonesTable->addBehavior("MultiBranches.MultiBranch");


    $ordersTable = TableRegistry::getTableLocator()->get("Orders");
    $ordersTable->addBehavior("MultiBranches.MultiBranch");

    $driversTable = TableRegistry::getTableLocator()->get("Drivers");
    $driversTable->addBehavior("MultiBranches.MultiBranch");

    $transactionTable = TableRegistry::getTableLocator()->get("Transactions");
    $transactionTable->addBehavior("MultiBranches.MultiBranch");

    $stockTransactionTable = TableRegistry::getTableLocator()->get("Stocks.Transactions");
    $stockTransactionTable->addBehavior("MultiBranches.MultiBranch");

    $stockProductsTable = TableRegistry::getTableLocator()->get("Stocks.Products");
    $stockProductsTable->addBehavior("MultiBranches.MultiBranch");

// error on reports
    $glTransactionTable = TableRegistry::getTableLocator()->get("GeneralLedger.Transes");
    $glTransactionTable->addBehavior("MultiBranches.MultiBranch");

    $warehouseTable = TableRegistry::getTableLocator()->get("Stocks.Warehouses");
    $warehouseTable->addBehavior("MultiBranches.MultiBranch");
}
//TODO: orders
