<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateMlbBranches extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('mlb_branches');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('location', 'text', [
            'default' => null,
            'null' => false,
        ]);

        //Bank relation
        $table->addColumn('bank_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        // Currency Relation
        $table->addColumn('currency_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);

        // Manager Relation
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);

        //Zone Relation
        $table->addColumn('zone_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);

        //Warehouse relation
        $table->addColumn('warehouse_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);


        $table->addColumn('contact_info', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('open_at', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('closed_at', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('delivery_fees', 'decimal', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('payment_method', 'enum', [
            'default' => 'COD',
            'null' => false,
            'values' => ['COD', 'Online']

        ]);
        $table->addColumn('inventory', 'boolean', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table->create();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('mlb_branches')->drop()->save();

    }
}
