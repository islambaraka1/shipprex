<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddBranchAndCurruncyToGlTranses extends AbstractMigration
{
    use MultiBranches\Helper\Schema;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('gl_transes');
        $this->append($table);

    }


    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $table = $this->table('orders');
        $this->remove($table);

    }
}
