<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddBranchAndCurrencyToProducts extends AbstractMigration
{
    use MultiBranches\Helper\Schema;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('stok_products');

        $this->append($table);

    }
    public function down()
    {
        $table = $this->table('stok_products');
        $this->remove($table);
    }
}
