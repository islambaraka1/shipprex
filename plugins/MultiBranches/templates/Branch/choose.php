<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $branch
 * @var \App\Model\Entity\GlBank[]|\Cake\Collection\CollectionInterface $glBanks
 * @var \App\Model\Entity\MlbCurrency[]|\Cake\Collection\CollectionInterface $mlbCurrencies
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Zone[]|\Cake\Collection\CollectionInterface $zones
 * @var \App\Model\Entity\StokWarehouse[]|\Cake\Collection\CollectionInterface $stokWarehouse
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<div class="branch form content">
    <?= $this->Form->create($branch) ?>
    <fieldset>
        <legend><?= __('Choose Branch') ?></legend>
        <?php

        echo $this->Form->control('branch_id', ['options' => $branches]);

        ?>
        <div class="mb-3 form-group select">
            <label class="form-label" for="branch-id"><?php echo __("Show All Branches")?></label>
            <input class="" value="0" type="checkbox" name="is_all">

        </div>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
