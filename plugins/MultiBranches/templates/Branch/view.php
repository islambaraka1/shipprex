<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $branch
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Branch'), ['action' => 'edit', $branch->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Branch'), ['action' => 'delete', $branch->id], ['confirm' => __('Are you sure you want to delete # {0}?', $branch->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Branch'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Branch'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Gl Banks'), ['controller' => 'GlBanks', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Gl Bank'), ['controller' => 'GlBanks', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Mlb Currencies'), ['controller' => 'MlbCurrencies', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Mlb Currency'), ['controller' => 'MlbCurrencies', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Zones'), ['controller' => 'Zones', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Zone'), ['controller' => 'Zones', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="branch view large-9 medium-8 columns content">
    <h3><?= h($branch->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($branch->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Gl Bank') ?></th>
                <td><?= $branch->has('gl_bank') ? $this->Html->link($branch->gl_bank->name, ['controller' => 'GlBanks', 'action' => 'view', $branch->gl_bank->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Mlb Currency') ?></th>
                <td><?= $branch->has('mlb_currency') ? $this->Html->link($branch->mlb_currency->name, ['controller' => 'MlbCurrencies', 'action' => 'view', $branch->mlb_currency->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $branch->has('user') ? $this->Html->link($branch->user->username, ['controller' => 'Users', 'action' => 'view', $branch->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Zone') ?></th>
                <td><?= $branch->has('zone') ? $this->Html->link($branch->zone->name, ['controller' => 'Zones', 'action' => 'view', $branch->zone->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Stok Warehouse') ?></th>
                <td><?= $branch->has('stok_warehouse') ? $this->Html->link($branch->stok_warehouse->name, ['controller' => 'StokWarehouse', 'action' => 'view', $branch->stok_warehouse->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Payment Method') ?></th>
                <td><?= h($branch->payment_method) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($branch->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Delivery Fees') ?></th>
                <td><?= $this->Number->format($branch->delivery_fees) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Open At') ?></th>
                <td><?= h($branch->open_at) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Closed At') ?></th>
                <td><?= h($branch->closed_at) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($branch->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($branch->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Inventory') ?></th>
                <td><?= $branch->inventory ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
    </div>
    <div class="text">
        <h4><?= __('Location') ?></h4>
        <?= $this->Text->autoParagraph(h($branch->location)); ?>
    </div>
    <div class="text">
        <h4><?= __('Contact Info') ?></h4>
        <?= $this->Text->autoParagraph(h($branch->contact_info)); ?>
    </div>
</div>
