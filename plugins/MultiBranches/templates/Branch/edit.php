<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $branch
 * @var \App\Model\Entity\GlBank[]|\Cake\Collection\CollectionInterface $glBanks
 * @var \App\Model\Entity\MlbCurrency[]|\Cake\Collection\CollectionInterface $mlbCurrencies
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Zone[]|\Cake\Collection\CollectionInterface $zones
 * @var \App\Model\Entity\StokWarehouse[]|\Cake\Collection\CollectionInterface $stokWarehouse
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $branch->id], ['confirm' => __('Are you sure you want to delete # {0}?', $branch->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Branch'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Gl Banks'), ['controller' => 'GlBanks', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Gl Bank'), ['controller' => 'GlBanks', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Mlb Currencies'), ['controller' => 'MlbCurrencies', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Mlb Currency'), ['controller' => 'MlbCurrencies', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Zones'), ['controller' => 'Zones', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Zone'), ['controller' => 'Zones', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="branch form content">
    <?= $this->Form->create($branch) ?>
    <fieldset>
        <legend><?= __('Edit Branch') ?></legend>
        <?php
        echo $this->Form->control('name');
        echo $this->Form->control('location');
        echo $this->Form->control('bank_id', ['options' => $Banks, 'empty' => true]);
        echo $this->Form->control('currency_id', ['options' => $Currencies, 'empty' => true]);
        echo $this->Form->control('user_id', ['options' => $Users, 'empty' => true]);
        echo $this->Form->control('zone_id', ['options' => $Zones, 'empty' => true]);
        echo $this->Form->control('warehouse_id', ['options' => $kWarehouses, 'empty' => true]);
        echo $this->Form->control('contact_info');
        echo $this->Form->control('open_at');
        echo $this->Form->control('closed_at');
        echo $this->Form->control('delivery_fees');
        echo $this->Form->control('payment_method', ['options' => $paymentMethods, 'empty' => true]);

        echo $this->Form->control('inventory');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
