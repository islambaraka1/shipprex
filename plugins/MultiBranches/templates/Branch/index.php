<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $branch
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Branch'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Gl Banks'), ['controller' => 'GlBanks', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Gl Bank'), ['controller' => 'GlBanks', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Mlb Currencies'), ['controller' => 'MlbCurrencies', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Mlb Currency'), ['controller' => 'MlbCurrencies', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Zones'), ['controller' => 'Zones', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Zone'), ['controller' => 'Zones', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Stok Warehouse'), ['controller' => 'StokWarehouse', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('name')?></th>
        <?php if (get_option_value(\MultiBranches\Enums\MultiBranchesSettingEnum::BANKS)){?>
        <th scope="col"><?= $this->Paginator->sort('bank_id') ?></th>
        <?php };?>
        <?php if (get_option_value(\MultiBranches\Enums\MultiBranchesSettingEnum::CURRENCIES)){?>
        <th scope="col"><?= $this->Paginator->sort('currency_id') ?></th>
        <?php };?>

        <?php if (get_option_value(\MultiBranches\Enums\MultiBranchesSettingEnum::USER_GROUP)){?>
        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
        <?php };?>

        <th scope="col"><?= $this->Paginator->sort('zone_id') ?></th>
        <?php if (get_option_value(\MultiBranches\Enums\MultiBranchesSettingEnum::INVENTORIES)){?>
        <th scope="col"><?= $this->Paginator->sort('warehouse_id') ?></th>
        <?php };?>

        <th scope="col"><?= $this->Paginator->sort('open_at') ?></th>
        <th scope="col"><?= $this->Paginator->sort('closed_at') ?></th>
        <th scope="col"><?= $this->Paginator->sort('delivery_fees') ?></th>
        <th scope="col"><?= $this->Paginator->sort('payment_method') ?></th>
        <th scope="col"><?= $this->Paginator->sort('inventory') ?></th>

        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($branch as $branch) : ?>
        <tr>
            <td><?= $this->Number->format($branch->id) ?></td>
            <td><?= h($branch->name) ?></td>
            <td><?= $branch->has('gl_bank') ? $this->Html->link($branch->gl_bank->name, ['controller' => 'GlBanks', 'action' => 'view', $branch->gl_bank->id]) : '' ?></td>
            <td><?= $branch->has('mlb_currency') ? $this->Html->link($branch->mlb_currency->name, ['controller' => 'MlbCurrencies', 'action' => 'view', $branch->mlb_currency->id]) : '' ?></td>
            <td><?= $branch->has('user') ? $this->Html->link($branch->user->username, ['controller' => 'Users', 'action' => 'view', $branch->user->id]) : '' ?></td>
            <td><?= $branch->has('zone') ? $this->Html->link($branch->zone->name, ['controller' => 'Zones', 'action' => 'view', $branch->zone->id]) : '' ?></td>
            <td><?= $branch->has('stok_warehouse') ? $this->Html->link($branch->stok_warehouse->name, ['controller' => 'StokWarehouse', 'action' => 'view', $branch->stok_warehouse->id]) : '' ?></td>
            <td><?= h($branch->open_at) ?></td>
            <td><?= h($branch->closed_at) ?></td>
            <td><?= $this->Number->format($branch->delivery_fees) ?></td>
            <td><?= h($branch->payment_method) ?></td>
            <td><?= h($branch->inventory) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $branch->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('«', ['label' => __('First')]) ?>
        <?= $this->Paginator->prev('‹', ['label' => __('Previous')]) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('›', ['label' => __('Next')]) ?>
        <?= $this->Paginator->last('»', ['label' => __('Last')]) ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
