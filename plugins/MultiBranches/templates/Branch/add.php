<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $branch
 * @var \App\Model\Entity\GlBank[]|\Cake\Collection\CollectionInterface $glBanks
 * @var \App\Model\Entity\MlbCurrency[]|\Cake\Collection\CollectionInterface $mlbCurrencies
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Zone[]|\Cake\Collection\CollectionInterface $zones
 * @var \App\Model\Entity\StokWarehouse[]|\Cake\Collection\CollectionInterface $stokWarehouse
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<div class="branch form content">
    <?= $this->Form->create($branch) ?>
    <fieldset>
        <legend><?= __('Add Branch') ?></legend>
        <?php
        echo $this->Form->control('name');
        echo $this->Form->control('location');
        echo $this->Form->control('bank_id', ['options' => $Banks, 'empty' => true]);
        echo $this->Form->control('currency_id', ['options' => $Currencies, 'empty' => true]);
        echo $this->Form->control('user_id', ['options' => $Users, 'empty' => true]);
        echo $this->Form->control('zone_id', ['options' => $Zones, 'empty' => true]);
        echo $this->Form->control('warehouse_id', ['options' => $Warehouses, 'empty' => true]);
        echo $this->Form->control('contact_info');
        echo $this->Form->control('open_at');
        echo $this->Form->control('closed_at');
        echo $this->Form->control('delivery_fees');
        echo $this->Form->control('payment_method', ['options' => $paymentMethods, 'empty' => true]);
        echo $this->Form->control('inventory');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
