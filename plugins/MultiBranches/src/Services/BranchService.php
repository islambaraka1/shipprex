<?php

namespace MultiBranches\Services;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use MultiBranches\Controller\AppController;

class BranchService
{

    public object $data;
    public function getBranchs():void
    {
        $branches=TableRegistry::getTableLocator()->get('MultiBranches.Branch');
        $this->data = $branches->find()->all();
    }

    public function buildBranchesHtmlOptions(){
        $options=[];
        $this->getBranchs();
        foreach ($this->data as $item){
            if (isset($_COOKIE['branch']) && $item->id == $_COOKIE['branch'])
                $options[]='<option value="'.$item->id.'" selected >' . $item->name . '</option>';
            else
                $options[]='<option value="'.$item->id.'"  >' . $item->name . '</option>';

        }
        return $options;
    }
}
