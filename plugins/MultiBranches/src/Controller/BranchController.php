<?php
declare(strict_types=1);

namespace MultiBranches\Controller;

use MultiBranches\Controller\AppController;
use MultiBranches\Enums\MultiBranchesSettingEnum;
/**
 * Branch Controller
 *
 * @property \MultiBranches\Model\Table\BranchTable $Branch
 * @method \MultiBranches\Model\Entity\Branch[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BranchController extends AppController
{
    private array $paymentMethods=['COD'=>'COD','Online'=>'Online'];
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Banks', 'Users', 'Zones', 'Warehouses'],
        ];
        $branch = $this->paginate($this->Branch);

        $this->set(compact('branch'));
    }

    /**
     * View method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $branch = $this->Branch->get($id, [
            'contain' => ['Banks', 'Currencies', 'Users', 'Zones', 'Warehouses'],
        ]);

        $this->set(compact('branch'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $branch = $this->Branch->newEmptyEntity();
        if ($this->request->is('post')) {
            $branch = $this->Branch->patchEntity($branch, $this->request->getData());
            if ($this->Branch->save($branch)) {
                $this->Flash->success(__('The branch has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The branch could not be saved. Please, try again.'));
        }

        $this->set(array_merge($this->checkSettings(),['branch' =>$branch]));
    }

    /**
     * Edit method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $branch = $this->Branch->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $branch = $this->Branch->patchEntity($branch, $this->request->getData());
            if ($this->Branch->save($branch)) {
                $this->Flash->success(__('The branch has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The branch could not be saved. Please, try again.'));
        }


        $this->set(array_merge($this->checkSettings(),['branch' =>$branch]));
    }

    /**
     * Delete method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $branch = $this->Branch->get($id);
        if ($this->Branch->delete($branch)) {
            $this->Flash->success(__('The branch has been deleted.'));
        } else {
            $this->Flash->error(__('The branch could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function checkSettings() {



        $compactArray=[];

        if (get_option_value(MultiBranchesSettingEnum::BANKS)){
            $Banks = $this->Branch->Banks->find('list', ['limit' => 200])->all();
            $compactArray["Banks"]=$Banks;
        }
        if (get_option_value(MultiBranchesSettingEnum::CURRENCIES)){
            $Currencies = $this->Branch->Currencies->find('list', ['limit' => 200])->all();
            $compactArray["Currencies"]=$Currencies;
        }

        if (get_option_value(MultiBranchesSettingEnum::USER_GROUP)){
            $users = $this->Branch->Users->find('list', ['limit' => 200,
                'condition'=>['group_id =',get_option_value(MultiBranchesSettingEnum::USER_GROUP)]])->all();
            $compactArray["users"]=$users;
        }

        if (get_option_value(MultiBranchesSettingEnum::INVENTORIES)){
            $Warehouses = $this->Branch->Warehouses->find('list', ['limit' => 200])->all();
            $compactArray["Warehouses"]=$Warehouses;
        }

        $Zones = $this->Branch->Zones->find('list', ['limit' => 200])->all();
        $paymentMethods=$this->paymentMethods;
        $compactArray["Zones"]=$Zones;
        $compactArray["paymentMethods"]=$paymentMethods;

        return $compactArray;
    }
    public function choose()
    {
        if ($this->request->is(['post','ajax'])) {

            if ($this->Branch->find('list',[
                "conditions" =>['id'=>$this->request->getData('branch_id')]
            ])->all()->count() || $this->request->getData('branch_id') == 0) {

                $branch_id = $this->request->getData('branch_id');
                setcookie("branch", $branch_id, time() + (86400 * 30), "/");

                return $this->redirect(['action' => 'index']);
            }
            return $this->redirect(['action' => 'index']);

        }

    }

}
