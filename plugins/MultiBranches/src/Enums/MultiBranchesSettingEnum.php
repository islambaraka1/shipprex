<?php


namespace MultiBranches\Enums;


use Cake\ORM\TableRegistry;
use UsersManager\Model\Entity\Group;

class MultiBranchesSettingEnum
{

    const PATH = 'Settings.MultiBranches';
    const ENABLE_PLUGIN = 'toggle_multi_branches';
    const USER_GROUP = 'user_group_multi_branches';
    const CURRENCIES = 'currencies_multi_branches';
    const INVENTORIES = 'inventories_multi_branches';
    const BANKS = 'banks_multi_branches';

    public static function getGroups(){
        $groups = TableRegistry::getTableLocator()->get('Groups');
        $groups = $groups->find()->all();

        $mapping=[];
        foreach ($groups as $group)
            $mapping[$group->id] = $group->name;

        return $mapping;
    }
}
