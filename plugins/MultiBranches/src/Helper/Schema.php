<?php

namespace MultiBranches\Helper;

trait Schema
{

    public function append($table){
        // Check if branch_id column already exists
        if (!$table->hasColumn('branch_id')) {
            $table->addColumn('branch_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);
        }

        // Check if currency_id column already exists
        if (!$table->hasColumn('currency_id')) {
            $table->addColumn('currency_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);
        }
    }

    public function remove($table){
        $table->removeColumn('branch_id');
        $table->removeColumn('currency_id');
    }
}
