<?php
declare(strict_types=1);

namespace MultiBranches\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use MultiBranches\Enums\MultiBranchesSettingEnum;

/**
 * Branch Model
 *
 * @property \MultiBranches\Model\Table\GlBanksTable&\Cake\ORM\Association\BelongsTo $GlBanks
 * @property \MultiBranches\Model\Table\MlbCurrenciesTable&\Cake\ORM\Association\BelongsTo $MlbCurrencies
 * @property \MultiBranches\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \MultiBranches\Model\Table\ZonesTable&\Cake\ORM\Association\BelongsTo $Zones
 *
 * @method \MultiBranches\Model\Entity\Branch newEmptyEntity()
 * @method \MultiBranches\Model\Entity\Branch newEntity(array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Branch[] newEntities(array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Branch get($primaryKey, $options = [])
 * @method \MultiBranches\Model\Entity\Branch findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \MultiBranches\Model\Entity\Branch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Branch[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Branch|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \MultiBranches\Model\Entity\Branch saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \MultiBranches\Model\Entity\Branch[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Branch[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Branch[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Branch[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BranchTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('mlb_branches');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id',
            'className' => 'GeneralLedger.Banks',
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'className' => 'MultiBranches.Currency',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'UsersManager.Users',
        ]);
        $this->belongsTo('Zones', [
            'foreignKey' => 'zone_id',
            'className' => 'Zones',
        ]);
        $this->belongsTo('Warehouses', [
            'foreignKey' => 'warehouse_id',
            'className' => 'Stocks.Warehouse',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('location')
            ->requirePresence('location', 'create')
            ->notEmptyString('location');

        $validator
            ->integer('bank_id')
            ->allowEmptyString('bank_id');

        $validator
            ->integer('currency_id')
            ->allowEmptyString('currency_id');

        $validator
            ->integer('user_id')
            ->allowEmptyString('user_id');

        $validator
            ->integer('zone_id')
            ->allowEmptyString('zone_id');

        $validator
            ->integer('warehouse_id')
            ->allowEmptyString('warehouse_id');

        $validator
            ->scalar('contact_info')
            ->requirePresence('contact_info', 'create')
            ->notEmptyString('contact_info');

        $validator
            ->time('open_at')
            ->requirePresence('open_at', 'create')
            ->notEmptyTime('open_at');

        $validator
            ->time('closed_at')
            ->requirePresence('closed_at', 'create')
            ->notEmptyTime('closed_at');

        $validator
            ->decimal('delivery_fees')
            ->notEmptyString('delivery_fees');

        $validator
            ->scalar('payment_method')
            ->notEmptyString('payment_method');

        $validator
            ->boolean('inventory')
            ->notEmptyString('inventory');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        if (get_option_value(MultiBranchesSettingEnum::BANKS))
            $rules->add($rules->existsIn('bank_id', 'Banks'), ['errorField' => 'bank_id']);

        if (get_option_value(MultiBranchesSettingEnum::CURRENCIES))
            $rules->add($rules->existsIn('currency_id', 'Currencies'), ['errorField' => 'currency_id']);

        if (get_option_value(MultiBranchesSettingEnum::USER_GROUP))
            $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);

        if (get_option_value(MultiBranchesSettingEnum::INVENTORIES))
            $rules->add($rules->existsIn('warehouse_id', 'Warehouses'), ['errorField' => 'warehouse_id']);

        $rules->add($rules->existsIn('zone_id', 'Zones'), ['errorField' => 'zone_id']);


        return $rules;
    }
}
