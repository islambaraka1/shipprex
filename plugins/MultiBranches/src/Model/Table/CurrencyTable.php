<?php
declare(strict_types=1);

namespace MultiBranches\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Currency Model
 *
 * @method \MultiBranches\Model\Entity\Currency newEmptyEntity()
 * @method \MultiBranches\Model\Entity\Currency newEntity(array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Currency[] newEntities(array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Currency get($primaryKey, $options = [])
 * @method \MultiBranches\Model\Entity\Currency findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \MultiBranches\Model\Entity\Currency patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Currency[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \MultiBranches\Model\Entity\Currency|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \MultiBranches\Model\Entity\Currency saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \MultiBranches\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \MultiBranches\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CurrencyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('mlb_currencies');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('code')
            ->maxLength('code', 4)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        return $validator;
    }
}
