<?php
declare(strict_types=1);

namespace MultiBranches\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\Event\Event;
use ArrayObject;

/**
 * MultiBranch behavior
 */
class MultiBranchBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array<string, mixed>
     */
    protected $_defaultConfig = [];

    public function beforeSave(Event $event, $entity, $options)
    {
        if (!isset($entity->branch_id)) {
            // set the current user's branch_id if it's not set
            $entity->branch_id = $this->getCurrentUserBranchId();
        }
    }

    public function beforeFind(Event $event, $query, ArrayObject $options)
    {

        $currentBranchId = $this->getCurrentUserBranchId();

        $tableRegistryAlias=$this->_table->getRegistryAlias();
        if(strpos($tableRegistryAlias,'.')){
            $tableRegistryAlias=explode('.',$tableRegistryAlias)[1];
        }

        if($currentBranchId != 0)
            $query->where([$tableRegistryAlias.'.branch_id' => $currentBranchId]);
    }

    private function getCurrentUserBranchId()
    {

        // TODO: check user group gets null
        // TODO: what about branch_id = 0 and create an order or zone etc
        if ($_SESSION['Auth']['User']['group_id'] == 1 )
            return (isset($_COOKIE['branch']) && $_COOKIE['branch'] != 0) ? $_COOKIE['branch']: null ;// null from settings

        else
            return $_SESSION['Auth']['User']['branch_id'] ;

    }
}
