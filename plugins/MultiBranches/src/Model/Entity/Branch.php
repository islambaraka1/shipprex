<?php
declare(strict_types=1);

namespace MultiBranches\Model\Entity;

use Cake\ORM\Entity;

/**
 * Branch Entity
 *
 * @property int $id
 * @property string $name
 * @property string $location
 * @property int|null $bank_id
 * @property int|null $currency_id
 * @property int|null $user_id
 * @property int|null $zone_id
 * @property int|null $warehouse_id
 * @property string $contact_info
 * @property \Cake\I18n\FrozenTime $open_at
 * @property \Cake\I18n\FrozenTime $closed_at
 * @property string $delivery_fees
 * @property string $payment_method
 * @property bool $inventory
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \MultiBranches\Model\Entity\GlBank $gl_bank
 * @property \MultiBranches\Model\Entity\MlbCurrency $mlb_currency
 * @property \MultiBranches\Model\Entity\User $user
 * @property \MultiBranches\Model\Entity\Zone $zone
 */
class Branch extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'location' => true,
        'bank_id' => true,
        'currency_id' => true,
        'user_id' => true,
        'zone_id' => true,
        'warehouse_id' => true,
        'contact_info' => true,
        'open_at' => true,
        'closed_at' => true,
        'delivery_fees' => true,
        'payment_method' => true,
        'inventory' => true,
        'created' => true,
        'modified' => true,
        'bank' => true,
        'currency' => true,
        'user' => true,
        'zone' => true,
    ];
}
