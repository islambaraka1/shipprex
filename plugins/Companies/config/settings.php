<?php

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

Configure::write('Settings.Companies', []);

easy_new_setting('Settings.Companies','companies_enabled','Enable or turn this feature off','on_off',0);
//get the list of available groups
//$groups = TableRegistry::getTableLocator()->get('Groups')->find('list')->where(['is_companies' => true])->toArray();
//easy_new_setting('Settings.Companies','companies_companies_admin_group','Select the main companies admin group','select',0,$groups);
//easy_new_setting('Settings.Companies','companies_companies_access_edit_profile','Change the delegated order notes field to [DELEGATED TO : DATE]','on_off',0);

easy_new_setting('Settings.Companies','companies_show_company_users_in_user_list','Show the companies users in the user list','on_off',0);
