<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use MultiBranches\Enums\MultiBranchesSettingEnum;

$hooks = \App\Hooks\Hooks::getInstance();
$groupsTable = TableRegistry::getTableLocator()->get('UsersManager.Groups');
$groupsTable->getEventManager()->on('Model.beforeSave', function ($event, $entity, $options){
    $entity->is_companies = $_POST['is_companies']??0;
});


if (get_option_value('companies_enabled') == 1){
    if(!function_exists('add_companies_to_groups')){
        $hooks->add_action('admin_group_form', 'add_companies_to_groups');
        function add_companies_to_groups()
        {
            $hooks = \App\Hooks\Hooks::getInstance();
            //try to alter the entity
            echo $hooks->currentView->Form->control('is_companies', ['type' => 'checkbox', 'label' => 'Companies']);
        }
    }

    if(!get_option_value('companies_show_company_users_in_user_list')){
        if(!function_exists('users_conditions_for_companies')){
            $hooks->add_filter('users_index_conditions', 'users_conditions_for_companies');
            function users_conditions_for_companies($conditions)
            {
                $groupsTable = TableRegistry::getTableLocator()->get('UsersManager.Groups');
                $groups = $groupsTable->find()->where(['is_companies' => false])->toArray();
                //return array of ids
                $conditions['group_id IN'] = array_map(function ($group) {
                    return $group->id;
                }, $groups);
                return $conditions;
            }
        }
    }

    // Support companies menu in the companies users
    if(!function_exists('companies_add_to_menu')) {
        $hooks->add_filter('menu_group_id', 'companies_add_to_menu');
        function companies_add_to_menu($group_id)
        {
            $groupsTable = TableRegistry::getTableLocator()->get('UsersManager.Groups');
            $group = $groupsTable->get($group_id);
            if($group->is_companies){
                //check if this group has a menu
                $menusTable = TableRegistry::getTableLocator()->get('Menus.Menus');
                $menu = $menusTable->find()->where(['group_id' => $group_id])->first();
                if(!isset($menu->id)){
                    return 3;
                }
                return $group_id;
            }
            return $group_id;
        }
    }


    // Support companies menu in the companies users
    if(!function_exists('companies_user_id')) {
        $hooks->add_filter('get_user_id_for_companies', 'companies_user_id');
        function companies_user_id($user_id)
        {
            $companiesTable = TableRegistry::getTableLocator()->get('Companies.Companies');
            $usersTable = TableRegistry::getTableLocator()->get('UsersManager.Users');
            // get the current looged ing company id
            $groupTable = TableRegistry::getTableLocator()->get('Groups');
            $group = $groupTable->find()->where(['id' => $_SESSION['Auth']['User']['group_id']])->first();
            if($group->is_companies == 1){
                $user = $usersTable->get($user_id);
                $company = $companiesTable->get($user->company_id);
                return $company->user_id;
            }
            return $user_id;

        }
    }


    // Excel Filter for user id
    if(!function_exists('get_company_user_for_excel')) {
        $hooks->add_filter('user_id_on_excel_save', 'get_company_user_for_excel');
        function get_company_user_for_excel($user_id)
        {
            if($_SESSION['Auth']['User']['company_id'] != null){
                // update the order to the company user id
                $companiesTable = TableRegistry::getTableLocator()->get('Companies.Companies');
                $company = $companiesTable->get($_SESSION['Auth']['User']['company_id']);
                return $company->user_id;
            }
            return $user_id;
        }
    }

    //check if the order saved by a user company for the single add action
    $tableOrders = TableRegistry::getTableLocator()->get('Orders');

    $tableOrders->getEventManager()->on('Model.Order.afterPlace',  function($event, $entity){
        $eventData = $event->getData();
        //check if the current logged in user is company user
        if($_SESSION['Auth']['User']['company_id'] != null){
            // update the order to the company user id
            $companiesTable = TableRegistry::getTableLocator()->get('Companies.Companies');
            $company = $companiesTable->get($_SESSION['Auth']['User']['company_id']);
            $entity->user_id = $company->user_id;
            //support the parent fees
            $entity->fees = CompanyFeesForCity($company->user_id,$entity->city);
            $tableOrders = TableRegistry::getTableLocator()->get('Orders');
            $tableOrders->save($entity);
        }
    });


    function CompanyFeesForCity($company_user,$city): float
    {
        $zonesTable = TableRegistry::getTableLocator()->get('Zones');
        $city = $zonesTable->find()->where(['name' => $city])->first();
        $zonesUsersTable = TableRegistry::getTableLocator()->get('ZonesUsers');
        $companyFees = $zonesUsersTable->find()->where(['user_id' => $company_user, 'zone_id' => $city->id])->first();
        if(isset($companyFees->price)){
            return $companyFees->price;
        }
        return $city->default_price;
    }


}
