<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CompaniesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('companies');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])
            ->addColumn('address', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])
            ->addColumn('phone', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
            ->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
            ->addColumn('logo', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

    }
}
