<?php

use MultiBranches\Enums\MultiBranchesSettingEnum;
use MultiBranches\Services\BranchService;

if (get_option_value('companies_enabled')) {
    $hooks->create_new_root_menu('companies', 'Companies', '#', 'store', [1,3]);
    $hooks->add_sub_menu_item_to_root('companies', 'List Companies', '/companies/companies', [1]);
    $hooks->add_sub_menu_item_to_root('companies', 'Add Company', '/companies/companies/add', [1]);
    $hooks->add_sub_menu_item_to_root('companies', 'Company Profile', '/companies/companies/edit', [3]);
    $hooks->add_sub_menu_item_to_root('companies', 'Users List', '/companies/companies/companyUsers', [3]);
    $hooks->add_sub_menu_item_to_root('companies', 'Add User', '/companies/companies/add-user', [3]);


}



