<?php
declare(strict_types=1);

namespace Companies\Controller;

use Companies\Controller\AppController;

/**
 * Companies Controller
 *
 * @property \Companies\Model\Table\CompaniesTable $Companies
 * @method \Companies\Model\Entity\Company[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompaniesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $companies = $this->paginate($this->Companies);

        $this->set(compact('companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set(compact('company'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $company = $this->Companies->newEmptyEntity();
        if ($this->request->is('post')) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company could not be saved. Please, try again.'));
        }
        $users = $this->Companies->Users->find('list', ['limit' => 200])->where(['group_id' => '3'])->all();
        $this->set(compact('company', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($id == null){
            $id = $this->Auth->user('id');
            $companyId = $this->Companies->find('all')->where(['user_id' => $id])->first();
            if(!isset($companyId->id))
            {
                $this->Flash->error(__('You are not a company user. Please, ask admin to create you a company profile.
                    <a href="https://shiprexnow.com/" target="_blank">Learn More</a>.'), [
                    'escape' => false,
                ]);
                return $this->redirect(['controller' => 'Orders', 'action' => 'index','plugin'=> null]);
            }
            $id = $companyId->id;
        }
        $company = $this->Companies->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company could not be saved. Please, try again.'));
        }
        $users = $this->Companies->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('company', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*
     * Add user to company we need to capture the company id from the user as well as adding the parent_user_id
     *
     */

    function addUser(){
        $company = $this->checkIfTheCurrentUserIsACompany();
        $this->Companies->find('all')->where(['id' => $company])->first();
        $user = $this->Companies->Users->newEmptyEntity();
        $groups = $this->Companies->Users->Groups->find('list', ['limit' => 200])->where(['is_companies' => true])->all();
        if ($this->request->is('post')) {
            $user = $this->Companies->Users->patchEntity($user, $this->request->getData());
            $user->company_id = $company;
            $user->active = 1;
            $user->email_verfied = 1;
            if ($this->Companies->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index','plugin'=> null,'controller' => 'Orders']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'groups'));
    }

    /*
     * This method is for displaying the company profile
     */
    public function companyProfile(){

    }

    /**
     * @return \Cake\Http\Response|void|null
     */
    private function checkIfTheCurrentUserIsACompany()
    {
// check if the current user is a company
        $user = $this->Auth->user();
        $company = $this->Companies->find('all')->where(['user_id' => $user['id']])->first();
        if ($company) {
            $company_id = $company->id;
            return $company_id;
        } else {
            //redirect to the dashboard with an error message you are not a company
            $this->Flash->error(__('You are not a company'));
            return $this->redirect(['controller' => 'Orders', 'action' => 'index', 'plugin' => null]);
        }
    }

    public function companyUsers($id =null){
        $company = $this->checkIfTheCurrentUserIsACompany();
        $company = $this->Companies->find('all')->where(['id' => $company])->first();
        $users = $this->Companies->Users->find('all')->where(['company_id' => $company->id])->contain(['Groups'])->all();
        $this->set(compact('users'));
    }
}
