<?php
declare(strict_types=1);

namespace Companies\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $address
 * @property int|null $user_id
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $logo
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \Companies\Model\Entity\User $user
 * @property \Companies\Model\Entity\Phinxlog[] $phinxlog
 */
class Company extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'user_id' => true,
        'phone' => true,
        'email' => true,
        'logo' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'phinxlog' => true,
    ];
}
