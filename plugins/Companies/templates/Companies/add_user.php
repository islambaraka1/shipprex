
<div class="users form content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
        echo $this->Form->control('username' );
        echo $this->Form->control('full_name' );
        echo $this->Form->control('password');
        echo $this->Form->control('email');
        echo $this->Form->control('phone');
        echo $this->Form->control('address');
        echo $this->Form->control('city');
        echo $this->Form->control('group_id', ['options' => $groups]);
        //            echo $this->Form->control('login_token');
        //            echo $this->Types->renderUserProfileForm($profileFields,$form_types);
        //            echo $this->Form->control('profile_fields._ids', ['options' => $profileFields]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
