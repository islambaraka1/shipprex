<table class="table table-striped" id="example1">
    <thead>
    <tr>
        <th scope="col"><?= __('id') ?></th>
        <th scope="col"><?= __('username') ?></th>
        <th scope="col"><?= __('email') ?></th>
        <th scope="col"><?= __('Phone') ?></th>
        <th scope="col"><?= __('group_id') ?></th>
        <th scope="col"><?= __('created') ?></th>
        <th scope="col"><?= __('modified') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user) : ?>
    <tr>
        <td><?= $this->Number->format($user->id) ?></td>
        <td><?= h($user->username) ?></td>
        <td><?= h($user->email) ?></td>
        <td><?= h($user->phone) ?></td>
        <td><?= $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
        <td><?= h($user->created) ?></td>
        <td><?= h($user->modified) ?></td>


        <td class="actions">

            <div class="dropdown" style="display: inline-block" >
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                    <?= $this->Html->link(__('Zones'), ['action' => 'zones', $user->id], ['title' => __('Zones'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                    <?= $this->Html->link(__('Edit Password'), ['action' => 'edit-password', $user->id], ['title' => __('Password'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                    <?php
                    if($user->active == 0){
                        echo $this->Html->link(__('Active account'), ['action' => 'active', $user->id],
                            ['class' => 'dropdown-item btn btn-secondary']);
                    } else {
                        echo $this->Html->link(__('suspend account'), ['action' => 'suspend', $user->id],
                            ['class' => 'dropdown-item btn btn-secondary']);

                    }
                    ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['title' => __('Edit'), 'class' => 'dropdown-item btn btn-secondary']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'title' => __('Delete'), 'class' => 'dropdown-item btn btn-danger d-none']) ?>


                </div>
            </div>
        </td>


        <?php endforeach; ?>
    </tbody>
</table>
<?=$this->element('backend/datatablejs')?>
