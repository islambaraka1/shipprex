<?php
declare(strict_types=1);

namespace Notifictions\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Notifictions\Model\Table\FcmsTable;

/**
 * Notifictions\Model\Table\FcmsTable Test Case
 */
class FcmsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Notifictions\Model\Table\FcmsTable
     */
    protected $Fcms;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Notifictions.Fcms',
        'plugin.Notifictions.Users',
        'plugin.Notifictions.Groups',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fcms') ? [] : ['className' => FcmsTable::class];
        $this->Fcms = TableRegistry::getTableLocator()->get('Fcms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Fcms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
