<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
$noteListner = new NotificationListener();
//EventManager::instance()->on($noteListner );
TableRegistry::getTableLocator()->get('Notifictions.Notifications')
    ->getEventManager()
    ->on($noteListner);



require_once 'events.php';
require_once 'settings.php';
