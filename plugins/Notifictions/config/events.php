<?php

use Cake\ORM\TableRegistry;
use App\Event\DriversListener;
use App\Event\InvoicesListener;
$table = TableRegistry::getTableLocator()->get('Orders');
if(get_option_value('notifications_on_new_orders') == 1) {
    $table->getEventManager()
        ->on('Model.afterSave', function ($event, $entity) {
            // send Order notification for all the users on group admin
            if ($entity->isNew()){
                $noteTable = TableRegistry::getTableLocator()->get('Notifictions.Notifications');
                $noteTable->sendNoteForOrder($entity);
            }
        });
}

//
//// notify driver of new comment on orders blongs to him
//$tableComments = TableRegistry::getTableLocator()->get('Comments.Comments');
//$tableComments->getEventManager()
//    ->on('Model.afterSave', function ($event, $entity) {
//        // get if any driver comment on this one
//        $tableComments = TableRegistry::getTableLocator()->get('Comments.Comments');
//        $commentsForDrivers = $tableComments->find()->where(['order_id'=>$entity->order_id , 'driver_id >' =>0 ])->toArray();
//        $fcmTable = TableRegistry::getTableLocator()->get('Notifictions.Fcms');
//        foreach ($commentsForDrivers as $driver){
//            $driverId = $driver->driver_id;
//            $fcmForDriver = $fcmTable->find()->where(['driver_id' => $driverId])->first();
//            // var_dump($fcmForDriver->token);
//
//            $json_data = [
//                "to" => $fcmForDriver->token,
//                "notification" => [
//                    "body" => "new comment for order # ".DID($entity->order_id),
//                    "title" => "Comment Received",
//                    "icon" => "ic_launcher"
//                ],
//                "data" => [
//                    "click_action" => 'http://localhost/cake/shipping/orders/view/1000'
//                ]
//            ];
//            // var_dump($fcmTable->sendFCM($json_data));
//            // die();
//
//
//        }
//    });

// insert notifiction for new orders comment
$tableComments = TableRegistry::getTableLocator()->get('Comments.Comments');
$tableComments->getEventManager()
    ->on('Model.afterSave', function ($event, $entity) {
        // check if this comment made by driver
        $tableComments = TableRegistry::getTableLocator()->get('Comments.Comments');
        $commentsForDrivers = $tableComments->find()->where(['order_id'=>$entity->order_id , 'driver_id >' =>0 ])->toArray();
        $noteTable = TableRegistry::getTableLocator()->get('Notifictions.Notifications');
        if(count($commentsForDrivers) > 0){
            // this comment made by driver
            // insert notification for driver
            $noteTable->sendNoteForNewCommentToDriver($entity);
        }
        //send notifiction for admin
        $noteTable->sendNoteForNewCommentToAdmin($entity);
        //send notifiction for user
        $noteTable->sendNoteForNewCommentToUser($entity);
    });

/*
    ->getEventManager()
    ->on('Model.Orders.afterSave', function($event, $entity)
    {
        var_dump($event);
        var_dump($entity);
        die();
    });


*/
