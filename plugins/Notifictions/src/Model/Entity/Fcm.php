<?php
declare(strict_types=1);

namespace Notifictions\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fcm Entity
 *
 * @property int $id
 * @property string $token
 * @property int $user_id
 * @property int $group_id
 * @property \Cake\I18n\FrozenTime $last_login
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Notifictions\Model\Entity\User $user
 * @property \Notifictions\Model\Entity\Group $group
 */
class Fcm extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'token' => true,
        'user_id' => true,
        'driver_id' => true,
        'group_id' => true,
        'last_login' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'group' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token',
    ];
}
