<?php
declare(strict_types=1);

namespace Notifictions\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Notifications Model
 *
 * @property \Notifictions\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \Notifictions\Model\Entity\Notification newEmptyEntity()
 * @method \Notifictions\Model\Entity\Notification newEntity(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification[] newEntities(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification get($primaryKey, $options = [])
 * @method \Notifictions\Model\Entity\Notification findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Notifictions\Model\Entity\Notification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Notification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NotificationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('notifications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Notifictions.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('body')
            ->allowEmptyString('body');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model');

        $validator
            ->boolean('send_email')
            ->requirePresence('send_email', 'create')
            ->notEmptyString('send_email');

        $validator
            ->boolean('send_phone')
            ->requirePresence('send_phone', 'create')
            ->notEmptyString('send_phone');

        $validator
            ->scalar('params')
            ->allowEmptyString('params');

        $validator
            ->boolean('red')
            ->requirePresence('red', 'create')
            ->notEmptyString('red');

        $validator
            ->boolean('new')
            ->requirePresence('new', 'create')
            ->notEmptyString('new');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function readIt($id){
        $note = $this->get($id);
        $note->set('red' , 1);
        $this->save($note);
    }
    public function oldIt($id){
        $note = $this->get($id);
        $note->set('new' , 0);
        $this->save($note);
    }

    public function sendNoteForOrder($order){
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "New Order Created";
        $note->body = "Please check this new order #".DID($order->id);
        $note->send_fcm = 1;
        $this->sendToGroup($note,1);
    }
    public function sendToUser($note,$user_id){
        //send order for user id
        $note->user_id = $user_id;
        $this->save($note);
    }
    public function  sendToGroup($note,$group){
        $usersTable = TableRegistry::getTableLocator()->get('UsersManager.Users');
        $userListInGroup = $usersTable->find('all')->where(['group_id' =>$group])->toArray();
        foreach ($userListInGroup as $user){
            $entity = $this->newEmptyEntity();
            $entity = $note;
            $entity->user_id = $user->id;
            $this->save($entity);
        }
    }
    public function sendNoteForOrderStatus($order){
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "Order Status Changed";
        $note->body = "Order #".DID($order->id)." status changed to ".$order->status;
        $note->send_fcm = 1;
        $this->sendToGroup($note,1);
    }
    public function sendNoteForOrderStatusToUser($order){
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "Order Status Changed";
        $note->body = "Order #".DID($order->id)." status changed to ".$order->status;
        $note->send_fcm = 1;
        $note->user_id = $order->user_id;
        $this->save($note);
    }
    //function to create notification for new comment on order
    public function sendNoteForNewCommentToAdmin($comment){
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "New Comment";
        $note->body = $this->getNewCommentMessage($comment);
        $note->send_fcm = 1;
        $this->sendToGroup($note,1);
    }
    //function to create notification for new comment on order sent to order user
    public function sendNoteForNewCommentToUser($comment){
        // get order user id
        $order = TableRegistry::getTableLocator()->get('Orders')->get($comment->order_id);
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "New Comment";
        $note->body = $this->getNewCommentMessage($comment);
        $note->send_fcm = 1;
        $note->user_id = $order->user_id;
        $this->save($note);
    }
    //function to create notification for new comment on order sent to order driver
    public function sendNoteForNewCommentToDriver($comment){
        // get order user id
        $order = TableRegistry::getTableLocator()->get('Orders')->get($comment->order_id);
        // get the current active driver for this order
        $driver = TableRegistry::getTableLocator()->get('Orders')->getDriver($order->id);
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "New Comment";
        $note->body = $this->getNewCommentMessage($comment);
        $note->send_fcm = 1;
        $note->driver_id = $driver->driver_id;
        $this->save($note);
    }

    //function to notify the admin and seller user_id for an action on the order
    public function sendNotificationForAdminAndSellerOfOrder($order,$action){
        //send order for admin group
        $note = $this->newEmptyEntity();
        $note->title = "Order ".$action;
        $note->body = "Order #".DID($order->id)." ".$action;
        $note->send_fcm = 1;
        $this->sendToGroup($note,1);
        //send order for seller group
        $note = $this->newEmptyEntity();
        $note->title = "Order ".$action;
        $note->body = "Order #".DID($order->id)." ".$action;
        $note->send_fcm = 1;
        $this->sendToUser($note,$order->user_id);
    }

    /**
     * @param $comment
     * @return string
     */
    private function getNewCommentMessage($comment): string
    {
        return "New comment on order #" . DID($comment->order_id);
    }


}
