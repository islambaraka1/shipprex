<?php
declare(strict_types=1);

namespace Notifictions\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fcms Model
 *
 * @property \Notifictions\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Notifictions\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 *
 * @method \Notifictions\Model\Entity\Fcm newEmptyEntity()
 * @method \Notifictions\Model\Entity\Fcm newEntity(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Fcm[] newEntities(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Fcm get($primaryKey, $options = [])
 * @method \Notifictions\Model\Entity\Fcm findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Notifictions\Model\Entity\Fcm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Fcm[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Fcm|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Fcm saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Fcm[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Fcm[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Fcm[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Fcm[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FcmsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('fcms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
            'className' => 'Notifictions.Users',
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'LEFT',
            'className' => 'Notifictions.Groups',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('token')
            ->requirePresence('token', 'create')
            ->notEmptyString('token');

        $validator
            ->dateTime('last_login')
            ->requirePresence('last_login', 'create')
            ->notEmptyDateTime('last_login');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['user_id'], 'Users'));
//        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }

    function sendFCM($json_data){
        $data = json_encode($json_data);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = 'AAAApuuCTOY:APA91bE_FJjPb2TmsX0KtujmyKGGZaoX7EF4jOL1d4qU8g8uVUn1M3qFU4QHPwn4Qv3bQmRmWyehJrpdDWKEexHhSb9NqC8pbwz2hbUu4-LuaEwQUvKfD9MZaHxJZiLlPCGtYcWSkZ8x';
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
//CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}
