<?php
declare(strict_types=1);

namespace Notifictions\Controller;

use Cake\I18n\Date;
use Cake\ORM\TableRegistry;

/**
 * Fcms Controller
 *
 * @property \Notifictions\Model\Table\FcmsTable $Fcms
 *
 * @method \Notifictions\Model\Entity\Fcm[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FcmsController extends AppController
{



    /**
     * View method
     *
     * @param string|null $id Fcm id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fcm = $this->Fcms->get($id, [
            'contain' => ['Users', 'Groups'],
        ]);

        $this->set('fcm', $fcm);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($driver= null)
    {
        $this->disableAutoRender();
        $fcm = $this->Fcms->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $fcm = $this->Fcms->patchEntity($fcm, $data);
            // check if this is a driver view

            if(isset($driver)){
                $fcm->driver_id = $driver;
                $fcm->user_id =0;
                $fcm->group_id = 0;
                //delete any old token for the same user or driver before save new one
                $this->Fcms->deleteAll(['driver_id' => $driver]);
            }else{
                $fcm->user_id = $_SESSION['Auth']['User']['id'];
                $fcm->group_id = $_SESSION['Auth']['User']['group_id'];
                //delete any old token for the same user or driver before save new one
                $this->Fcms->deleteAll(['user_id' => $_SESSION['Auth']['User']['id']]);
            }
            //missing case when the fcm need to be updated from user to driver
            $this->Fcms->deleteAll(['token' => $fcm->token]);

            $fcm->last_login = Date::now();
            if ($this->Fcms->save($fcm)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode(['data'=>$fcm]));
            }
            return $this->response->withType('application/json')
                ->withStringBody(json_encode(['data'=>'error','debug'=>$fcm]));
        }
    }





    public function sendNote($userId, $nonfictionId ){
        $this->disableAutoRender();
        //Get the user token from FCm table
        $fcm = $this->Fcms->find()->where(['user_id' => $userId])->first();
        $noteTable = TableRegistry::getTableLocator()->get('Notifications');
        // get the notification from the Notification table by id send in the parameter
        $note = $noteTable->get($nonfictionId);
       echo  json_encode( $this->Fcms->sendFCM([
            "to" => "dV1spjdLnyFz0ka1wMYnwC:APA91bHlGfj0-Cjvw5D839fW97ENRknu5d5N0fFcQLhu26pZDANRBj_9HuGMNBfseElvnYByTF8yMHvZeOnXX21k3noOiH_1AEAIJk8ss1TReoEwY8gKnBsRz-Do68jtpy-Am1cnLRq8",
            "notification" => [
                "body" => $note->body,
                "title" => $note->title,
                "icon" => "ic_launcher"
            ],
            "data" => [
                "click_action" => 'http://localhost/cake/shipping/orders/view/1000'
            ]
        ]));

    }

    /**
     * Edit method
     *
     * @param string|null $id Fcm id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fcm = $this->Fcms->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fcm = $this->Fcms->patchEntity($fcm, $this->request->getData());
            if ($this->Fcms->save($fcm)) {
                $this->Flash->success(__('The fcm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fcm could not be saved. Please, try again.'));
        }
        $users = $this->Fcms->Users->find('list', ['limit' => 200]);
        $groups = $this->Fcms->Groups->find('list', ['limit' => 200]);
        $this->set(compact('fcm', 'users', 'groups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Fcm id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fcm = $this->Fcms->get($id);
        if ($this->Fcms->delete($fcm)) {
            $this->Flash->success(__('The fcm has been deleted.'));
        } else {
            $this->Flash->error(__('The fcm could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
