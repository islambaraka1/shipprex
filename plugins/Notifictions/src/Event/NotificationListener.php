<?php
namespace Notifictions\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;


class NotificationListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Users.afterRegister' => 'afterRegister',
            'Model.Users.afterRegister' => 'afterRegister',
            'Model.Users.afterRemove' => 'afterRemove',
            'Model.afterSave' => 'NotifictionAfterSave',
            'Model.Orders.afterSave' => 'OrdersAfterSave',
        ];
    }

    public function afterRegister($event, $user)
    {
//        Log::write('debug', $user['username'] . ' Register Called.');
//        die();
        /*
        $email = new Email('default');
        $email->setFrom(['support@example.com' => 'Example Site'])
            ->setTo('backend@example.com')
            ->setSubject('New User Registration - ' . $user['email'])
            ->send('User has registered in your application');
        */
    }

    public function afterRemove($event, $user)
    {
//        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }

    public function NotifictionAfterSave(Event $event , $note){

        // the logic for this event
        if($note->send_fcm == 1){
            $fcmTable = TableRegistry::getTableLocator()->get('Notifictions.Fcms');
            // handel bussines case where its sending to single driver not a user
            if($note->user_id != null){
                $fcm = $fcmTable->find()->where(['user_id' => $note->user_id])->first();
            }else{
                $fcm = $fcmTable->find()->where(['driver_id' => $note->driver_id])->first();
            }
//            $userRow = $fcmTable->find('all')->where(['user_id' => $note->user_id])->first();
            if(isset($fcm->token)){
                $fcmTable->sendFCM([
                    "to" => $fcm->token,
                    "notification" => [
                        "body" => $note->body,
                        "title" => $note->title,
                        "icon" => "ic_launcher"
                    ],
                    "data" => [
                        "click_action" => 'http://localhost/cake/shipping/orders/view/1000'
                    ]
                ]);
            }

        }
        if($note->send_email == 1){
            // send the email for this notification
            $mailer = new Mailer('default');
            //check on live servers
            try {
                $mailer->setFrom(['testing@ibaraka.com' => PROJECT_NAME ])
                    ->setTo('me@ibaraka.com')
                    ->setSubject('About')
                    ->deliver('My message');
            } catch (\Exception $exception){

            }

        }
    }

    public function OrdersAfterSave(Event $event , $order){
//        var_dump($event);
//        var_dump($order);
//        die();
    }


}
?>
