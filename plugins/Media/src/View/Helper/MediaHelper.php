<?php
declare(strict_types=1);

namespace Media\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Media helper
 */
class MediaHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

}
