<?php
declare(strict_types=1);

namespace Media\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Media\View\Helper\MediaHelper;

/**
 * Media\View\Helper\MediaHelper Test Case
 */
class MediaHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Media\View\Helper\MediaHelper
     */
    protected $Media;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Media = new MediaHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Media);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
