<?php
declare(strict_types=1);

namespace Comments\Controller;

use Cake\Event\Event;

/**
 * Comments Controller
 *
 * @property \Comments\Model\Table\CommentsTable $Comments
 *
 * @method \Comments\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Orders', 'Drivers'],
        ];
        $comments = $this->paginate($this->Comments);

        $this->set(compact('comments'));
    }
    /**
     * mylist method
     *
     * @return \Cake\Http\Response|null
     */
    public function mylist()
    {
       $comments    = $this->Comments->find('all')->contain(['Drivers','Users','Orders'])->where(['Comments.user_id' => 1]);
        $comments = $this->paginate($comments);

        $this->set(compact('comments'));
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Users', 'Orders', 'Drivers'],
        ]);

        $this->set('comment', $comment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEmptyEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                $event = new Event('Model.Comments.addNewComment', $this, [
                    'comment' => $comment
                ]);
                $this->getEventManager()->dispatch($event);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $orders = $this->Comments->Orders->find('list', ['limit' => 200]);
        $drivers = $this->Comments->Drivers->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'users', 'orders', 'drivers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $orders = $this->Comments->Orders->find('list', ['limit' => 200]);
        $drivers = $this->Comments->Drivers->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'users', 'orders', 'drivers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($id);
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    function upload($model, $id){
        $this->autoRender = false;
        header('Content-Type: application/json'); // set json response headers
        $outData = $this->uploader($model,$id); // a function to upload the bootstrap-fileinput files
        echo json_encode($outData); // return json data
        exit(); // terminate
    }
    function renameTempFolder($model,$id,$tempFolder){
        $this->autoRender = false;
        $newFolder = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$id.DS; // your upload path
        $tempFolder = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$tempFolder.DS; // your temp upload path
        if(!is_dir($newFolder)){
            mkdir($newFolder,0777,true);
        }
        $files = scandir($tempFolder);
        foreach($files as $file){
            if($file != '.' && $file != '..'){
                rename($tempFolder.DS.$file,$newFolder.DS.$file);
            }
        }
        rmdir($tempFolder);
        return $this->response->withType('application/json')->withStringBody(json_encode(['success'=>true]));
    }

    private function uploader($model,$id){

        $preview = $config = $errors = [];
        $input = 'kartik-input-705'; // the input name for the fileinput plugin
        if (empty($_FILES[$input])) {
            return [];
        }
        $total = count($_FILES[$input]['name']); // multiple files
        $path = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$id.DS; // your upload path
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        for ($i = 0; $i < $total; $i++) {
            $tmpFilePath = $_FILES[$input]['tmp_name'][$i]; // the temp file path
            $fileName = $_FILES[$input]['name'][$i]; // the file name
            $fileSize = $_FILES[$input]['size'][$i]; // the file size

            //Make sure we have a file path
            if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = $path . $fileName;
                $newFileUrl = ROOT_URL."uploads/$model/$id/" . $fileName;

                //Upload the file into the new path
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                    // brodcast the file upload event to order model
                    $event = new Event('Model.Comments.uploadNewFile', $this, [
                        'model' => $model,
                        'id' => $id,
                        'fileName' => $fileName,
                        'fileSize' => $fileSize,
                        'newFileUrl' => $newFileUrl
                    ]);
                    $this->Comments->getEventManager()->dispatch($event);
                    $fileId = $fileName . $i; // some unique key to identify the file
                    $preview[] = $newFileUrl;
                    $config[] = [
                        'key' => $fileId,
                        'caption' => $fileName,
                        'size' => $fileSize,
                        'downloadUrl' => $newFileUrl, // the url to download the file
                        'url' => ROOT_URL.'comments/comments/deletefile/'.$model.'/'.$id.'/'.$fileName, // server api to delete the file based on key
                    ];
                } else {
                    $errors[] = $fileName;
                }
            } else {
                $errors[] = $fileName;
            }
        }
        $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
        if (!empty($errors)) {
            $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
            $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
        }
        return $out;

    }
    function deletefile($model,$id,$file){
        $this->autoRender = false;
        $path = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$id.DS.$file; // your upload path
        if(file_exists($path)){
            unlink($path);
        }
        return $this->response->withType('application/json')->withStringBody(json_encode(['success'=>true]));
    }
    // image upload inside a modal for any order
    function images($orderId){
        $this->set('orderId',$orderId);

    }
}
