<?php
declare(strict_types=1);

namespace Comments\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Comments Model
 *
 * @property \Comments\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Comments\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 * @property \Comments\Model\Table\DriversTable&\Cake\ORM\Association\BelongsTo $Drivers
 * @property \Comments\Model\Table\PhinxlogTable&\Cake\ORM\Association\BelongsToMany $Phinxlog
 *
 * @method \Comments\Model\Entity\Comment newEmptyEntity()
 * @method \Comments\Model\Entity\Comment newEntity(array $data, array $options = [])
 * @method \Comments\Model\Entity\Comment[] newEntities(array $data, array $options = [])
 * @method \Comments\Model\Entity\Comment get($primaryKey, $options = [])
 * @method \Comments\Model\Entity\Comment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Comments\Model\Entity\Comment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Comments\Model\Entity\Comment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Comments\Model\Entity\Comment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Comments\Model\Entity\Comment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Comments\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Comments\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Comments\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Comments\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserManagement.Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
            'className' => 'Comments.Users',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
            'className' => 'Comments.Orders',
        ]);
        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
            'joinType' => 'LEFT',
            'className' => 'Comments.Drivers',
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));

        return $rules;
    }

    public function get_all_orders_comments_for_a_driver($driverId){
        $driverOrdersTables = TableRegistry::getTableLocator()->get('DriversOrders');
        $driverOrders = $driverOrdersTables->find('all')->where(['DriversOrders.driver_id' =>$driverId]);
        $ordersList = [];
        foreach ($driverOrders as $key => $order){
            $ordersList[] = $order->order_id;
        }
        if(empty($ordersList))
            return [];
        $comments = $this->find('all')
            ->select(['description','order_id','created'])
            ->where(['Comments.order_id IN' => $ordersList ])
            ->order(['Comments.created'=>'DESC'])
            ->limit(get_option_value('driver_comments_limit_for_widget'))
            ->toArray();
        return $comments;
    }
}
