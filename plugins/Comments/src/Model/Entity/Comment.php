<?php
declare(strict_types=1);

namespace Comments\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property int $driver_id
 * @property string $description
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Comments\Model\Entity\User $user
 * @property \Comments\Model\Entity\Order $order
 * @property \Comments\Model\Entity\Driver $driver
 * @property \Comments\Model\Entity\Phinxlog[] $phinxlog
 */
class Comment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'order_id' => true,
        'driver_id' => true,
        'description' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'order' => true,
        'driver' => true,
    ];
}
