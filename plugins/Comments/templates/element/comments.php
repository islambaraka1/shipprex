<style>

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    .btn-circle.btn-lg {
        width: 50px;
        height: 50px;
        padding: 10px 16px;
        font-size: 18px;
        line-height: 1.33;
        border-radius: 25px;
    }
    .btn-circle.btn-xl {
        width: 70px;
        height: 70px;
        padding: 10px 16px;
        font-size: 24px;
        line-height: 1.33;
        border-radius: 35px;
    }
</style>
<div class="col-md-12">

    <div class="box box-success direct-chat direct-chat-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?=__('Order Comments')?></h3>

        </div>

        <div class="box-body">

            <div class="direct-chat-messages">
<?php

foreach ($comments as $comment){
    //check if this is the order owner or not
    if($comment->user_id == $order->user_id){
        ?>
        <div class="direct-chat-msg right">
            <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right"><?=$comment->user->username?></span>
                <span class="direct-chat-timestamp pull-left"><?=$comment->created?></span>
            </div>

            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-user"></i></button>
            <div class="direct-chat-text">
                <?=$comment->description?>
            </div>

        </div>
        <?php
    } else {


    ?>
                <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left"><?=isset($comment->driver->name)?$comment->driver->name:$comment->user->username?></span>
                        <span class="direct-chat-timestamp pull-right"><?=$comment->created?></span>
                    </div>

                    <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-user"></i></button>
                    <div class="direct-chat-text">
                        <?=$comment->description?>
                    </div>

                </div>
                <?php
    }
}
?>





            </div>


            <div class="direct-chat-contacts">
                <ul class="contacts-list">
                    <li>
                        <a href="#">
                            <img class="contacts-list-img" src="<?=ROOT_URL?>dist/img/user1-128x128.jpg" alt="User Image">
                            <div class="contacts-list-info">
<span class="contacts-list-name">
Count Dracula
<small class="contacts-list-date pull-right">2/28/2015</small>
</span>
                                <span class="contacts-list-msg">How have you been? I was...</span>
                            </div>

                        </a>
                    </li>

                </ul>

            </div>

        </div>

        <div class="box-footer">
            <form action="#" method="post">
                <div class="input-group">
                    <input type="text" name="message" id="comment-content" placeholder="<?=__('Type Message ...')?>" class="form-control">
                    <span class="input-group-btn">
<button type="submit" class="btn btn-success btn-flat" id="comment-send-btn"><?=__('Send')?></button>
</span>
                </div>
            </form>
        </div>

    </div>

</div>

<script>

<?php
$this->Html->scriptStart(['block' => 'scriptcode']);
?>
$(document).ready(function(){
    $('#comment-send-btn').click(function(e){
        $comment = $('#comment-content').val();
        url = "<?=ROOT_URL?>/comments/comments/add"
        $.ajax({
            url: url,
            method: "POST",
            data:{
                description:$comment,
                user_id:<?=$_SESSION['Auth']['User']['id']?>,
                order_id:<?=$order->id?>
            }
        }).done(function($data) {
            console.log($data);
            //clear the input field
            $('#comment-content').val('');
            // add the comment in the markup

            //refresh
            location.reload();



        });
        e.preventDefault();
    })
});
<?php
$this->Html->scriptEnd();
?>

</script>
