<?php
use Cake\Core\Configure;
Configure::write('Settings.Comments', []);
//
//$hooks = \App\Hooks\Hooks::getInstance();
//
//$hooks->create_new_root_menu('test','Test','test','users');
//$hooks->add_sub_menu_item_to_root('test','Test','test');
//$hooks->add_sub_menu_item_to_root('test','Test222','test22' ,[4,1]);
easy_new_setting('Settings.Comments','images_label','Images in comments settings',"label");
easy_new_setting('Settings.Comments','images_upload_enabled','Enable image upload',"on_off");
easy_new_setting('Settings.Comments','image_upload_for_seller','Enable image upload for seller dashboard',"on_off");
easy_new_setting('Settings.Comments','image_upload_for_driver_online','Enable image upload for driver dashboard',"on_off");
easy_new_setting('Settings.Comments','image_upload_show_image_inside_comment_or_image_section',
    'Image display section',"select",null,[
        'inside_comment'=>'Inside comment',
        'image_section'=>'Image section'
    ]);
easy_new_setting('Settings.Comments','image_upload_for_order_creation','Enable image upload on order creation panel',"on_off");
easy_new_setting('Settings.Comments','image_upload_send_notifications','Send Notification on upload new image for any orders',"on_off");


// function i dont have a clue where to set
function renameTempFolder($model,$id,$tempFolder){
    $newFolder = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$id.DS; // your upload path
    $tempFolder = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$tempFolder.DS; // your temp upload path
    if(!is_dir($tempFolder))
        return false;
    if(!is_dir($newFolder)){
        mkdir($newFolder,0777,true);
    }
    $files = scandir($tempFolder);
    foreach($files as $file){
        if($file != '.' && $file != '..'){
            rename($tempFolder.DS.$file,$newFolder.DS.$file);
        }
    }
    rmdir($tempFolder);
    return true;
}
