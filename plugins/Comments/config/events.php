<?php
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
use Cake\Controller\Controller;

$hooks = \App\Hooks\Hooks::getInstance();


if (!function_exists('after_details_order')) {
    $hooks->add_action('orders_view_under_details', 'after_details_order');
    function after_details_order()
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        $orderId = $hooks->currentView->get('order')->id;
        $commentsTable = TableRegistry::getTableLocator()->get('Comments.Comments');
        $commentList = $commentsTable->find('all')->where(['Comments.order_id'=> $orderId])->contain(['Users','Drivers'])->toArray();
        echo $hooks->currentView->element('Comments.comments',['comments'=>$commentList]);
    }
}
// add the field to the below products fields
if (!function_exists('add_image_gallery_section')) {
    //setting based add the action
    $hooks->add_action('orders_add_form_before_end', 'add_image_uploader');
    function add_image_uploader()
    {
        $condition = false;
        if(isset($_SESSION)){
            if(get_option_value('images_upload_enabled') == 1){
                if(get_option_value('image_upload_for_order_creation') == 1){
                    $group_id = $_SESSION['Auth']['User']['group_id'];
                    $condition = in_array($group_id,[4,1,3]);
                    if ($group_id == 3){
                        $condition = get_option_value('image_upload_for_seller');
                    }
                }
            }
        }
        if(!$condition){
            return "";
        }
        $rand_num = rand(1000000000000, 9999999999999);
       echo  '
           <input type="hidden" name="Meta[image_folder]" value="'.$rand_num.'" />

    <div class="file-loading">
        <input id="input-705" name="kartik-input-705[]"  data-model="Orders" data-folder="'.$rand_num.'"  type="file" accept="image/*" multiple>
    </div>
    ';
    }
}
if (!function_exists('driver_online_widget')) {
    $hooks->add_action('driver_online_widget', 'driver_online_widget');
    function driver_online_widget($driverId)
    {
        $hooks = \App\Hooks\Hooks::getInstance();
//        $orderId = $hooks->currentView->get('order')->id;
        $commentsTable = TableRegistry::getTableLocator()->get('Comments.Comments');
        $commentList = $commentsTable->get_all_orders_comments_for_a_driver($driverId);
        echo $hooks->currentView->element('Utils.backend/widgets/data_table',[
            'color' => 'green',
            'size' => '6',
            'max_height' => '500',
            'table_data' => $commentList,
            'title' => __('Latest Comments '),
            'icon' => 'file-invoice-dollar',
            'sub_msg' => 'Order Comments you received ' ,

        ]);
    }
}


// add the button for feature of Upload images for drivers
if (!function_exists('add_upload_image_action')) {
//    $hooks->add_action('driver_view_action_status_update', 'add_upload_image_action');
//    $hooks->add_action('orders_action_buttons_list', 'add_upload_image_action');
    // add driver workflow
    $hooks->add_action('driver_online_order_actions', 'add_upload_image_action');

    function add_upload_image_action($id)
    {
        $hooks = \App\Hooks\Hooks::getInstance();
        echo $hooks->currentView->Html->link(__('Attach Images'), ['action' => 'images', 'controller' => 'Comments',
            'plugin' => 'Comments', $id],['class' => 'dropdown-item btn btn-secondary btn-sm ajax_form_open_modal']);
    }
}


// on order save handel order id for image uploaded by user
$orderTable = TableRegistry::getTableLocator()->get('Orders');
$orderTable->getEventManager()->on('Model.afterSave', function (\Cake\Event\Event $event, $entity, $options) {
    if(isset($_POST['Meta']['image_folder']))
        renameTempFolder('Orders',$entity->id,$_POST['Meta']['image_folder']); // rename temp folder to order id
});



// Add images to the Order details view
// add the field to the below products fields
if (!function_exists('add_image_gallery_section')) {
    $hooks->add_action('orders_view_under_details', 'add_image_gallery_section');
    function add_image_gallery_section($orderId)
    {
        $orderId = $orderId->id;
        $hooks = \App\Hooks\Hooks::getInstance();
        $view = $hooks->currentView;
        $model = 'Orders';
        $id = $orderId;
        $newFolder = ROOT.DS.'webroot'.DS.'uploads'.DS.$model.DS.$id.DS; // your upload path
        //check if the dir exist
        if (!is_dir($newFolder)) {
            //echo alert error

            echo '<div class="alert alert-secondary full-width col-12" role="alert">
                  '.__('No Images Uploaded').'
                </div>';
           return false;
        }
        $files = scandir($newFolder);
        $images = [];
        foreach($files as $file){
            if($file != '.' && $file != '..'){
                $images[] = ROOT_URL."uploads/Orders/$orderId/".$file;
            }
        }
        echo  $view->element('Comments.gallery',['images'=>$images]);
    }
}


// send notification to order owner when a file is uploaded
if(get_option_value('image_upload_send_notifications') == 1){
    $commentsTable = TableRegistry::getTableLocator()->get('Comments.Comments');
    $commentsTable->getEventManager()->on('Model.Comments.uploadNewFile', function (\Cake\Event\Event $event, $entity, $options) {
        $entity = $event->getData();
        // send the notification to the user
        $orderTable = TableRegistry::getTableLocator()->get('Orders');
        $order = $orderTable->get($entity['id']);
        $userTable = TableRegistry::getTableLocator()->get('Users');
        $user = $userTable->get($order->user_id);
        $notificationTable = TableRegistry::getTableLocator()->get('Notifictions.Notifications');
        $notificationTable->sendNotificationForAdminAndSellerOfOrder($order,"New File Uploaded","New File Uploaded for Order #".$order->id);
    });
}

