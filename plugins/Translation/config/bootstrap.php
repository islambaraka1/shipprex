<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
$hooks = \App\Hooks\Hooks::getInstance();
require_once 'settings.php';
require_once 'events.php';
