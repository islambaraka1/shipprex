<?php
declare(strict_types=1);

namespace Translation\Controller;
use Cake\Cache\Cache;
use Cake\Routing\Router;
use Google\Cloud\Translate\V2\TranslateClient;

/**
 * Translations Controller
 *
 * @property \Utils\Model\Table\TranslationsTable $Translations
 *
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TranslationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $translations = $this->paginate($this->Translations);
        $langsTexts = $this->Translations->find('all')->count();
        $langData = [];
        foreach (LANG_LIST as $key => $lang){
            $langData[$key] = [
                'name' =>$lang,
                'count'=>$this->Translations->find('all')->where(['locale'=>$key])->count(),
                'un_translated'=>$this->Translations->find('all')->where(['locale'=>$key , 'value_0 IS'=> NULL ])->count()
                ];
        }
        $this->set(compact('translations','langsTexts','langData'));
    }
    public function getData(){
        //check if its a post request and if the user is logged in
        if($this->request->is('post') ){
            $this->viewBuilder()->disableAutoLayout();
            $this->disableAutoRender();
            ## Read value
            $draw = $_POST['draw'];
            $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value'] ; // Search value

            ## Search
            $searchQuery = " ";
            if($searchValue != ''){
                $searchCondition = ["OR"=>[
                    "singular LIKE"=>"%".$searchValue."%",
                    "value_0 LIKE"=>"%".$searchValue."%",
                ]];
            } else {
                $searchCondition = [];
            }

            $totalRecords =$this->Translations->find('all')->count();

            $totalRecordwithFilter = $this->Translations->find('all')->where($searchCondition)->count();
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordwithFilter,
                "aaData" => $this->Translations->find('all')->where($searchCondition)->limit($rowperpage)->offset($row)->toArray()
            );

            return $this->response->withType('application/json')
                ->withStringBody(json_encode($response));
        }
    }


    function update(){
        if($this->request->is('post') ){
            $this->viewBuilder()->disableAutoLayout();
            $this->disableAutoRender();
            $data = $this->request->getData();
            $translation = $this->Translations->get($data['id']);
            $translation->value_0 = $data['value_0'];
            $this->Translations->save($translation);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode(['success'=>true]));
        }
    }

    // return only page data by limit and start index
    public function getPageData($limit, $start,$data){
        $this->viewBuilder()->disableAutoLayout();
        $this->disableAutoRender();
        $translations = $this->Translations->find('all')->limit($limit)->offset($start);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($translations));
    }

    function changelang($lang){
        $_SESSION['lang'] = $lang;
        $this->redirect(Router::url( $this->referer(), true ) );
    }

    public function autotranslate($lang){
        $this->disableAutoRender();
        $translate = new TranslateClient([
            'key' => 'AIzaSyBmnTMfIF6gnMSedCK2O1o0EhOZQs8cKII'
        ]);
        $texts = $this->Translations->find('all')->where(['locale'=>$lang , 'value_0 IS'=> NULL ])->toArray();
        foreach ($texts as $record){
            $result = $translate->translate($record->singular, [
                'target' => $lang
            ]);
            $record->value_0 = $result['text'];
            $this->Translations->save($record);
        }
        $this->redirect(['action' => 'index']);


    }

    /**
     * function to clear all cache data
     * by default accessible only for admin
     *
     * @access Public
     * @return void
     */
    public function clearCache() {
        $this->viewBuilder()->disableAutoLayout();
        $this->disableAutoRender();
        Cache::clearAll();
        $this->Flash->success(__('Cache cleared successfully'));
        $this->redirect(['action' => 'index']);
    }

    function findReplace(){
        if($this->request->is('post') ){
            $data = $this->request->getData();
            pr($data);
            $translations = $this->Translations->find('all')->where(['value_0 LIKE'=>"%".$data['value_0']."%"])->toArray();
            foreach ($translations as $key => $translation){
                $translations[$key]['replace'] = str_replace($data['value_0'],$data['replace'],$translation->value_0);
                if($data['confirm'] == 1){
                    $translation->value_0 = $translations[$key]['replace'];
                    $this->Translations->save($translation);
                }
            }
            if($data['confirm'] == 1){
                $this->Flash->success(__('Replacement done successfully'));
                $this->redirect(['action' => 'index']);
            }
            $this->set('translations',$translations);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $translation = $this->Translations->get($id, [
            'contain' => [],
        ]);

        $this->set('translation', $translation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $translation = $this->Translations->newEmptyEntity();
        if ($this->request->is('post')) {
            $translation = $this->Translations->patchEntity($translation, $this->request->getData());
            if ($this->Translations->save($translation)) {
                $this->Flash->success(__('The translation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The translation could not be saved. Please, try again.'));
        }
        $this->set(compact('translation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $translation = $this->Translations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $translation = $this->Translations->patchEntity($translation, $this->request->getData());
            if ($this->Translations->save($translation)) {
                $this->Flash->success(__('The translation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The translation could not be saved. Please, try again.'));
        }
        $this->set(compact('translation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $translation = $this->Translations->get($id);
        if ($this->Translations->delete($translation)) {
            $this->Flash->success(__('The translation has been deleted.'));
        } else {
            $this->Flash->error(__('The translation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
