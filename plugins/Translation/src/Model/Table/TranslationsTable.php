<?php
declare(strict_types=1);

namespace Translation\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Translations Model
 *
 * @method \Utils\Model\Entity\Translation newEmptyEntity()
 * @method \Utils\Model\Entity\Translation newEntity(array $data, array $options = [])
 * @method \Utils\Model\Entity\Translation[] newEntities(array $data, array $options = [])
 * @method \Utils\Model\Entity\Translation get($primaryKey, $options = [])
 * @method \Utils\Model\Entity\Translation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Utils\Model\Entity\Translation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Utils\Model\Entity\Translation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Utils\Model\Entity\Translation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Utils\Model\Entity\Translation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Utils\Model\Entity\Translation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TranslationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('i18n_messages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 100)
            ->requirePresence('domain', 'create')
            ->notEmptyString('domain');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 5)
            ->requirePresence('locale', 'create')
            ->notEmptyString('locale');

        $validator
            ->scalar('singular')
            ->maxLength('singular', 255)
            ->requirePresence('singular', 'create')
            ->notEmptyString('singular');

        $validator
            ->scalar('plural')
            ->maxLength('plural', 255)
            ->allowEmptyString('plural');

        $validator
            ->scalar('context')
            ->maxLength('context', 50)
            ->allowEmptyString('context');

        $validator
            ->scalar('value_0')
            ->maxLength('value_0', 255)
            ->allowEmptyString('value_0');

        $validator
            ->scalar('value_1')
            ->maxLength('value_1', 255)
            ->allowEmptyString('value_1');

        $validator
            ->scalar('value_2')
            ->maxLength('value_2', 255)
            ->allowEmptyString('value_2');

        return $validator;
    }


    function DublicateAndSave($entity){
        $entity->singular = strtolower($entity->singular);
        $row = $this->find('all')->where([
            'domain' =>$entity->domain,
            'locale' =>$entity->locale,
            'singular' => $entity->singular,
        ])->count();
        if($row==0){
            $this->save($entity);
        }
    }
}
