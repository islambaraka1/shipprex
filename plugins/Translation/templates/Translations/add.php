<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $translation
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Translations'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="translations form content">
    <?= $this->Form->create($translation) ?>
    <fieldset>
        <legend><?= __('Add Translation') ?></legend>
        <?php
            echo $this->Form->control('domain');
            echo $this->Form->control('locale');
            echo $this->Form->control('singular');
            echo $this->Form->control('plural');
            echo $this->Form->control('context');
            echo $this->Form->control('value_0');
            echo $this->Form->control('value_1');
            echo $this->Form->control('value_2');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
