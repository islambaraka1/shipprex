<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $translations
 */
?>
<div class="row">
    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'purple',
        'count' => count(LANG_LIST),
        'title' => __('Number of languages'),
        'icon' => 'globe',
        'sub_msg' => __('List texts'),
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'dark',
        'count' => $langsTexts,
        'title' => __('Number of phrases'),
        'icon' => 'globe',
        'sub_msg' => __('List texts'),
    ]);
    foreach ($langData as $key => $lang){
        echo $this->element('Utils.backend/widgets/count_block',[
            'color' => 'blue',
            'count' => $lang['un_translated'],
            'title' => __('Untranslated '.$lang['name'].' Text'),
            'icon' => 'language',
            'url' =>  ROOT_URL.'/translation/translations/autotranslate/'.$key,
            'sub_msg' => __('Click here to run auto translate for '.$lang['name']),
        ]);
    }



    ?>
</div>
<div class="row flex-row ">
    <?= $this->Html->link(__('Clear Cache'), ['action' => 'clearCache'], ['class' => 'btn btn-primary mb-3 m-2']) ?>
    <?= $this->Html->link(__('Find and replace'), ['action' => 'findReplace'], ['class' => 'btn btn-primary mb-3 m-2']) ?>
</div>

<?= $this->element('backend/ajax_data_table'); ?>
