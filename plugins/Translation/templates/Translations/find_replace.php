<?php
        // create html search form for words in the translation model
        echo $this->Form->create([], ['id' => 'search_form']);
        echo $this->Form->control('locale',['type'=>'select','options'=>LANG_LIST]);
        echo $this->Form->control('singular',['type'=>'text']);
        echo $this->Form->control('value_0',['type'=>'text']);
        echo $this->Form->control('replace',['type'=>'text']);
        echo $this->Form->control('confirm',['type'=>'hidden','value'=>'0']);

// submit button
        echo $this->Form->button(__('Search'),['type'=>'submit']);
        // end the form
        echo $this->Form->end();

    ?>

<table class="table table-striped">
    <tr>
        <th>Id</th>
        <th>Locale</th>
        <th>singular</th>
        <th>value_0</th>
        <th>After Modifications</th>
    </tr>
        <?php foreach ($translations as $translation): ?>
            <tr>
                <td><?= $translation->id ?></td>
                <td><?= $translation->locale ?></td>
                <td><?= $translation->singular ?></td>
                <td><?= $translation->value_0 ?></td>
                <td><b><?= $translation->replace ?></b></td>
            </tr>
        <?php endforeach; ?>
    <?php if (empty($translations)): ?>
        <tr>
            <td colspan="5">No translations found</td>
        </tr>
    <?php endif; ?>

</table>
    <?php if (!empty($translations)): ?>
        <?php
        echo $this->Form->button(__('Confirm bulk changes'),['type'=>'button','id'=>'confirm_button','class'=>'btn btn-primary']);
        ?>
    <?php endif; ?>

<script>
    <?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
    $(document).ready(function(){
        $('#confirm_button').click(function(){
            alert('Confirm bulk changes');
            $('#confirm').val(1);
            $('#search_form').submit();
        });
    });
    <?php $this->Html->scriptEnd(['block' => 'scriptcode']); ?>

</script>
