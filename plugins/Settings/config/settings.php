<?php

use Cake\Collection\Collection;
use Cake\Core\Configure;


$countries = [];
//load countries only if the current url matches /settings/config/global_setting/
if (strpos($_SERVER['REQUEST_URI'], '/settings/config/global_setting/') !== false) {
    //get data from https://restcountries.com/v3.1/all and store it in $countries
    $countries = json_decode(file_get_contents('https://restcountries.com/v3.1/all?fields=name'), true);
    //extract only the country names
    $countries = new Collection($countries??[]);
    $countries = $countries->extract('name.common')->toArray();
}

// create the setting array

$array =[

        'enable_balance_check'  =>[
            'name'      => __('Check for licensing'),
            'slug'      => 'check_for_licensing',
            'default'   => 'true',
            'fixOptions'=> true,

            'field'     =>[
                'type'      =>'on_off',
                'field_help'=>__('this is for balance check'),
                'on_click'  =>null,
                'class'     =>null,
                'id'        =>'test_id',
                'parent_tab'=>null
            ]
        ],
        'api_master_token'  =>[
            'name'      => __('master admin token'),
            'slug'      => 'master_admin_token',
            'default'   => 'true',
            'fixOptions'=> true,
            'field'     =>[
                'type'      =>'text_input',
                'field_help'=>__('this is for main usage over the api level'),
                'on_click'  =>null,
                'class'     =>null,
                'id'        =>'test_id',
                'parent_tab'=>null
            ]
        ],

];


Configure::write('Settings.Settings', $array);
Configure::write('Settings.Orders', $array);


set_new_setting('Settings.Settings',[
    'auto_translate'  =>[
    'name'      => __('use the auto translate'),
    'slug'      => 'auto_translate',
    'default'   => 'true',
    'fixOptions'=> true,
    'field'     =>[
        'type'      =>'on_off',
        'field_help'=>__('Turn this on if you want to use auto translate (effect performance)'),
        'on_click'  =>null,
        'class'     =>null,
        'id'        =>'',
        'parent_tab'=>null
    ]
]
]);
set_new_setting('Settings.Settings',[
    'enable_api'  =>[
    'name'      => __('use the api plugin'),
    'slug'      => 'enable_api',
    'default'   => 'true',
    'fixOptions'=> true,

        'field'     =>[
        'type'      =>'on_off',
        'field_help'=>__('Turn this on if you want to use api'),
        'on_click'  =>null,
        'class'     =>null,
        'id'        =>'',
        'parent_tab'=>null
    ]
]
]);

set_new_setting('Settings.Settings',[
    'enable_api_controllers'  =>[
    'name'      => __('use the api controllers'),
    'slug'      => 'enable_api_controllers',
    'default'   => '',
    'fixOptions'=> true,
        'field'     =>[
        'type'      =>'checkbox',
        'field_help'=>__('Turn this on if you want to use api'),
        'options' => ['Orders','Users','Zones','Invoices'],
        'on_click'  =>null,
        'class'     =>null,
        'id'        =>'',
        'parent_tab'=>null
    ]
]
]);

set_new_setting('Settings.Settings',[
    'main_bank_account'  =>[
    'name'      => __('The main bank account to use'),
    'slug'      => 'main_bank_account',
    'default'   => '',
    'fixOptions'=> true,
    'field'     =>[
        'type'      =>'select',
        'field_help'=>__('Turn this on if you want to use api'),
        'options' => ['Bank1','Bank2','Bank3','Bank4'],
        'on_click'  =>null,
        'class'     =>null,
        'id'        =>'',
        'parent_tab'=>null
    ]
]
]);
set_new_setting('Settings.Settings',[
    'starter_bank_account'  =>[
    'name'      => __('The main bank account to use'),
    'slug'      => 'starter_bank_account',
    'default'   => '',
    'fixOptions'=> true,
        'field'     =>[
        'type'      =>'radio',
        'field_help'=>__('Turn this on if you want to use api'),
        'options' => ['Bank1','Bank2','Bank3','Bank4'],
        'on_click'  =>null,
        'class'     =>null,
        'id'        =>'',
        'parent_tab'=>null
    ]
]
]);


easy_new_setting('Settings.Settings','allow_driver_screen','Allow the driver access link',$type="on_off");
easy_new_setting('Settings.Settings','intl_local_label','localization for the system',"label");
easy_new_setting('Settings.Settings','datetime_field_escape_localizations','Disable localization for date,datetime fields',"on_off",0);
easy_new_setting('Settings.Settings','intl_local_default_lang','What is the system default language',"select",0,LANG_LIST,'',false);
easy_new_setting('Settings.Settings','intl_local_country','What is the HQ country',"select",0,$countries,'eg: United States',true,'intl_local_country');
easy_new_setting('Settings.Settings','intl_local_country_code','What is the country code',"text_input",0,[],'eg: EG');
easy_new_setting('Settings.Settings','intl_local_phone_number_root','What is phone number root',"text_input");
easy_new_setting('Settings.Settings','intl_local_currency','What is your system default currency',"text_input");

// Orders Options
easy_new_setting('Settings.Orders','columns_view_in_the_dashboard','Select Columns in the dashboard view for (active orders)',
    $type="checkbox",'',get_columns_names('Orders'));
easy_new_setting('Settings.Orders','allow_delete_for_orders','Turn this on will allow admin to delete the orders(which is not recommended)',
    $type="on_off",'0');
// orders Print options
easy_new_setting('Settings.Orders','print_labels','Control the printing of Invoices ',"label");
easy_new_setting('Settings.Orders','print_show_sender_name','Show Sender name in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_sender_city','Show Sender city in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_sender_phone','Show Sender phone in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_sender_ref','Show Sender reference in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_receiver_name','Show Receiver name in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_receiver_email','Show Receiver email in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_receiver_phone','Show Receiver phone in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_receiver_address','Show Receiver address in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_ship_type','Show package shipping type in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_ship_notes','Show package shipping notes in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_ship_description','Show package description in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_show_signature','Show signature in the print slip',"on_off",'0');
easy_new_setting('Settings.Orders','print_support_print_slip','Show print Slip option on printing',"on_off",'0');
easy_new_setting('Settings.Orders','print_font_size_label','Font size for printing invoice',"label");
easy_new_setting('Settings.Orders','print_font_size_number_in_pixels','Font size',"number",14);
easy_new_setting('Settings.Orders','in_active_limit','the number of in active orders (higher = slower)',"number", 1000);
easy_new_setting('Settings.Orders','excel_template','Upload the excel file as a template',"label");
easy_new_setting('Settings.Orders','excel_template_file','Upload the excel file as a template',"upload");
easy_new_setting('Settings.Orders','order_serial_in_print_label','Set serial columns in prints',"label");
easy_new_setting('Settings.Orders','order_serial_in_print','Show serial in invoices',"on_off",'0');


easy_new_setting('Settings.Orders','order_label_client_dashboard','Client dashboard settings',"label");
easy_new_setting('Settings.Orders','order_show_hide_cancel_from_the_client','Show Cancel on client dashboard',"on_off",'0');

easy_new_setting('Settings.Orders','order_other_settings','Other settings',"label");


easy_new_setting('Settings.Orders','driver_unassigned_before_re_assign','Force Un Assign for any new order of old Driver',"on_off",'0');
easy_new_setting('Settings.Orders','order_status_for_new_widget','Order Status for the 24 hour active widget',"select",'',state_active_type);
easy_new_setting('Settings.Orders','order_dashboard_status_count','Order Status data table (number of statues to show) ',"number",4);


/**
 * Terminated  Settings
 */
easy_new_setting('Settings.Orders','order_invoices_terminated_label','Terminated orders settings','label');
easy_new_setting('Settings.Orders','order_invoices_should_collect_terminated','Invoice should collect Terminated orders',"on_off",0);
easy_new_setting('Settings.Orders','order_invoices_terminated_default_fees','Terminated orders default fees',"number",0);
easy_new_setting('Settings.Orders','order_invoices_terminated_capture_the_old_cod','Terminated orders to capture the old COD in notes field? ',"on_off",0);
easy_new_setting('Settings.Orders','order_invoices_terminated_use_the_current_fees','Terminated orders fees to use ',"select",null,
    ['Free','Use default Fees','Use Order Fees'],'Free = 0 , default the same as above field value , order fees = the same if this order was delivered ');


easy_new_setting('Settings.Orders','order_status_colors','Set status colors',"label");

foreach (types_colors as $type => $color){
    easy_new_setting('Settings.Orders','order_status_color_'.$type,$type.' status',"color" ,$color);
}


/**
 * Zones Settings
 */
$zonesTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Zones');
$zones = $zonesTable->find('list')->toArray();
Configure::write('Settings.Zones', []);
easy_new_setting('Settings.Zones','zones_label','Zones Settings',"label");
easy_new_setting('Settings.Zones','zones_default_zone','Please select the system default zone for any unknown location ',"select",null,$zones
    ,'This will be used for the fees of any unknowing locations',false);
easy_new_setting('Settings.Zones','zones_default_zone_name_show','Use the default zone name instead of the entered name in the excel ',"on_off",0,null
    ,'This will be used for the show of any unknowing locations and will remove the entered value by the customer ');


/**
 * Drivers Settings
 */
$zonesTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Zones');
$zones = $zonesTable->find('list')->toArray();
Configure::write('Settings.Driver', []);
easy_new_setting('Settings.Driver','drivers_label_1','Drivers Settings',"label");
easy_new_setting('Settings.Driver','drivers_show_comments_in_online','Show comments for updates list in (online view) ',"on_off");
easy_new_setting('Settings.Driver','drivers_update_status_allowed','The Update status which the driver can control ',"checkbox",null, types_for_state);
easy_new_setting('Settings.Driver','driver_comments_limit_for_widget','The number limit for online view widget for comments ',"number",10);
easy_new_setting('Settings.Driver','driver_assign_show_status','Order Status for the assign list',"checkbox",null, types_for_state);
easy_new_setting('Settings.Driver','driver_online_sec','Driver online security module',"select",null, ['none','otp','hash']);
easy_new_setting('Settings.Driver','driver_multi_zones','Allow driver different prices per zones',"on_off");


/**
 * Zones Settings
 */
$zonesTable = \Cake\ORM\TableRegistry::getTableLocator()->get('Zones');
$zones = $zonesTable->find('list')->toArray();
Configure::write('Settings.Reports', []);
easy_new_setting('Settings.Reports','reports_label_1','Reports Settings',"label");
easy_new_setting('Settings.Reports','reports_add_search_for_profit_table','Show profit filters option  ',"on_off");
easy_new_setting('Settings.Reports','reports_duration_for_profit_timeline','How many days to count for single block ',"number",30);



/**
 * Theme Controls and settings
 */
Configure::write('Settings.Themes', []);
easy_new_setting('Settings.Themes','theme_basic_colors_label','Set Theme colors',"label");
easy_new_setting('Settings.Themes','theme_basic_color_accent','Theme Main Text Accent  color ',"color" ,'');
easy_new_setting('Settings.Themes','theme_basic_color_top_bar','Theme Top bar background color ',"color" ,'');
easy_new_setting('Settings.Themes','theme_basic_color_left_bar','Theme left sidebar background color ',"color" ,'');
easy_new_setting('Settings.Themes','theme_basic_color_brand_bg','Theme Brand background color ',"color" ,'');






$very_local_statues_colors22 = [
    'Forward'                   => get_option_value('order_status_color_Forward'),
    'Exchange'                  => get_option_value('order_status_color_Exchange'),
    'Cash Collection'           => get_option_value('order_status_color_Cash Collection'),
    'Customer Return Pickup'    => get_option_value('order_status_color_Customer Return Pickup'),
    'Processing'                => get_option_value('order_status_color_Processing'),
    'Picked up'                 =>  get_option_value('order_status_color_Picked up'),
    'In warehouse'              => get_option_value('order_status_color_In warehouse'),
    'On route'                  => get_option_value('order_status_color_On route'),
    'Delivered'                 => get_option_value('order_status_color_Delivered'),
    'Collected'                 => get_option_value('order_status_color_Collected'),
    'Returned to warehouse'     => get_option_value('order_status_color_Returned to warehouse'),
    'Terminated'            => get_option_value('order_status_color_Terminated'),
    'Canceled by client'    => get_option_value('order_status_color_Canceled by client'),
];
define('types_colors_selection',$very_local_statues_colors22);


function HSTT($st){
    $hooks = \App\Hooks\Hooks::getInstance();
    if(isset(types_colors_selection[$st])){
        $color = types_colors_selection[$st];
    } else {
        $color = "primary";
    }
    $st = $st != null ? __($st) : '';
    return $hooks->apply_filters('order_status',"<button class='btn btn-sm bg-$color btn-$color'> ".$st." </button>");
}


$setting = get_setting_array('Settings.Settings');
//$test = Configure::read('Settings');
$table = \Cake\ORM\TableRegistry::getTableLocator()->get('Settings.Options');
//var_dump($table->create_new_option_or_fail('testing','Settings','Hello world'));
//var_dump($table->create_or_update_option('testin22gssss','Settings','Hello worldss'));
//var_dump($table->get_feature_id_by_name('Settings'));
//var_dump($table->get_option_record('testings'));
//var_dump($table->get_option_value('testing'));
//var_dump($setting);
//die();

//function to encrypt decrypted values
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
