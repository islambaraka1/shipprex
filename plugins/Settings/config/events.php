<?php

$hooks = \App\Hooks\Hooks::getInstance();

$hooks->create_new_root_menu('settings','Settings','#','cog',[1]);
$hooks->add_sub_menu_item_to_root('settings','General','settings/config/global_setting/',[1]);
$hooks->add_sub_menu_item_to_root('settings','Translation dashboard','translation/translations/',[1]);
$hooks->add_sub_menu_item_to_root('settings','Tags Management ','order-extera/tags/',[1]);
$hooks->do_action('settings_menu_patching');
if(get_option_value('print_support_print_slip') == 1){
    $hooks->add_action('orders_action_buttons_list',function($order_id){
        $hooks = \App\Hooks\Hooks::getInstance();
        echo $hooks->currentView->Html->link(__('Print Slip'), ['action' => 'slip', $order_id], ['title' => __('View'), 'class' => 'dropdown-item btn btn-secondary' ,'target' => '_blank']);
    });
}
