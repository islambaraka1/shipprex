<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;

$hooks = \App\Hooks\Hooks::getInstance();
if (!function_exists('testing_funciton_hello')) {
    $hooks->add_action('action_test', 'testing_funciton_hello');
    function testing_funciton_hello($test)
    {
        echo "Hi : " . $test;
    }
}
if (!function_exists('Orders_List_afterTable')) {
    $hooks->add_action('Orders_List_afterTable', 'Orders_List_afterTable');
    function Orders_List_afterTable()
    {
        $view = new \App\View\AppView();
        echo $view->element('backend/datatablejs');
    }
}

$hooks->add_verify('system_show_data_table');
if (!function_exists('example_callback')) {

    function example_callback($example)
    {
        if (1 == 2) {
            return $example;
        } else
            return "";
    }
}

$hooks->add_filter( 'example_filter', 'example_callback' );
//$hooks->remove_verify('system_show_data_table','system_show_data_table');
//
//if (!function_exists('bottom_of_gl_menu')) {
//
//    $hooks->add_filter('bottom_of_gl_menu', 'bottom_of_gl_menu');
//    function bottom_of_gl_menu()
//    {
//        return '<li class="nav-item">
//                            <a href="' . ROOT_URL . 'general-ledger/banks/settings" class="nav-link ">
//                                <i class="far fa-circle nav-icon"></i>
//                                <p>' . __('GL Settings') . '</p>
//                            </a>
//                        </li>';
//    }
//}

require_once 'settings.php';
require_once 'events.php';
