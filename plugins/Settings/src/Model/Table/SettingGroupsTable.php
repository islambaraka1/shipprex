<?php
declare(strict_types=1);

namespace Settings\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SettingGroups Model
 *
 * @property \Settings\Model\Table\SettingGroupsTable&\Cake\ORM\Association\BelongsTo $ParentSettingGroups
 * @property \Settings\Model\Table\SettingFieldsTable&\Cake\ORM\Association\HasMany $SettingFields
 * @property \Settings\Model\Table\SettingGroupsTable&\Cake\ORM\Association\HasMany $ChildSettingGroups
 *
 * @method \Settings\Model\Entity\SettingGroup newEmptyEntity()
 * @method \Settings\Model\Entity\SettingGroup newEntity(array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingGroup[] newEntities(array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingGroup get($primaryKey, $options = [])
 * @method \Settings\Model\Entity\SettingGroup findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Settings\Model\Entity\SettingGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingGroup[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingGroup|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\SettingGroup saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\SettingGroup[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingGroup[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingGroup[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingGroup[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SettingGroupsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('setting_groups');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('ParentSettingGroups', [
            'className' => 'Settings.SettingGroups',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('SettingFields', [
            'foreignKey' => 'setting_group_id',
            'className' => 'Settings.SettingFields',
        ]);
        $this->hasMany('ChildSettingGroups', [
            'className' => 'Settings.SettingGroups',
            'foreignKey' => 'parent_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentSettingGroups'));

        return $rules;
    }
}
