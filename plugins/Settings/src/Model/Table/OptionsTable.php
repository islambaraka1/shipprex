<?php
declare(strict_types=1);

namespace Settings\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Options Model
 *
 * @property \Settings\Model\Table\FeaturesTable&\Cake\ORM\Association\BelongsTo $Features
 *
 * @method \Settings\Model\Entity\Option newEmptyEntity()
 * @method \Settings\Model\Entity\Option newEntity(array $data, array $options = [])
 * @method \Settings\Model\Entity\Option[] newEntities(array $data, array $options = [])
 * @method \Settings\Model\Entity\Option get($primaryKey, $options = [])
 * @method \Settings\Model\Entity\Option findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Settings\Model\Entity\Option patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Settings\Model\Entity\Option[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Settings\Model\Entity\Option|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\Option saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\Option[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Option[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Option[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Option[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OptionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('options');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Features', [
            'foreignKey' => 'feature_id',
            'joinType' => 'INNER',
            'className' => 'Settings.Features',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->requirePresence('value', 'create')
            ->notEmptyString('value');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        return $rules;
    }

    /**
     * this method try to locate the target option and if
     * cant find it then create the new option in the database
     * @author Islam Baraka
     * @version 1.0.1
     * @since version 1.0.1
     * @param $option_name
     * @param $related_feature
     * @param $value
     * @param null $plugin
     * @return bool[mixed]
     */
    public function create_new_option_or_fail($option_name,$related_feature,$value,$plugin = ''){
        $feature = $this->get_feature_id_by_name($related_feature);
        if( isset( $feature)){
            $finder = $this->find()->where([
                'name'          =>$option_name,
                'feature_id'    =>$feature,
                'plugin'        =>$plugin
            ])->first();
            if(isset ($finder->id)){
                return false;
            }else{
                return $this->save_new_option($option_name, $feature, $value, $plugin);
            }
        }
        return false;
    }

    /**
     * this method saves the record if not found or update it's current value if found
     * this method will be a good wrapper for saving anything into the option table
     * this is the main saving point for any options for not duplicate any entries
     * @author Islam Baraka
     * @version 1.0.1
     * @since 1.0.1
     * @param $option_name
     * @param $related_feature
     * @param $value
     * @param string $plugin
     * @param int $autoload
     * @return bool||Mixed Option Entity
     * @uses save_new_option
     * @uses update_exist_option
     * @updated 1.0.2 deprecated the feature_id parameter and replaced it with the feature name instead SHIP-678 , SHIP-680
     */
    public function create_or_update_option($option_name,$related_feature=null,$value=null,$plugin = '',$autoload = 1 ){
//        $feature = $this->get_feature_id_by_name($related_feature);
//        if( isset( $feature)){
            $value = is_array($value)?json_encode($value):$value;
            $finder = $this->find()->where([
                'name'          =>$option_name
            ])->first();
            if(isset ($finder->id)){
                //found the option
                $finder->value = $value;
                if($this->save($finder)){
                    $event = new Event('Model.Options.OptionUpdated', $this, ['Option' => $finder] );
                    $this->getEventManager()->dispatch($event);
                    return $finder;
                }else{
                    return false;
                }
            }else{
                return $this->save_new_option($option_name, $related_feature, $value, $plugin);
            }
//        }
        return false;
    }

    /**
     * This was a refactoring to the main method implementation at method create_new_option_or_fail
     * used to create a new option record in the database
     * @author Islam Baraka
     * @version 1.0.1
     * @param $option_name
     * @param $feature
     * @param $value
     * @param string|null $plugin
     * @return bool
     */
    public function save_new_option($option_name, $feature, $value, ?string $plugin): bool
    {
        //create new option and save it
        $entity = $this->newEmptyEntity();
        $entity->name = $option_name;
        $entity->feature_id = $feature??0;
        $entity->value = $value;
        $entity->plugin = $plugin;
        $entity->autoload = 1;
        if ($this->save($entity)) {
            $event = new Event('Model.Options.OptionCreated', $this, ['Option' => $entity] );
            $this->getEventManager()->dispatch($event);
            return true;
        } else {
            return false;
        }
    }

    /**
     * this method is used to find the RECORD of the option
     * selecting is by option name or the use of the option feature ID and plugin name
     * use this with the full params is the best practice using only option name is not recommended
     * @author Islam Baraka
     * @version 1.0.1
     * you can get the feature id before selecting any value by method @get_feature_id_by_name
     * @param $option_name
     * @param null $feature_id
     * @param null $plugin
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function get_option_record($option_name,$feature_id = null,$plugin = null ){
        $conditions = ['name'=>$option_name];
        if($feature_id !== null )
            $conditions['feature_id']= $feature_id;
        if($plugin !== null )
            $conditions['plugin']= $plugin;
        return $this->find()->where($conditions)->first();
    }

    /**
     * a simple method for getting the feature id by name or null object if not
     * @param $name
     * @param bool $fullRecord
     * @return mixed
     * @author Islam Baraka
     */
    public function get_feature_id_by_name($name,$fullRecord = false){
        $finder = $this->Features->find()->where(['name' => $name])->first();
        if($fullRecord)
            return isset($finder->id)?$finder:null;
        return isset($finder->id)?$finder->id:null;
    }

    /**
     * this method used to retrieve the option value as a string
     * if option not found this method return null as a value
     * this method can make some confusing between null|false values on fail
     * @author Islam Barkaa
     * @version 1.0.1
     * @todo return error if not found option over the record finding
     * @param $option_name
     * @param null $feature_id
     * @param null $plugin
     * @return String|null
     */
    public function get_option_value($option_name,$feature_id = null,$plugin = null ){
        $conditions = ['name'=>$option_name];
        if($feature_id !== null )
            $conditions['feature_id']= $feature_id;
        if($plugin !== null )
            $conditions['plugin']= $plugin;
        $record = $this->find()->where($conditions)->first();
        return isset($record->value)?$record->value:null;
    }
}
