<?php
declare(strict_types=1);

namespace Settings\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SettingFields Model
 *
 * @property \Settings\Model\Table\SettingGroupsTable&\Cake\ORM\Association\BelongsTo $SettingGroups
 *
 * @method \Settings\Model\Entity\SettingField newEmptyEntity()
 * @method \Settings\Model\Entity\SettingField newEntity(array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingField[] newEntities(array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingField get($primaryKey, $options = [])
 * @method \Settings\Model\Entity\SettingField findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Settings\Model\Entity\SettingField patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingField[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Settings\Model\Entity\SettingField|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\SettingField saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\SettingField[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingField[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingField[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\SettingField[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SettingFieldsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('setting_fields');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SettingGroups', [
            'foreignKey' => 'setting_group_id',
            'joinType' => 'INNER',
            'className' => 'Settings.SettingGroups',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('stored_val')
            ->allowEmptyString('stored_val');

        $validator
            ->scalar('help_text')
            ->allowEmptyString('help_text');

        $validator
            ->scalar('placeholder')
            ->maxLength('placeholder', 255)
            ->allowEmptyString('placeholder');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['setting_group_id'], 'SettingGroups'));

        return $rules;
    }
}
