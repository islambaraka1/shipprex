<?php
declare(strict_types=1);

namespace Settings\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Features Model
 *
 * @property \Settings\Model\Table\OptionsTable&\Cake\ORM\Association\HasMany $Options
 *
 * @method \Settings\Model\Entity\Feature newEmptyEntity()
 * @method \Settings\Model\Entity\Feature newEntity(array $data, array $options = [])
 * @method \Settings\Model\Entity\Feature[] newEntities(array $data, array $options = [])
 * @method \Settings\Model\Entity\Feature get($primaryKey, $options = [])
 * @method \Settings\Model\Entity\Feature findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Settings\Model\Entity\Feature patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Settings\Model\Entity\Feature[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Settings\Model\Entity\Feature|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\Feature saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Settings\Model\Entity\Feature[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Feature[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Feature[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Settings\Model\Entity\Feature[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FeaturesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('features');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Options', [
            'foreignKey' => 'feature_id',
            'className' => 'Settings.Options',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('plugin')
            ->maxLength('plugin', 255)
            ->requirePresence('plugin', 'create')
            ->notEmptyString('plugin');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->scalar('depend_on')
            ->requirePresence('depend_on', 'create')
            ->notEmptyString('depend_on');

        $validator
            ->scalar('version')
            ->maxLength('version', 10)
            ->requirePresence('version', 'create')
            ->notEmptyString('version');

        return $validator;
    }


    public function add_new_feature($name,$pluginName,$version,$status){
        //check if this feature is already exist
        $feature = $this->find()->where(['name' => $name,'plugin' =>$pluginName])->first();
        if(isset($feature->id)){
            return false;
        }else{
            //create new feat
            $newfeat = $this->newEmptyEntity();
            $newfeat->name = $name;
            $newfeat->plugin = $pluginName;
            $newfeat->active = $status;
            $newfeat->version = $version;
            if($this->save($newfeat)){
                return true;
            }
            return false;
        }
    }



    public function update_version($name,$pluginName,$version){
        //check if this feature is already exist
        $feature = $this->find()->where(['name' => $name,'plugin' =>$pluginName])->first();
        if(isset($feature->id)){
            $newfeat =$feature;
            $newfeat->version = $version;
            if($this->save($newfeat)){
                return true;
            }else{
                return false;
            }
        }else{
            //create new feat
            return false;
        }
    }
}
