<?php
declare(strict_types=1);

namespace Settings\Model\Entity;

use Cake\ORM\Entity;

/**
 * SettingField Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $type
 * @property string|null $stored_val
 * @property int $setting_group_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string|null $help_text
 * @property string|null $placeholder
 *
 * @property \Settings\Model\Entity\SettingGroup $setting_group
 */
class SettingField extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'type' => true,
        'options_values' => true,
        'stored_val' => true,
        'setting_group_id' => true,
        'created' => true,
        'modified' => true,
        'help_text' => true,
        'placeholder' => true,
        'setting_group' => true,
    ];
}
