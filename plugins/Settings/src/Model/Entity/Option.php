<?php
declare(strict_types=1);

namespace Settings\Model\Entity;

use Cake\ORM\Entity;

/**
 * Option Entity
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $plugin
 * @property int $feature_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $autoload
 *
 * @property \Settings\Model\Entity\Feature $feature
 */
class Option extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'value' => true,
        'plugin' => true,
        'feature_id' => true,
        'created' => true,
        'modified' => true,
        'autoload' => true,
        'feature' => true,
    ];
}
