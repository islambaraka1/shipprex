<?php
declare(strict_types=1);

namespace Settings\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * SettingFields helper
 */
class SettingFieldsHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->_defaultConfig = $config;
    }
    public $helpers = ['Html','Form'];
    var $setting;
    var $colors = [
        'primary',
        'secondary',
        'info',
        'danger',
        'success',
        'indigo',
        'pink',
        'navy',
        'teal',
        'cyan',
        'dark',
        'gray-dark',
        'gray',
        'light',
        'warning',
        'orange',
        'maroon',
    ];
    /**
     * this method is the one responsible for generating the full form after looping  all of the field data
     * @author Islam Baraka
     * @since 01/08/2022
     * @param $full_array
     * @return string
     */
    function generate_input_from_array($full_array,$setting){
        $this->setting = $setting;
        $return ='';
        $return .= $this->Form->create(null, [
            'align' => 'horizontal',
            'type' => 'file'
        ]);
        foreach ($full_array as $field_key => $fieldData){
            $fieldData = load_setting_record_by_setting_object($fieldData,null);
            $return .= $this->generate_html_output_for_field($field_key,$fieldData);
        }
        $return .= $this->Form->button(__('Submit')) ;
        $return .= $this->Form->end();
        return $return;
    }

    /**
     * pre-call for the adaptor
     * @todo remove this layer
     * @param $field_key
     * @param $fieldData
     * @return mixed
     */
    function generate_html_output_for_field($field_key,$fieldData){
        $fieldType = $fieldData['field']['type'];
        return $this->generateType($fieldType,$fieldData);
    }

    /**
     * adaptor pattern caller for the field type generator
     * to pass rge returned value to the form generator
     * @param $type
     * @param $fieldData
     * @return mixed
     */
    private function generateType($type,$fieldData){
        return $this->$type($fieldData);
    }

    /**
     * this method used for rendering the on_off toggle input
     * this method require a additional style include and javascript as well
     * @param $data
     * @return string
     */
    private function on_off($data){
        if (!isset($data['field']['value'])){
            $data['field']['value'] = $data['default'];
        }
        $checker = $data['field']['value'] == 1 ? "checked":"";
        return '
        <div class="setting_row">
            <div class="toggel_handler"> <dic class="checkbox-inline">
                <div class="row">
                    <div class="col-9"><b>'.$data['name'].'</b>
                    <br> <small>'.$data['field']['field_help'].' </small></div>
                    <div class="col-3">
                        <input type="hidden" class="above_the_on_off" name="'.$this->setting.'['.$data['slug'].']"  value="'.$data['field']['value'].'">
                        <input class="onOffToggel" type="checkbox"  '.$checker.' value="'.$data['field']['value'].'" data-toggle="toggle" data-width="50">
                    </div>
                </div>
            </div>
        </div>';
    }

    /**
     * this method is responsible for render the text inputs in the settings page
     * added the value
     * @author Islam Baraka
     * @version 1.0.2
     * @param $data
     * @return string
     */
    private function text_input($data){
        return '<div class="setting_row">'.
        $this->Form->control($this->setting.'.'.$data['slug'],[
            'label' => $data['name'] ,
            'type'  => 'text',
            'value' => $data['field']['value'],
            'help' => $data['field']['field_help'],
        ])
           .'</div>' ;
    }
    /**
     * this method is responsible for render the number inputs in the settings page
     * @author Islam Baraka
     * @version 1.0.1
     * @param $data
     * @return string
     */
    private function number($data){
        $value = isset($data['field']['value'])?$data['field']['value']:$data['default'];
        return '<div class="setting_row">'.
        $this->Form->control($this->setting.'.'.$data['slug'],[
            'label' => $data['name'] ,
            'type'  => 'number',
            'value' => $value,
            'help' => $data['field']['field_help'],
        ])
           .'</div>' ;
    }
    /**
     * this method is responsible for render the label inputs in the settings page
     * @author Islam Baraka
     * @todo generates the field set legeend for the next fields and auto closing for it
     * @version 1.0.1
     * @param $data
     * @return string Mixed string for form labels
     */
    private function label($data){
        return '<div class="setting_row options_label">'.
                     $this->Form->label($data['name'],null,['class' =>'options_label'])
            .'</div>' ;
    }

    /**
     * this method is responsible for render the upload  inputs in the settings page
     * @author Islam Baraka
     * @todo generates the field set legeend for the next fields and auto closing for it
     * @version 1.0.1
     * @param $data
     * @return string Mixed string for form labels
     */
    private function upload($data){
        $value = isset($data['field']['value'])?$data['field']['value']:$data['default'];

        return '<div class="setting_row">
                <p>old file link is : <a href="'. $value .'">Here</a> </p>
                '.
                    $this->Form->file($this->setting.'.'.$data['slug'] ,['class' =>'fileUp'])
            .'</div>' ;
    }


    /**
     * This method for creating the checkbox as a select rendered as checkbox
     * @author Islam Baraka
     * @version 1.0.1
     * @param $data
     * @return string
     */
    private function checkbox($data){
        $value = isset($data['field']['value'])? json_decode($data['field']['value']):$data['default'];

        $returner = '<div class="setting_row">';
        if($this->has_options($data)){
            $fixOptions = $data['fixOptions'] == true? $this->fixOptions($data['field']['options']):$data['field']['options'] ;
            $returner  .= $this->Form->control($this->setting.'.'.$data['slug'],['label'=>$data['name'],'type'=>'select',     'multiple' => 'checkbox',
                'help' => $data['field']['field_help'],
                'options' => $fixOptions,
                'value' =>$value
                ]);
        }
        $returner .= '</div>';
        return $returner;
    }

    /**
     * this method renders the select box for any setting
     * @author Islam Baraka
     * @version 1.0.1
     * @param $data
     * @return string<html element>
     */
    private function select($data){
        $value = isset($data['field']['value'])?$data['field']['value']:$data['default'];
        $returner = '<div class="setting_row">';
        if($this->has_options($data)){
            $fixOptions = $data['fixOptions'] == true? $this->fixOptions($data['field']['options']):$data['field']['options'] ;
            $returner  .= $this->Form->control($this->setting.'.'.$data['slug'],['label'=>$data['name'],'type'=>'select',
                    'help' => $data['field']['field_help'],
                     'options' => $fixOptions,
                    'value'=>$value
                ]);

        }
        $returner .= '</div>';
        return $returner;
    }

    /**
     * this method is the one renders the radio inputs
     * @param $data
     * @return string
     */
    private function radio($data){
        $value = isset($data['field']['value'])?$data['field']['value']:$data['default'];
        $returner =  $returner = '<div class="setting_row">';
        if($this->has_options($data)){
            $fixOptions = $data['fixOptions'] == true? $this->fixOptions($data['field']['options']):$data['field']['options'] ;
            $returner  .= $this->Form->control($this->setting.'.'.$data['slug'], ['type' =>'radio',
                'options' => $fixOptions ,
                'help' => $data['field']['field_help'],
                'value'=> $value
                ]);
        }
        $returner .= '</div>';
        return $returner;
    }
    /**
     * this method is the one renders the radio inputs
     * @param $data
     * @return string
     */
    private function color($data){
        $value = isset($data['field']['value'])?$data['field']['value']:$data['default'];
        $returner =  $returner = '<div class="setting_row">';
        $returner .= $this->Form->hidden($this->setting.'.'.$data['slug'],['value'=>$value]);
        $returner .=  $this->Form->label($data['name'],null,['class' =>'']);

        foreach ($this->colors as $color){
            $selected = $value == $color?"selectedColor":'';
            $returner .='<div class="bg-'.$color.' elevation-2 color_selector '.$selected.'" data-value="'.$color.'"
            style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; display: inline-block; cursor: pointer;"></div>';
        }
        $returner .= '</div>';
        return $returner;
    }

    /**
     * this method check of the passed field data have options within it
     * used for the validate of checkbox and radio and select
     * @author Islam Baraka
     * @param $data
     * @return bool
     */
    private function has_options($data){
        return is_array($data['field']['options'])?true:false;
    }

    /**
     * convert options array to a database frindly format
     * return a fixed array such as ['test' => 'test']
     * @param $options
     * @return array
     */
    private function fixOptions($options){
        return array_combine($options,$options);
    }

}
