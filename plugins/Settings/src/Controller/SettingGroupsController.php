<?php
declare(strict_types=1);

namespace Settings\Controller;

/**
 * SettingGroups Controller
 *
 * @property \Settings\Model\Table\SettingGroupsTable $SettingGroups
 *
 * @method \Settings\Model\Entity\SettingGroup[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingGroupsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentSettingGroups'],
        ];
        $settingGroups = $this->paginate($this->SettingGroups);

        $this->set(compact('settingGroups'));
    }

    /**
     * View method
     *
     * @param string|null $id Setting Group id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $settingGroup = $this->SettingGroups->get($id, [
            'contain' => ['ParentSettingGroups', 'SettingFields', 'ChildSettingGroups'],
        ]);

        $this->set('settingGroup', $settingGroup);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $settingGroup = $this->SettingGroups->newEmptyEntity();
        if ($this->request->is('post')) {
            $settingGroup = $this->SettingGroups->patchEntity($settingGroup, $this->request->getData());
            if ($this->SettingGroups->save($settingGroup)) {
                $this->Flash->success(__('The setting group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting group could not be saved. Please, try again.'));
        }
        $parentSettingGroups = $this->SettingGroups->ParentSettingGroups->find('list', ['limit' => 200]);
        $this->set(compact('settingGroup', 'parentSettingGroups'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Setting Group id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $settingGroup = $this->SettingGroups->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $settingGroup = $this->SettingGroups->patchEntity($settingGroup, $this->request->getData());
            if ($this->SettingGroups->save($settingGroup)) {
                $this->Flash->success(__('The setting group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting group could not be saved. Please, try again.'));
        }
        $parentSettingGroups = $this->SettingGroups->ParentSettingGroups->find('list', ['limit' => 200]);
        $this->set(compact('settingGroup', 'parentSettingGroups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Setting Group id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $settingGroup = $this->SettingGroups->get($id);
        if ($this->SettingGroups->delete($settingGroup)) {
            $this->Flash->success(__('The setting group has been deleted.'));
        } else {
            $this->Flash->error(__('The setting group could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
