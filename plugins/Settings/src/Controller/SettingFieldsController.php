<?php
declare(strict_types=1);

namespace Settings\Controller;

/**
 * SettingFields Controller
 *
 * @property \Settings\Model\Table\SettingFieldsTable $SettingFields
 *
 * @method \Settings\Model\Entity\SettingField[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingFieldsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SettingGroups'],
        ];
        $settingFields = $this->paginate($this->SettingFields);

        $this->set(compact('settingFields'));
    }

    /**
     * View method
     *
     * @param string|null $id Setting Field id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $settingField = $this->SettingFields->get($id, [
            'contain' => ['SettingGroups'],
        ]);

        $this->set('settingField', $settingField);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $settingField = $this->SettingFields->newEmptyEntity();
        if ($this->request->is('post')) {
            $settingField = $this->SettingFields->patchEntity($settingField, $this->request->getData());
            if ($this->SettingFields->save($settingField)) {
                $this->Flash->success(__('The setting field has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting field could not be saved. Please, try again.'));
        }
        $settingGroups = $this->SettingFields->SettingGroups->find('list', ['limit' => 200]);
        $this->set(compact('settingField', 'settingGroups'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Setting Field id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $settingField = $this->SettingFields->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $settingField = $this->SettingFields->patchEntity($settingField, $this->request->getData());
            if ($this->SettingFields->save($settingField)) {
                $this->Flash->success(__('The setting field has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting field could not be saved. Please, try again.'));
        }
        $settingGroups = $this->SettingFields->SettingGroups->find('list', ['limit' => 200]);
        $this->set(compact('settingField', 'settingGroups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Setting Field id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $settingField = $this->SettingFields->get($id);
        if ($this->SettingFields->delete($settingField)) {
            $this->Flash->success(__('The setting field has been deleted.'));
        } else {
            $this->Flash->error(__('The setting field could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
