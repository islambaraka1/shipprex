<?php
declare(strict_types=1);

namespace Settings\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Settings\View\Helper\SettingFieldsHelper;

/**
 * Settings\View\Helper\SettingFieldsHelper Test Case
 */
class SettingFieldsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Settings\View\Helper\SettingFieldsHelper
     */
    protected $SettingFields;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->SettingFields = new SettingFieldsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SettingFields);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
