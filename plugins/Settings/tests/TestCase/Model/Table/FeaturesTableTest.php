<?php
declare(strict_types=1);

namespace Settings\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Settings\Model\Table\FeaturesTable;

/**
 * Settings\Model\Table\FeaturesTable Test Case
 */
class FeaturesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Settings\Model\Table\FeaturesTable
     */
    protected $Features;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Settings.Features',
        'plugin.Settings.Options',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Features') ? [] : ['className' => FeaturesTable::class];
        $this->Features = TableRegistry::getTableLocator()->get('Features', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Features);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
