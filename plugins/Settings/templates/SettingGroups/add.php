<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $settingGroup
 * @var \App\Model\Entity\ParentSettingGroup[]|\Cake\Collection\CollectionInterface $parentSettingGroups
 * @var \App\Model\Entity\SettingField[]|\Cake\Collection\CollectionInterface $settingFields
 * @var \App\Model\Entity\ChildSettingGroup[]|\Cake\Collection\CollectionInterface $childSettingGroups
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Setting Groups'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Parent Setting Groups'), ['controller' => 'SettingGroups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Parent Setting Group'), ['controller' => 'SettingGroups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Setting Fields'), ['controller' => 'SettingFields', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Setting Field'), ['controller' => 'SettingFields', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="settingGroups form content">
    <?= $this->Form->create($settingGroup) ?>
    <fieldset>
        <legend><?= __('Add Setting Group') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('parent_id', ['options' => $parentSettingGroups, 'empty' => true]);
            echo $this->Form->control('description_body');
            echo $this->Form->control('icon');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
