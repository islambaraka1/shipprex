<div class="pd-20 card-box">
    <h5 class="h4 text-blue mb-20"><?=__('System Settings')?></h5>
    <div class="tab">
        <div class="row clearfix">
            <div class="col-md-3 col-sm-12">
                <ul class="nav flex-column vtabs nav-tabs customtab" role="tablist">
                    <?php
                        $active = 'active';
                        $count = 1;
                        $settings = get_all_settings();
                        foreach ($settings as $setting => $fields){
                            ?>
                    <li class="nav-item">
                        <a class="nav-link <?=$active?>" data-toggle="tab" href="#<?=$setting?><?=$count?>" role="tab" aria-selected="true"><?=$setting?></a>
                    </li>
                    <?php
                            $active = '';
                            $count++;
                        }
                    ?>

                </ul>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="tab-content" style="background: #fff">
                    <?php
                    $active = 'show active';
                    $count = 1;
                    foreach ($settings as $setting => $fields){

                        ?>
                        <div class="tab-pane fade <?=$active?>" id="<?=$setting?><?=$count?>" role="tabpanel">
                            <div class="pd-20 p-5">
                                <?php
                                    echo $this->SettingFields->generate_input_from_array($fields,$setting);
                                ?>
                            </div>
                        </div>
                        <?php
                        $active = '';
                        $count++;
                    }
                    ?>




                </div>
            </div>
        </div>
    </div>
</div>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>
.setting_row:nth-child(even) {background: rgba(12, 45, 72, 0.03 ) }
    .options_label label{
        display: block!important;
        background: #7088ff;
        padding: 10px;
        color: #fff;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .selectedColor{
        border: 2px solid red;
        padding: 5px;
        width: 60px!important;
    }
</style>

<?php
echo $this->Html->script('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js', ['block' => 'scriptBottom']);
?>
<script>


<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
    $(document).ready(function() {
        // fix label input styling
        $('.options_label label').removeAttr('class');
        // $('.onOffToggel').change(function() {
        //     $hidden =$(this).parent('div').parent().find('.above_the_on_off');
        //     $value = $($hidden).attr('value') == 1?0:1;
        //     $($hidden).attr('value',$value);
        // })
        $('.color_selector').click(function(){
            //remove selection of others
            $($(this)).parents('div.setting_row').find('.color_selector').removeClass('selectedColor');
            // add selection tag to this
            $($(this)).addClass('selectedColor');
            //store in hidden input
            $($(this)).parents('div.setting_row').find('input').val($(this).data('value'));
        })

        // ajax the full form submitted
        // $('form').submit(function (e) {
        //     e.preventDefault(); // avoid to execute the actual submit of the form.
        //
        //     var form = $(this);
        //     var actionUrl = form.attr('action');
        //
        //     $.ajax({
        //         type: "POST",
        //         url: actionUrl,
        //         data: form.serialize(), // serializes the form's elements.
        //         success: function(data)
        //         {
        //             alert(data); // show response from the php script.
        //         }
        //     });
        //
        // })
    })

<?php
$this->Html->scriptEnd();
?>

</script>



