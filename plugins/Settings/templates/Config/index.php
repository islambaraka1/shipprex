<?php
$record = $settingTable->toArray()[0];
echo "<h3>".__('Setting  '.$record->name) ."</h3>";
?>
<div class="users form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <p><?= __($record->description_body) ?></p>
        <?php
            foreach($record->child_setting_groups as $setting){
        ?>
                <div class="row">
                    <div class="col-md-3">
                        <h1><?=$setting['name']?></h1>
                        <p><?=$setting['description_body']?></p>
                    </div>
                    <div class="col-md-9">
                        <?php
//                            pr($setting);
                        echo $this->Types->renderFormFields($setting->setting_fields,TYPES_CUSTOM_FIELD);

                        foreach ($setting->setting_fields as $settingKey){
//                                pr($settingKey);
                            }
                        ?>
                    </div>
                </div>

        <?php
           }
//            echo $this->Form->control('login_token');
//            echo $this->Types->renderUserProfileForm($profileFields,$form_types);
//            echo $this->Form->control('profile_fields._ids', ['options' => $profileFields]);
                ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
