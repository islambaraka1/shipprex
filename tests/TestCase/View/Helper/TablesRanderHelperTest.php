<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\TablesRanderHelper;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\TablesRanderHelper Test Case
 */
class TablesRanderHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\TablesRanderHelper
     */
    protected $TablesRander;
    protected $tableData;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->TablesRander = new TablesRanderHelper($view);
        $model = TableRegistry::getTableLocator()->get('Orders');
        $this->tableData = $model->find('all')->toArray();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TablesRander);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
    //function

}
