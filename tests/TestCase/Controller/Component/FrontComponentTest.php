<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\FrontComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\FrontComponent Test Case
 */
class FrontComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\FrontComponent
     */
    protected $Front;

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Front = new FrontComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Front);

        parent::tearDown();
    }
}
